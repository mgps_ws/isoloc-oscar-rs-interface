<%@page import="org.springframework.core.Ordered"%>
<%@page import="java.util.ArrayList"%>
<%@page import="info.mgps.isoloc.interfaceGPS.web.Models.UserWeb"%>
<%@page import="info.mgps.isoloc.interfaceGPS.main.WorkManagerStart"%>
<%@page import="info.mgps.isoloc.interfaceGPS.web.Models.Straddle"%>
<%@page import="info.mgps.isoloc.interfaceGPS.main.StraddleManager"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<!-- script du menu -->
	<!-- d�t�ction du navigateur -->
	<script type="text/javascript" src="js/functions.js"></script>
	<title>Home</title>
	<style type="text/css">
	    @import "style.css";
	    @import "css/theme-default.css";
	</style>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    
    <link rel="icon" href="img/logoMGPSpetitpetit.png" type="image/x-icon" />
    <!-- END META SECTION -->
    
    <!-- LESSCSS INCLUDE -->        
    <link rel="stylesheet/less" type="text/css" href="css/styles.less"/>
    <script type="text/javascript" src="js/plugins/lesscss/less.min.js"></script>  
    
    <script type="text/javascript"
    src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
    <script type="text/javascript">
		function coucou(){
			//alert(document.formErr.buttonerr);
			//document.formErr.buttonerr.submit();
		}
	 			
	 </script>
	 <%UserWeb user = (UserWeb)request.getAttribute("user"); 
	 StraddleManager sm =(StraddleManager)request.getAttribute("straddles");%>
	<script type="text/javascript">
	    function refresh() {
	    	var idList="<input type='text' name='idList' value='" + document.getElementById("idList").value + "'>";
	    	if(!(document.getElementById("presencepopup") != null && document.getElementById("presencepopup").value == "oui"))	    		
		    	if (window.location.toString().indexOf("Parametrage") == -1 && window.location.toString().indexOf("Recherche") == -1){
		    		myRedirect("home.do", "button", "Actualiser", "user", "<%=user.getUsername()%>", idList);		    		
		    	}
	    }
	</script>
	 
	<script type="text/javascript">
	    var intervalId = 0;
	    intervalId = setInterval(refresh,<%=sm.getRefresh()%>);
	</script>
	
</head>
<!-- javascript:msgStraddleShuntP(); -->
<body onload=" javascript:msgNotStraddle('<%=user.getUsername() %>'); javascript:msgStraddleShuntP(); javascript:check();">
		<%
			//reception du tableau des cavaliers de la part d'une action de la classe MyController
			//response.setIntHeader("Refresh", 5);
			boolean err = false;
			session = request.getSession();//D�clarations de la session
			
			if(sm == null){				
				err = true;
				sm = (StraddleManager)request.getAttribute("straddleserr");
				%>
				<input style="display:none" id="errshunt" value="Shunt P impossible" ></input>
				<%
			}
				 
		%>
		<%String id,id1,id2,id3,classe;%>
		<!-- des trucs du template -->
        <!-- START PAGE CONTAINER -->
        <div class="page-container"  style="background-color:#F0F0F0">
            
            <!-- START PAGE SIDEBAR -->
            <div class="page-sidebar" >
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                         <a href="javascript:myRedirect('<%=request.getContextPath()%>/views/home.do','button','accueil','user','<%=user.getUsername()%>')">ISOLOC</a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li>
                    	<!-- bouton de la page de recherche -->
                        <a href="javascript:myRedirect('<%=request.getContextPath()%>/views/home.do','button','Recherche','user','<%=user.getUsername()%>')">
                        <span class="fa fa-desktop"></span> <span class="xn-text">Recherche</span></a>
                    </li>
                    <%if(user.getRole() == 0 || user.getRole() == 1 || user.getRole() == 2){ %>
                    <li>
                    	<!-- bouton de la page de recherche -->
                        <a href="javascript:myRedirect('<%=request.getContextPath()%>/views/home.do','button','Parametrage','user','<%=user.getUsername()%>')">
                        <span class="fa fa-desktop"></span> <span class="xn-text">Gestion des utilisateurs</span></a>
                    </li>
                    <%}else{%>
                    <li>
                    	<!-- bouton de la page de recherche -->
                        <a href="javascript:myRedirect('<%=request.getContextPath()%>/views/home.do','button','accueil','user','<%=user.getUsername()%>')">
                        <span class="fa fa-desktop"></span> <span class="xn-text">Gestion des utilisateurs</span></a>
                    </li>
                    <%} %>
                    <%if(user.getRole() == 0 || user.getRole() == 1 || user.getRole() == 2){ %>
                    <li>
                    	<!-- bouton de la page de recherche -->
                        <a href="javascript:myRedirect('<%=request.getContextPath()%>/views/home.do','button','checkList','user','<%=user.getUsername()%>')">
                        <span class="fa fa-desktop"></span> <span class="xn-text">CheckList</span></a>
                    </li>
                    <%}
                    if(user.getRole() == 0 || user.getRole() == 1 || user.getRole() == 2){%>
                    <li>
                    	<!-- bouton de la page de recherche -->
                        <a href="http://<%=request.getAttribute("checklistlink")%>:8090/CavalierCheckList" target="_blank">
                        <span class="fa fa-desktop"></span> <span class="xn-text">Param�trage CheckList</span></a>
                    </li>
                    
                    <%} 
                    %>
                    <li>
                         <a href="javascript:myRedirectNT('http://<%=request.getAttribute("checklistlink")%>:8090/CavalierCheckList/faces/views/viewsOperator/BlockPage.xhtml','login','<%=user.getUsername()%>','what','<%=user.getUsername()%>mlsqkdhflhvlkjdsqhfmlkdsqhflksqjf<%= Math.random()%>')" >
                        <span class="fa fa-desktop"></span> <span class="xn-text">CheckList Unlock</span></a>
                    </li>
                    <%
                    if(user.getRole() == 0 || user.getRole() == 1 || user.getRole() == 2){ %>
                    <li>
                    	<!-- bouton de la page de recherche -->
                        <a href="javascript:myRedirect('<%=request.getContextPath()%>/views/home.do','button','pesee','user','<%=user.getUsername()%>')">
                        <span class="fa fa-desktop"></span> <span class="xn-text">Activer/Desactiver Pesee</span></a>
                    </li>
                    <%}
                    %>
                    <li>
                    <% ArrayList<String> check = (ArrayList<String>)session.getAttribute("checks");%>
                    <li id="menuFiltre" class="xn-openable" onclick="javascript:activatePopupNoRefresh();">
                        <a href="#" ><span class="fa fa-files-o" ></span> <span class="xn-text">Afficher/Cacher</span></a>
                        <ul>
                        <li>
                        <a href="#" onclick="javascript:activatePopupNoRefresh(false);"><input type="button" value="Valider" name="button"  class="btn btn-info btn-rounded"></a>
                        </li>
                        <li>
                        <b>all</b>
	                    
	                    <div class="col-md-3">
                   		<label class="switch switch-small">
                   		<%if((check == null) || check.isEmpty()){%>
								<input type="checkbox" class="switch" name="check" id="all" onclick="aff('all',<%=sm.getOrderedList().size()%>);" value="all" checked="checked"/>
						<%} else{ %>
						<input type="checkbox" class="switch" name="check" id="all" onclick="aff('all',<%=sm.getOrderedList().size()%>);" value="all" />
						<%} %>
                          		<span></span>
						</label>
                        </div>
                        </li>
                        <li>
                        
                        <table class="table">
                    			<%for(int i=0;i<sm.getOrderedList().size();i+=3){ %>
                    			<tr>
                    			<td width="60px">
                    			<div>                    			
                                  <b><%=sm.getOrderedList().get(i).getNum()%></b>
                                  </div>
                                  		<div class="col-md-3">
                        					<label class="switch switch-small">
                        					<%if((check != null) && check.contains("cavalier"+i)){%>
													<input type="checkbox" class="switch" name="check" id="<%="checkbox" + i%>" onclick="javascript:aff(<%=i%>,<%=sm.getOrderedList().size()%>);" value="<%=sm.getOrderedList().size()%>" />
													<%} else{ %>
													<input type="checkbox" class="switch" name="check" id="<%="checkbox" + i%>" onclick="javascript:aff(<%=i%>,<%=sm.getOrderedList().size()%>);" value="<%=sm.getOrderedList().size()%>" checked="checked"/>
													<%} %>
                               						<span></span>
											</label>
			                            </div>
			                     
                                  </td>
                                  <%if(i+1<sm.getOrderedList().size()) {%>
                                  <td>
                                  <div>
                                  <b><%=sm.getOrderedList().get(i+1).getNum()%></b>
                                  </div>
                                  		<div class="col-md-3">
                        					<label class="switch switch-small">
                        					<%if((check != null) && check.contains("cavalier"+(i+1))){%>
                        						
													<input type="checkbox" class="switch" name="check" id="<%="checkbox" + (i+1)%>" onclick="javascript:aff(<%=(i+1)%>,<%=sm.getOrderedList().size()%>);" value="<%=sm.getOrderedList().size()%>" />
													<%} else{ %>
													<input type="checkbox" class="switch" name="check" id="<%="checkbox" + (i+1)%>" onclick="javascript:aff(<%=(i+1)%>,<%=sm.getOrderedList().size()%>);" value="<%=sm.getOrderedList().size()%>" checked="checked"/>
													<%} %>
                               						<span></span>
											</label>
			                            </div>
                                  </td>
                                  <%} %>
                                  <%if(i+2<sm.getOrderedList().size()) {%>
                                  <td>
                                  <div>
                                  <b><%=sm.getOrderedList().get(i+2).getNum()%></b>
                                  </div>
                                  		<div class="col-md-3">
                        					<label class="switch switch-small">
                        					<%if((check != null) && check.contains("cavalier"+(i+2))){%>
                        						
													<input type="checkbox" class="switch" name="check" id="<%="checkbox" + (i+2)%>" onclick="javascript:aff(<%=(i+2)%>,<%=sm.getOrderedList().size()%>);" value="<%=sm.getOrderedList().size()%>" />
													<%} else{ %>
													<input type="checkbox" class="switch" name="check" id="<%="checkbox" + (i+2)%>" onclick="javascript:aff(<%=(i+2)%>,<%=sm.getOrderedList().size()%>);" value="<%=sm.getOrderedList().size()%>" checked="checked"/>
													<%} %>
                               						<span></span>
											</label>
			                            </div>
                                  </td>
                                  <%} %>
                                  
                                  </tr>
                                <%}%>
                                </table>
                                </li>
                                
                                </ul>
                              	
                    </li>
	                    
                    
                    
                </ul>
                
                    
                    <%-- <div class="col-md-3">
				               
				                    <!-- le panneau du filtre  -->
											<a href="#" data-toggle="dropdown" class="btn btn-primary dropdown-toggle">Filtre<span class="caret"></span></a>
				                            <ul class="dropdown-menu pull-right" role="menu">
				                            
				                            	<!-- quand on selectionne un filtre l'action filtre.do est activ�(le code dans Mycontroller)  -->
				                                <li><a href="javascript:myRedirect('<%=request.getContextPath()%>/views/filtre.do','filtre','tout','user','<%=user.getUsername()%>')">All</a></li>
				                                <li><a href="javascript:myRedirect('<%=request.getContextPath()%>/views/filtre.do','filtre','evenements','user','<%=user.getUsername()%>')">Events</a></li>				                              
				                            </ul>
					</div> --%>
					
                    	<%-- <div class="xn-openable active" onclick="javascript:activatePopupNoRefresh(true);">
                              <a href="#" data-toggle="dropdown" class="btn btn-primary ">Dropdown<span class="caret" ></span></a>
                              <ul class="dropdown-menu " role="menu" id="menutodescativate">
                              <table class="table" id="tabletodesactivate">
                    			<%for(int i=0;i<sm.getOrderedList().size();i+=4){ %>
                    			<tr>
                    			<td width="60px">
                    			<div>                    			
                                  <b><%=sm.getOrderedList().get(i).getNum()%></b>
                                  </div>
                                  		<div class="col-md-3" id="divmerde">
                        					<label class="switch switch-small">
                        					<%if((check != null) && check.contains("cavalier"+i)){%>
                        						<%System.out.println("check.contains(\"cavalier\"+i) "+i+" "+check.contains("cavalier"+i)); %>
													<input type="checkbox" class="switch" name="check" id="<%="checkbox" + i%>" onclick="javascript:aff(<%=i%>,<%=sm.getOrderedList().size()%>);" value="<%=sm.getOrderedList().size()%>" />
													<%} else{ %>
													<input type="checkbox" class="switch" name="check" id="<%="checkbox" + i%>" onclick="javascript:aff(<%=i%>,<%=sm.getOrderedList().size()%>);" value="<%=sm.getOrderedList().size()%>" checked="checked"/>
													<%} %>
                               						<span></span>
											</label>
			                            </div>
			                     
                                  </td>
                                  <%if(i+1<sm.getOrderedList().size()) {%>
                                  <td>
                                  <div>
                                  <b><%=sm.getOrderedList().get(i+1).getNum()%></b>
                                  </div>
                                  		<div class="col-md-3">
                        					<label class="switch switch-small">
                        					<%if((check != null) && check.contains("cavalier"+(i+1))){%>
                        						<%System.out.println("check.contains(\"cavalier\"+i) "+(i+1)+" "+check.contains("cavalier"+(i+1))); %>
													<input type="checkbox" class="switch" name="check" id="<%="checkbox" + (i+1)%>" onclick="javascript:aff(<%=(i+1)%>,<%=sm.getOrderedList().size()%>);" value="<%=sm.getOrderedList().size()%>" />
													<%} else{ %>
													<input type="checkbox" class="switch" name="check" id="<%="checkbox" + (i+1)%>" onclick="javascript:aff(<%=(i+1)%>,<%=sm.getOrderedList().size()%>);" value="<%=sm.getOrderedList().size()%>" checked="checked"/>
													<%} %>
                               						<span></span>
											</label>
			                            </div>
                                  </td>
                                  <%} %>
                                  <%if(i+2<sm.getOrderedList().size()) {%>
                                  <td>
                                  <div>
                                  <b><%=sm.getOrderedList().get(i+2).getNum()%></b>
                                  </div>
                                  		<div class="col-md-3">
                        					<label class="switch switch-small">
                        					<%if((check != null) && check.contains("cavalier"+(i+2))){%>
                        						<%System.out.println("check.contains(\"cavalier\"+i) "+(i+2)+" "+check.contains("cavalier"+(i+2))); %>
													<input type="checkbox" class="switch" name="check" id="<%="checkbox" + (i+2)%>" onclick="javascript:aff(<%=(i+2)%>,<%=sm.getOrderedList().size()%>);" value="<%=sm.getOrderedList().size()%>" />
													<%} else{ %>
													<input type="checkbox" class="switch" name="check" id="<%="checkbox" + (i+2)%>" onclick="javascript:aff(<%=(i+2)%>,<%=sm.getOrderedList().size()%>);" value="<%=sm.getOrderedList().size()%>" checked="checked"/>
													<%} %>
                               						<span></span>
											</label>
			                            </div>
                                  </td>
                                  <%} %>
                                  <%if(i+3<sm.getOrderedList().size()) {%>
                                  <td>
                                  <div>
                                  <b><%=sm.getOrderedList().get(i+3).getNum()%></b>
                                  </div>
                                  		<div class="col-md-3">
                        					<label class="switch switch-small">
                        					<%if((check != null) && check.contains("cavalier"+(i+3))){%>
                        						<%System.out.println("check.contains(\"cavalier\"+i) "+(i+3)+" "+check.contains("cavalier"+(i+3))); %>
													<input type="checkbox" class="switch" name="check" id="<%="checkbox" + (i+3)%>" onclick="javascript:aff(<%=(i+3)%>,<%=sm.getOrderedList().size()%>);" value="<%=sm.getOrderedList().size()%>" />
													<%} else{ %>
													<input type="checkbox" class="switch" name="check" id="<%="checkbox" + (i+3)%>" onclick="javascript:aff(<%=(i+3)%>,<%=sm.getOrderedList().size()%>);" value="<%=sm.getOrderedList().size()%>" checked="checked"/>
													<%} %>
                               						<span></span>
											</label>
			                            </div>
                                  </td>
                                  <%} %>
                                  </tr>
                                <%}%>
                                </table>
                              	<a href="javascript:activatePopupNoRefresh(false);"><input type="button" value="Valider" name="button"  class="btn btn-info btn-rounded"></a>  
                              </ul>
                              
                        </div> --%>
                   
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    <!-- END TOGGLE NAVIGATION -->
               		<li>
	                    <div class="panel-body">
	                       	<label class="col-md-4 control-label text-left" style="width: auto;">Connexion OSCAR</label>
	                       	<div class="col-md-3">
                            <label class="switch">
                            <%if(sm.getCnxECN4()==0){ %>
                            	<input type="checkbox" class="switch" value="1" onclick="return false"/>
                                <span></span>
                            <%}else {%>
                            	<input type="checkbox" class="switch" value="1" checked onclick="return false"/>
                            	<span></span>
                            <%} %>
                        	</label>
                        	</div>
                        	
	                    </div>
                   </li>
                   <li>
	                    <div class="panel-body">
	                       	<label class="col-md-4 control-label text-left" style="width: auto;">Serveur en ecoute</label>
	                       	<div class="col-md-3">
                            <label class="switch">
                            <%if(sm.getCnxServer()==0){ %>
                                <input type="checkbox" class="switch" value="1" onclick="return false"/>
                                <span></span>
                            <%}else { %>
                            	<input type="checkbox" class="switch" value="1" checked onclick="return false"/>
                            	<span></span>
                            <%} %>
                        	</label>
                        	</div>
                        	
                        		
							
                        	
                        	
	                    </div>
	                    
                    </li>
                    <div class ="pull-left" align="right">
                   	<br>
                    <FONT size="4pt" color="red">ISO</FONT><FONT size="4pt"  color="blue">LOC</FONT><FONT size="2.5pt" color="white"> par MGPS soci�t� du groupe <B>TTC Holding</B></FONT></label>
                    </div>
                    <!-- POWER OFF -->
                    <li class=" pull-right last" align="right" >                    	 
                    	<a onclick="javascript:activatePopupNoRefresh(true)" href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> D�connexion</a>
                                        
                    </li> 
                    <!-- END POWER OFF -->
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                     
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                    <div class="row">
                    <div class="col-md-3">
					</div>
				</div>
			</div>
			 <!-- PAGE CONTENT WRAPPER -->
                
				 <!-- PAGE CONTENT WRAPPER -->
				 <!-- l'attribut action permet a ce formulaire de declancher l'action home.do(dans MyController) quand on clique sur
				un bouton de type="submit" -->
				<div class="page-content-wrap">
                    <div class="row">
                    <div class="col-md-12">
                    <div class="panel panel-default">
                    
					</div>
					</div>
					</div>
				</div>
				<div class="page-content-wrap">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
			                    	<!-- la bouton de l'actualisation -->
			                    	<form action="home.do" method="post">
										
							            <%if(check==null){%>
										<%check=new ArrayList<String>();
										  session.setAttribute("checks", check); }
							            else {%>
										<input id="checksSize" type="text" value="<%=check.size()%>" style="display: none">
										<input id="listSize" type="text" value="<%=sm.size()%>" style="display: none">
										<select id="checks" style="display: none">
											<%for(int i=0;i<check.size();i++){%>
												<option value="<%=check.get(i)%>"><%=check.get(i)%></option>
											<%}%>
										</select>
										<%} %>
										
										<input type="text" id="idList" name="idList" style="display: none">
										
										
										<input style="display:none" type="text" id="msgNotStraddle" value="<%=sm.getMsgNotStraddle()%>">
										<input style="display:none" type="text" name = "user" id="user" value="<%=user.getUsername()%>">
										<input type="submit" id="gobutton5" value="Actualiser" name="button" class="btn btn-primary pull-left">
									</form>
				                    <div class="col-md-3">
				               
				                    <!-- le panneau du filtre  -->
											<a href="#" data-toggle="dropdown" class="btn btn-primary dropdown-toggle">Filtre<span class="caret"></span></a>
				                            <ul class="dropdown-menu" role="menu">
				                            
				                            	<!-- quand on selectionne un filtre l'action filtre.do est activ�(le code dans Mycontroller)  -->
				                                <li><a href="javascript:myRedirect('<%=request.getContextPath()%>/views/filtre.do','filtre','tout','user','<%=user.getUsername()%>')">All</a></li>
				                                <li><a href="javascript:myRedirect('<%=request.getContextPath()%>/views/filtre.do','filtre','evenements','user','<%=user.getUsername()%>')">Events</a></li>				                              
				                            </ul>
									</div>
                                    <h3 class="panel-title">Activit�</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                    <!-- le tableau des cavaliers -->
                                    <table class="table">
                                            <thead>
                                                <tr>
			
													<th style="border: none;"></th>
													<td>Cavalier</td>
													<td>Mission en cours</td>
													<td>position Mission</td>
													<td>postion GPS</td>
													<td>shunt</td>
													<td>Renvoi</td>
													<td>Shunt P avant</td>
													<td>connexion au serveur</td>
												</tr>
											</thead>
												<!-- on met le tableau qu'on a re�u dans StraddleManager sm dans une orderedList et on commance la boucle -->
												<%
													List<Straddle> ordredList = sm.getOrderedList();
													for (int i = 0; i < ordredList.size(); i++) {
												%>
												<!-- la variable classe et responsable des changements des couleurs des evenements dans le tableau -->
												<!-- quand getStringState donne "unavailble" le cavalier aura une couleur gris -->
												<!-- quand getShuntable donne "YES" le cavalier aura une couleur orange -->
												<!-- quand getOccur donne "Y" le cavalier a une couleur rouge -->
												<%classe=ordredList.get(i).getTempoMission()+" , "+ordredList.get(i).getStringState() + " , " + ordredList.get(i).getShuntable() + " , " + ordredList.get(i).getErreur().getOccur();%>
												<% if(check.contains("cavalier"+i)){%>
												<tr class="<%=classe%>" id="<%="cavalier" + i%>" style="display:none" >
												
												<!-- ////////ZAKKKKK -->
													<!-- des id qu'on utilise dans le javascript -->
													<%id="straddleBellow" + i ; %>
													<%id1="straddleErrorMsgBellow" + i ; %>
													<%id2="posGpsBellow" + i ; %>
													<%id3="straddleAutoShunt" + i ; %>
													<!-- l'affichage de l'image d'erreur (la petite image rouge avec la main blanche) -->		
													<%if(ordredList.get(i).getErreur().getOccur().equals("Y")){ %>
														<th style="border: none;"><img src="<%=request.getContextPath()%>/ressources/error.jpg" class="errorIcon" onclick="javascript:popUps(<%=i %>,'error','<%=user.getUsername()%>')"></th>
													<%} else{ %>
														<th style="border: none;"></th>
													<%} %>
													<td style="display:none"  id="<%=id %>"><%=ordredList.get(i).getNum()%></td>
													<td style="display:none" ><%=ordredList.get(i).getDisplayMessage()%></td>
													<td style="display:none" ><%=ordredList.get(i).getMission().getPosition() %></td>
													<td style="display:none"  id="<%=id2%>" ><%=ordredList.get(i).getGpsPositionDisplay() %></td>
													<td style="display:none" >
													<%if(ordredList.get(i).getShuntable().equals("YES")){ %>
														<input type="button" value="Shunt" name="button" onclick="javascript:popUps(<%=i %>,'shunt','<%=user.getUsername()%>');" class="btn btn-info btn-rounded">
													<%} else{ %>
														<input type="button" value="Shunt" name="button" class="btn btn-default btn-rounded">
													<%} %>
													</td>
													<td style="display:none" >
													<%if(ordredList.get(i).getTempoMission().equalsIgnoreCase("OUI")){ %>
													
														<input type="button" value="Renvoi" name="button" onclick="javascript:myRedirect('<%=request.getContextPath()%>/views/renvoi.do','straddleCheck','<%=ordredList.get(i).getNum()%>','user','<%=user.getUsername()%>')" class="btn btn-info btn-rounded">
													
													<%} else{ %>
														<input type="button" value="Renvoi" name="button" class="btn btn-default btn-rounded">
													<%} %>
													</td>
													<td style="display:none" >
													
	                       								<div class="col-md-3">
                            							<label class="switch">
                            							<%if(ordredList.get(i).isShuntP_Before()){%>
														<input id ="<%="shuntPA"+i%>" type="checkbox" class="switch" value="1" onchange="javascript:popUps(<%=i%>,'shuntPA','<%=user.getUsername()%>');" checked/>
														<span></span>
														<%}else{ %>
															<input id ="<%="shuntPA"+i%>" type="checkbox" class="switch" value="1" onchange="javascript:popUps(<%=i%>,'shuntPA','<%=user.getUsername()%>');"/>
															<span></span>
														<%}%>
													</label>
													</div>
													</td>
													<%-- <td>
													<%if(ordredList.get(i).isAutoShunt()){%>
													<input type="button" width="5%" value="Shunt Auto" name="button" class="btn btn-success btn-rounded">
													<%}else{ %>
													<input type="button" width="5%" value="Shunt Auto" name="button" class="btn btn-danger btn-rounded">
													<%} %>
													</td> --%>
													<td style="display:none" >
														<div class="col-md-3">
                            								<label class="switch">
																<%if(ordredList.get(i).getCnxServer()==1) {%>
																	<input type="checkbox" class="switch" value="1" checked onclick="return false"/>
			                                						<span></span>
																<%}else{ %>
																	<input type="checkbox" class="switch" value="1" onclick="return false"/>
			                                						<span></span>
																<%}%>
															</label>
			                                			</div>
													</td>
													<!-- des cases du tableau qui contienne des informations importantes pour le fonctionnement de la page mes qu'on ne voit pas -->
													<td style="display:none" id="<%=id1 %>"><%=ordredList.get(i).getErreur().getErrorMsg() %></td>
													<td style="display:none" id="<%=id3 %>"><%=ordredList.get(i).isAutoShunt() %></td>
												</tr>
												<%} else {%>
												<tr class="<%=classe%>" id="<%="cavalier" + i%>">
												<!-- des id qu'on utilise dans le javascript -->
													<%id="straddleBellow" + i ; %>
													<%id1="straddleErrorMsgBellow" + i ; %>
													<%id2="posGpsBellow" + i ; %>
													<%id3="straddleAutoShunt" + i ; %>
													<!-- l'affichage de l'image d'erreur (la petite image rouge avec la main blanche) -->		
													<%if(ordredList.get(i).getErreur().getOccur().equals("Y")){ %>
														<th style="border: none;"><img src="<%=request.getContextPath()%>/ressources/error.jpg" class="errorIcon" onclick="javascript:popUps(<%=i %>,'error','<%=user.getUsername()%>')"></th>
													<%} else{ %>
														<th style="border: none;"></th>
													<%} %>
													<td id="<%=id %>"><%=ordredList.get(i).getNum()%></td>
													<td><%=ordredList.get(i).getDisplayMessage()%></td>
													<td><%=ordredList.get(i).getMission().getPosition() %></td>
													<td id="<%=id2%>" ><%=ordredList.get(i).getGpsPositionDisplay() %></td>
													<td>
													<%if(ordredList.get(i).getShuntable().equals("YES")){ %>
														<input type="button" value="Shunt" name="button" onclick="javascript:popUps(<%=i %>,'shunt','<%=user.getUsername()%>');" class="btn btn-info btn-rounded">
													<%} else{ %>
														<input type="button" value="Shunt" name="button" class="btn btn-default btn-rounded">
													<%} %>
													</td>
													<td>
													<%if(ordredList.get(i).getTempoMission().equalsIgnoreCase("OUI")){ %>
													
														<input type="button" value="Renvoi" name="button" onclick="javascript:myRedirect('<%=request.getContextPath()%>/views/renvoi.do','straddleCheck','<%=ordredList.get(i).getNum()%>','user','<%=user.getUsername()%>')" class="btn btn-info btn-rounded">
													
													<%} else{ %>
														<input type="button" value="Renvoi" name="button" class="btn btn-default btn-rounded">
													<%} %>
													</td>
													<td>
													
	                       								<div class="col-md-3">
                            							<label class="switch">
                            							<%if(ordredList.get(i).isShuntP_Before()){%>
														<input id ="<%="shuntPA"+i%>" type="checkbox" class="switch" value="1" onchange="javascript:popUps(<%=i%>,'shuntPA','<%=user.getUsername()%>');" checked/>
														<span></span>
														<%}else{ %>
															<input id ="<%="shuntPA"+i%>" type="checkbox" class="switch" value="1" onchange="javascript:popUps(<%=i%>,'shuntPA','<%=user.getUsername()%>');"/>
															<span></span>
														<%}%>
													</label>
													</div>
													</td>
													<%-- <td>
													<%if(ordredList.get(i).isAutoShunt()){%>
													<input type="button" width="5%" value="Shunt Auto" name="button" class="btn btn-success btn-rounded">
													<%}else{ %>
													<input type="button" width="5%" value="Shunt Auto" name="button" class="btn btn-danger btn-rounded">
													<%} %>
													</td> --%>
													<td>
														<div class="col-md-3">
                            								<label class="switch">
																<%if(ordredList.get(i).getCnxServer()==1) {%>
																	<input type="checkbox" class="switch" value="1" checked onclick="return false"/>
			                                						<span></span>
																<%}else{ %>
																	<input type="checkbox" class="switch" value="1" onclick="return false"/>
			                                						<span></span>
																<%}%>
															</label>
			                                			</div>
													</td>
													<!-- des cases du tableau qui contienne des informations importantes pour le fonctionnement de la page mes qu'on ne voit pas -->
													<td style="display:none" id="<%=id1 %>"><%=ordredList.get(i).getErreur().getErrorMsg() %></td>
													<td style="display:none" id="<%=id3 %>"><%=ordredList.get(i).isAutoShunt() %></td>
												</tr>
												<%} %>
												
												<%
													}
												%>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

	</div>
	</div>
		<!-- des trucs du template -->
        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn"  id="mb-signout">
            <div class="mb-container" >
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">                        	
                            <a href="javascript:myRedirect('<%=request.getContextPath()%>/views/home.do','button','deconnexion','user','<%=user.getUsername()%>')" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close" onclick="activatePopupNoRefresh(false)">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="topreventPopup"></div>
        <!-- END MESSAGE BOX-->
   		<!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap.min.js"></script>        
        <!-- END PLUGINS -->
        
        <!-- START THIS PAGE PLUGINS-->        
        <script type='text/javascript' src='js/plugins/icheck/icheck.min.js'></script>
        <script type="text/javascript" src="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="js/demo_tables.js"></script>
        <script type='text/javascript' src='js/plugins/noty/jquery.noty.js'></script>
        <script type='text/javascript' src='js/plugins/noty/layouts/topCenter.js'></script>
        <script type='text/javascript' src='js/plugins/noty/layouts/topLeft.js'></script>
        <script type='text/javascript' src='js/plugins/noty/layouts/topRight.js'></script>            
        <script type='text/javascript' src='js/plugins/noty/themes/default.js'></script>
        <!-- END THIS PAGE PLUGINS-->  
        
        <!-- START TEMPLATE -->
        
        
        <script type="text/javascript" src="js/plugins.js"></script>        
        <script type="text/javascript" src="js/actions.js"></script>        
        <!-- END TEMPLATE -->
   	 <!-- END SCRIPTS -->
   	 <!-- les pop-ups qu'on utilise dans les erreurs les shunts et les autoshunts -->
   	 <!-- les actions "shunt.do","autoShunt.o" et "errorSubmit" sont declanch� depuis les balises au dessous (on ne vois pas les <form></form> car il sont dans
   	 la page javascript "/js/functions.js"-->
   	<ul class="" id="noty_container" style="top: 20px; right: 20px; position: fixed; width: auto; height: auto; margin: 0px; padding: 0px;
   	 list-style-type: none; z-index: 10000000;">
   	 <!-- le popUp des erreurs -->
 	 <li id="noty_topRight_layout_container_error" style="display:none ;overflow: hidden; border-radius: 0px 0px 3px 3px; border: 1px solid rgb(51, 51, 51); box-shadow: rgba(0, 0, 0, 0.2) 0px 1px 2px;
 	  color: rgb(255, 255, 255); opacity: 1; width: auto; background: rgb(51, 51, 51);">
 	 </li>
 	 <!-- le popUp des shunts -->
 	 <li id="noty_topRight_layout_container_shunt" style="display:none ;overflow: hidden; border-radius: 0px 0px 3px 3px; border: 1px solid rgb(51, 51, 51); box-shadow: rgba(0, 0, 0, 0.2) 0px 1px 2px;
 	  color: rgb(255, 255, 255); opacity: 1; width: auto; background: rgb(51, 51, 51);">
 	 </li>
 	 <!-- le popUp des autoshunts -->
 	 <li id="noty_topRight_layout_container_autoShunt" style="display:none ;overflow: hidden; border-radius: 0px 0px 3px 3px; border: 1px solid rgb(51, 51, 51); box-shadow: rgba(0, 0, 0, 0.2) 0px 1px 2px;
 	  color: rgb(255, 255, 255); opacity: 1; width: auto; background: rgb(51, 51, 51);">
 	 </li>
 	 <!-- le popUp de l'erreur pas cavalier -->
 	 <li id="noty_topRight_layout_container_msgNotStraddle" style="display:none ;overflow: hidden; border-radius: 0px 0px 3px 3px; border: 1px solid rgb(51, 51, 51); box-shadow: rgba(0, 0, 0, 0.2) 0px 1px 2px;
 	  color: rgb(255, 255, 255); opacity: 1; width: auto; background: rgb(51, 51, 51);">
 	 </li>
 	 <!-- le popUp de l'erreur shunt P impossible -->
 	 <li id="noty_topRight_layout_container_shuntP" style="display:none ;overflow: hidden; border-radius: 0px 0px 3px 3px; border: 1px solid rgb(51, 51, 51); box-shadow: rgba(0, 0, 0, 0.2) 0px 1px 2px;
 	  color: rgb(255, 255, 255); opacity: 1; width: auto; background: rgb(51, 51, 51);">
 	 </li>
 	 <!-- le popUp de la confirmation shuntP avant -->
 	 <li id="noty_topRight_layout_container_shuntPA" style="display:none ;overflow: hidden; border-radius: 0px 0px 3px 3px; border: 1px solid rgb(51, 51, 51); box-shadow: rgba(0, 0, 0, 0.2) 0px 1px 2px;
 	  color: rgb(255, 255, 255); opacity: 1; width: auto; background: rgb(51, 51, 51);">
 	 </li>
 	 
 	</ul> 	
	</body>
</html>
