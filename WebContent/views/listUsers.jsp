<%@page import="info.mgps.isoloc.interfaceGPS.web.Models.UserWeb"%>
<%@page import="info.mgps.isoloc.interfaceGPS.web.Models.Utilisateur"%>
<%@page import="info.mgps.isoloc.interfaceGPS.main.WorkManagerStart"%>
<%@page import="info.mgps.isoloc.interfaceGPS.web.Models.Straddle"%>
<%@page import="info.mgps.isoloc.interfaceGPS.main.StraddleManager"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<!-- script du menu -->
	<!-- d�t�ction du navigateur -->
	<script type="text/javascript" src="js/functions.js"></script>
	<title>Home</title>
	<style type="text/css">
	    @import "style.css";
	    @import "css/theme-default.css";
	</style>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    
    <link rel="icon" href="img/logoMGPSpetitpetit.png" type="image/x-icon" />
    <!-- END META SECTION -->
    
    <!-- LESSCSS INCLUDE -->        
    <link rel="stylesheet/less" type="text/css" href="css/styles.less"/>
    <script type="text/javascript" src="js/plugins/lesscss/less.min.js"></script>  
    
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.10.1.min.js"></script>


</head>
<!-- javascript:msgStraddleShuntP(); -->
<body>
		<%UserWeb user = (UserWeb)request.getAttribute("user"); %>
		<!-- des trucs du template -->
        <!-- START PAGE CONTAINER -->
        <div class="page-container" style="background-color:#F0F0F0">
            
            <!-- START PAGE SIDEBAR -->
            <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                         <a href="javascript:myRedirect('<%=request.getContextPath()%>/views/home.do','button','accueil','user','<%=user.getUsername()%>')">ISOLOC</a>
                         <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li>
                    	<!-- bouton de la page de recherche -->
                        <a href="javascript:myRedirect('<%=request.getContextPath()%>/views/home.do','button','accueil','user','<%=user.getUsername()%>')">
                        <span class="fa fa-desktop"></span> <span class="xn-text">Accueil</span></a>
                    </li>
                    <li>
                    	<!-- bouton de la page de recherche -->
                        <a href="javascript:myRedirect('<%=request.getContextPath()%>/views/home.do','button','Recherche','user','<%=user.getUsername()%>')">
                        <span class="fa fa-desktop"></span> <span class="xn-text">Recherche</span></a>
                    </li>
                    <%if(user.getRole() == 0 || user.getRole() == 1){ %>
                    <li>
                    	<!-- bouton de la page de recherche -->
                        <a href="javascript:myRedirect('<%=request.getContextPath()%>/views/home.do','button','listUsers','user','<%=user.getUsername()%>')">
                        <span class="fa fa-desktop"></span> <span class="xn-text">Gestion des utilisateurs</span></a>
                    </li>
                    <li>
                    	<!-- bouton de la page de recherche -->
                        <a href="javascript:myRedirect('<%=request.getContextPath()%>/views/home.do','button','Parametrage','user','<%=user.getUsername()%>')">
                        <span class="fa fa-desktop"></span> <span class="xn-text">Param�trages</span></a>
                    </li>
                    <%}else{%>
                    <li>
                    	<!-- bouton de la page de recherche -->
                        <a href="javascript:myRedirect('<%=request.getContextPath()%>/views/home.do','button','accueil','user','<%=user.getUsername()%>')">
                        <span class="fa fa-desktop"></span> <span class="xn-text">Gestion des utilisateurs</span></a>
                    </li>
                    <%} %>
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    <!-- END TOGGLE NAVIGATION -->
               		<li>
	                    <div class="panel-body">
	                       	
	                    </div>
                   </li>
                   <li>
	                    <div class="panel-body">
	                       	
	                    </div>
                    </li>
                    <!-- POWER OFF -->
                    <li class=" pull-right last" >
                        <a onclick="javascript:activatePopupNoRefresh(true)" href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> D�connexion</a>
                                              
                    </li> 
                    <!-- END POWER OFF -->
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                     
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                    <div class="row">
                    <div class="col-md-3">
					</div>
				</div>
			</div>
			 <!-- PAGE CONTENT WRAPPER -->
                
				 <!-- PAGE CONTENT WRAPPER -->
				 <!-- l'attribut action permet a ce formulaire de declancher l'action home.do(dans MyController) quand on clique sur
				un bouton de type="submit" -->
				<div class="page-content-wrap">
                    <div class="row">
                    <div class="col-md-12">
                    <div class="panel panel-default">
                    
					</div>
					</div>
					</div>
				</div>
				<div class="page-content-wrap">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
			                    	
				                    <div class="col-md-3">				                    
									</div>
                                    <h3 class="panel-title">Gestion des utilisateurs</h3>
                                </div>
                                <%
                               	List<Utilisateur> utilisateurs = (List<Utilisateur>)request.getAttribute("utilisateurs");
                                
                                %>
                                
                                    
                                    <!-- le tableau des cavaliers -->
                                
                                <div class="panel panel-default">
                               
                                <div class="panel-heading">
                                <h3 class="panel-title">Liste des utilisateurs</h3>
                                    <div class="btn-group pull-right">
                                        <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Exporter les donn�es</button>
                                        <ul class="dropdown-menu">
                                            
                                            <li><a href="#" onClick ="$('#tableau').tableExport({type:'xml',escape:'false'});"><img src='img/icons/xml.png' width="24"/> XML</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#" onClick ="$('#tableau').tableExport({type:'csv',escape:'false'});"><img src='img/icons/csv.png' width="24"/> CSV</a></li>
                                            <li><a href="#" onClick ="$('#tableau').tableExport({type:'txt',escape:'false'});"><img src='img/icons/txt.png' width="24"/> TXT</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#" onClick ="$('#tableau').tableExport({type:'excel',escape:'false'});"><img src='img/icons/xls.png' width="24"/> XLS</a></li>
                                            <li><a href="#" onClick ="$('#tableau').tableExport({type:'doc',escape:'false'});"><img src='img/icons/word.png' width="24"/> Word</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#" onClick ="$('#tableau').tableExport({type:'png',escape:'false'});"><img src='img/icons/png.png' width="24"/> PNG</a></li>
                                            <li><a href="#" onClick ="$('#tableau').tableExport({type:'pdf',escape:'false'});"><img src='img/icons/pdf.png' width="24"/> PDF</a></li>
                                        </ul>
                                    </div>
                                    
                                                                       
                                    
                                </div>                                  
                                </div>
                                <div class="panel-body">
                                <div class="table-responsive">
                                <form action="listUsers.do" method="post">
                                    
                                        <table id="tableau" class="table datatable">
                                            <thead>
                                            	<tr>
                                                <th>
														Selection
													</th>
													<th>
														Nom
													</th>
													<th>
														Prenom
													</th>
													<th>
														Identifiant
													</th>
													<th>
														Role
													</th>
												</tr>
                                            </thead>
                                            <tbody>
                                                <%
												 if(utilisateurs != null && utilisateurs.size() > 0){
													 for(int i=0;i<utilisateurs.size();i++){
														 %>
														 <tr >														
														<td><input id="checkbox" name="checkbox" value = "<%=utilisateurs.get(i).getIdentifiant() %>" type="checkbox"/></td>													
														<td><%=utilisateurs.get(i).getNom()%></td>		
														<td><%=utilisateurs.get(i).getPrenom()%></td>	
														<td><%=utilisateurs.get(i).getIdentifiant()%></td>	
														<td><%=utilisateurs.get(i).getStringRole()%></td>										
												</tr>
														 <%
													 }
												 }
												%>
												
                                            </tbody>
                                        </table>
                                        <br>
                                        <table>
                                        	<tr>
                                        		<td>
                                        			<input type="submit" id="supprimer" value="Ajouter" name="button" class="btn btn-primary pull-left">
                                        		</td>
											
												<td>
													<input type="submit" id="modifier" value="Modifier" name="button" class="btn btn-primary pull-left">
												</td>
											
												<td>
													<input type="submit" id="supprimer" value="Supprimer" name="button" class="btn btn-primary pull-left">
												</td>
											
											</tr>
										</table>
												<input style="display:none" type="text" name = "user" id="user" value="<%=user.getUsername()%>">
												</form>	
                                    </div>
                                </div>
                                
                            
							
									
								
								
							</div>
						</div>
					</div>
				</div>
				<!-- START DEFAULT DATATABLE -->
                            
			</div>

	</div>
		<!-- des trucs du template -->
        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn"  id="mb-signout">
            <div class="mb-container" >
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">                        	
                            <a href="javascript:myRedirect('<%=request.getContextPath()%>/views/home.do','button','deconnexion','user','<%=user.getUsername()%>')" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close" onclick="activatePopupNoRefresh(false)">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
           
        <!-- END TEMPLATE -->
   	 <!-- END SCRIPTS -->
   	 <!-- les pop-ups qu'on utilise dans les erreurs les shunts et les autoshunts -->
   	 <!-- les actions "shunt.do","autoShunt.o" et "errorSubmit" sont declanch� depuis les balises au dessous (on ne vois pas les <form></form> car il sont dans
   	 la page javascript "/js/functions.js"-->
   	<ul class="" id="noty_container" style="top: 20px; right: 20px; position: fixed; width: auto; height: auto; margin: 0px; padding: 0px;
   	 list-style-type: none; z-index: 10000000;">
   	 <!-- le popUp des erreurs -->
 	 <li id="noty_topRight_layout_container_error" style="display:none ;overflow: hidden; border-radius: 0px 0px 3px 3px; border: 1px solid rgb(51, 51, 51); box-shadow: rgba(0, 0, 0, 0.2) 0px 1px 2px;
 	  color: rgb(255, 255, 255); opacity: 1; width: auto; background: rgb(51, 51, 51);">
 	 </li>
 	 <!-- le popUp des shunts -->
 	 <li id="noty_topRight_layout_container_shunt" style="display:none ;overflow: hidden; border-radius: 0px 0px 3px 3px; border: 1px solid rgb(51, 51, 51); box-shadow: rgba(0, 0, 0, 0.2) 0px 1px 2px;
 	  color: rgb(255, 255, 255); opacity: 1; width: auto; background: rgb(51, 51, 51);">
 	 </li>
 	 <!-- le popUp des autoshunts -->
 	 <li id="noty_topRight_layout_container_autoShunt" style="display:none ;overflow: hidden; border-radius: 0px 0px 3px 3px; border: 1px solid rgb(51, 51, 51); box-shadow: rgba(0, 0, 0, 0.2) 0px 1px 2px;
 	  color: rgb(255, 255, 255); opacity: 1; width: auto; background: rgb(51, 51, 51);">
 	 </li>
 	 <!-- le popUp de l'erreur pas cavalier -->
 	 <li id="noty_topRight_layout_container_msgNotStraddle" style="display:none ;overflow: hidden; border-radius: 0px 0px 3px 3px; border: 1px solid rgb(51, 51, 51); box-shadow: rgba(0, 0, 0, 0.2) 0px 1px 2px;
 	  color: rgb(255, 255, 255); opacity: 1; width: auto; background: rgb(51, 51, 51);">
 	 </li>
 	 <!-- le popUp de l'erreur shunt P impossible -->
 	 <li id="noty_topRight_layout_container_shuntP" style="display:none ;overflow: hidden; border-radius: 0px 0px 3px 3px; border: 1px solid rgb(51, 51, 51); box-shadow: rgba(0, 0, 0, 0.2) 0px 1px 2px;
 	  color: rgb(255, 255, 255); opacity: 1; width: auto; background: rgb(51, 51, 51);">
 	 </li>
 	 <!-- le popUp de la confirmation shuntP avant -->
 	 <li id="noty_topRight_layout_container_shuntPA" style="display:none ;overflow: hidden; border-radius: 0px 0px 3px 3px; border: 1px solid rgb(51, 51, 51); box-shadow: rgba(0, 0, 0, 0.2) 0px 1px 2px;
 	  color: rgb(255, 255, 255); opacity: 1; width: auto; background: rgb(51, 51, 51);">
 	 </li>
 	 
 	</ul> 	
 	  <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap.min.js"></script>        
        <!-- END PLUGINS -->                

        <!-- THIS PAGE PLUGINS -->
        <script type='text/javascript' src='js/plugins/icheck/icheck.min.js'></script>
        <script type="text/javascript" src="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        
        <script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>    
        <script type="text/javascript" src="js/plugins/tableexport/tableExport.js"></script>
		<script type="text/javascript" src="js/plugins/tableexport/jquery.base64.js"></script>
		<script type="text/javascript" src="js/plugins/tableexport/html2canvas.js"></script>
		<script type="text/javascript" src="js/plugins/tableexport/jspdf/libs/sprintf.js"></script>
		<script type="text/javascript" src="js/plugins/tableexport/jspdf/jspdf.js"></script>
		<script type="text/javascript" src="js/plugins/tableexport/jspdf/libs/base64.js"></script>    
        <!-- END PAGE PLUGINS -->

        <!-- START TEMPLATE -->
        
        
        <script type="text/javascript" src="js/plugins.js"></script>        
        <script type="text/javascript" src="js/actions.js"></script>        
        <!-- END TEMPLATE -->
	</body>
	
</html>
