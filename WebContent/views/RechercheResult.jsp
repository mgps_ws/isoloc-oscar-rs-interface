<%@page import="java.text.ParseException"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="info.mgps.isoloc.interfaceGPS.web.Models.UserWeb"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<!-- script du menu -->
	<!-- d�t�ction du navigateur -->
	<script type="text/javascript" src="js/functions.js"></script>
	<title>Home</title>
	<style type="text/css">
	    @import "style.css";
	    @import "css/theme-default.css";
	    div.scrollable {
		    width: 100%;
		    height: 100%;
		    margin: 0;
		    padding: 0;
		    overflow: auto;
		}
	</style>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    
    <link rel="icon" href="img/logoMGPSpetitpetit.png" type="image/x-icon" />
    <!-- END META SECTION -->
    
    <!-- LESSCSS INCLUDE -->        
    <link rel="stylesheet/less" type="text/css" href="css/styles.less"/>
    <script type="text/javascript" src="js/plugins/lesscss/less.min.js"></script>  
    
    <script type="text/javascript"
    src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
</head>
<body onload="javascript:paginationBunttons();">
	<%ArrayList<String[]> ar=(ArrayList<String[]>)request.getAttribute("result");%>
	<%String typeResult = (String)request.getAttribute("type");%>
	<%UserWeb user = (UserWeb)request.getAttribute("user");
	SimpleDateFormat formatter2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");;
	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	Date dateD = null;%>
	<!-- des trucs du template -->
        <!-- START PAGE CONTAINER -->
        <div class="page-container" style="background-color:#F0F0F0">
            
            <!-- START PAGE SIDEBAR -->
            <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                         <a href="javascript:myRedirect('<%=request.getContextPath()%>/views/home.do','button','accueil','user','<%=user.getUsername()%>')">ISOLOC</a>
                         <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li>
                    	<!-- bouton de la page de recherche -->
                        <a href="javascript:myRedirect('<%=request.getContextPath()%>/views/home.do','button','accueil','user','<%=user.getUsername()%>')">
                        <span class="fa fa-desktop"></span> <span class="xn-text">Accueil</span></a>
                    </li>
                    <li>
                    	<!-- bouton de la page de recherche -->
                    	<a href="javascript:myRedirect('<%=request.getContextPath()%>/views/home.do','button','Parametrage','user','<%=user.getUsername()%>')">
                        <span class="fa fa-desktop"></span> <span class="xn-text">Gestion Utilisateurs</span></a>
                    </li> 
                    <li>
                    	<!-- bouton de la page de recherche -->
                    	<a href="javascript:myRedirect('<%=request.getContextPath()%>/views/home.do','button','Recherche','user','<%=user.getUsername()%>')">
                        <span class="fa fa-desktop"></span> <span class="xn-text">Nouvelle recherche</span></a>
                    </li>
                    <li>
                    	<!-- bouton retour a la recherche pr�c�dente -->
                    	<a href="javascript:history.back()">
                        <span class="fa fa-desktop"></span> <span class="xn-text">Retour</span></a>
                    </li> 
                    
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    <!-- POWER OFF -->
                   <li class=" pull-right last" >
                        <a onclick="javascript:activatePopupNoRefresh(true)" href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> D�connexion</a>
                                              
                    </li> 
                    <!-- END POWER OFF -->
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                     
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                    <div class="row">
                    <div class="col-md-3">
					</div>
				</div>
			</div>
			 <!-- PAGE CONTENT WRAPPER -->
                
				 <!-- PAGE CONTENT WRAPPER -->
				 <!-- l'attribut action permet a ce formulaire de declancher l'action home.do(dans MyController) quand on clique sur
				un bouton de type="submit" -->
				<div class="page-content-wrap">
                    <div class="row">
                    <div class="col-md-12">
                    <div class="panel panel-default">
                    
					</div>
					</div>
					</div>
				</div>
				<div class="page-content-wrap">
                    <div class="row">
                        <div class="col-md-12">
							<div class="panel panel-default">
                                <div class="panel-heading">                                
                                    <h3 class="panel-title">Resultat de la recherche</h3>
                                </div>
                                <div class="btn-group pull-right">
                                        <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Exporter les donn�es</button>
                                        <ul class="dropdown-menu">
                                            
                                            <li><a href="#" onClick ="$('#tableau').tableExport({type:'xml',escape:'false'});"><img src='img/icons/xml.png' width="24"/> XML</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#" onClick ="$('#tableau').tableExport({type:'csv',escape:'false'});"><img src='img/icons/csv.png' width="24"/> CSV</a></li>
                                            <li><a href="#" onClick ="$('#tableau').tableExport({type:'txt',escape:'false'});"><img src='img/icons/txt.png' width="24"/> TXT</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#" onClick ="$('#tableau').tableExport({type:'excel',escape:'false'});"><img src='img/icons/xls.png' width="24"/> XLS</a></li>
                                            <li><a href="#" onClick ="$('#tableau').tableExport({type:'doc',escape:'false'});"><img src='img/icons/word.png' width="24"/> Word</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#" onClick ="$('#tableau').tableExport({type:'png',escape:'false'});"><img src='img/icons/png.png' width="24"/> PNG</a></li>
                                            <li><a href="#" onClick ="$('#tableau').tableExport({type:'pdf',escape:'false'});"><img src='img/icons/pdf.png' width="24"/> PDF</a></li>
                                        </ul>
                                    </div>
                                <div class="panel-body">
                                    <div id="tableau" class="table-responsive">
                                        <table class="table datatable" style="table-layout: fixed; overflow: hidden;" width="100%">
                                        <%if(typeResult.equalsIgnoreCase("resultActivity")){ %>
                                            <thead>
                                                <tr>
                                                    <th width="20%">Date</th>
                                                    <th width="10%">Cavalier</th>
                                                    <th width="70%">Message</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <% for(int i =0;i<ar.size();i++){%>
											<tr>
												<%try {
															dateD = formatter.parse(ar.get(i)[0]);
														} 
													catch (ParseException e) {
															e.printStackTrace();
														}
													%>
													<td style="padding-top: 0px; padding-bottom: 0px; vertical-align: middle; font-size:15px;" width="20%" >
														<%= formatter2.format(dateD) %>
													</td>
													<td style="padding-top: 0px; padding-bottom: 0px; vertical-align: middle; font-size:15px;" width="10%">
														<%= ar.get(i)[1] %>
													</td>
													<td style="padding-top: 0px; padding-bottom: 0px; vertical-align: middle; font-size:15px; word-wrap:break-word;" width="70%">
															<%= ar.get(i)[2] %>
													</td>	
											</tr>
											<%} %>
                                            </tbody>
                                            <%}else
                                            	if(typeResult.equalsIgnoreCase("pesee")){%>
                                            		<thead>
                                                <tr>
                                                    <th width="20%">Date</th>
                                                    <th width="10%">Cavalier</th>
                                                    <th width="20%">Conteneur</th>
                                                    <th width="15%">Poids Mission Kg</th>
                                                    <th width="15%">Poids Straddle Kg</th>
                                                    <th width="10%">Doute</th>
                                                    <th width="10%">Ecriture TOS</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <% for(int i =0;i<ar.size();i++){%>
											<tr>
												<%try {
															dateD = formatter.parse(ar.get(i)[0]);
														} 
													catch (ParseException e) {
															e.printStackTrace();
														}
													float poidsMission = Float.parseFloat(ar.get(i)[3])*1000;
													float poidsChe = Float.parseFloat(ar.get(i)[4]);
													String doute = "Non";
													if(ar.get(i)[5].equalsIgnoreCase("2")){
														doute = "Oui";
													}
													String ecritureTOS = "Oui";
													if(!ar.get(i)[6].equalsIgnoreCase("1")){
														ecritureTOS = "Non";
													}
													%>
													<td style="padding-top: 0px; padding-bottom: 0px; vertical-align: middle; font-size:15px;" width="20%" >
														<%= formatter2.format(dateD) %>
													</td>
													<td style="padding-top: 0px; padding-bottom: 0px; vertical-align: middle; font-size:15px;" width="10%">
														<%= ar.get(i)[1] %>
													</td>
													<td style="padding-top: 0px; padding-bottom: 0px; vertical-align: middle; font-size:15px; word-wrap:break-word;" width="20%">
															<%= ar.get(i)[2] %>
													</td>	
													<td style="padding-top: 0px; padding-bottom: 0px; vertical-align: middle; font-size:15px; word-wrap:break-word;" width="15%">
															<%= poidsMission %>
													</td>	
													<td style="padding-top: 0px; padding-bottom: 0px; vertical-align: middle; font-size:15px; word-wrap:break-word;" width="15%">
															<%= poidsChe %>
													</td>
													<td style="padding-top: 0px; padding-bottom: 0px; vertical-align: middle; font-size:15px; word-wrap:break-word;" width="10%">
															<%= doute %>
													</td>		
													<td style="padding-top: 0px; padding-bottom: 0px; vertical-align: middle; font-size:15px; word-wrap:break-word;" width="10%">
															<%= ecritureTOS %>
													</td>	
											</tr>
											<%} %>
                                            </tbody>
                                            	<%} %>
                                        </table>
                                        
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
				</div>
	</div>
	</div>
	<!-- des trucs du template -->
    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn"  id="mb-signout">
            <div class="mb-container" >
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">                        	
                            <a href="javascript:myRedirect('<%=request.getContextPath()%>/views/home.do','button','deconnexion','user','<%=user.getUsername()%>')" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close" onclick="activatePopupNoRefresh(false)">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- END MESSAGE BOX-->
	<!-- START SCRIPTS -->
    <!-- START PLUGINS -->
    <script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="js/plugins/jquery/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/plugins/bootstrap/bootstrap.min.js"></script>        
    <!-- END PLUGINS -->
    
    <!-- START THIS PAGE PLUGINS-->        
    <script type="text/javascript" src="js/demo_tables.js"></script>
    <script type='text/javascript' src='js/plugins/noty/jquery.noty.js'></script>
    <script type='text/javascript' src='js/plugins/noty/layouts/topCenter.js'></script>
    <script type='text/javascript' src='js/plugins/noty/layouts/topLeft.js'></script>
    <script type='text/javascript' src='js/plugins/noty/layouts/topRight.js'></script>            
    <script type='text/javascript' src='js/plugins/noty/themes/default.js'></script>
    <!-- END THIS PAGE PLUGINS-->  
    <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <!-- END PLUGINS -->                

        <script type="text/javascript" src="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        
        <script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>    
        <!-- END PAGE PLUGINS -->


        <!-- THIS PAGE PLUGINS -->
        <script type='text/javascript' src='js/plugins/icheck/icheck.min.js'></script>
        
        <script type="text/javascript" src="js/plugins/tableexport/tableExport.js"></script>
		<script type="text/javascript" src="js/plugins/tableexport/jquery.base64.js"></script>
		<script type="text/javascript" src="js/plugins/tableexport/html2canvas.js"></script>
		<script type="text/javascript" src="js/plugins/tableexport/jspdf/libs/sprintf.js"></script>
		<script type="text/javascript" src="js/plugins/tableexport/jspdf/jspdf.js"></script>
		<script type="text/javascript" src="js/plugins/tableexport/jspdf/libs/base64.js"></script>    
        <!-- END PAGE PLUGINS -->

        <!-- START TEMPLATE -->
        
        
        <script type="text/javascript" src="js/plugins.js"></script>        
        <script type="text/javascript" src="js/actions.js"></script>        
        <!-- END TEMPLATE -->
    <!-- END SCRIPTS --> 
    <!-- START TEMPLATE -->
</html>