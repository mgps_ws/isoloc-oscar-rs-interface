<!DOCTYPE html>
<html lang="en" class="body-full-height">
    <head>        
        <!-- META SECTION -->
        <title>ISOLOC -- Interface</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="img/logoMGPSpetitpetit.png" type="image/x-icon" />
        <!-- END META SECTION -->
	    <style type="text/css">
		    @import "style.css";
		    @import "css/theme-default.css";
		</style>
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->    
    </head>
    <body>
        
        <div class="login-container">
        	<%String message = (String)request.getAttribute("message"); 
        	%>
        	
            <div class="login-box animated fadeInDown">
            <%if(message != null){ %>
            	<div class="alert alert-success" role="alert" align="center">            	
                  <strong><%=message %></strong>                   
            	</div>
            	<%} %>
                
                <div class="login-body">     
	                <div class="login-logo" align="center">
						<img src="img/logo.png" type="image" />
	                </div>
                    <div class="login-title"><strong>Bienvenue</strong>,<br> Saisir votre identifiant et votre mot de passe SVP</div>
                   
                    <form action="begin.do" class="form-horizontal" method="post"><!-- ce formulaire declanche l'action begin.do (le code de l'action est dans
                    la classe MyController) apr�s le clique sur le bouton de type="submit" -->
	                    <div class="form-group">
	                        <div class="col-md-12">
	                            <input type="text" name = "username" class="form-control" placeholder="Nom d'utilisateur"/>
	                        </div>
	                    </div>
	                    <div class="form-group">
	                        <div class="col-md-12">
	                            <input type="password" name = "password" class="form-control" placeholder="Mot de passe"/>
	                        </div>
	                    </div>
	                    <div class="form-group">
	                        <div class="col-md-6">
	                        
	            			</div>
	                        <div class="col-md-6">
	                            <input type="submit" value="Log In" class="btn btn-info btn-block" name="button">
	                            <!-- ce bouton affiche la page de simulation de navis dans une nouvelle fenetre  -->
<!-- 	                            <input type="button" value="Sim" name="button" onclick="javascript:window.open('begin.do?button=Sim','','width=1200, height=300')" class="btn btn-info btn-block"> -->
	                        </div>
	                    </div>
                    </form>
                </div>
                
                <div class="login-footer">
                    <div class="pull-left">
                        &copy; 2015 ISOLOC
                    </div>
                    <div class="pull-right">
                        <a href="#">A propos</a> |
                        <a href="#">Privacy</a> |
                        <a href="#">Contactez nous</a>
                    </div>
                </div>
            </div>
            
        </div>
        
    </body>
</html>