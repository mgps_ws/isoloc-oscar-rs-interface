
<!DOCTYPE html>
<%@page import="java.util.Calendar"%>
<%@page import="info.mgps.isoloc.interfaceGPS.web.Models.UserWeb"%>
<%@page import="org.springframework.web.util.WebUtils"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>Atlant - Responsive Bootstrap Admin Template</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="img/logoMGPSpetitpetit.png" type="image/x-icon" />
        <!-- END META SECTION -->
                        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->    
        <style type="text/css">
	    @import "style.css";
	    @import "css/theme-default.css";
		</style>
	    
	    <!-- LESSCSS INCLUDE -->        
	    <link rel="stylesheet/less" type="text/css" href="css/styles.less"/>
	    <script type="text/javascript" src="js/functions.js"></script>
	    <script type="text/javascript" src="js/plugins/lesscss/less.min.js"></script>  
	    <script type="text/javascript"
	    src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
	    <script type="text/javascript">
	    	function controlText(){
	    		var text=document.getElementById("positionText");
	    		var errorMsg=document.getElementById("msgControleText");
	    		if(text.value.length==0){
	    			errorMsg.style.display="none";
    				document.getElementById("gobutton1").type="submit";
	    		}
	    		else if(text.value.length==5){
	    			if(text.value.indexOf("Z")==0 && text.value.indexOf("C")==1){
	    				errorMsg.style.display="none";
	    				document.getElementById("gobutton1").type="submit";
	    			}
	    			else if(text.value.indexOf("G")==0 && text.value.charCodeAt(1)<91 && text.value.charCodeAt(1)>64){
	    				errorMsg.style.display="block";
	    				document.getElementById("gobutton1").type="button";
	    			}
	    			else{
	    				text.value=text.value + ".";
	    			}
	    		}
	    		else if(text.value.length==7) {
	    			if(text.value.indexOf(".")==5){
	    				if(text.value.indexOf(".B")==5 || text.value.indexOf("M")==6 || text.value.indexOf("H")==6){
	    					errorMsg.style.display="none";
	    					document.getElementById("gobutton1").type="submit";
	    				}
	    				else{
	    					errorMsg.style.display="block";
	    					document.getElementById("gobutton1").type="button";
	    				}
	    			}
	    			else{
	    				errorMsg.style.display="block";
	    				document.getElementById("gobutton1").type="button";
	    			}
	    		}
	    		else if(text.value.length==6){
	    			if(text.value.indexOf("G")==0 && text.value.charCodeAt(1)<91 && text.value.charCodeAt(1)>64){
	    				text.value=text.value + ".";
	    			}
	    			else{
	    				errorMsg.style.display="block";
	    				document.getElementById("gobutton1").type="button";
	    			}
	    		}
	    		else if(text.value.length==8) {
	    			if(text.value.indexOf(".")==6){
	    				errorMsg.style.display="none";
	    			}
	    			else{
	    				errorMsg.style.display="block";
	    				document.getElementById("gobutton1").type="button";
	    			}
	    		}
	    		else {
	    			errorMsg.style.display="block";
	    			document.getElementById("gobutton1").type="button";
	    		}
	    	}
	    	
	    	function messageType(id,name) {
	    		if(id == 3){
	    			document.getElementById("tos").style.display = 'block';
	    			document.getElementById("doute").style.display = 'block';
	    		}else{
	    			document.getElementById("tos").style.display = 'none';
	    			document.getElementById("doute").style.display = 'none';	    			
	    		}
				document.getElementById("msgType").value=id;
				document.getElementById("msgTypeList").innerHTML=name;
				document.getElementById("peseeDoute").value=0;
				document.getElementById("peseeDouteList").innerHTML='Filtre';
				document.getElementById("peseeTOS").value=0;
				document.getElementById("peseeTOSList").innerHTML='Filtre';
			}
	    	function doutePesee(id,name) {
				document.getElementById("peseeDoute").value=id;
				document.getElementById("peseeDouteList").innerHTML=name;
			}
	    	function peseeTOS(id,name) {
				document.getElementById("peseeTOS").value=id;
				document.getElementById("peseeTOSList").innerHTML=name;
			}
	    	function loadInit(){
	    		document.getElementById("msgType").value=0;
				document.getElementById("msgTypeList").innerHTML='Filtre';
				document.getElementById("peseeDoute").value=0;
				document.getElementById("peseeDouteList").innerHTML='Filtre';
				document.getElementById("peseeTOS").value=0;
				document.getElementById("peseeTOSList").innerHTML='Filtre';
	    	}
	    	
	    </script>
    </head>
    <body onLoad="javascript: loadInit()">
    	<%SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); %>
    	<%UserWeb user = (UserWeb)request.getAttribute("user"); %>     
        <!-- START PAGE CONTAINER -->
        <div class="page-container" style="background-color:#F0F0F0">
            
            <!-- START PAGE SIDEBAR -->
            <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                         <a href="javascript:myRedirect('<%=request.getContextPath()%>/views/home.do','button','accueil','user','<%=user.getUsername()%>')">ISOLOC</a>
                         <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li>
                    	<!-- bouton de la page de recherche -->
                        <a href="javascript:myRedirect('<%=request.getContextPath()%>/views/home.do','button','accueil','user','<%=user.getUsername()%>')">
                        <span class="fa fa-desktop"></span> <span class="xn-text">Accueil</span></a>
                    </li>
                    <li>
                    	<!-- bouton de la page de recherche -->
                    	<a href="javascript:myRedirect('<%=request.getContextPath()%>/views/home.do','button','Parametrage','user','<%=user.getUsername()%>')">
                        <span class="fa fa-desktop"></span> <span class="xn-text">Gestion Utilisateurs</span></a>
                    </li>                    
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    <!-- POWER OFF -->
                    <li class=" pull-right last" >
                        <a onclick="javascript:activatePopupNoRefresh(true)" href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> D�connexion</a>
                                              
                    </li> 
                    <!-- END POWER OFF -->
                </ul>                   
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                    
                    <div class="row">                        
                        <div class="col-md-12">
                            <!-- START DATE AND TIME PICKER -->
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <h3>Filtres de recherche</h3>  
                                                     
                                    <form class="form-horizontal" role="form" action="recherche.do" method="post">                                    
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Date de debut : </label>
                                            <div class="col-md-5">
                                                <input type="text" class="form-control datepicker" value="<%=formatter.format(new Date()) %>" name="StartDate">
                                            </div>
                                        </div>
                                        <%Calendar cal = Calendar.getInstance(); // creates calendar
                                            cal.add(Calendar.MINUTE, -30);// adds one hour
                                            Date newDate=cal.getTime();
                                            SimpleDateFormat formatter2 = new SimpleDateFormat("HH : mm : ss");
                                         %>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Heure de debut :</label>
                                            <div class="col-md-5">
                                                <div class="input-group bootstrap-timepicker">
                                                    <input type="text" class="form-control timepicker24" name="StartHour" value="<%=formatter2.format(newDate) %>"/>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Date de fin :</label>
                                            <div class="col-md-5">
                                                <input type="text" class="form-control datepicker" value="<%=formatter.format(new Date()) %>" name="EndDate">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Heure de fin :</label>
                                            <div class="col-md-5">
                                                <div class="input-group bootstrap-timepicker">
                                                    <input type="text" class="form-control timepicker24" name="EndHour"/>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <label class="col-md-3 control-label">Cavalier :</label>
                                        <div class="col-md-5">
                                        	<input type="text" name="mobile" onKeyUp="javascript:cavalierEntier();" id="straddleNum"/>
                                        	<b>EX : C09 pour le cavalier 9</b>
                                        </div>
                                        <br/>
				                        <br/>
				                        <br/>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Type de messages :</label>
											<div class="col-md-3">
											<a href="#" data-toggle="dropdown" class="btn btn-primary dropdown-toggle" id="msgTypeList">Filtre<span class="caret"></span></a>
												<ul class="dropdown-menu" role="menu" >
					                                <li><a onclick="javascript: messageType('0','TOUS')">TOUS</a></li>
					                                <li><a onclick="javascript: messageType('1','Cavalier')">Cavalier</a></li>
					                                <li><a onclick="javascript: messageType('2','Navis')">Navis</a></li>
					                                <li><a onclick="javascript: messageType('3','Pesee')">Pes�e</a></li>
				                            	</ul>
											</div>
											<input type="text" name="typeMsg" id="msgType" style="display: none;" value="0">
                                        </div>
                                        <div class="form-group" style="display: none;" id="tos">
                                            <label class="col-md-3 control-label">Transmis au TOS ? :</label>
											<div class="col-md-3">
											<a href="#" data-toggle="dropdown" class="btn btn-primary dropdown-toggle" id="peseeTOSList">Filtre<span class="caret"></span></a>
												<ul class="dropdown-menu" role="menu" >
													<li><a onclick="javascript: peseeTOS('0','TOUS')">TOUS</a></li>
					                                <li><a onclick="javascript: peseeTOS('1','OUI')">OUI</a></li>
					                                <li><a onclick="javascript: peseeTOS('2','NON')">NON</a></li>
				                            	</ul>
											</div>
											<input type="text" name="peseeTOS" id="peseeTOS" style="display: none;" value="0">
											
                                        </div>
                                        <div class="form-group" style="display: none;" id="doute">
                                            <label class="col-md-3 control-label">Pesee avec doute > 5% ou �chec pes�e:</label>
											<div class="col-md-3">
											<a href="#" data-toggle="dropdown" class="btn btn-primary dropdown-toggle" id="peseeDouteList">Filtre<span class="caret"></span></a>
												<ul class="dropdown-menu" role="menu" >
													<li><a onclick="javascript: doutePesee('0','TOUS')">TOUS</a></li>
					                                <li><a onclick="javascript: doutePesee('1','OUI')">OUI</a></li>
					                                <li><a onclick="javascript: doutePesee('2','NON')">NON</a></li>
					                                <li><a onclick="javascript: doutePesee('3','ECHEC')">ECHEC</a></li>
				                            	</ul>
											</div>
											<input type="text" name="peseeDoute" id="peseeDoute" style="display: none;" value="0">
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Position :</label>
                                            <div class="col-md-5">
                                            	<input type="text" name="emplacement" id="positionText" onKeyUp="javascript:this.value=this.value.toUpperCase();controlText();"/>
                                            	<b>EX : B0101.B</b>
                                            	<b id="msgControleText" style="display: none;">
                                            		<span style="color: red;">la position est incorrecte</span>
                                            	</b>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Numero du conteneur : </label>
                                            <div class="col-md-5">
                                            	<input type="text" name="conteneur" onKeyUp="javascript:this.value=this.value.toUpperCase();"/>
                                            	<b>EX : ECMU4545454</b>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Shunt</label>
                                            <div class="col-md-5">
                                            	<input type="checkbox" name="shunt" value="1"/>
                                            	<b>selectionner pour les messages qui contiennent des shunts</b>
                                            </div>
                                        </div>
                                        <input style="display:none" type="text" name = "user" id="user" value="<%=user.getUsername()%>">
                                        <div class="form-group">
                                            <input type="submit" id="gobutton1" value="Chercher" name="button" class="btn btn-primary"/>
                                        </div>
                                        
                                    </form>
                                </div>
                            </div>      
                            <!-- END DATE AND TIME PICKER -->
                        </div>
                    </div>
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->
        
        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn"  id="mb-signout">
            <div class="mb-container" >
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">                        	
                            <a href="javascript:myRedirect('<%=request.getContextPath()%>/views/home.do','button','deconnexion','user','<%=user.getUsername()%>')" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close" onclick="activatePopupNoRefresh(false)">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
        <audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
        <!-- END PRELOADS -->             
        
    <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap.min.js"></script>                
        <!-- END PLUGINS -->
        
        <!-- THIS PAGE PLUGINS -->
        <script type='text/javascript' src='js/plugins/icheck/icheck.min.js'></script>
        <script type="text/javascript" src="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap-timepicker.min.js"></script>
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap-colorpicker.js"></script>
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap-file-input.js"></script>
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap-select.js"></script>
        <script type="text/javascript" src="js/plugins/tagsinput/jquery.tagsinput.min.js"></script>
        <!-- END THIS PAGE PLUGINS -->       
        
        <!-- START TEMPLATE -->
        
        <script type="text/javascript" src="js/plugins.js"></script>        
        <script type="text/javascript" src="js/actions.js"></script>        
        <!-- END TEMPLATE -->
        
        <script>
            $(function(){
                //Spinner
                $(".spinner_default").spinner()
                $(".spinner_decimal").spinner({step: 0.01, numberFormat: "n"});                
                //End spinner
                
                //Datepicker
                $('#dp-2').datepicker();
                $('#dp-3').datepicker({startView: 2});
                $('#dp-4').datepicker({startView: 1});                
                //End Datepicker
            });
        </script>
        
    <!-- END SCRIPTS -->                   
    </body>
</html>






