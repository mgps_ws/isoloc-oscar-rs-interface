<%@page import="info.mgps.isoloc.interfaceGPS.web.Models.Straddle"%>
<%@page import="info.mgps.isoloc.interfaceGPS.main.StraddleManager"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript" src="js/functions.js"></script>
<title>Simulator</title>
<style type="text/css">
    @import "style.css";
</style>
</head>
<body>
	<form action="simNavis.do">
		<%StraddleManager sm =(StraddleManager)request.getAttribute("straddles");
		List<Straddle> ordredList = sm.getOrderedList();
		%>
		<select name="straddleNum">
			<%for(int i=0;i<sm.size();i++){ %>
				<option><%=ordredList.get(i).getNum() %></option>
			<%} %>
		</select>
		<table>
			<tr>
				<th>connection cavalier</th>
				<th><input type="submit" value="Connecter" name="button"/></th>
				<th><input type="submit" value="Deconnecter" name="button"/></th>
			</tr>
			<tr>
				<th>envoyer mission</th>
				<th>
					<select id="mission" onchange="javascript:dynamicMissionMenu()" name="mission">
						<option>Vas en</option>
						<option>Prendre le conteneur</option>
						<option>Deposer le conteneur</option>
					</select>
				</th>
				<th>
					<select name="missionType">
						<option>yard</option>
						<option>vessel</option>
						<option>truck</option>
					</select>
				</th>
				<th id="conteneur" style="display:none">conteneur<input type="text" size="20" name="conteneur"/></th>
				<th id="position" style="display:block">postion<input type="text" size="20" name="position"/></th>
			</tr>
			<tr>
				<th>position gps</th>
				<th><input type="text" size="20" name="gPSPosition"/></th>
			</tr>
			<tr>
				<th>message d'erreur</th>
				<th><input type="text" size="20" name="msgError"/></th>
			</tr>
			<tr>
				<th><input type="submit" value="Envoyer" name="button"/></th>
			</tr>
		</table>
	</form>
</body>
</html>