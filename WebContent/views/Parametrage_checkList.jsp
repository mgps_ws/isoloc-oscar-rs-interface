<%@page import="info.mgps.isoloc.interfaceGPS.web.Models.UserWeb"%>
<%@page import="info.mgps.isoloc.interfaceGPS.main.WorkManagerStart"%>
<%@page import="info.mgps.isoloc.interfaceGPS.web.Models.Straddle"%>
<%@page import="info.mgps.isoloc.interfaceGPS.main.StraddleManager"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<!-- script du menu -->
	<!-- d�t�ction du navigateur -->
	<script type="text/javascript" src="js/functions.js"></script>
	<title>Home</title>
	<style type="text/css">
	    @import "style.css";
	    @import "css/theme-default.css";
	</style>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    
    <link rel="icon" href="img/logoMGPSpetitpetit.png" type="image/x-icon" />
    <!-- END META SECTION -->
    
    <!-- LESSCSS INCLUDE -->        
    <link rel="stylesheet/less" type="text/css" href="css/styles.less"/>
    <script type="text/javascript" src="js/plugins/lesscss/less.min.js"></script>  
</head>
<body>
		<%
			//reception du tableau des cavaliers de la part d'une action de la classe MyController
			//response.setIntHeader("Refresh", 5);
			StraddleManager sm =(StraddleManager)request.getAttribute("straddles");
			UserWeb user = (UserWeb)request.getAttribute("user");
		%>
		<!-- des trucs du template -->
        <!-- START PAGE CONTAINER -->
        <div class="page-container" style="background-color:#F0F0F0">
            
            <!-- START PAGE SIDEBAR -->
            <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                         <a href="javascript:myRedirect('<%=request.getContextPath()%>/views/home.do','button','accueil','user','<%=user.getUsername()%>')">ISOLOC</a>
                         <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li>
                    	<!-- bouton de la page de recherche -->
                        <a href="javascript:myRedirect('<%=request.getContextPath()%>/views/home.do','button','accueil','user','<%=user.getUsername()%>')">
                        <span class="fa fa-desktop"></span> <span class="xn-text">Accueil</span></a>
                    </li>
                    <li>
                    	<!-- bouton de la page de recherche -->
                        <a href="javascript:myRedirect('<%=request.getContextPath()%>/views/home.do','button','Recherche','user','<%=user.getUsername()%>')">
                        <span class="fa fa-desktop"></span> <span class="xn-text">Recherche</span></a>
                    </li>
                    <%if(user.getRole() == 0 || user.getRole() == 1){ %>
                    <li>
                    	<!-- bouton de la page de recherche -->
                        <a href="javascript:myRedirect('<%=request.getContextPath()%>/views/home.do','button','listUsers','user','<%=user.getUsername()%>')">
                        <span class="fa fa-desktop"></span> <span class="xn-text">Gestion des utilisateurs</span></a>
                    </li>
                    <%} %>
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->
            <!-- PAGE CONTENT -->
            <div class="page-content">
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    <!-- END TOGGLE NAVIGATION -->
                    <!-- POWER OFF -->
                    <li class=" pull-right last" >
                        <a onclick="javascript:activatePopupNoRefresh(true)" href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> D�connexion</a>
                                              
                    </li> 
                    <!-- END POWER OFF -->
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                     
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                    <div class="row">
                    <div class="col-md-3">
					</div>
				</div>
			</div>
			<div class="page-content-wrap">
                    <div class="row">
                        <div class="col-md-12">
                        
                        
                        
                        
                        <div class="panel panel-default">
                                <div class="panel-heading">
			                    	
				                    <div class="col-md-3">				                    
									</div>
                                    <h3 class="panel-title">Gestion CheckList</h3>
                                </div>
                                
                                
                                    
                                    <!-- le tableau des cavaliers -->
                                
                                <div class="panel panel-default">
                               
                                                                 
                                </div>
                                <div class="panel-body">
                                <div class="table-responsive">
                                    <form action="parametrage_checkList.do" method="post">
                                    <input type="submit" class="btn btn-primary pull-right" value="valider">  
                                        <table class="table">
                                            	<thead>
                                                <tr>
													<td>Cavalier</td>
													<td>CheckList</td>
												</tr>
												</thead>
												<%String id; %>
												<!-- on met le tableau qu'on a re�u dans StraddleManager sm dans une orderedList et on commance la boucle -->
												<%
													List<Straddle> ordredList = sm.getOrderedList();
													for (int i = 0; i < ordredList.size(); i++) {
												%>
												<tr>
													<%id="name" + i ; %>
													<td><%=ordredList.get(i).getNum()%></td>
													<%if(sm.isCheckListEnabled(ordredList.get(i).getNum())) {%>
													<td>
													<label class="switch">
                            
                            							<input type="checkbox" class="switch" name="checkbox" id="1" value="<%=ordredList.get(i).getNum()%>"  checked/>
                            							<span></span>
                            
                        								</label>
													</td>
														<td></td>
														<%}else{ %>
														<td>
													<label class="switch">
                            
                            							<input type="checkbox" class="switch" name="checkbox" id="<%=id%>" value="<%=ordredList.get(i).getNum()%>" />
                            							<span></span>
                            
                        								</label>
													</td>
														<%} %>
												</tr>
												<%
													}
												%>
									</table>
									<input style="display:none" type="text" name = "user" id="user" value="<%=user.getUsername()%>">
									
									</form>	
			                    	
                                    </div>
                                </div>
                                 
                            
							
									
								
								
							</div>
                        
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- des trucs du template -->
        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn"  id="mb-signout">
            <div class="mb-container" >
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">                        	
                            <a href="javascript:myRedirect('<%=request.getContextPath()%>/views/home.do','button','deconnexion','user','<%=user.getUsername()%>')" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close" onclick="activatePopupNoRefresh(false)">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->
   		<!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap.min.js"></script>        
        <!-- END PLUGINS -->
        <!-- START THIS PAGE PLUGINS-->        
        <script type='text/javascript' src='js/plugins/icheck/icheck.min.js'></script>
        <script type="text/javascript" src="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="js/demo_tables.js"></script>
        <script type='text/javascript' src='js/plugins/noty/jquery.noty.js'></script>
        <script type='text/javascript' src='js/plugins/noty/layouts/topCenter.js'></script>
        <script type='text/javascript' src='js/plugins/noty/layouts/topLeft.js'></script>
        <script type='text/javascript' src='js/plugins/noty/layouts/topRight.js'></script>            
        <script type='text/javascript' src='js/plugins/noty/themes/default.js'></script>
        <!-- END THIS PAGE PLUGINS-->  
        <!-- START TEMPLATE -->
        <script type="text/javascript" src="js/plugins.js"></script>        
        <script type="text/javascript" src="js/actions.js"></script>        
        <!-- END TEMPLATE -->
   	 <!-- END SCRIPTS -->
</html>
