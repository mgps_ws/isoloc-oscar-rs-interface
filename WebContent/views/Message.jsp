<!DOCTYPE html>
<%@page import="java.util.ArrayList"%>
<%@page import="info.mgps.isoloc.interfaceGPS.main.StraddleManager"%>
<html lang="en" class="body-full-height">
    <head>  
        <!-- META SECTION -->
        <title>Atlant - Responsive Bootstrap Admin Template</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="img/logoMGPSpetitpetit.png" type="image/x-icon" />
        <!-- END META SECTION -->
	    <style type="text/css">
		    @import "style.css";
		    @import "css/theme-default.css";
		</style>
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->    
</head>
	<%ArrayList<Object> ar =(ArrayList)request.getAttribute("message");
	StraddleManager sm =(StraddleManager)ar.get(1);
	%>
		
<body>
               <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    <!-- END TOGGLE NAVIGATION -->
               		<li>
	                    <div class="panel-body">
	                       	<label class="col-md-4 control-label text-left" style="width: auto;">Connexion Navis</label>
	                       	<div class="col-md-3">
                            <label class="switch">
                            <%if(sm.getCnxECN4()==0){ %>
                                <input type="checkbox" class="switch" value="1"/>
                                <span></span>
                            <%}else {%>
                            	<input type="checkbox" class="switch" value="1" checked/>
                            	<span></span>
                            <%} %>
                        	</label>
                        	</div>
	                    </div>
                   </li>
                   <li>
	                    <div class="panel-body">
	                       	<label class="col-md-4 control-label text-left" style="width: auto;">Serveur en ecoute</label>
	                       	<div class="col-md-3">
                            <label class="switch">
                            <%if(sm.getCnxServer()==0){ %>
                                <input type="checkbox" class="switch" value="1"/>
                                <span></span>
                            <%}else { %>
                            	<input type="checkbox" class="switch" value="1" checked/>
                            	<span></span>
                            <%} %>
                        	</label>
                        	</div>
	                    </div>
                    </li>
                    <!-- POWER OFF -->
                    <li class="xn-icon-button pull-right last">
                        <a href="#"><span class="fa fa-power-off"></span></a>
                        <ul class="xn-drop-left animated zoomIn">
                            <li><a href="pages-lock-screen.html"><span class="fa fa-lock"></span> Lock Screen</a></li>
                            <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                        </ul>                        
                    </li> 
                    <!-- END POWER OFF -->
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->    
                
       <div class="login-container">      
           <div class="login-box animated fadeInDown">
               <div class="login-logo"></div>
               <div class="login-body">
                    <div class="login-title"><strong><%=ar.get(0) %></strong></div>
               </div>
           </div>         
       </div>
</body>
</html>