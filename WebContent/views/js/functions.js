function msgNotStraddle(user){
	if(document.getElementById("msgNotStraddle").value != ""){
		document.getElementById("noty_topRight_layout_container_msgNotStraddle").style.display="block";
		document.getElementById("noty_topRight_layout_container_msgNotStraddle").innerHTML="<div class='noty_bar noty_type_alert' id='noty_856893311746911100'>" +
		"<form action='msgNotStraddle.do'>" +
		"<div class='noty_message' style='font-size: 11px; line-height: 14px; text-align: left; padding: auto; width: auto; position: relative;'>" +
		"<span class='noty_text' id='noty_msg'> " + "&nbsp;&nbsp;<b>" + document.getElementById("msgNotStraddle").value + "</b>" + 
		"</span>" +
		"<span style='display:none;'><input type='text' id='presencepopup' name='presencepopup' value='oui'></span>" +
		"<span style='display:none;'><input type='text' id='user' name='user' value='"+user+"'></span>" +
		"</div>" +
		"<div class='noty_buttons' style='padding: 5px; text-align: right; border-top-width: 0px; background-color: rgb(51, 51, 51);'>" +
		"<button class='btn btn-success btn-clean' id='button-0' style='margin-left: 0px;' type='submit' value='OK'>Ok</button>" +
		"</div></form></div>";
	}
}

function msgStraddleShuntP(){
	if(document.getElementById("errshunt") != null ){
		document.getElementById("noty_topRight_layout_container_shuntP").style.display="block";
		document.getElementById("noty_topRight_layout_container_shuntP").innerHTML="<div class='noty_bar noty_type_alert' id='noty_856893311746911100'>" +
		"" +
		"<div class='noty_message' style='font-size: 11px; line-height: 14px; text-align: left; padding: auto; width: auto; position: relative;'>" +
		"<span class='noty_text' id='noty_msg'> " + "&nbsp;&nbsp;<b>" + document.getElementById("errshunt").value + "</b>" + 
		"</span>" +
		"<span style='display:none;'><input type='text' id='presencepopup' name='presencepopup' value='oui'></span>" +
		"</div>" +
		"<div class='noty_buttons' style='padding: 5px; text-align: right; border-top-width: 0px; background-color: rgb(51, 51, 51);'>" +
		"<button class='btn btn-success btn-clean' id='button-0' style='margin-left: 0px;' type ='button' onclick='javascript:cancelPopUps();' value='OK'>Ok</button>" +
		"</div></div>";
	}
}

function popUps(id,type,user) {	
	if(type=="shunt"){
		if (document.getElementById("noty_topRight_layout_container_shunt").style.display == "none") {
			//document.getElementById("noty_container").style.display = "block";
			document.getElementById("noty_topRight_layout_container_shunt").style.display="block";
			document.getElementById("noty_topRight_layout_container_shunt").innerHTML="<div class='noty_bar noty_type_alert' id='noty_856893311746911100'>" +
			"<form action='shunt.do'  method='post'>" +
			"<div class='noty_message' style='font-size: 11px; line-height: 14px; text-align: left; padding: auto; width: auto; position: relative;'>" +
			"<span class='noty_text' id='noty_cav_num'> " + "<b>" + document.getElementById("straddleBellow"+id).innerHTML + "</b>" + "</span>" +
			"<span style='display:none;'><input type='text' id='notyCavNum' name='GPSPosition' value=" + document.getElementById("posGpsBellow"+id).innerHTML +
			"></span>" +
			"<span style='display:none;'><input type='text' id='notyCavNum' name='straddleCheck' value=" + document.getElementById("straddleBellow"+id).innerHTML +
			"></span>" +
			"<span style='display:none;'><input type='text' id='presencepopup' name='presencepopup' value='oui'></span>" +
			"<span style='display:none;'><input type='text' id='user' name='user' value='"+user+"'></span>" +
			"</div>" +
			"<div class='noty_buttons' style='padding: 5px; text-align: right; border-top-width: 0px; background-color: rgb(51, 51, 51);'>" +
			"<button name='button' class='btn btn-success btn-clean' id='button-0' style='margin-left: 0px;' type='submit' value='P'>P</button>&nbsp;&nbsp;" +
			"<button name='button' class='btn btn-success btn-clean' id='button-0' style='margin-left: 0px;' type='submit' value='S'>S</button>&nbsp;&nbsp;" +
			"<button name='button' class='btn btn-success btn-clean' id='button-0' style='margin-left: 0px;' type='button' value='M' onclick='javascript:shuntM();'>M</button>" +
			"<button class='btn btn-danger btn-clean' id='button-1' style='margin-left: 5px;' type='button' onclick='javascript:cancelPopUps();'>Cancel</button>" +
			"<div class='form-group' style='display:none;' id='shuntM'>" +
			"taper la position manuellement&nbsp;&nbsp;<input type='text' class='form-control' placeholder='nouvel position' name='newPosition' id='shuntMText' onKeyUp='javascript:this.value=this.value.toUpperCase();controlText();'/>" +
			"<div class='noty_buttons' style='padding: 5px; text-align: right; border-top-width: 0px; background-color: rgb(51, 51, 51);'>" +
			"<button name='button' class='btn btn-success btn-clean' id='shuntMButton' style='margin-left: 0px;' type='submit' value='Valider'>valider</button>" +
			"<button class='btn btn-danger btn-clean' id='button-1' style='margin-left: 5px;' type='button' onclick='javascript:cancelPopUps();'>Cancel</button>" +
			"</div></div></div></form></div>";
			//document.getElementById("posGpsCheck").value=document.getElementById("posGpsBellow"+id).innerHTML;
		}
	}
	
	else if (type=="error"){
		if (document.getElementById("noty_topRight_layout_container_error").style.display == "none") {
			var cavNum=document.getElementById("straddleBellow"+id).innerHTML;
			//document.getElementById("noty_container").style.display = "block";
			document.getElementById("noty_topRight_layout_container_error").style.display="block";
			document.getElementById("noty_topRight_layout_container_error").innerHTML="<div class='noty_bar noty_type_alert' id='noty_856893311746911100'>" +
					"<form action='errorSubmit.do'  method='post'>" +
					"<div class='noty_message' style='font-size: 11px; line-height: 14px; text-align: left; padding: auto; width: auto; position: relative;'>" +
					"<span class='noty_text' id='noty_cav_num'> " + "<b>" + document.getElementById("straddleBellow"+id).innerHTML + "</b>" + "</span>" +
					"<span class='noty_text' id='noty_msg'> " + "&nbsp;&nbsp;<b>" + document.getElementById("straddleErrorMsgBellow" + id).innerHTML + "</b>" + 
					"</span>" +
					"<span style='display:none;'><input type='text' id='notyCavNum' name='straddleCheck' value=" + cavNum + "></span>" +
					
					"<span style='display:none;'><input type='text' id='presencepopup' name='presencepopup' value='oui'></span>" +
					"<span style='display:none;'><input type='text' id='user' name='user' value='"+user+"'></span>" +
					"</div>" +
					"<div class='noty_buttons' style='padding: 5px; text-align: right; border-top-width: 0px; background-color: rgb(51, 51, 51);'>" +
					"<button class='btn btn-success btn-clean' id='button-0' style='margin-left: 0px;' type='submit' value='OK'>Ok</button>" +
					"<button class='btn btn-danger btn-clean' id='button-1' style='margin-left: 5px;' type='button' onclick='javascript:cancelPopUps();'>Cancel</button>" +
					"</div></form></div>";
		}
	}else
		if(type == "shuntPA"){
			if (document.getElementById("noty_topRight_layout_container_shuntPA").style.display == "none") {
				var texte = "";
				if(document.getElementById("shuntPA"+id).checked)
					texte = "Activer shunt P avant ";
				else
					texte = "D&eacute;sactiver shunt P avant ";
				//document.getElementById("noty_container").style.display = "block";
				document.getElementById("noty_topRight_layout_container_shuntPA").style.display="block";
				document.getElementById("noty_topRight_layout_container_shuntPA").innerHTML="<div class='noty_bar noty_type_alert' id='noty_856893311746911100'>" +
				"<form action='shuntPA.do'  method='post'>" +
				"<div class='noty_message' style='font-size: 11px; line-height: 14px; text-align: left; padding: auto; width: auto; position: relative;'>" +
				"<span class='noty_text' id='noty_cav_num'> " + "<b>"+texte+" " + document.getElementById("straddleBellow"+id).innerHTML + "</b>" + "</span>" +
				"<span style='display:none;'><input type='text' id='notyCavNum' name='straddleCheck' value=" + document.getElementById("straddleBellow"+id).innerHTML +
				"></span>" +
				"<span style='display:none;'><input type='text' id='notyCavNum' name='shuntPA' value=" + document.getElementById("shuntPA"+id).checked +
				"></span>" +
				"<span style='display:none;'><input type='text' id='presencepopup' name='presencepopup' value='oui'></span>" +
				"<span style='display:none;'><input type='text' id='user' name='user' value='"+user+"'></span>" +
				"</div>" +
				"<div class='noty_buttons' style='padding: 5px; text-align: right; border-top-width: 0px; background-color: rgb(51, 51, 51);'>" +
				"<button name='button' class='btn btn-success btn-clean' id='shuntMButton' style='margin-left: 0px;' type='submit' value='Valider'>valider</button>" +
				"<button class='btn btn-danger btn-clean' id='button' style='margin-left: 5px;' type='button' onclick='javascript:cancelPopUps();'>Cancel</button>" +
				"</div></div></form></div>";
				//document.getElementById("posGpsCheck").value=document.getElementById("posGpsBellow"+id).innerHTML;
			}
		}
	else if (type=="autoShunt"){
		if (document.getElementById("straddleAutoShunt"+id).innerHTML == "false"){
			if (document.getElementById("noty_topRight_layout_container_autoShunt").style.display == "none") {
				//document.getElementById("noty_container").style.display = "block";
				document.getElementById("noty_topRight_layout_container_autoShunt").style.display="block";
				document.getElementById("noty_topRight_layout_container_autoShunt").innerHTML="<div class='noty_bar noty_type_alert' id='noty_856893311746911100'>" +
				"<form action='autoShunt.do'  method='post'>" +
				"<div class='noty_message' style='font-size: 11px; line-height: 14px; text-align: left; padding: auto; width: auto; position: relative;'>" +
				"<span class='noty_text' id='noty_cav_num'><b> voulez vous vraiment shunter automatiquement le cavalier " + document.getElementById("straddleBellow"+id).innerHTML + 
				" automatiquement ?</b>" + "</span>" +
				"<span style='display:none;'><input type='text' id='notyCavNum' name='straddleNum' value=" + document.getElementById("straddleBellow"+id).innerHTML +
				"></span>" +
				"<span style='display:none;'><input type='text' id='presencepopup' name='presencepopup' value='oui'></span>" +
				"<span style='display:none;'><input type='text' id='user' name='user' value='"+user+"'></span>" +
				"</div>" +
				"<div class='noty_buttons' style='padding: 5px; text-align: right; border-top-width: 0px; background-color: rgb(51, 51, 51);'>" +
				"<button class='btn btn-success btn-clean' id='button-0' style='margin-left: 0px;' type='submit' value='OK'>Ok</button>" +
				"<button class='btn btn-danger btn-clean' id='button-1' style='margin-left: 5px;' type='button' onclick='javascript:cancelPopUps();'>Cancel</button>" +
				"</div></form></div>";
			} else {
				document.getElementById("autoShuntPopUp").style.display = "none";
			}
		}
		else if (document.getElementById("straddleAutoShunt"+id).innerHTML == "true"){
			if (document.getElementById("noty_topRight_layout_container_autoShunt").style.display == "none") {
				//document.getElementById("noty_container").style.display = "block";
				document.getElementById("noty_topRight_layout_container_autoShunt").style.display="block";
				document.getElementById("noty_topRight_layout_container_autoShunt").innerHTML="<div class='noty_bar noty_type_alert' id='noty_856893311746911100'>" +
				"<form action='autoShunt.do'  method='post'>" +
				"<div class='noty_message' style='font-size: 11px; line-height: 14px; text-align: left; padding: auto; width: auto; position: relative;'>" +
				"<span class='noty_text' id='noty_cav_num'><b> voulez vous vraiment annuler le shunt automatique pour le cavalier " + document.getElementById("straddleBellow"+id).innerHTML + 
				" automatiquement ?</b>" + "</span>" +
				"<span style='display:none;'><input type='text' id='notyCavNum' name='straddleNum' value=" + document.getElementById("straddleBellow"+id).innerHTML +
				"></span>" +
				"<span style='display:none;'><input type='text' id='presencepopup' name='presencepopup' value='oui'></span>" +
				"<span style='display:none;'><input type='text' id='user' name='user' value='"+user+"'></span>" +
				"</div>" +
				"<div class='noty_buttons' style='padding: 5px; text-align: right; border-top-width: 0px; background-color: rgb(51, 51, 51);'>" +
				"<button class='btn btn-success btn-clean' id='button-0' style='margin-left: 0px;' type='submit' value='OK'>Ok</button>" +
				"<button class='btn btn-danger btn-clean' id='button-1' style='margin-left: 5px;' type='button' onclick='javascript:cancelPopUps();'>Cancel</button>" +
				"</div></form></div>";
			}
		}
	}
}

function controlText(){
	var text=document.getElementById("shuntMText");
	var button=document.getElementById("shuntMButton");
	if(text.value.length==5){
		if(text.value.indexOf("Z")==0 && text.value.indexOf("C")==1){
			button.disabled=false;
		}
		else if(text.value.indexOf("G")==0 && text.value.charCodeAt(1)<91 && text.value.charCodeAt(1)>64){
			button.disabled=true;
		}
		else{
			text.value=text.value + ".";
		}
	}
	else if(text.value.length==7) {
		if(text.value.indexOf(".")==5){
			if(text.value.indexOf(".B")==5 || text.value.indexOf("M")==6 || text.value.indexOf("H")==6){
				button.disabled=false;
			}
			else{
				button.disabled=true;
			}
		}
		else{
			button.disabled=true;
		}
	}
	else if(text.value.length==6){
		if(text.value.indexOf("G")==0 && text.value.charCodeAt(1)<91 && text.value.charCodeAt(1)>64){
			text.value=text.value + ".";
		}
		else{
			button.disabled=true;
		}
	}
	else if(text.value.length==8) {
		if(text.value.indexOf(".")==6){
			button.disabled=false;
		}
		else{
			button.disabled=true;
		}
	}
	else {
		button.disabled=true;
	}
}

function cancelPopUps(){
	//document.getElementById("noty_container").style.display = "none";
	document.getElementById("noty_topRight_layout_container_shunt").style.display = "none";
	document.getElementById("noty_topRight_layout_container_error").style.display = "none";
	document.getElementById("noty_topRight_layout_container_autoShunt").style.display = "none";
	document.getElementById("noty_topRight_layout_container_shunt").innerHTML= "";
	document.getElementById("noty_topRight_layout_container_error").innerHTML= "";
	document.getElementById("noty_topRight_layout_container_autoShunt").innerHTML= "";
	document.getElementById("noty_topRight_layout_container_shuntP").innerHTML= "";
	document.getElementById("noty_topRight_layout_container_shuntPA").innerHTML= "";
}

function position(id) {
		document.getElementById("posGPS"+ id).value=document.getElementById("posV"+id).value;
}

function testErrorCav(id) {
	document.getElementById("testErrorCheck").value=document.getElementById("straddleBellow"+id).innerHTML;
}

function shuntM(){
	if (document.getElementById("shuntM").style.display == "none") {
		document.getElementById("shuntM").style.display = "block";
	} else {
		document.getElementById("shuntM").style.display = "none";
	}
}

function testError(){
	if (document.getElementById("testErreur").style.display == "none") {
		document.getElementById("testErreur").style.display = "block";
	} else {
		document.getElementById("testErreur").style.display = "none";
	}
}

function dynamicMissionMenu(){
	if (document.getElementById("mission").value == "Vas en") {
		document.getElementById("position").style.display = "block";
		document.getElementById("conteneur").style.display = "none";
	} else  {
		document.getElementById("position").style.display = "block";
		document.getElementById("conteneur").style.display = "block";
	}
}

function myRedirect(redirectUrl, arg, value, arg1, value1, idList) {
	
	  var form = $('<form action="' + redirectUrl + '" method="post">' +
	  '<input type="hidden" name="'+ arg +'" value="' + value + '"></input>' +
	  '<input type="hidden" name="'+ arg1 +'" value="' + value1 + '"></input>' + idList + '</form>');
	  $('body').append(form);
	  $(form).submit();
	  
	  /*if(arg == "filtre"){
			document.getElementById("presencepopup").value = "";
		}*/
	};
	function myRedirectNT(redirectUrl, arg, value, arg1, value1) {
		
		  var form = $('<form action="' + redirectUrl + '" method="post"  target="_newtab">' +
		  '<input type="hidden" name="'+ arg +'" value="' + value + '"></input>' +
		  '<input type="hidden" name="'+ arg1 +'" value="' + value1 + '"></input></form>');
		  $('body').append(form);
		  $(form).submit();
		  
		
		};
	
/*function setpopup(){
	
	document.getElementById("presencepopup").value = "oui";
	alert(document.getElementById("presencepopup"));
} */
	function activatePopupNoRefresh(active){
		if(active){
			document.getElementById("topreventPopup").innerHTML="<div id=\"topreventPopup\">"+
	        "<span style='display:none;'><input type='text' id='presencepopup' name='presencepopup' value='oui'></span>" +
	    "</div>";
			menuFiltre
		}
		
		else{
			document.getElementById("topreventPopup").innerHTML="<div id=\"topreventPopup\" >"+ "</div>";
		}		
	}

	function activatePopupNoRefresh(){
		
		if(document.getElementById("menuFiltre").className  == "xn-openable"){
			document.getElementById("topreventPopup").innerHTML="<div id=\"topreventPopup\">"+
	        "<span style='display:none;'><input type='text' id='presencepopup' name='presencepopup' value='oui'></span>" +
	    "</div>";
			menuFiltre
		}
		
		else{
			document.getElementById("topreventPopup").innerHTML="<div id=\"topreventPopup\" >"+ "</div>";
		}		
	}


function aff(id,size){
	if(id=="all"){
		if(document.getElementById(id).checked){
			document.getElementById("idList").value="";
			for(i=0;i<size;i++){
				if(document.getElementById("cavalier" + i).style.display=="none"){
					document.getElementById("cavalier" + i).style.display="block";
					document.getElementById("checkbox" + i).checked="checked";
				}
			}
			
		}
		else{
			document.getElementById("all").checked="";
			var s=document.getElementById("idList").value;
			document.getElementById("idList").value="";
			for(i=0;i<size;i++){
				if(!(document.getElementById("cavalier" + i).style.display=="none")){
					document.getElementById("cavalier" + i).style.display="none";
					document.getElementById("checkbox" + i).checked="";
					document.getElementById("idList").value=document.getElementById("idList").value + "cavalier" + i + ";";
				}
				//alert(document.getElementById("idList").value);
			}
			/*for(i=0;i<size;i++){				
			document.getElementById("cavalier" + i).style.display="none";
			
			}*/
		}
	}
	else{
		if(document.getElementById("checkbox" + id).checked){
			document.getElementById("all").checked="";
			var s=document.getElementById("idList").value;
			document.getElementById("cavalier" + id).style.display="block";
			if(id<10){
				document.getElementById("idList").value=s.substring(0, s.indexOf("cavalier" + id)) + s.substring(s.indexOf("cavalier" + id)+9, s.length);
			}
				
			else{
				document.getElementById("idList").value=s.substring(0, s.indexOf("cavalier" + id)) + s.substring(s.indexOf("cavalier" + id)+10, s.length);
			}
				
		}
		else{
			document.getElementById("all").checked="";
			var s=document.getElementById("idList").value;
			//document.getElementById("cavalier" + id).style.display="none";
			document.getElementById("idList").value=document.getElementById("idList").value + "cavalier" + id + ";";
		}
		
	}
	
}

function check(){
	var checkboxes=document.getElementById("checks");
	var checksSize=document.getElementById("checksSize").value;
	var listSize=document.getElementById("listSize").value;
	for(i=0;i<checksSize;i++){
		document.getElementById("idList").value=document.getElementById("idList").value + checkboxes.options[i].value + ";";
		/*if(checkboxes.options[i].value=="all"){
			document.getElementById("all").checked="checked";
			for(i=0;i<listSize;i++){
				document.getElementById("cavalier" + i).style.display="block";
			}
		}
		else{*/
			document.getElementById("all").checked="";
			if(checkboxes.options[i].value.length > 9)
				document.getElementById("checkbox" + checkboxes.options[i].value.substring(8, 10)).checked="";
			else
				document.getElementById("checkbox" + checkboxes.options[i].value.substring(8, 9)).checked="";
			document.getElementById(checkboxes.options[i].value).style.display="none";
		//}
	}
	var cp;
	for(i=0;i<checksSize;i++){
		if(document.getElementById("checkbox" + checkboxes.options[i].value.substring(8, 9)).checked=="checked"){
			cp=cp+1;
		}
	}
	if(cp==checksSize){
		document.getElementById("all").checked="checked";
	}
}
