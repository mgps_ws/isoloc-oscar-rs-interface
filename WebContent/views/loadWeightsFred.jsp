<!DOCTYPE html>
<html lang="en" class="body-full-height">
    <head>  
        <!-- META SECTION -->
        <title>Envoyer les poids vers le TOS</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="img/logoMGPSpetitpetit.png" type="image/x-icon" />
        <!-- END META SECTION -->
	    <style type="text/css">
		    @import "style.css";
		    @import "css/theme-default.css";
		</style>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <!-- END META SECTION -->
    
    <!-- LESSCSS INCLUDE -->        
    <link rel="stylesheet/less" type="text/css" href="css/styles.less"/>
    <script type="text/javascript" src="js/plugins/lesscss/less.min.js"></script>  
    
    <script type="text/javascript"
    src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->    
</head>
<body>
        <div class="login-container">      
            <div class="login-box animated fadeInDown">
                <div class="login-logo"></div>
                <div class="login-body">
                    <div class="login-title"><strong>MAJ poids CHE WEIGHT</strong></div>
                    <form action="majWeight.do" class="form-horizontal" method="post"><!-- ce formulaire d�clenche le lancement de l'application interface -->
	                    <div class="form-group">
	                        <div class="col-md-6">
	                            <input type="submit" value="MAJ" class="btn btn-info btn-block" name="button">
  							</div>
	                    </div>	                    
                    </form>
                </div>
            </div>         
        </div>
</body>
</html>