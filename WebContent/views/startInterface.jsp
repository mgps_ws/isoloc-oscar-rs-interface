<!DOCTYPE html>
<html lang="en" class="body-full-height">
    <head>  
        <!-- META SECTION -->
        <title>Atlant - Responsive Bootstrap Admin Template</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="img/logoMGPSpetitpetit.png" type="image/x-icon" />
        <!-- END META SECTION -->
	    <style type="text/css">
		    @import "style.css";
		    @import "css/theme-default.css";
		</style>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <!-- END META SECTION -->
    
    <!-- LESSCSS INCLUDE -->        
    <link rel="stylesheet/less" type="text/css" href="css/styles.less"/>
    <script type="text/javascript" src="js/plugins/lesscss/less.min.js"></script>  
    
    <script type="text/javascript"
    src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->    
</head>
<body>
        <div class="login-container">      
            <div class="login-box animated fadeInDown">
                <div class="login-logo"></div>
                <div class="login-body">
                    <div class="login-title"><strong>Start Interface</strong></div>
                    <form action="startinterface.do" class="form-horizontal" method="post"><!-- ce formulaire d�clenche le lancement de l'application interface -->
	                    <div class="form-group">
	                        <div class="col-md-6">
	                            <input type="submit" value="Start" class="btn btn-info btn-block" name="button">
  							</div>
	                    </div>
	                    <div class="form-group">
	                    	<div class="col-md-12">
	                            <strong>Frequence refresh (secondes): </strong><input type="number" class="form-control" name=refreshfreq value="6"/>
	                        </div>
	                        <div class="col-md-12" >
	                            <strong>Ip address Ecn4 server : </strong><input type="text" class="form-control" name=ipAdressClient value="10.120.2.71"/>
	                        </div>
	                        <div class="col-md-12" >
	                            <strong>Port Ecn4 server : </strong><input type="text" class="form-control" name=portComClient value="13101"/>
	                        </div>
	                        <div class="col-md-12" >
	                            <strong>Thread Ecn4 name : </strong><input type="text" class="form-control" name=nameThreadClient value="ClientECN4"/>
	                        </div>
	                        <div class="col-md-12" >
	                            <strong>Extension of Time for EcN4's connection (en ms): </strong><input type="text" class="form-control" name=delaiReconnexionClient value="1000"/>
	                        </div>
	                        <div class="col-md-12" >
	                            <strong>Maximum number of connections (Interface) : </strong><input type="text" class="form-control" name=nombreConnexionServer value="60"/>
	                        </div>
	                        <div class="col-md-12" >
	                            <strong>Port Interface server : </strong><input type="text" class="form-control" name=portServer value="13230"/>
	                        </div>
	                        <div class="col-md-12" >
	                            <strong>Thread Interface name : </strong><input type="text" class="form-control" name=nameThreadServer value="ServerStraddle"/>
	                        </div>
	                        <div class="col-md-12" >
	                            <strong>Extension of Time for Interface's connection (en ms) : </strong><input type="text" class="form-control" name=delaiReconnexionServer value="1000"/>
	                        </div> 	    
	                        <div class="col-md-12" >
	                            <strong>Ip address Web Service Server Authentification: </strong><input type="text" class="form-control" name=ipAdressWebService value="10.120.2.89:10080"/>
	                        </div> 	     
	                        <div class="col-md-12" >
	                            <strong>Ip address Web Service Vessel Name : </strong><input type="text" class="form-control" name=ipAdressWebServiceShip value="10.120.2.89:10080"/>
	                        </div> 	 
	                        <div class="col-md-3">
	                            <strong>Activer VesselName : </strong>	  
	                            <label class="switch">                          
										<input type="checkbox" name ="vesselName" class="switch" value="1" checked/>
	                          			<span></span>   
                          			</label>                       		
	                        </div> 
	                        <div class="col-md-3">
	                            <strong>Activer detection ligne (lien anticollision) : </strong>	  
	                            <label class="switch">                          
										<input type="checkbox" name ="detectionRow" class="switch" value="1" checked/>
	                          			<span></span>   
                          			</label>                       		
	                        </div>
	                        <div class="col-md-3">
	                            <strong>Activer CheckList (tous les Cavaliers) : </strong>	  
	                            <label class="switch">                          
										<input type="checkbox" name ="checkList" class="switch" value="1" />
	                          			<span></span>   
                          			</label>                       		
	                        </div>
	                        <div class="col-md-3">
	                            <strong>Mode debug : </strong>	  
	                            <label class="switch">                          
										<input type="checkbox" name ="debug" class="switch" value="1" />
	                          			<span></span>   
                          			</label>                       		
	                        </div>   
	                                                          	                        	                        	                        
                    	</div>
                    </form>
                </div>
            </div>         
        </div>
</body>
</html>