package info.mgps.isotop.task.applus.body.imp;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAttribute;


/**
 * Class represents Import Reference Object which is a compilation of the attributes of ReferenceAmq, ReferenceBad, ReferenceCbk entities
 * @author EV
 * @version 1.0
 */

public class ReferenceDec implements Serializable{ 
	
	
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	private String sic;
	private String statut;
	private String ori;
	private String date;
     
    public ReferenceDec() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ReferenceDec(ReferenceDec ref) {
		super();
		this.id = ref.getId();
		this.sic = ref.getSic();
		this.ori = ref.getOri();
		this.date = ref.getDate();
		this.statut = ref.getStatut();
		
	}
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@XmlAttribute(name = "sic")
    public String getSic() { 
        return sic; 
    } 
  
    public void setSic(String sic) { 
        this.sic = sic; 
    } 

    @XmlAttribute(name = "date")
    public String getDate() { 
        return date; 
    } 
  
    public void setDate(String date) { 
        this.date = date; 
    } 

    @XmlAttribute(name = "statut")
    public String getStatut() { 
        return statut; 
    } 
  
    public void setStatut(String statut) { 
        this.statut = statut; 
    }
    
    @XmlAttribute(name = "ori")
	public String getOri() {
		return ori;
	}

	public void setOri(String ori) {
		this.ori = ori;
	}

	@Override
	public String toString() {
		return "ReferenceDec [id=" + id + ", sic=" + sic + ", statut=" + statut
				+ ", ori=" + ori + ", date=" + date + "]";
	} 
  
   

}