package info.mgps.isotop.task.applus.body.imp;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAttribute;


public class Tiers implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long idTiers;
	private String reptransem;
	private String reptransemLib;
    private String reptranssm;
    private String reptranssmLib;
    private String armsm;
    private String armsmLib;
    private String armem;
    private String armemLib;
    private String courtierarr;
    private String courtierarrLib;
    private String courtierdep;
    private String courtierdepLib;
    private String steCreateRdv;
    private String steCreateRdvLib;
    private String steFinRdv;
    private String creat;
	private String creatlib;
	private String dem; 
	private String demlib;
	private String trans; 
	private String translib;
	private String exp; 
	private String explib;
	private String manut;
	private String manutLib; 
	private String lieu;
	private String lieuLib; 
	private String zone;
    private String zoneLib;
	private String nomdem; 
	private String ctatrans;
	private String afret;  
	private String afretLib;
	private String amq; 
	private String amqLib;

    public Tiers() {
		super();
	}
    
	public Tiers(Tiers tiers) {
		super();
		this.reptransem = tiers.getReptransem();
		this.reptranssm = tiers.getReptranssm();
		this.armsm = tiers.getArmsm();
		this.armem = tiers.getArmem();
		this.reptransemLib = tiers.getReptransemLib();
		this.reptranssmLib = tiers.getReptranssmLib();
		this.armsmLib = tiers.getArmsmLib();
		this.armemLib = tiers.getArmemLib();
		this.courtierarr = tiers.getCourtierarr();
		this.courtierdep = tiers.getCourtierdep();
		this.creat = tiers.getCreat();
		this.creatlib = tiers.getCreatlib();
		this.dem = tiers.getDem();
		this.demlib = tiers.getDemlib();
		this.trans = tiers.getTrans();
		this.translib = tiers.getTranslib();
		this.exp = tiers.getExp();
		this.explib = tiers.getExplib();
		this.manut = tiers.getManut();
		this.manutLib = tiers.getManutLib();
		this.lieu = tiers.getLieu();
		this.lieuLib = tiers.getLieuLib();
		this.zone = tiers.getZone();
		this.zoneLib = tiers.getZoneLib();
		this.nomdem = tiers.getNomdem();
		this.ctatrans = tiers.getCtatrans();
		this.afret = tiers.getAfret();
		this.afretLib = tiers.getAfretLib();
		this.amq = tiers.getAmq();
		this.amqLib = tiers.getAmqLib();
		this.steCreateRdv = tiers.getSteCreateRdv();
		this.steFinRdv = tiers.getSteFinRdv();
	}
	
	
	public Long getIdTiers() {
		return idTiers;
	}

	public void setIdTiers(Long idTiers) {
		this.idTiers = idTiers;
	}

	@XmlAttribute(name = "steCreateRdv")
	public String getSteCreateRdv() {
		return steCreateRdv;
	}

	public void setSteCreateRdv(String steCreateRdv) {
		this.steCreateRdv = steCreateRdv;
	}
	@XmlAttribute(name = "steCreateRdvLib")
	public String getSteCreateRdvLib() {
		return steCreateRdvLib;
	}

	public void setSteCreateRdvLib(String steCreateRdvLib) {
		this.steCreateRdvLib = steCreateRdvLib;
	}
	@XmlAttribute(name = "steExecRdv")
	public String getSteFinRdv() {
		return steFinRdv;
	}

	public void setSteFinRdv(String steFinRdv) {
		this.steFinRdv = steFinRdv;
	}

	//tiers amq
	@XmlAttribute(name = "amq")
	public String getAmq() {
		return amq;
	}

	public void setAmq(String amq) {
		this.amq = amq;
	}
	@XmlAttribute(name = "amq-lib")
	public String getAmqLib() {
		return amqLib;
	}

	public void setAmqLib(String amqLib) {
		this.amqLib = amqLib;
	}
	@XmlAttribute(name = "afret")
	public String getAfret() {
		return afret;
	}

	public void setAfret(String afret) {
		this.afret = afret;
	}
	@XmlAttribute(name = "afret-lib")
	public String getAfretLib() {
		return afretLib;
	}

	public void setAfretLib(String afretLib) {
		this.afretLib = afretLib;
	}

	//tiers cbk
	@XmlAttribute(name = "dem")
	public String getDem() {
		return dem;
	}

	public void setDem(String dem) {
		this.dem = dem;
	}
	@XmlAttribute(name = "dem-lib")
	public String getDemlib() {
		return demlib;
	}

	public void setDemlib(String demlib) {
		this.demlib = demlib;
	}
	@XmlAttribute(name = "exp")
	public String getExp() {
		return exp;
	}

	public void setExp(String exp) {
		this.exp = exp;
	}
	@XmlAttribute(name = "exp-lib")
	public String getExplib() {
		return explib;
	}

	public void setExplib(String explib) {
		this.explib = explib;
	}
	@XmlAttribute(name = "ctatrans")
	public String getCtatrans() {
		return ctatrans;
	}

	public void setCtatrans(String ctatrans) {
		this.ctatrans = ctatrans;
	}

	@XmlAttribute(name = "trans")
	public String getTrans() { 
		return trans; 
	} 

	public void setTrans(String trans) { 
		this.trans = trans; 
	} 

	@XmlAttribute(name = "trans-lib")
	public String getTranslib() { 
		return translib; 
	} 

	public void setTranslib(String translib) { 
		this.translib = translib; 
	} 

	@XmlAttribute(name = "creat")
	public String getCreat() { 
		return creat; 
	} 

	public void setCreat(String creat) { 
		this.creat = creat; 
	} 

	@XmlAttribute(name = "creat-lib")
	public String getCreatlib() { 
		return creatlib; 
	} 

	public void setCreatlib(String creatlib) { 
		this.creatlib = creatlib; 
	} 

	@XmlAttribute(name = "manut")
	public String getManut() { 
		return manut; 
	} 

	public void setManut(String manut) { 
		this.manut = manut; 
	}

	@XmlAttribute(name = "lieu-lib")
	public String getLieulib() { 
		return lieuLib; 
	} 

	public void setLieulib(String lieulib) { 
		this.lieuLib = lieulib; 
	} 

	@XmlAttribute(name = "zone")
	public String getZone() { 
		return zone; 
	} 

	public void setZone(String zone) { 
		this.zone = zone; 
	} 

	@XmlAttribute(name = "zone-lib")
	public String getZonelib() { 
		return zoneLib; 
	} 

	public void setZonelib(String zonelib) { 
		this.zoneLib = zonelib; 
	} 

	@XmlAttribute(name = "nomdem")
	public String getNomdem() { 
		return nomdem; 
	} 

	public void setNomdem(String nomdem) { 
		this.nomdem = nomdem; 
	} 

	// tiers atp
    @XmlAttribute(name = "reptransem")
    public String getReptransem() { 
        return reptransem; 
    } 
  
    public void setReptransem(String reptransem) { 
        this.reptransem = reptransem; 
    }
    
    @XmlAttribute(name = "reptransem-lib")
    public String getReptransemLib() {
		return reptransemLib;
	}

	public void setReptranssmLib(String reptranssmLib) {
		this.reptranssmLib = reptranssmLib;
	}
	
	@XmlAttribute(name = "reptranssm-lib")
	public String getReptranssmLib() {
		return reptranssmLib;
	}
    public void setReptranssm(String reptranssm) { 
        this.reptranssm = reptranssm; 
    } 
    @XmlAttribute(name = "armsm-lib")
    public String getArmsmLib() {
		return armsmLib;
	}

	public void setArmsmLib(String armsmLib) {
		this.armsmLib = armsmLib;
	}
	  @XmlAttribute(name = "armem-lib")
	public String getArmemLib() {
		return armemLib;
	}

	public void setArmemLib(String armemLib) {
		this.armemLib = armemLib;
	}
    
    
    
	@XmlAttribute(name = "reptranssm")
    public String getReptranssm() { 
        return reptranssm; 
    } 
	public void setReptransemLib(String reptransemLib) {
		this.reptransemLib = reptransemLib;
	}

   
    @XmlAttribute(name = "armsm")
    public String getArmsm() { 
        return armsm; 
    } 
  
    public void setArmsm(String armsm) { 
        this.armsm = armsm; 
    } 
    
    @XmlAttribute(name = "armem")
    public String getArmem() { 
        return armem; 
    } 
  
    public void setArmem(String armem) { 
        this.armem = armem; 
    }
	@XmlAttribute(name = "courtierarr")
    public String getCourtierarr() { 
        return courtierarr; 
    } 
    public void setCourtierarr(String courtierarr) { 
        this.armsm = courtierarr; 
    } 
    @XmlAttribute(name = "courtierdep")
    public String getCourtierdep() { 
        return courtierdep; 
    } 
    public void setCourtierdep(String courtierdep) { 
        this.courtierdep = courtierdep; 
    }
    @XmlAttribute(name = "courtierarr-lib")
    public String getCourtierarrLib() {
		return courtierarrLib;
	}

	public void setCourtierarrLib(String courtierarrLib) {
		this.courtierarrLib = courtierarrLib;
	}
	@XmlAttribute(name = "courtierdep-lib")
	public String getCourtierdepLib() {
		return courtierdepLib;
	}

	public void setCourtierdepLib(String courtierdepLib) {
		this.courtierdepLib = courtierdepLib;
	}

	@XmlAttribute(name = "zone-lib")
	public String getZoneLib() {
		return zoneLib;
	}
	public void setZoneLib(String zoneLib) {
		this.zoneLib = zoneLib;
	}
	 @XmlAttribute(name = "lieu")
	public String getLieu() {
		return lieu;
	}

	public void setLieu(String lieu) {
		this.lieu = lieu;
	}
	 @XmlAttribute(name = "lieu-lib")
	public String getLieuLib() {
		return lieuLib;
	}

	public void setLieuLib(String lieuLib) {
		this.lieuLib = lieuLib;
	}
	 
	@XmlAttribute(name = "manu-lib")
	public String getManutLib() {
		return manutLib;
	}

	public void setManutLib(String manutLib) {
		this.manutLib = manutLib;
	}
    
   /* public void addAgentDpaqToTiers(AgentDpaq child) {
        child.setTiers(this);
        this.agents.add(child);
    }*/
	@Override
	public String toString() {
		return "Tiers [idTiers=" + idTiers + ", reptransem=" + reptransem
				+ ", reptranssm=" + reptranssm + ", armsm=" + armsm
				+ ", armem=" + armem + ", courtierarr=" + courtierarr
				+ ", courtierdep=" + courtierdep + ", creat=" + creat
				+ ", creatlib=" + creatlib + ", dem=" + dem + ", demlib="
				+ demlib + ", trans=" + trans + ", translib=" + translib
				+ ", exp=" + exp + ", explib=" + explib + ", manut=" + manut
				+ ", lieu=" + lieu + ", zone=" + zone
				+ ", nomdem=" + nomdem + ", ctatrans=" + ctatrans + ", afret="
				+ afret + ", amq=" + amq
				+ ", amqLib=" + amqLib + ", afretLib=" + afretLib
				+ ", zoneLib=" + zoneLib + ", lieuLib=" + lieuLib
				+ ", manutLib=" + manutLib + "]";
	}
	
}
