package info.mgps.isotop.task.applus.body.imp;

import info.ttc.egee.architecture.activity.egee.notifications.Rdv;
import info.ttc.egee.architecture.activity.egee.notifications.Vag;
import info.ttc.egee.architecture.activity.egee.notifications.Vap;
import info.ttc.egee.architecture.activity.egee.notifications.Vze;


import java.io.Serializable;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * Class represents import Equipement Object which is a compilation of the attributes of EquipementAmq, EquipementBad, and EquipementCbk entities
 * @author EV
 * @version 1.0
 */

//@PersistenceUnit(name="EgeePU")
public class Equipement implements Serializable{ 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	private Long idEquipement;
	// debut cbk
	//@Id
	private String code;
	private String codeConstat;
	private String id;
	private String description;
	private String codelib;
	private String nb; 
	private String poids;
	private String poidsConstat;
	private String rang;
	private String vide; 
	private String dim; 
	private String frigo; 
	private String dgx; 
	private String polluant; 
	private String typeInRDV; // updated when we receive a rdv msg
	private String referenceInRDV;
	private String sens;
	private String amq;
	private String min; 
	private String max; 
	private String consigne; 
	private String unite; 
	// fin cbk
	private String tare;
	private String fcllcl;
	private String haulage;
	private String sic; 
	private String cvq; 
	private String date;
	private String random; 
	private String lng; 
	private String oh; 
	private String olf; 
	private String olb; 
	private String owl; 
	private String owr;
	private Long voyage_IdVoyage;
	private Rdv rdv;
	private Vag vag;
	private Vze vze;
	private Vap vap;

	public Equipement() {
		super();

	}
	public Equipement(long idEquip) {
		idEquipement = idEquip;
	}

	public Equipement(Equipement equip) {
		this.idEquipement = equip.getIdEquipement();
		this.id = equip.getId();
		//this.code = notificationId.concat(equip.getCode());
		this.code = equip.getCode();
		this.codelib = equip.getCodelib();
		this.poids = equip.getPoids();
		this.nb = equip.getNb();
		this.rang = equip.getRang();
		this.sic = equip.getSic();
		this.date = equip.getDate();
		this.dgx = equip.getDgx();
		this.dim = equip.getDim();
		this.frigo = equip.getFrigo();
		this.vide = equip.getVide();
		this.polluant = equip.getPolluant();
		this.min = equip.getMin();
		this.max = equip.getMax();
		this.unite = equip.getUnite();
		this.amq = equip.getAmq();
		this.consigne =  equip.getConsigne();
		this.description = equip.getDescription();
		this.tare = equip.getTare();

	}
	/* public Equipement(Equipement equip) {
    	this.id = equip.getId();
		this.tare = equip.getTare();
		this.code = equip.getCode();
		this.codelib = equip.getCodelib();
		this.poids = equip.getPoids();
		this.fcllcl = equip.getFcllcl();
		this.haulage = equip.getHaulage();
		this.nb = equip.getNb();
		this.rang = equip.getRang();
		this.sic = equip.getSic();
		this.cvq = equip.getCvq();
		this.date = equip.getDate();
		this.random = equip.getRandom();
		this.dgx = equip.getDgx();
		this.dim = equip.getDim();
		this.frigo = equip.getFrigo();
		this.vide = equip.getVide();
		this.polluant = equip.getPolluant();
		this.lng = equip.getLng();
		this.oh = equip.getOh();
		this.olf = equip.getOlf();
		this.olb = equip.getOlb();
		this.owl = equip.getOwl();
		this.owr = equip.getOwr();
		this.min = equip.getMin();
		this.max = equip.getMax();
		this.unite = equip.getUnite();
		this.instruction = equip.getInstruction();
		this.restitution = equip.getRestitution();
		this.ressources = equip.getRessources();
		this.capacite = equip.getCapacite();
		this.amq = equip.getAmq();
		this.consigne =  equip.getConsigne();
		this.description = equip.getDescription();

	}*/

	public Long getIdEquipement() {
		return idEquipement;
	}
	public void setIdEquipement(Long idEquipement) {
		this.idEquipement = idEquipement;
	}

	@XmlAttribute(name = "id")
	public String getId() { 
		return id; 
	} 

	public void setId(String id) { 
		this.id = id; 
	} 

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}


	@XmlAttribute(name = "amq")
	public String getAmq() {
		return amq;
	}
	public void setAmq(String amq) {
		this.amq = amq;
	}

	@XmlAttribute(name = "consigne")
	public String getConsigne() {
		return consigne;
	}
	public void setConsigne(String consigne) {
		this.consigne = consigne;
	}
	@XmlAttribute(name = "tare")
	public String getTare() { 
		return tare; 
	} 

	public void setTare(String tare) { 
		this.tare = tare; 
	} 

	@XmlAttribute(name = "code")
	public String getCode() { 
		return code; 
	} 

	public void setCode(String code) { 
		this.code = code; 
	} 

	public String getCodeConstat() {
		return codeConstat;
	}

	public void setCodeConstat(String codeConstat) {
		this.codeConstat = codeConstat;
	}

	@XmlAttribute(name = "code-lib")
	public String getCodelib() { 
		return codelib; 
	} 

	public void setCodelib(String codelib) { 
		this.codelib = codelib; 
	} 

	@XmlAttribute(name = "poids")
	public String getPoids() { 
		return poids; 
	} 

	public void setPoids(String poids) { 
		this.poids = poids; 
	} 

	public String getPoidsConstat() {
		return poidsConstat;
	}

	public void setPoidsConstat(String poidsConstat) {
		this.poidsConstat = poidsConstat;
	}

	@XmlAttribute(name = "fcllcl")
	public String getFcllcl() { 
		return fcllcl; 
	} 

	public void setFcllcl(String fcllcl) { 
		this.fcllcl = fcllcl; 
	} 

	@XmlAttribute(name = "haulage")
	public String getHaulage() { 
		return haulage; 
	} 

	public void setHaulage(String haulage) { 
		this.haulage = haulage; 
	} 

	@XmlAttribute(name = "nb")
	public String getNb() { 
		return nb; 
	} 

	public void setNb(String nb) { 
		this.nb = nb; 
	} 

	@XmlAttribute(name = "rang")
	public String getRang() { 
		return rang; 
	} 

	public void setRang(String rang) { 
		this.rang = rang; 
	}

	@XmlAttribute(name = "sic")
	public String getSic() { 
		return sic; 
	} 

	public void setSic(String sic) { 
		this.sic = sic; 
	} 

	@XmlAttribute(name = "cvq")
	public String getCvq() { 
		return cvq; 
	} 

	public void setCvq(String cvq) { 
		this.cvq = cvq; 
	} 

	@XmlAttribute(name = "date")
	public String getDate() { 
		return date; 
	} 

	public void setDate(String date) { 
		this.date = date; 
	} 

	@XmlAttribute(name = "random")
	public String getRandom() { 
		return random; 
	} 

	public void setRandom(String random) { 
		this.random = random; 
	} 

	@XmlAttribute(name = "dgx")
	public String getDgx() {
		return dgx;
	}

	public void setDgx(String dgx) {
		this.dgx = dgx;
	}

	@XmlAttribute(name = "dim")
	public String getDim() {
		return dim;
	}

	public void setDim(String dim) {
		this.dim = dim;
	}

	@XmlAttribute(name = "frigo")
	public String getFrigo() {
		return frigo;
	}

	public void setFrigo(String frigo) {
		this.frigo = frigo;
	}

	@XmlAttribute(name = "vide")
	public String getVide() {
		return vide;
	}

	public void setVide(String vide) {
		this.vide = vide;
	}

	@XmlAttribute(name = "polluant")
	public String getPolluant() {
		return polluant;
	}

	public void setPolluant(String polluant) {
		this.polluant = polluant;
	}

	@XmlAttribute(name = "long")
	public String getLng() {
		return lng;
	}

	public void setLng(String lng) {
		this.lng = lng;
	}

	@XmlAttribute(name = "oh")
	public String getOh() {
		return oh;
	}

	public void setOh(String oh) {
		this.oh = oh;
	}

	@XmlAttribute(name = "olf")
	public String getOlf() {
		return olf;
	}

	public void setOlf(String olf) {
		this.olf = olf;
	}

	@XmlAttribute(name = "olb")
	public String getOlb() {
		return olb;
	}

	public void setOlb(String olb) {
		this.olb = olb;
	}

	@XmlAttribute(name = "owl")
	public String getOwl() {
		return owl;
	}

	public void setOwl(String owl) {
		this.owl = owl;
	}

	@XmlAttribute(name = "owr")
	public String getOwr() {
		return owr;
	}

	public void setOwr(String owr) {
		this.owr = owr;
	}

	@XmlAttribute(name = "min")
	public String getMin() {
		return min;
	}

	public void setMin(String min) {
		this.min = min;
	}

	@XmlAttribute(name = "max")
	public String getMax() {
		return max;
	}

	public void setMax(String max) {
		this.max = max;
	}

	@XmlAttribute(name = "unite")
	public String getUnite() {
		return unite;
	}

	public void setUnite(String unite) {
		this.unite = unite;
	}
	public Rdv getRdv() {
		return rdv;
	}

	public void setRdv(Rdv rdv) {
		this.rdv = rdv;
	}
	@XmlAttribute(name = "type")
	public String getTypeInRDV() {
		return typeInRDV;
	}
	public void setTypeInRDV(String typeInRDV) {
		this.typeInRDV = typeInRDV;
	}
	@XmlAttribute(name = "reference")
	public String getReferenceInRDV() {
		return referenceInRDV;
	}
	public void setReferenceInRDV(String referenceInRDV) {
		this.referenceInRDV = referenceInRDV;
	}
	@XmlAttribute(name = "sens")
	public String getSens() {
		return sens;
	}
	public void setSens(String sens) {
		this.sens = sens;
	}
	public Vag getVag() {
		return vag;
	}
	public void setVag(Vag vag) {
		this.vag = vag;
	}
	public Vze getVze() {
		return vze;
	}
	public void setVze(Vze vze) {
		this.vze = vze;
	}

	public Vap getVap() {
		return vap;
	}
	public void setVap(Vap vap) {
		this.vap = vap;
	}
	public Long getVoyage_IdVoyage() {
		return voyage_IdVoyage;
	}

	public void setVoyage_IdVoyage(Long voyage_IdVoyage) {
		this.voyage_IdVoyage = voyage_IdVoyage;
	}
	@Override
	public String toString() {
		return "Equipement [idEquipement=" + idEquipement + ", code=" + code
				+ ", codeConstat=" + codeConstat + ", id=" + id
				+ ", description=" + description + ", codelib=" + codelib
				+ ", nb=" + nb + ", poids=" + poids + ", poidsConstat="
				+ poidsConstat + ", rang=" + rang + ", vide=" + vide + ", dim="
				+ dim + ", frigo=" + frigo + ", dgx=" + dgx + ", polluant="
				+ polluant + ", typeInRDV=" + typeInRDV + ", referenceInRDV="
				+ referenceInRDV + ", sens=" + sens + ", amq=" + amq + ", min="
				+ min + ", max=" + max + ", consigne=" + consigne + ", unite="
				+ unite + ", tare=" + tare + ", fcllcl=" + fcllcl
				+ ", haulage=" + haulage + ", sic=" + sic + ", cvq=" + cvq
				+ ", date=" + date + ", random=" + random + ", lng=" + lng
				+ ", oh=" + oh + ", olf=" + olf + ", olb=" + olb + ", owl="
				+ owl + ", owr=" + owr + ", voyage_IdVoyage=" + voyage_IdVoyage
				+ ", rdv=" + rdv + ", vag=" + vag + ", vze=" + vze + ", vap="
				+ vap + "]";
	}

	
} 
