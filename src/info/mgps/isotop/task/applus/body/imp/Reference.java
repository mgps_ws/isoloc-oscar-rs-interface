package info.mgps.isotop.task.applus.body.imp;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * Class represents Import Reference Object which is a compilation of the attributes of ReferenceAmq, ReferenceBad, ReferenceCbk entities
 * @author EV
 * @version 1.0
 */
//@PersistenceUnit(name="EgeePU")
public class Reference implements Serializable{ 
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	//debut CBK
	private String sic;
	private String ext;
	private String cot;
	private String dem;
	private String trans;
	private String statut;
	private String trsic;
	private String demsic;
	private String dateFromXml;
	private Date date;
	private String carriermerchant;
	private String mad;
	private String paiement;
	private String version;

	//fin CBK D�but AMQ

	private String hist;
	private String cbk;
	private String extcbk;
	private String dos;
	private String extdos;
	private String doc;
	private String extdoc; 
	private String bad; 
	private String extbad; 
	private String random; 
	private String amq;
	private String extamq;
	private String ori;
	private String rang;
	private String last;
	private String commentaires;
	private String complements;
	private String dateProgrameFromXml;
	private Date datePrograme;
	private String dateFinFromXml;
	private Date dateFin;
	private String voieGate;
	private String chid;// nom straddle pour vap
	private String datePriseTcZEFromXml;
	private String datePoseTcParcFromXml;
	private String posPriseTcZE;
	private String posPoseTcParc;
	private String typeMvtVap;//type du mouvement dans vap
	
	
	


	public Reference() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Reference(Reference ref) {
		super();
		this.id = ref.getId();
		this.sic = ref.getSic();
		this.hist = ref.getHist();
		this.date = ref.getDate();
		this.statut = ref.getStatut();
		this.cbk = ref.getCbk();
		this.extcbk = ref.getExtcbk();
		this.dos = ref.getDos();
		this.extdos = ref.getExtdos();
		this.doc = ref.getDoc();
		this.extdoc = ref.getExtdoc();
		this.bad = ref.getBad();
		this.extbad = ref.getExtbad();
		this.random = ref.getRandom();
		this.amq = ref.getAmq();
		this.extamq = ref.getExtamq();
		this.ext = ref.getExt();
		this.rang = ref.getRang();
		this.last = ref.getLast();
		this.cot = ref.getCot();
		this.dem = ref.getDem();
		this.trans = ref.getTrans();
		this.trsic = ref.getTrsic();
		this.demsic = ref.getDemsic();
		this.carriermerchant = ref.getCarriermerchant();
		this.mad = ref.getMad();
		this.paiement = ref.getMad();
		this.version = ref.getVersion();
		this.commentaires = ref.getCommentaires();
		this.complements = ref.getComplements();
		this.datePrograme = ref.getDatePrograme();
		this.dateFin = ref.getDateFin();
		this.voieGate = ref.getVoieGate();
		this.chid = ref.getChid();
		this.posPriseTcZE = ref.getPosPriseTcZE();
		this.posPoseTcParc = ref.getPosPoseTcParc();
		this.typeMvtVap = ref.getTypeMvtVap();
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@XmlAttribute(name = "sic")
	public String getSic() { 
		return sic; 
	} 

	public void setSic(String sic) { 
		this.sic = sic; 
	} 

	@XmlAttribute(name = "hist")
	public String getHist() { 
		return hist; 
	} 

	public void setHist(String hist) { 
		this.hist = hist; 
	} 

	@XmlAttribute(name = "statut")
	public String getStatut() { 
		return statut; 
	} 

	public void setStatut(String statut) { 
		this.statut = statut; 
	} 

	@XmlAttribute(name = "date")
	public String getDateFromXml() {
		return dateFromXml;
	}
	
	public void setDateFromXml(String dateFromXml) {
		this.dateFromXml = dateFromXml;
	}

	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		String  endDateFormat;	
		endDateFormat =	"yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat formatter = new SimpleDateFormat(endDateFormat);
		String dateMSG = formatter.format(date);
		try {
			date=formatter.parse(dateMSG);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void convertDate() throws ParseException {
		String initDateFormat, endDateFormat;
		initDateFormat = "dd/MM/yyyy HH:mm";	endDateFormat =	"yyyy-MM-dd HH:mm";
		SimpleDateFormat formatter = new SimpleDateFormat(endDateFormat);
		Date initDate ;
		initDate = new SimpleDateFormat(initDateFormat).parse(dateFromXml);
		dateFromXml = formatter.format(initDate); 
		date=formatter.parse(dateFromXml);
	}
	public void formatDate() throws ParseException {
		String initDateFormat, endDateFormat;
		initDateFormat = "dd/MM/yyyy HH:mm";	endDateFormat =	"yyyy-MM-dd HH:mm";
		SimpleDateFormat formatter = new SimpleDateFormat(endDateFormat);
		Date initDate ,dateDepart;
		String dateetd = null;
		
		initDate = new SimpleDateFormat(initDateFormat).parse(dateetd);
		dateetd = formatter.format(initDate);
		dateDepart=formatter.parse(dateetd);
	}
	public void convertDateFin() throws ParseException {
		String initDateFormat, endDateFormat;
		initDateFormat = "dd/MM/yyyy HH:mm";	endDateFormat =	"yyyy-MM-dd HH:mm";
		SimpleDateFormat formatter = new SimpleDateFormat(endDateFormat);
		Date initDate ;
		initDate = new SimpleDateFormat(initDateFormat).parse(dateFinFromXml);
		dateFinFromXml = formatter.format(initDate);
		dateFin=formatter.parse(dateFinFromXml);
	}

	public void convertDatePrograme() throws ParseException {
		String initDateFormat, endDateFormat;
		initDateFormat = "dd/MM/yyyy HH:mm";	endDateFormat =	"yyyy-MM-dd HH:mm";
		SimpleDateFormat formatter = new SimpleDateFormat(endDateFormat);
		Date initDate ;
		initDate = new SimpleDateFormat(initDateFormat).parse(dateProgrameFromXml);
		dateProgrameFromXml = formatter.format(initDate); 
		datePrograme=formatter.parse(dateProgrameFromXml);
	}
	@XmlAttribute(name = "datePrograme")
	public String getDateProgrameFromXml() {
		return dateProgrameFromXml;
	}

	public void setDateProgrameFromXml(String dateProgrameFromXml) {
		this.dateProgrameFromXml = dateProgrameFromXml;
	}

	public Date getDatePrograme() {
		return datePrograme;
	}

	public void setDatePrograme(Date datePrograme) {
		String  endDateFormat;	
		endDateFormat =	"yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat formatter = new SimpleDateFormat(endDateFormat);
		String dateMSG = formatter.format(date);
		try {
			datePrograme=formatter.parse(dateMSG);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@XmlAttribute(name = "dateFin")
	public String getDateFinFromXml() {
		return dateFinFromXml;
	}

	public void setDateFinFromXml(String dateFinFromXml) {
		this.dateFinFromXml = dateFinFromXml;
	}

	public Date getDateFin() {
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		String  endDateFormat;	
		endDateFormat =	"yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat formatter = new SimpleDateFormat(endDateFormat);
		String dateMSG = formatter.format(date);
		try {
			dateFin=formatter.parse(dateMSG);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	@XmlAttribute(name = "voieGate")
	public String getVoieGate() {
		return voieGate;
	}

	public void setVoieGate(String voieGate) {
		this.voieGate = voieGate;
	}

	@XmlAttribute(name = "cbk")
	public String getCbk() { 
		return cbk; 
	} 

	public void setCbk(String cbk) { 
		this.cbk = cbk; 
	} 

	@XmlAttribute(name = "extcbk")
	public String getExtcbk() { 
		return extcbk; 
	} 

	public void setExtcbk(String extcbk) { 
		this.extcbk = extcbk; 
	} 

	@XmlAttribute(name = "dos")
	public String getDos() { 
		return dos; 
	} 

	public void setDos(String dos) { 
		this.dos = dos; 
	} 

	@XmlAttribute(name = "extdos")
	public String getExtdos() { 
		return extdos; 
	} 

	public void setExtdos(String extdos) { 
		this.extdos = extdos; 
	} 

	@XmlAttribute(name = "doc")
	public String getDoc() { 
		return doc; 
	} 

	public void setDoc(String doc) { 
		this.doc = doc; 
	} 

	@XmlAttribute(name = "extdoc")
	public String getExtdoc() { 
		return extdoc; 
	} 

	public void setExtdoc(String extdoc) { 
		this.extdoc = extdoc; 
	} 

	@XmlAttribute(name = "bad")
	public String getBad() { 
		return bad; 
	} 

	public void setBad(String bad) { 
		this.bad = bad; 
	} 

	@XmlAttribute(name = "extbad")
	public String getExtbad() { 
		return extbad; 
	} 

	public void setExtbad(String extbad) { 
		this.extbad = extbad; 
	} 

	@XmlAttribute(name = "random")
	public String getRandom() { 
		return random; 
	} 

	public void setRandom(String random) { 
		this.random = random; 
	} 

	@XmlAttribute(name = "amq")
	public String getAmq() { 
		return amq; 
	} 

	public void setAmq(String amq) { 
		this.amq = amq; 
	} 

	@XmlAttribute(name = "extamq")
	public String getExtamq() { 
		return extamq; 
	} 

	public void setExtamq(String extamq) { 
		this.extamq = extamq; 
	}

	@XmlAttribute(name = "ext")
	public String getExt() { 
		return ext; 
	} 

	public void setExt(String ext) { 
		this.ext = ext; 
	} 

	@XmlAttribute(name = "rang")
	public String getRang() { 
		return rang; 
	} 

	public void setRang(String rang) { 
		this.rang = rang; 
	} 

	@XmlAttribute(name = "last")
	public String getLast() { 
		return last; 
	} 

	public void setLast(String last) { 
		this.last = last; 
	} 

	@XmlAttribute(name = "cot")
	public String getCot() {
		return cot;
	}
	public void setCot(String cot) {
		this.cot = cot;
	}
	@XmlAttribute(name = "dem")
	public String getDem() {
		return dem;
	}
	public void setDem(String dem) {
		this.dem = dem;
	}
	@XmlAttribute(name = "trans")
	public String getTrans() {
		return trans;
	}
	public void setTrans(String trans) {
		this.trans = trans;
	}
	@XmlAttribute(name = "trsic")
	public String getTrsic() {
		return trsic;
	}
	public void setTrsic(String trsic) {
		this.trsic = trsic;
	}
	@XmlAttribute(name = "demsic")
	public String getDemsic() {
		return demsic;
	}
	public void setDemsic(String demsic) {
		this.demsic = demsic;
	}
	@XmlAttribute(name = "carriermerchant")
	public String getCarriermerchant() {
		return carriermerchant;
	}
	public void setCarriermerchant(String carriermerchant) {
		this.carriermerchant = carriermerchant;
	}
	@XmlAttribute(name = "MAD")
	public String getMad() {
		return mad;
	}
	public void setMad(String mad) {
		this.mad = mad;
	}
	@XmlAttribute(name = "paiement")
	public String getPaiement() {
		return paiement;
	}
	public void setPaiement(String paiement) {
		this.paiement = paiement;
	}
	@XmlAttribute(name = "version")
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}

	@XmlAttribute(name = "ori")
	public String getOri() {
		return ori;
	}

	public void setOri(String ori) {
		this.ori = ori;
	}
	@XmlAttribute(name = "commentaires")
	public String getCommentaires() {
		return commentaires;
	}
	
	public void setCommentaires(String commentaires) {
		this.commentaires = commentaires;
	}
	@XmlAttribute(name = "complements")
	public String getComplements() {
		return complements;
	}

	public void setComplements(String complements) {
		this.complements = complements;
	}
	@XmlAttribute(name = "chid")
	public String getChid() {
		return chid;
	}

	public void setChid(String chid) {
		this.chid = chid;
	}
	@XmlAttribute(name = "datePrise")
	public String getDatePriseTcZEFromXml() {
		return datePriseTcZEFromXml;
	}

	public void setDatePriseTcZEFromXml(String datePriseTcZE) {
		this.datePriseTcZEFromXml = datePriseTcZE;
	}
	@XmlAttribute(name = "datePose")
	public String getDatePoseTcParcFromXml() {
		return datePoseTcParcFromXml;
	}

	public void setDatePoseTcParcFromXml(String datePoseTcParc) {
		this.datePoseTcParcFromXml = datePoseTcParc;
	}
	@XmlAttribute(name = "posPrise")
	public String getPosPriseTcZE() {
		return posPriseTcZE;
	}
	
	public void setPosPriseTcZE(String posPriseTcZE) {
		this.posPriseTcZE = posPriseTcZE;
	}
	@XmlAttribute(name = "posPose")
	public String getPosPoseTcParc() {
		return posPoseTcParc;
	}

	public void setPosPoseTcParc(String posPoseTcParc) {
		this.posPoseTcParc = posPoseTcParc;
	}
	@XmlAttribute(name = "typeMvt")
	public String getTypeMvtVap() {
		return typeMvtVap;
	}

	public void setTypeMvtVap(String typeMvtVap) {
		this.typeMvtVap = typeMvtVap;
	}

	@Override
	public String toString() {
		return "Reference [id=" + id + ", sic=" + sic + ", ext=" + ext
				+ ", cot=" + cot + ", dem=" + dem + ", trans=" + trans
				+ ", statut=" + statut + ", trsic=" + trsic + ", demsic="
				+ demsic + ", carriermerchant="
				+ carriermerchant + ", mad=" + mad + ", paiement=" + paiement
				+ ", version=" + version + ", hist=" + hist + ", cbk=" + cbk
				+ ", extcbk=" + extcbk + ", dos=" + dos + ", extdos=" + extdos
				+ ", doc=" + doc + ", extdoc=" + extdoc + ", bad=" + bad
				+ ", extbad=" + extbad + ", random=" + random + ", amq=" + amq
				+ ", extamq=" + extamq + ", ori=" + ori + ", rang=" + rang
				+ ", last=" + last + ", commentaires=" + commentaires
				+ ", complements=" + complements + ", dateFromXml=" + dateFromXml +  ", date=" + date + ", dateFin=" + dateFin
				+ ", datePrograme=" + datePrograme + "]";
	} 

}