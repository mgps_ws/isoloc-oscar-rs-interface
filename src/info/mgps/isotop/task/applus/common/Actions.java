package info.mgps.isotop.task.applus.common;

import info.mgps.isotop.task.applus.body.imp.Tiers;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;


/**
 * Class represents the JAXB ApplusMess Object.
 * Import classes : AMQ, ATP, BAD, CBK must extend this class
 * Export classes : CCH, CEN, CRE, CVQ, FCH must extend this class
 * @author EV
 * @version 1.0
 */
public abstract class Actions implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long idApplusMsg;
	//@OneToOne(orphanRemoval=true,cascade=CascadeType.ALL)
	//private Intervenant intervenant;
	private Date dateAction;
	//@OneToOne(orphanRemoval=true,cascade=CascadeType.ALL)
	private Tiers tiersFromXml;
	
	public Long getIdApplusMsg() {
		return idApplusMsg;
	}

	public void setIdApplusMsg(Long idApplusMsg) {
		this.idApplusMsg = idApplusMsg;
	}

	public Date getDateAction() {
		return dateAction;
	}

	public void setDateAction(Date date) {
		String  endDateFormat;	
		endDateFormat =	"yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat formatter = new SimpleDateFormat(endDateFormat);
		String dateactionStr = formatter.format(date);
		try {
			dateAction=formatter.parse(dateactionStr);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void formatDate() throws ParseException {
		
	}
	// apres faut tester sur le setter
	@XmlElements({
		@XmlElement(name = "tiers-dpaq"),
		@XmlElement(name = "tiers-cst"),
		@XmlElement(name = "tiers-amq"),
		@XmlElement(name = "tiers-dec"),
		@XmlElement(name = "tiers-cbk"),
		@XmlElement(name = "tiers")
	    
	})
	public Tiers getTiers() {
		return tiersFromXml;
	}

	public void setTiers(Tiers tiersFromXml) {
		this.tiersFromXml = tiersFromXml;
	}

	public abstract String getType();

	@Override
	public String toString() {
		return "Actions [idApplusMsg=" + idApplusMsg  + ", dateAction=" + dateAction + "]";
	}

	
}
