package info.mgps.isotop.task.applus.common;

import java.io.Serializable;

import info.mgps.isotop.task.applus.header.imp.Message;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * Class represents the JAXB ApplusMessType Object.
 * Import classes : Notification, Response must extend this class
 * Export classe Request must extend this class
 * @author EV
 * @version 1.0
 */
public abstract class ApplusMessType implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    private String id; 
	private Message message;
	
	
	
	
	public ApplusMessType() {
		
	}

	public ApplusMessType(String id) {
		super();
		this.id = id;
	}

	@XmlAttribute(name = "id")
	public String getId() { 
	   return id; 
	} 
	  
	public void setId(String id) { 
	   this.id = id; 
	}
	
	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	//notification ou response
	public abstract String getMessType();

	@Override
	public String toString() {
		return "ApplusMessType [id=" + id + "]";
	}
	
}
