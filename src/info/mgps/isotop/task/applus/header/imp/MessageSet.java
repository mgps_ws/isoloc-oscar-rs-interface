package info.mgps.isotop.task.applus.header.imp;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;



/**
 * Class represents the import JAXB MessageSet Object
 * @author EV
 * @version 1.0
 */
public class MessageSet implements Serializable{ 
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long idMessageSet;
    private String id;
    private String icid;
    private String date;
    private Destinataire destinataire;
    private Emetteur emetteur;
    private Message messages;
   

   
    public MessageSet() {
		
		
	}

	public MessageSet(Long idMessageSet, String id, String icid, String date,
			Destinataire destinataire, Emetteur emetteur) {
		super();
		this.idMessageSet = idMessageSet;
		this.id = id;
		this.icid = icid;
		this.date = date;
		this.destinataire = destinataire;
		this.emetteur = emetteur;

		
	}
	public MessageSet(Long idMessageSet, String id, String icid, String date) {
		super();
		this.idMessageSet = idMessageSet;
		this.id = id;
		this.icid = icid;
		this.date = date;

		
	}
	
	public Long getIdMessageSet() {
		return idMessageSet;
	}

	public void setIdMessageSet(Long idMessageSet) {
		this.idMessageSet = idMessageSet;
	}

	@XmlAttribute(name = "id")
    public String getId() { 
        return id; 
    } 
  
    public void setId(String id) { 
        this.id = id; 
    } 
    
    @XmlAttribute(name = "icid")
    public String getIcid() { 
        return icid; 
    } 
  
    public void setIcid(String icid) { 
        this.icid = icid; 
    } 
   
    @XmlAttribute(name = "date")
    public String getDate() { 
        return date; 
    } 
  
    public void setDate(String date) { 
        this.date = date; 
    } 
    
    @XmlElement(name = "Destinataire")
    public Destinataire getDestinataire() { 
        return destinataire; 
    } 
  
    public void setDestinataire(Destinataire destinataire) { 
        this.destinataire = destinataire; 
    } 
    
    @XmlElement(name = "Emetteur")
    public Emetteur getEmetteur() { 
        return emetteur; 
    } 
  
    public void setEmetteur(Emetteur emetteur) { 
        this.emetteur = emetteur; 
    } 
    
    @XmlElement(name = "Messages")
    public Message getMessages() { 
        return messages; 
    } 
  
    public void setMessages(Message messages) { 
        this.messages = messages; 
    }

	
	@Override
	public String toString() {
		return "MessageSet [id=" + id + ", icid=" + icid + ", date=" + date
				+ ", destinataire=" + destinataire + ", emetteur=" + emetteur
				+ ", messages=" + messages + "]";
	} 
  
} 
