package info.mgps.isotop.task.applus.header.imp;


import java.io.Serializable;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * Class represents the import JAXB Interchange Object
 * @author EV
 * @version 1.0
 */

// Attention : Elément déterminant (root) de l'arbre XML

@XmlRootElement(name="Interchanges")
public class Interchange implements Serializable{ 
 
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	
	private String id;
    private String inter_from;
    private String inter_to;//fetch = FetchType.EAGER, cascade=CascadeType.PERSIST
    private MessageSet messageSet;
    
    public Interchange() {
    
    }

	public Interchange(String id) {
		
		this.id = id;
	}

	@XmlAttribute(name = "id")
    public String getId() { 
        return id; 
    } 
  
    public void setId(String id) { 
        this.id = id; 
    }
    
    @XmlAttribute(name = "from")
    public String getInter_from() { 
        return inter_from; 
    } 
  
    public void setInter_from(String from) { 
        this.inter_from = from; 
    } 
   
    @XmlAttribute(name = "to")
    public String getInter_to() { 
        return inter_to; 
    } 
  
    public void setInter_to(String to) { 
        this.inter_to = to; 
    } 
    
    @XmlElement(name = "MessageSet")
    public MessageSet getMessageSet() { 
        return messageSet; 
    } 
  
    public void setMessageSet(MessageSet messageSet) { 
        this.messageSet = messageSet; 
    }
      
	@Override
	public String toString() {
		return "Interchange [id=" + id + ", from=" + inter_from + ", to=" + inter_to
				+ ", messageSet=" + messageSet + "]";
	}

} 
