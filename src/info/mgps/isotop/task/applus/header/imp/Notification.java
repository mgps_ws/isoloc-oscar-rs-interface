package info.mgps.isotop.task.applus.header.imp;

import java.io.Serializable;
import info.mgps.isotop.task.applus.common.Actions;
import info.mgps.isotop.task.applus.common.ApplusMessType;
import info.ttc.egee.architecture.activity.egee.notifications.Rdv;
import info.ttc.egee.architecture.activity.egee.notifications.Vag;
import info.ttc.egee.architecture.activity.egee.notifications.Vap;
import info.ttc.egee.architecture.activity.egee.notifications.Vze;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;

/**
 * Class represents the import JAXB Notification Object. This class extends ApplusMessType
 * @author EV
 * @version 1.0
 * @see info.mgps.isotop.task.applus.common.ApplusMessType
 */

public class Notification extends ApplusMessType implements Serializable{ 
  

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String type; 
    private String action;
    private Actions applusmess;

    public Notification() {
		
	}

	
	public Notification(String id, String type, String action,Actions applusmess) {
		super(id);
		this.type = type;
		this.action = action;
		this.applusmess=applusmess;
	}

	@XmlAttribute(name = "type")
    public String getType() { 
        return type; 
    } 
  
    public void setType(String type) { 
        this.type = type; 
    } 
 
    @XmlAttribute(name = "action")
    public String getAction() { 
        return action; 
    } 
  
    public void setAction(String action) { 
        this.action = action; 
    }

	// ATTENTION : MATCHING XMLELEMENT(NAME) ==> VOIR CONTENU FICHIER XML !!!
    // EX: BAD ==> BON-A-DELIVRER  
    @XmlElements({
    			@XmlElement(name="rendez-vous", type=Rdv.class),
    			@XmlElement(name="Vue-a-Parc", type=Vap.class),
    			@XmlElement(name="gate", type=Vag.class),
    			@XmlElement(name="zone-echange", type=Vze.class)
    	    	})
    public Actions getApplusmess() {
        return this.applusmess; 
    } 
  
    public void setApplusmess(Actions applusmess) { 
        this.applusmess = applusmess; 
    } 
	@Override
	public String getMessType() {
		return "notification";
	}
	
	@Override
	public String toString() {
		return "Notification [type=" + type + ", action=" + action
				+ ", applusmess=" + applusmess + ", getId()=" + getId() + "]";
	}

	

} 
