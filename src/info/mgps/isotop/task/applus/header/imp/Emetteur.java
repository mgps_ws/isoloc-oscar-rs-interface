package info.mgps.isotop.task.applus.header.imp;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * Class represents the import JAXB Emetteur Object
 * @author EV
 * @version 1.0
 */
public class Emetteur implements Serializable{ 
	  
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long idEmetteur; 
    private String user; 
    private String tiersprof; 
   
	public Emetteur() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Emetteur(Long idEmetteur, String user, String tiersprof) {
		super();
		this.idEmetteur = idEmetteur;
		this.user = user;
		this.tiersprof = tiersprof;
	}

	

	public Long getIdEmetteur() {
		return idEmetteur;
	}

	public void setIdEmetteur(Long idEmetteur) {
		this.idEmetteur = idEmetteur;
	}

	@XmlAttribute(name = "user")
    public String getUser() { 
        return user; 
    } 
  
    public void setUser(String user) { 
        this.user = user; 
    } 
  
    @XmlAttribute(name = "tiersProf")
    public String getTiersprof() { 
        return tiersprof; 
    } 
  
    public void setTiersprof(String tiersprof) { 
        this.tiersprof = tiersprof; 
    }

	@Override
	public String toString() {
		return "Emetteur [idEmetteur=" + idEmetteur + ", user=" + user
				+ ", tiersprof=" + tiersprof + "]";
	} 
 
    
}