package info.mgps.isotop.task.applus.header.imp;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * Class represents the import JAXB Destinataire Object
 * @author EV
 * @version 1.0
 */
public class Destinataire implements Serializable{ 
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long idDestinataire; 
	private String user; 
    private String tiersprof; 
        
   
	public Destinataire() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Destinataire(Long idDestinataire, String user, String tiersprof) {
		super();
		this.idDestinataire = idDestinataire;
		this.user = user;
		this.tiersprof = tiersprof;
	}

	
	
	public Long getIdDestinataire() {
		return idDestinataire;
	}

	public void setIdDestinataire(Long idDestinataire) {
		this.idDestinataire = idDestinataire;
	}

	@XmlAttribute(name = "user")
    public String getUser() { 
        return user; 
    } 
  
    public void setUser(String user) { 
        this.user = user; 
    } 
  
    @XmlAttribute(name = "tiersProf")
    public String getTiersprof() { 
        return tiersprof; 
    } 
  
    public void setTiersprof(String tiersprof) { 
        this.tiersprof = tiersprof; 
    }

	@Override
	public String toString() {
		return "Destinataire [idDestinataire=" + idDestinataire + ", user="
				+ user + ", tiersprof=" + tiersprof + "]";
	} 
 
}