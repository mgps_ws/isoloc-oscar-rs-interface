package info.mgps.isotop.task.applus.header.imp;

import info.mgps.isotop.task.applus.common.ApplusMessType;

import java.util.List;
import java.util.Vector;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;

/**
 * Class represents the import JAXB Message Object
 * @author EV
 * @version 1.0
 */
public class Message { 

	private Long idMessage;
    private List<ApplusMessType> applusmesstypes = new Vector<ApplusMessType>(); 
	
   
	 public Message() {
		
	 }
	 public Message(List<ApplusMessType> applusmesstypes) {
			this.applusmesstypes = applusmesstypes;
		
	 }
	 // ATTENTION : MATCHING XMLELEMENT(NAME) ==> VOIR CONTENU FICHIER XML !!!
	 // EX: BAD ==> BON-A-DELIVRER
    @XmlElements({
    	@XmlElement(name="Notification", type=Notification.class)}) 
    public List<ApplusMessType> getApplusMessTypes() { 
        return applusmesstypes; 
    } 
  
   

	public Long getIdMessage() {		
		return idMessage;
	}

	public void setIdMessage(Long idMessage) {
		this.idMessage = idMessage;
	}

	public void setApplusMessTypes(List<ApplusMessType> applusmesstypes) { 
        this.applusmesstypes = applusmesstypes; 
    }
	
	public void addNotificationToMessage(Notification child) {
		child.setMessage(this);
    	this.applusmesstypes.add(child);
	}
	
	
	@Override
	public String toString() {
		return "Message [idMessage=" + idMessage + ", applusmesstypes="
				+ applusmesstypes + "]";
	} 
} 
