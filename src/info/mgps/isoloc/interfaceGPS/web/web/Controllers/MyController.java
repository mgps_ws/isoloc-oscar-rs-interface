package info.mgps.isoloc.interfaceGPS.web.web.Controllers;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import info.mgps.isoloc.interfaceGPS.main.WorkManagerStart;
import info.mgps.isoloc.interfaceGPS.service.log.AccessDb;
import info.mgps.isoloc.interfaceGPS.utilitaire.LogClass;
import info.mgps.isoloc.interfaceGPS.web.Models.Error;
import info.mgps.isoloc.interfaceGPS.web.Models.UserWeb;
import info.mgps.isoloc.interfaceGPS.web.Models.Utilisateur;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class MyController {
	
	private WorkManagerStart workManager;
	private List<Utilisateur> utilisateurs;
	private Map<String, Integer> logedUsers = new HashMap<String, Integer>();
	// String pour l'identifiant du user
	// Integer pour son role 1->admin,2->priviligie, 3->user standard
	// il faut penser � mettre une fonction pour supprimer les users qui se d�connectent en fermant la page
	private String ipAdressClient;
	private String portComClient;
	private String nameThreadClient;
	private String delaiReconnexionClient;
	private String nombreConnexionServer;
	private String portServer;
	private String nameThreadServer;
	private String delaiReconnexionServer;
	//private String ipAdressWebServiceAuth;
	
	private static boolean debug = false;
	private String ipAdressWebServiceAuth;
	private static int interfaceRefresh;
	private String portNumberWeight;
	private static float sdkVersion;
	private static String ipAdressWebServiceShip;
	private static boolean vesselNameService = false;
	private static String ipAdressAnticollisionServer="192.168.10.164";
	private static boolean isDetectionActive = false;
	private static String ipAdressCheckListServer ="192.168.10.100";
	private static boolean checkListUp=true;
	private static String ipAddressWebServiceCheWeightRequest="10.120.2.89:10080";
	private static String ipAddressWebServiceCheWeightInsert="10.120.2.89:9080";
	private static String vgmUser;
	private static String vgmPassword;
	private static String raz_frequence;
	private static boolean navisValidation = false;
	
	public static float getSdkVersion(){
		LogClass.ecriture("MyController","sdkVersion = "+sdkVersion+" equality "+(sdkVersion==(float) 3.7),1);
		//System.out.println("sdkVersion = "+sdkVersion+" equality "+(sdkVersion==(float) 3.7));
		return sdkVersion;
	}
	
	public static boolean isCheckListUp() {
		return checkListUp;
	}
	public static String getIpCheckListStraddle() {
		LogClass.ecriture("MyController","demande ip checklist = "+ipAdressCheckListServer,1);
		//System.out.println("demande ip checklist = "+ipAdressCheckListServer);
		return ipAdressCheckListServer;
	}
	public static String getIpAddressWebServiceShip(){
		return MyController.ipAdressWebServiceShip;
	}
	public static boolean isDebug(){
		return debug;
	}
	//StraddleManager straddles;
	//ShuntManager shunts;
	/**
	 *Home : action qui gere les bouton principales de la page d'accueil  
	 *s'execute apr�s le clique sur les bouton "Rechrche" ou "Actualisation"
	 * @param button : contient le nom du bouton sur lequel on a cliqu�
	 */
	///////////////////////////////////home/////////////////////////////////////
	@RequestMapping(value="/views/home.do", method=RequestMethod.POST)
	public ModelAndView Home(String user, String button,String idList, HttpServletRequest request){
		if (workManager == null)
			return new ModelAndView("Begin.jsp");
		// LogClass.ecriture("MyController\\Home" , ">>Home",1);
		if (button.equalsIgnoreCase("Recherche")) {
			Map<String, Object> mapToReturn = verifyUser(user);
			if (mapToReturn.get("user") != null) {
				return new ModelAndView("Recherche.jsp", mapToReturn);
			} else {
				return new ModelAndView("Begin.jsp", mapToReturn);
			}

		} else if (button.equals("Parametrage")) {
			
			Map<String, Object> mapToReturn = verifyUser(user, 2);
			if (mapToReturn.get("user") != null) {
				mapToReturn.put("straddles", workManager.getStraddleManager());
				return new ModelAndView("Parametrage.jsp", mapToReturn);
			} else {
				return new ModelAndView("Begin.jsp", mapToReturn);
			}
		}else if (button.equals("checkList")) {
			
			Map<String, Object> mapToReturn = verifyUser(user, 4);
			if (mapToReturn.get("user") != null) {
				mapToReturn.put("straddles", workManager.getStraddleManager());
				return new ModelAndView("Parametrage_checkList.jsp", mapToReturn);
			} else {
				return new ModelAndView("Begin.jsp", mapToReturn);
			}
		}
		else if (button.equals("pesee")) {
			
			Map<String, Object> mapToReturn = verifyUser(user, 4);
			if (mapToReturn.get("user") != null) {
				mapToReturn.put("straddles", workManager.getWeightingManager());
				return new ModelAndView("Parametrage_pesee.jsp", mapToReturn);
			} else {
				return new ModelAndView("Begin.jsp", mapToReturn);
			}
		}
		else if (button.equals("Actualiser")) {
			if(idList!=null){
				StringTokenizer st=new StringTokenizer(idList, ";");
				ArrayList<String> checks=new ArrayList<String>();
				String s;
				while(st.hasMoreTokens()){
					s=st.nextToken();
					checks.add(s);
				}
				HttpSession session = request.getSession();
				session.setAttribute("checks", checks);
			}
			Map<String, Object> mapToReturn = verifyUser(user);
			if (mapToReturn.get("user") != null) {
				mapToReturn.put("straddles",(workManager.isAllFilter(user)) ? workManager.getStraddleManager() : workManager.getActiveEvents());
				mapToReturn.put("checklistlink",ipAdressCheckListServer);
				return new ModelAndView("Home.jsp", mapToReturn);
			} else {
				return new ModelAndView("Begin.jsp", mapToReturn);
			}

		}// listUsers
		else if (button.equalsIgnoreCase("listUsers")) {
			Map<String, Object> mapToReturn = verifyUser(user, 1);
			if (mapToReturn.get("user") != null) {
				utilisateurs = AccessDb.getUsers();
				mapToReturn.put("utilisateurs", utilisateurs);
				return new ModelAndView("listUsers.jsp", mapToReturn);
			} else {
				return new ModelAndView("Begin.jsp", mapToReturn);
			}

		} else if (button.equalsIgnoreCase("deconnexion")
				|| button.equalsIgnoreCase("zzzdeconnexion")) {

			// Suppression de l'utilisateur de la map et sa d�connexion, envoie
			// vers begin.jsp avec message vous �tes bien d�connect�s
			if (logedUsers.containsKey(user)) {
				// suppression de la map
				logedUsers.remove(user);
				if (button.equalsIgnoreCase("deconnexion"))
					return new ModelAndView("Begin.jsp", "message",
							"Utilisateur " + user + " d�connect� avec succ�s");
				else
					return new ModelAndView("Begin.jsp");
			}
		} else if (button.equalsIgnoreCase("accueil")) {
			Map<String, Object> mapToReturn = verifyUser(user);
			if (mapToReturn.get("user") != null) {
				mapToReturn.put("straddles",(workManager.isAllFilter(user)) ? workManager.getStraddleManager() : workManager.getActiveEvents());
				mapToReturn.put("checklistlink",ipAdressCheckListServer);
				return new ModelAndView("Home.jsp", mapToReturn);
			} else {
				return new ModelAndView("Begin.jsp", mapToReturn);
			}
		}
		return new ModelAndView("Begin.jsp");
	}
	
	
	/**
	 * V�rification de l'existence de l'utilisateur dans la map et dans la base
	 * 
	 * @param user le nom de l'utilisateur
	 * @param roleMax le role maximum qu'il peut avoir, au dessus, il n'est pas autoris� � cette fonctionnalit�
	 * @return une map, qui contient soit le user, soit un message d'erreur
	 */
	private Map<String, Object> verifyUser(String user, int ...roleMax) {	
		Map<String, Object> mapToReturn = new HashMap<String, Object>();
		if(logedUsers.containsKey(user)){			
			int userRole = AccessDb.isUserNoAuth(user);
			if(roleMax == null || roleMax.length <=0){
				mapToReturn.put("user", new UserWeb(user,(userRole == -1)?userRole:logedUsers.get(user)));
				return mapToReturn;
			}else
				if(userRole<=roleMax[0])
					mapToReturn.put("user", new UserWeb(user,(userRole == -1)?userRole:logedUsers.get(user)));
				else
					mapToReturn.put("message", (user==null)?"Veuillez vous connecter SVP ":"Utilisateur "+user+" non autoris�");			
			return mapToReturn;
		}else{
			mapToReturn.put("message", (user==null)?"Veuillez vous connecter SVP ":"Utilisateur "+user+" non reconnu");
			return mapToReturn;
		}
					
	}
	/**
	 * shunt : action qui gere les shunts
	 * s'execute après le clique sur les bouton P,S ou Valider dans le pop-up de shunt
	 * @param button : contient le nom du bouton sur lequel on a cliquÃ© : P,S ou Valider si on choisi de faire un shunt M 
	 * @param straddleCheck : le numero du cavalier concernÃ© par le shunt 
	 * @param newPosition : la position qu'on ecrit Ã  la main dans un shunt M
	 * @return
	 */
	@RequestMapping(value="/views/shunt.do" , method=RequestMethod.POST)
	public ModelAndView shunt(String user, String button,String straddleCheck,String newPosition, String GPSPosition){
		Map<String, Object> mapToReturn = verifyUser(user);
		if(mapToReturn.get("user") == null){
			return new ModelAndView("Begin.jsp", mapToReturn);
		}
		LogClass.ecriture("MyController\\shunt" , ">>Shunt",1);
		if(button.equals("Valider")) button="M";
		if(button.equals("P")){			
			//Vérification de la position GPS
			//Si ok Position GPS ok donc shunt P ok sinon shunt P impossible
			if (workManager.verificationShuntP(straddleCheck)){
				workManager.shunt(button, straddleCheck, GPSPosition,GPSPosition);
			}else{
				mapToReturn.put("straddles",(workManager.isAllFilter(user)) ? workManager.getStraddleManager() : workManager.getActiveEvents());
				mapToReturn.put("checklistlink",ipAdressCheckListServer);
				return new ModelAndView("Home.jsp",mapToReturn);				
			}
			
		}else{
			if(button.equals("S")){
				workManager.shunt(button, straddleCheck,null,GPSPosition);
			}else if(button.equals("M")) {
				workManager.shunt(button, straddleCheck,newPosition,GPSPosition);
			}				
		}
		mapToReturn.put("straddles",(workManager.isAllFilter(user)) ? workManager.getStraddleManager() : workManager.getActiveEvents());
		mapToReturn.put("checklistlink",ipAdressCheckListServer);
			return new ModelAndView("Home.jsp",mapToReturn);
			
	}
	
	/**
	 * Shunt P avant step mission (effectue un shunt P anticip� pour le mouvement en cours)
	 * @param user
	 * @param straddleCheck
	 * @param shuntPA
	 * @return
	 */
	@RequestMapping("/views/shuntPA.do")
	public ModelAndView shuntPA(String user, String straddleCheck, String shuntPA){
		Map<String, Object> mapToReturn = verifyUser(user);
		if(mapToReturn.get("user") == null)
			return new ModelAndView("Begin.jsp",mapToReturn);
		
		if(shuntPA.equalsIgnoreCase("true"))
			workManager.getStraddleManager().getByNum(straddleCheck).setShuntP_Before(true);
		else
			workManager.getStraddleManager().getByNum(straddleCheck).setShuntP_Before(false);
		mapToReturn.put("straddles",(workManager.isAllFilter(user)) ? workManager.getStraddleManager() : workManager.getActiveEvents());
		mapToReturn.put("checklistlink",ipAdressCheckListServer);
		return new ModelAndView("Home.jsp", mapToReturn);
		
	}
	
	/**
	 * autoShunt : action pour autoShunter un controlleur
	 * s'execute aprÃ¨s le clique sur un bouton autoShunt
	 * @param straddleNum : le numero du cavalier concernÃ© par l'autoshunt 
	 * @return
	 */
	@RequestMapping("/views/autoShunt.do")
	public ModelAndView autoShunt(String user, String straddleNum){
		//LogClass.ecriture("MyController\\autoshunt" , ">>autoshunt " + straddleNum,1);
		Map<String, Object> mapToReturn = verifyUser(user);
		if(mapToReturn.get("user") == null)
			return new ModelAndView("Begin.jsp",mapToReturn);
		
		workManager.setAutoShunt(straddleNum);
		mapToReturn.put("straddles",(workManager.isAllFilter(user)) ? workManager.getStraddleManager() : workManager.getActiveEvents());
		mapToReturn.put("checklistlink",ipAdressCheckListServer);
		return new ModelAndView("Home.jsp", mapToReturn);
		
		
	}
	
	/**
	 * errorSubmit : confirmation de la l'ecture du message d'erreur
	 * s'execute aprÃ¨s le clique sur le bouton "OK" dans le pop-up d'erreur
	 * @param straddleCheck : le numero du cavalier concernÃ© par l'erreur
	 * @return
	 */
	@RequestMapping("/views/errorSubmit.do")
	public ModelAndView errorSubmit(String user, String straddleCheck,String errorMsgCheck){
		//LogClass.ecriture("MyController\\errorSubmit" , ">>Error",1);
		Map<String, Object> mapToReturn = verifyUser(user);
		if(mapToReturn.get("user") == null)
			return new ModelAndView("Begin.jsp",mapToReturn);
		workManager.setStradError(straddleCheck, new Error());
		mapToReturn.put("straddles",(workManager.isAllFilter(user)) ? workManager.getStraddleManager() : workManager.getActiveEvents());
		mapToReturn.put("checklistlink",ipAdressCheckListServer);
		return new ModelAndView("Home.jsp", mapToReturn);
		
	}
	/**
	 * l'action de recherche // a faire
	 * @param submit
	 * @param num
	 * @param shunt
	 * @param stepMission
	 * @param date
	 * @return
	 * @throws ParseException 
	 */
	@RequestMapping(value="/views/recherche.do",method=RequestMethod.POST)
	public ModelAndView recherche(String user, String StartDate, String EndDate,String StartHour,String EndHour, String mobile, String typeMsg, String peseeDoute, String peseeTOS, String emplacement,
			String conteneur, String shunt ,String msg) throws ParseException{
		Map<String, Object> mapToReturn = verifyUser(user);
		if(mapToReturn.get("user") == null)
			return new ModelAndView("Begin.jsp",mapToReturn);
		int shunty=0,typey=0,peseeDout=0,peseeTOSS=0;
		if(shunt!=null){
			shunty=Integer.parseInt(shunt);
		}
		typey=Integer.parseInt(typeMsg);
		peseeDout = Integer.parseInt(peseeDoute);
		peseeTOSS = Integer.parseInt(peseeTOS);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		Date dateD = formatter.parse(StartDate + " " + StartHour + ".000");
		Date dateF = formatter.parse(EndDate + " " + EndHour  + ".000");
		SimpleDateFormat formatter2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");
		ArrayList<String[]> ar = null;
		if(typey == 3){
			ar = AccessDb.SelectPeseeMessage(formatter2.format(dateD), formatter2.format(dateF), mobile, conteneur, peseeDout, peseeTOSS);
			mapToReturn.put("type", "pesee");
		}
			
		else{
			ar = AccessDb.SelectLogMessage(formatter2.format(dateD), formatter2.format(dateF), mobile, typey, emplacement, conteneur, shunty);
			mapToReturn.put("type", "resultActivity");
		}
		
		//ArrayList<String[]> ar = AccessDb.SelectLogMessage();
		
		mapToReturn.put("result", ar);
		return new ModelAndView("RechercheResult.jsp",mapToReturn);
	}
	

	
	/**
	 *action de redirection vers la page d'accueil Home.jsp 
	 *s'execute aprÃ¨s le clique sur le bouton "retour"
	 * @return
	 */
	@RequestMapping("/views/retour.do")
	public ModelAndView retour(){
		//LogClass.ecriture("MyController\\retour" , ">>Retour",1);
		return new ModelAndView("Begin.jsp");
	}
	
	////////////////////////filtre///////////////////////////////////////////////////////
	/**
	 * l'action pour filtrer l'affichage
	 * @param filtre : le type de filtre soit :tout ou par evenement
	 * @return modelandview
	 */
	@RequestMapping(value="/views/filtre.do", method=RequestMethod.POST)
	public ModelAndView flitrer(String user, String filtre){
		Map<String, Object> mapToReturn = verifyUser(user);
		if(mapToReturn.get("user") == null)
			return new ModelAndView("Begin.jsp",mapToReturn);
		//LogClass.ecriture("MyController\\filtre" , ">>Filtre",1);
		workManager.setStradFilter(user,filtre);
		mapToReturn.put("straddles",(workManager.isAllFilter(user)) ? workManager.getStraddleManager() : workManager.getActiveEvents());
		mapToReturn.put("checklistlink",ipAdressCheckListServer);
		return new ModelAndView("Home.jsp", mapToReturn);
	}
	/**
	 * l'action qui vide l'attribut msgNotStraddle après l'affichage de l'erreur dans la page home.jsp 
	 * @return
	 */
	@RequestMapping("/views/msgNotStraddle.do")
	public ModelAndView msgNotStraddle(String user){
		//LogClass.ecriture("MyController\\msgNotStraddle" , ">>Filtre",1);
		Map<String, Object> mapToReturn = verifyUser(user);
		if(mapToReturn.get("user") == null)
			return new ModelAndView("Begin.jsp",mapToReturn);
		workManager.getStraddleManager().setMsgNotStraddle("");
		mapToReturn.put("straddles",(workManager.isAllFilter(user)) ? workManager.getStraddleManager() : workManager.getActiveEvents());
		mapToReturn.put("checklistlink",ipAdressCheckListServer);
		return new ModelAndView("Home.jsp", mapToReturn);
	}
	
	
	
	/**
	 * l'action qui s'execute quand on clique sur le bouton login
	 * elle initialise le WorkManager
	 * @param button
	 * @return
	 */
	@RequestMapping("/views/begin.do")
	public ModelAndView Begin(String button, String username, String password){
		LogClass.ecriture("MyController\\begin" , ">>Begin",1);
		//LanceInterface("10.110.150.190", "13101", "ClientECN4", "1000", "60","13230", "ServeurStraddle", "1000","10.120.2.77:9080");
		chargementParameters();
		LanceInterface(this.interfaceRefresh,this.ipAdressClient, this.portComClient, this.nameThreadClient, this.delaiReconnexionClient, this.nombreConnexionServer,this.portServer, this.nameThreadServer, this.delaiReconnexionServer,this.ipAdressWebServiceAuth,MyController.ipAdressWebServiceShip, this.portNumberWeight);
		//LanceInterface(6,"192.168.10.163", "13101", "ClientECN4", "1000", "60","13230", "ServeurStraddle", "1000","10.120.1.51:9080","10.120.2.89:9080");
		//LanceInterface("192.168.10.162", "13101", "ClientECN4", "1000", "60","13230", "ServeurStraddle", "1000","10.120.1.51:9080");
		//LanceInterface("10.110.160.150", "13101", "ClientECN4", "1000", "60","13230", "ServeurStraddle", "1000","10.120.1.51:9080");
		
		if(button == null)
			return new ModelAndView("Begin.jsp");
		if(button.equalsIgnoreCase("Log In")){
			int role = AccessDb.isUser(username, password);
			int exist = -1;
			if(debug || role == 0 || role ==4){
				if(role == 0 && password.equals("ccevbe"))
					exist = 1;
				else
					if(role == 4 && password.equals("citron"))
						exist = 1;
			}
				
			else{
				exist = Authentication.authentication(username, password,ipAdressWebServiceAuth);
			}
				
			if(debug || exist != -1){			
				logedUsers.put(username, role);
				Map<String, Object> mapToReturn = new HashMap<String, Object>();
				mapToReturn.put("user", new UserWeb(username,role));
				mapToReturn.put("straddles", workManager.getStraddleManager());
				mapToReturn.put("checklistlink",ipAdressCheckListServer);
				ModelAndView toto = new ModelAndView("Home.jsp",mapToReturn);				
				return toto;
			}else{
				//affichage erreur
				return new ModelAndView("Begin.jsp");
			}
			
		}
		else return new ModelAndView("Simulator.jsp","straddles",workManager.getStraddleManager());
	}
	
	private void chargementParameters() {
		final Properties prop = new Properties();
		InputStream input = null;

		try {
			
			
			input = new FileInputStream("./interfaceProperties_dont_remove/controller.properties");

			// load a properties file
			prop.load(input);
			this.sdkVersion = Float.valueOf(prop.getProperty("interface.sdkversion"));
			this.ipAdressClient = prop.getProperty("interface.ipAdressClient");
			this.portComClient = prop.getProperty("interface.portComClient");
			this.nameThreadClient = prop.getProperty("interface.nameThreadClient");
			this.delaiReconnexionClient = prop.getProperty("interface.delaiReconnexionClient");
			this.nombreConnexionServer = prop.getProperty("interface.nombreConnexionServer");
			this.portServer = prop.getProperty("interface.portServer");
			this.nameThreadServer = prop.getProperty("interface.nameThreadServer");
			this.delaiReconnexionServer = prop.getProperty("interface.delaiReconnexionServer");
			this.ipAdressWebServiceAuth = prop.getProperty("interface.ipAdressWebServiceAuth");
			MyController.ipAdressWebServiceShip = prop.getProperty("interface.ipAdressWebServiceShip");
			MyController.ipAdressAnticollisionServer = prop.getProperty("interface.ipAdressAnticollisionServer");
			MyController.ipAdressCheckListServer = prop.getProperty("interface.ipAdressCheckListServer");
			MyController.interfaceRefresh = Integer.valueOf(prop.getProperty("interface.refresh"));
			MyController.debug = Boolean.valueOf(prop.getProperty("interface.debug"));
			MyController.vesselNameService = Boolean.valueOf(prop.getProperty("interface.vesselName"));
			MyController.navisValidation = Boolean.valueOf(prop.getProperty("interface.navisValidation"));
			LogClass.ecriture("MyController","navisvalidation = "+MyController.navisValidation,1);
			//System.out.println("navisvalidation = "+MyController.navisValidation);
			
			MyController.checkListUp = Boolean.valueOf(prop.getProperty("interface.checkList"));
			
			this.portNumberWeight = prop.getProperty("interface.portNumberWeight");
			
			MyController.ipAddressWebServiceCheWeightRequest = prop.getProperty("interface.IpAdressWebServiceCheWeightRequest");
			LogClass.ecriture("MyController","ipAddressWebServiceCheWeightRequest "+ipAddressWebServiceCheWeightRequest,1);
			//System.out.println("ipAddressWebServiceCheWeightRequest "+ipAddressWebServiceCheWeightRequest);
			MyController.ipAddressWebServiceCheWeightInsert = prop.getProperty("interface.IpAdressWebServiceCheWeightInsert");
			LogClass.ecriture("MyController","ipAddressWebServiceCheWeightInsert "+ipAddressWebServiceCheWeightInsert,1);
			//System.out.println("ipAddressWebServiceCheWeightInsert "+ipAddressWebServiceCheWeightInsert);
			LogClass.ecriture("MyController","checklist server = "+ipAdressCheckListServer,1);
			//System.out.println("checklist server = "+ipAdressCheckListServer);
			MyController.vgmUser = prop.getProperty("interface.usernameVGM");
			MyController.vgmPassword = prop.getProperty("interface.passwordVGM");
			// get the property value and print it out
			MyController.raz_frequence = prop.getProperty("interface.raz_freq");
			

		} catch (final IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (final IOException e) {
					e.printStackTrace();
				}
			}
		}		
	}
	@RequestMapping(value="/views/addUser.do", method=RequestMethod.POST)
	public ModelAndView addUser(String user, String nom, String prenom, String identifiant, String role){
		Map<String, Object> mapToReturn = verifyUser(user,1);
		if(mapToReturn.get("user") == null)
			return new ModelAndView("Begin.jsp",mapToReturn);
		if(nom !="" && prenom != "" && identifiant != "" && role != ""){
			if(AccessDb.addUser(nom, prenom, identifiant, role)){
				mapToReturn.put("message","Utilisateur cr�� avec succ�s");
				return new ModelAndView("addUser.jsp", mapToReturn);
			}
			else{
				mapToReturn.put("message","Erreur lors de la cr�ation de l'utilisateur");
				return new ModelAndView("addUser.jsp", mapToReturn);
			}
		}else{
			mapToReturn.put("message","Erreur renseignez les champs SVP");
			return new ModelAndView("addUser.jsp", mapToReturn);
		}
	}
	
	@RequestMapping("/views/updateUser.do")
	public ModelAndView updateUser(String user, String[] nom, String[] prenom, String[] identifiant, String[] role){
		Map<String, Object> mapToReturn = verifyUser(user,1);
		if(mapToReturn.get("user") == null)
			return new ModelAndView("Begin.jsp",mapToReturn);
		
		List<Utilisateur> listeErr = new ArrayList<Utilisateur>();
		boolean success = true;
		int taille = nom.length;
		for(int i=0;i<taille;i++){
			if(AccessDb.updateUser(nom[i], prenom[i], identifiant[i], role[i])){
				// Rien tout va bien user modifi�
			}else{
				success = false;
				listeErr.add(new Utilisateur(nom[i], prenom[i], identifiant[i], role[i]));				
			}
			
		}
		if(success){
			
			mapToReturn.put("message","Utilisateur cr�� avec succ�s");
			utilisateurs = AccessDb.getUsers();
			mapToReturn.put("utilisateurs", utilisateurs);
			return new ModelAndView("listUsers.jsp", mapToReturn);
		}else{
			
			mapToReturn.put("utilisateurs",listeErr);
			return new ModelAndView("updateUser.jsp", mapToReturn);
		}

	}
	
	@RequestMapping("/views/deleteUser.do")
	public ModelAndView deleteUser(String user, String[] nom, String[] prenom, String[] identifiant, String[] role){
		
		Map<String, Object> mapToReturn = verifyUser(user,1);
		if(mapToReturn.get("user") == null)
			return new ModelAndView("Begin.jsp",mapToReturn);
		int taille = nom.length;
		List<Utilisateur> listeErr = new ArrayList<Utilisateur>();
		boolean success = true;
		for(int i=0;i<taille;i++){
			if(AccessDb.deleteUser(identifiant[i])){
				// Rien a faire
			}else{
				success = false;
				listeErr.add(new Utilisateur(nom[i], prenom[i], identifiant[i], role[i]));				
			}
		}
		if(success){
			
			mapToReturn.put("message","Utilisateur cr�� avec succ�s");
			utilisateurs = AccessDb.getUsers();
			mapToReturn.put("utilisateurs", utilisateurs);
			return new ModelAndView("listUsers.jsp", mapToReturn);
		}else{

			mapToReturn.put("message","Erreur de suppression consulter les utilisateurs ci dessous");
			mapToReturn.put("utilisateurs",listeErr);
			return new ModelAndView("deleteUser.jsp", mapToReturn);
		}
		
		
	}
	
	@RequestMapping(value="/views/listUsers.do", method = RequestMethod.POST)
	public ModelAndView listUsers(String user, String button, String... checkbox){
		Map<String, Object> mapToReturn = verifyUser(user,1);
		if(mapToReturn.get("user") == null)
			return new ModelAndView("Begin.jsp",mapToReturn);
		if(button == null || button.equalsIgnoreCase("")){
			// Premier affichage -> lister les utilisateurs
			this.utilisateurs = AccessDb.getUsers();
		}
		if(button.equalsIgnoreCase("Modifier")){
			// modification d'un ou plusieurs utilisateurs
			List<Utilisateur> liste = null;
			if(checkbox != null && checkbox.length>0){
				liste = AccessDb.getUsers(checkbox);
				mapToReturn.put("utilisateurs",liste);
				return new ModelAndView("updateUser.jsp", mapToReturn);
			}else{
				mapToReturn.put("utilisateurs",utilisateurs);
				return new ModelAndView("listUsers.jsp", mapToReturn);
			}			
			
		}else
			if(button.equalsIgnoreCase("Supprimer")){
				// Suppression d'un ou plusieurs utilisateur
				List<Utilisateur> liste = null;
				if(checkbox != null && checkbox.length>0){
					liste = AccessDb.getUsers(checkbox);
					mapToReturn.put("utilisateurs",liste);
					return new ModelAndView("deleteUser.jsp", mapToReturn);
				}else{
					mapToReturn.put("utilisateurs",utilisateurs);
					return new ModelAndView("listUsers.jsp", mapToReturn);
				}	
				
			}else
				if(button.equalsIgnoreCase("Ajouter")){
					mapToReturn.put("message","");
					return new ModelAndView("addUser.jsp", mapToReturn);
					// Ajout d'un nouvel utilisateur
				}
		mapToReturn.put("message","Erreur renseignez les champs SVP");
		return new ModelAndView("addUser.jsp", mapToReturn);
	}
	
	/**
	 * l'action qui s'execute quand on clique sur le bouton login
	 * elle initialise le WorkManager
	 * @param button
	 * @return
	 */
	@RequestMapping("/views/startinterface.do")
	public ModelAndView StartInterface(String refreshfreq, String debug, String vesselName, String detectionRow){
		//LogClass.ecriture("MyController\\start" , ">>Start" + ipAdressClient,1);
		ArrayList<Object> ar = new ArrayList<Object>();
		if(Integer.valueOf(refreshfreq) > 0){
			if(workManager != null && workManager.getStraddleManager() != null)
				workManager.getStraddleManager().setRefresh(Integer.valueOf(refreshfreq));
		}
		if(debug != null && debug.equals("1"))
			MyController.debug = true;
		else
			MyController.debug = false;
		if(vesselName != null && vesselName.equals("1"))
			MyController.vesselNameService  = true;
		else
			MyController.vesselNameService = false;
		if(detectionRow != null && detectionRow.equals("1"))
			MyController.isDetectionActive  = true;
		else
			MyController.isDetectionActive = false;
		ar.add(LanceInterface(Integer.valueOf(refreshfreq), ipAdressClient,portComClient,nameThreadClient,delaiReconnexionClient,
				nombreConnexionServer, portServer, nameThreadServer,delaiReconnexionServer,ipAdressWebServiceAuth, ipAdressWebServiceShip, portNumberWeight));
		
		ar.add(workManager.getStraddleManager());
		return new ModelAndView("Message.jsp", "message",ar);
	}
	
	/**
	 * Permet de lancer l'interface
	 */
	private String LanceInterface(int refresh, String ipAdressClient,String portComClient, String nameThreadClient, String delaiReconnexionClient,
			String nombreConnexionServer, String portServer, String nameThreadServer, String delaiReconnexionServer,String ipAdressWebService, String ipAdressWebServiceShip, String portNumberWeight){
		try {
			if(workManager==null){
				workManager = new WorkManagerStart(refresh, ipAdressClient, Integer.parseInt(portComClient), nameThreadClient, true, 255,
						Integer.parseInt(delaiReconnexionClient), Integer.parseInt(portServer), Integer.parseInt(nombreConnexionServer),
						nameThreadServer, false,3, Integer.parseInt(delaiReconnexionServer), Integer.parseInt(portNumberWeight));
				this.ipAdressWebServiceAuth=ipAdressWebService;
				MyController.ipAdressWebServiceShip  = ipAdressWebServiceShip;
				LogClass.ecriture("MyController","Interface lanc�e",1);
				//System.out.println("Interface lanc�e");
				return "Interface lanc�e";
			}else {
				if(refresh > 0){
					workManager.getStraddleManager().setRefresh(refresh);
				}
				LogClass.ecriture("MyController","Interface d�j�lanc�e. refresh et/ou vesselService et/ou detection ligne actualis�",1);
				//System.out.println("Interface d�j�lanc�e. refresh et/ou vesselService et/ou detection ligne actualis�");
				return "Interface d�j�lanc�e. refresh et/ou vesselService et/ou detection ligne actualis�";
			}
		} catch (Exception e) {
			LogClass.ecriture("MyController","Probl�me au lancement : " + e.getStackTrace(),1);
			//System.out.println("Probl�me au lancement : " + e.getStackTrace());
			e.printStackTrace();
			return "Probl�me au lancement : " + e.getMessage();
		}
	}
	
	/**
	 * l'action de parametrage de l'auto shunt
	 * @param straddleNames
	 * @return
	 */
	@RequestMapping("/views/parametrage.do")
	public ModelAndView parametrage(String[] checkbox, String user){
		Map<String, Object> mapToReturn = verifyUser(user,2);
		if(mapToReturn.get("user") == null)
			return new ModelAndView("Begin.jsp",mapToReturn);
		
		workManager.getStraddleManager().majAutoShunt(checkbox);
		mapToReturn.put("straddles",workManager.getStraddleManager());
		return new ModelAndView("Parametrage.jsp",mapToReturn);			
	}
	//parametrage_checkList.do
	@RequestMapping("/views/parametrage_checkList.do")
	public ModelAndView parametrage_checkList(String[] checkbox, String user){
		Map<String, Object> mapToReturn = verifyUser(user,2);
		if(mapToReturn.get("user") == null)
			return new ModelAndView("Begin.jsp",mapToReturn);
		
		workManager.getStraddleManager().majCheckList(checkbox);
		mapToReturn.put("straddles",workManager.getStraddleManager());
		return new ModelAndView("Parametrage_checkList.jsp",mapToReturn);			
	}
	
	//parametrage_checkList.do
	@RequestMapping("/views/parametrage_pesee.do")
	public ModelAndView parametrage_pesee(String[] checkbox, String [] checkboxRAZ, String user, String[] ipPeson){
		Map<String, Object> mapToReturn = verifyUser(user,2);
		if(mapToReturn.get("user") == null)
			return new ModelAndView("Begin.jsp",mapToReturn);
		
		workManager.getWeightingManager().majPesee(checkbox, checkboxRAZ, ipPeson);
		mapToReturn.put("straddles",workManager.getWeightingManager());
		mapToReturn.put("message","");
		return new ModelAndView("Parametrage_pesee.jsp",mapToReturn);			
	}
	
	/**
	 * renvoie la mission en cours sur le straddle concern� 
	 * @param 
	 * @return
	 */
	@RequestMapping("/views/renvoi.do")
	public ModelAndView sendMission(String straddleCheck, String user){
		
		Map<String, Object> mapToReturn = verifyUser(user);
		if(mapToReturn.get("user") == null)
			return new ModelAndView("Begin.jsp",mapToReturn);
		
		//Je renvoie la mission en cours sur le straddle concern� sans v�rification de la carte d'identification		
		workManager.renvoieMission(straddleCheck);
		
		mapToReturn.put("straddles",(workManager.isAllFilter(user)) ? workManager.getStraddleManager() : workManager.getActiveEvents());
		mapToReturn.put("checklistlink",ipAdressCheckListServer);
		return new ModelAndView("Home.jsp",mapToReturn);
		
			
	}
	@RequestMapping("/views/majWeight.do")
	public ModelAndView majWeight(){
		Map<String, String> toReturn = new HashMap<String, String>();
		MAJWeights maj = new MAJWeights();
		toReturn.put("message", "MAJ avec succes");
		return new ModelAndView("loadWeightsFred.jsp", toReturn);		
	}
	@RequestMapping("/views/raz_peson.do")
	public ModelAndView razPeson(String userName, String nameStraddle){
		
		Map<String, Object> mapToReturn = verifyUser(userName,2);
		if(mapToReturn.get("user") == null)
			return new ModelAndView("Begin.jsp",mapToReturn);
		
		mapToReturn.put("straddles",workManager.getWeightingManager());
		boolean message = workManager.razPeson(nameStraddle);
		if(message)
			mapToReturn.put("message","RAZ du "+nameStraddle+" envoy� au straddle");
		else
			mapToReturn.put("message","RAZ du "+nameStraddle+" a �chou�, doit �tre �teint ou d�connect�");
		return new ModelAndView("Parametrage_pesee.jsp",mapToReturn);		
	}
	public static boolean isVesselNameOK() {
		return MyController.vesselNameService;
	}

	public static boolean isDetectionRowActive() {
		return MyController.isDetectionActive;
	}
	public static String getAntiCollisionServerAddress() {
		//System.err.println("getAntiCollisionServerAddressIP ipcomplete = "+serverAnticollisionAddress);
		String[] ip = ipAdressAnticollisionServer.split(":");
		//System.out.println("getAntiCollisionServerAddressIP "+ip[0]);
		return ip[0];
	}
	public static void setCheckListUp(boolean updown) {
		MyController.checkListUp = updown;
	}
	public static String getIpAddressWebServiceCheWeightRequest() {
		if(isNavisValidation()){
			return "35.156.242.90:10080";
		}
		return MyController.ipAddressWebServiceCheWeightRequest;
	}
	public static String getIpAddressWebServiceCheWeightInsert() {
		if(isNavisValidation()){
			return "35.156.242.90:10080";
		}
		return MyController.ipAddressWebServiceCheWeightInsert;
	}
	public static String getVGMUser() {	
		return "aptapi";
	}
	public static String getVGMPassword() {		
		return "password";
	}
	public static String getRAZFrequence(){
		if(MyController.raz_frequence == null)
			return "";
		return ";"+MyController.raz_frequence;
	}

	public static boolean isNavisValidation() {
		return MyController.navisValidation;
	}
}
