/*package info.mgps.isoloc.interfaceGPS.web.web.Controllers;

import info.mgps.isoloc.interfaceGPS.service.log.AccessDb;
import info.mgps.isoloc.interfaceGPS.web.web.Controllers.MyController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.tomcat.util.codec.binary.Base64;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import sun.net.www.protocol.http.HttpURLConnection;


@SuppressWarnings("restriction")
public class GetCheWeightThread extends Thread{
	private List<String> keys;
	private List<String> values;
	private String containerID;
	private int cheWeight;
	private String straddleName;
	private int jobWeight;
	
	
	*//**
	 * Constructeur avec le num�ro de visite
	 * @param containerID
	 *//*
	public GetCheWeightThread(String containerID, int cheWeight, String straddleName, int jobWeight){
		this.setName("GetCheWaitingThread");
		System.err.println("getName "+getName());
		System.out.println("mes couilles");
		
		this.containerID = containerID;
		this.cheWeight = cheWeight;
		this.straddleName = straddleName;
		this.jobWeight = jobWeight;
		this.keys = new ArrayList<String>();
		this.keys.add("filtername");
		this.keys.add("PARM_CTRID");
		this.keys.add("operatorId");
		this.keys.add("complexId");
		this.keys.add("facilityId");
		this.keys.add("yardId");
		this.values = new ArrayList<String>();
		this.values.add("CHEWEIGHT");
		this.values.add(vesselVisit);
		this.values.add("CHEWEIGHT");
		this.values.add(this.containerID);
		this.values.add("EUROFOS");
		this.values.add("FRFOS");
		this.values.add("TDM");
		this.values.add("TDM");
		System.out.println("la fin de creation du GETCHEWeightThread");
		
	}
	




	*//**
	 * Lecture et extraction du vesselname depuis la r�ponse xml
	 * @param message
	 * @return
	 *//*
	private static String read(String message){
		// Lecture du message xml de r�ponse
		String retour= "";
		if(message.contains("<data-table filter=\"CHEWEIGHT\" count=\"0\">")){
			// pas de correspondance
			return "";			
		}else{
			final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder;
			try {
				builder = factory.newDocumentBuilder();
				InputSource s = new InputSource(new StringReader(message)); 
				final Document document= builder.parse(s);
				final Element racine = document.getDocumentElement();
				System.out.println("   datatable : " + racine.getAttributeNode("rows"));
				NodeList fields = racine.getElementsByTagName("field");
								
				System.out.println("length "+fields.item(0).getFirstChild().getTextContent());
				System.out.println("length "+fields.item(8).getFirstChild().getTextContent());
				
				String ctnrID = fields.item(0).getFirstChild().getTextContent();
				String cheWeight = fields.item(8).getFirstChild().getTextContent();
				retour = ctnrID+":"+cheWeight;;
		 
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			} catch (SAXException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			
		}
		return retour;
		
	}

	
	*//**
	 * r�cup�ration de la r�ponse de l'url Navis
	 * @param adress l'url de base
	 * @param keys liste des noms des param�tres
	 * @param values liste des valeurs de ces param�tres
	 * @return le nom du Vessel
	 * @throws IOException
	 *//*
	public static String doGet(String adress,List<String> keys,List<String> values) throws IOException{
		String result = "";
		BufferedReader reader = null;
		
			// encodage des param�tres de la requ�te
			String data = "";
			for (int i = 0; i < keys.size(); i++) {
				if (i != 0)
					data += "&";
				data += URLEncoder.encode(keys.get(i), "UTF-8") + "="
						+ URLEncoder.encode(values.get(i), "UTF-8");
			}
			// cr�ation de la connection
			URL url = new URL(adress+"?"+data);
			String userPassword = "aptapi:password";
			String encoding = new String(new Base64().encode(userPassword
					.getBytes()));
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			// add reuqest header
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Content-Type", "text/xml");
			conn.setRequestProperty("charset", "UTF-8");
			conn.setRequestProperty("Authorization", "Basic " + encoding);
			conn.setDoOutput(true);
			// envoi de la requ�te
			
			
			// lecture de la r�ponse
			reader = new BufferedReader(new InputStreamReader(
					(InputStream) conn.getContent()));
			String ligne;
			while ((ligne = reader.readLine()) != null) {
				result += ligne;
			}
		 System.err.println("result universal query "+result);
		
		return read(result);
	}


	@Override
	public void start() {
		System.out.println("doget "+"http://"+MyController.getIpAddressWebServiceCheWeightRequest()+"/apex/api//query");
		//super.start();
		try {
			String result = doGet("http://"+MyController.getIpAddressWebServiceCheWeightRequest()+"/apex/api//query",keys, values);
			if(result != null){
				String[] res = result.split(":");
				if(res[1] == null || Integer.valueOf(res[1]) == 0){
					// ecriture log dans la base
					//// date, cavalier, conteneur, poids mission, poids constat�, etat (reussi, echec, doute), ecriture TOS ou non
					AccessDb.writeVGMInfos(new Date(), this.straddleName, this.containerID, this.jobWeight, this.cheWeight,1,false);
				}else{
					// ecriture succes dans le TOS et la base
					VGM.insertVGM(MyController.getVGMUser(), MyController.getVGMPassword(), this.containerID, this.cheWeight, MyController.getIpAddressWebServiceCheWeightInsert());
					AccessDb.writeVGMInfos(new Date(), this.straddleName,  this.containerID, this.jobWeight, this.cheWeight,1,true);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}





	


}
*/