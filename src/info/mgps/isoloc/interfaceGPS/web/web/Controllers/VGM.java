package info.mgps.isoloc.interfaceGPS.web.web.Controllers;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;

import com.navis.services.argobasicservice.ArgobasicService;
import com.navis.services.argobasicservice.ArgobasicServicePort;
import com.sun.xml.internal.messaging.saaj.util.Base64;

import info.mgps.isoloc.interfaceGPS.utilitaire.LogClass;

@SuppressWarnings("restriction")
public class VGM {

	public static int insertVGMTOS(String user, String password, String straddleID, String containerId, float cheWeight, String IpAdressWebService, String dateInsert){
		LogClass.ecriture("VGM","insert VGM "+containerId+" poids "+cheWeight+" ipaddress "+IpAdressWebService,1);
		//System.out.println("insert VGM "+containerId+" poids "+cheWeight+" ipaddress "+IpAdressWebService);
		try{
			if(user == null || user.equals(""))
				return -1;			
			ArgobasicService argoService = new ArgobasicService(new URL("http://"+IpAdressWebService +"/apex/services/argobasicservice?wsdl"));
			LogClass.ecriture("VGM","user "+user+" pass "+password,1);
			//System.out.println("user "+user+" pass "+password);
			ArgobasicServicePort argoServicePort = argoService.getArgobasicServicePort();
			Map<String,List<String>>headers = new HashMap<String,List<String>>(); 			
			byte[]   bytesEncoded = Base64.encode((user+":"+password).getBytes());
			//System.out.println("ecncoded value is " + "Basic "+new String(bytesEncoded));
			headers.put("Authorization",Collections.singletonList("Basic "+new String(bytesEncoded ))); 
			BindingProvider bp = ((BindingProvider) argoServicePort); 
			bp.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
			
			String response = argoServicePort.basicInvoke("EUROFOS/FRFOS/TDM/TDM", "<icu><units>"
					+ "<unit-identity id=\""+containerId+"\" type=\"CONTAINERIZED\"/>"
					+ "</units><properties>"
					+ "<property tag=\"GrossWeightKgYardMeasured\" "
					+ "value=\""+cheWeight+"\"/>"
					+ "</properties>"
					+ "<event id=\"UNIT_CHE_WEIGHT_UPDATE\" note=\"CHE WEIGHT "+straddleID+"\" time-event-applied=\""+dateInsert+"\" user-id=\""+"MGPS"+"\" />"
					+ "</icu>");
			if(MyController.isNavisValidation()){
				response = argoServicePort.basicInvoke("GEN/STRESS/TEST/GEN", "<icu><units>"
						+ "<unit-identity id=\""+containerId+"\" type=\"CONTAINERIZED\"/>"
						+ "</units><properties>"
						+ "<property tag=\"GrossWeightKgYardMeasured\" "
						+ "value=\""+cheWeight+"\"/>"
						+ "</properties>"
						+ "<event id=\"UNIT_CHE_WEIGHT_UPDATE\" note=\"CHE WEIGHT "+straddleID+"\" time-event-applied=\""+dateInsert+"\" user-id=\""+"MGPS"+"\" />"
						+ "</icu>");
			}
			LogClass.ecriture("VGM","response = "+response,1);
			//System.out.println("response = "+response);
			if(response.contains("<argo-response status=\"0\" status-id=\"OK\">")){
				//2016-06-01T11:23:24
				return 1;
			}else
				return -1;
		}catch(javax.xml.ws.soap.SOAPFaultException | MalformedURLException e){
			e.printStackTrace();
			return -1;// probl�me d'authentification
		}
	}
}
