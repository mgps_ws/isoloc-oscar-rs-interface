package info.mgps.isoloc.interfaceGPS.web.web.Controllers;

import java.util.List;

import info.mgps.isoloc.interfaceGPS.service.log.AccessDb;

public class MAJWeights {

	public MAJWeights(){
		List<String []> add = getWeights();
		//System.out.println(add.size());
		majNavisWeights(add);
	}
	
	private void majNavisWeights(List<String[]> weights) {
		/*for(int i=0;i<10;i++){
			ExecutionVGM.majWeightTOS(weights.get(i)[1], Float.valueOf(weights.get(i)[2]), "", 0, Integer.valueOf(weights.get(i)[0]));	
		}*/
		//ExecutionVGM.majWeightTOS(weights.get(0)[1], Float.valueOf(weights.get(0)[2]), "", 0, Integer.valueOf(weights.get(0)[0]));	
		for (int i=0;i<weights.size();i++){
			ExecutionVGM.majWeightTOS(weights.get(i)[1], Float.valueOf(weights.get(i)[2]), "", 0, Integer.valueOf(weights.get(i)[0]));	
		}
			
	}

	public List<String[]> getWeights(){
		return AccessDb.selectWeightsToUpdateFred();
	}
}
