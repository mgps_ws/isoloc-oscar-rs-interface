package info.mgps.isoloc.interfaceGPS.web.Models;

/**
 * calss de mission: contient toutes les informations qui concerne une mission et qu'on re�oit de navis
 * @author saad
 */

public class ParamsMission {
	private String conteneur="";//le numero du conteneur
	
	private String positionShift="";

	private String position=""; //Position venant de la mission  B0101B
	private String positionCible=""; //Position venant de la mission V:B0101B
	private int DisplayPosition=0;//1=affiche la position ; 0=n'affiche pas la position pour l'affichage dans le cavalier
		
	private String gpsPosition="";//venant du straddle ex: B0101B (last position)
	private String gpsPositionCible=""; // venant du straddle ex: V:B0101B (last cible)
	private String displayMessage=""; // le message a afficher dans le web. Colonne mission
	private String displayMessageTmp=""; // le message a afficher dans le web. Colonne mission message pour la temporisation
	
	private boolean ctrlGPS=false; //  position a controler ou non
	private String jpos=""; // CTR param�tre Navis normalement toujours �gal � CTR
	private String containerLength="";// la longueur du conteneur
	private String dgx ="";	//Notion de dangereux
	private String weight="";//Poids du conteneur		
	private String oog="";//OverDimension
	private boolean toShunt=false;// si cette mission doit etre shunter ou pas
	

	public ParamsMission(String conteneur, String position) {
		raz();
		
		this.conteneur = conteneur;
		this.position = position;
	
	}
	/**
	 * constructeur qui cree une mission vide;
	 */
	public ParamsMission() {
		raz();
	}
	
	public String getConteneur() {
		return conteneur;
	}

	public void setConteneur(String conteneur) {
		this.conteneur = conteneur;
	}

	
	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}
			
	public String getPositionCible() {
		return positionCible;
	}
	
	public void setPositionCible(String positionCible) {
		this.positionCible = positionCible;
	}
	
	public String getGpsPositionCible() {
		return gpsPositionCible;
	}
	
	public void setGpsPositionCible(String gpsPositionCibleDisplay) {
		this.gpsPositionCible = gpsPositionCibleDisplay;
	}
	
	public int getDisplayPosition() {
		return DisplayPosition;
	}
	
	public void setDisplayPosition(int displayPosition) {
		DisplayPosition = displayPosition;
	}
	
	public String getGpsPosition() {
		return gpsPosition;
	}

	public void setGpsPosition(String gpsPosition) {
		this.gpsPosition = gpsPosition;
	}
	public void setDisplayMessage(String displayMessage) {
		this.displayMessage = displayMessage;
	}
	public String getDisplayMessage() {
		return displayMessage;
	}	
	public String getDisplayMessageTmp() {
		return displayMessageTmp;
	}
	public void setDisplayMessageTmp(String displayMessageTmp) {
		this.displayMessageTmp = displayMessageTmp;
	}
	public boolean isCtrlGPS() {
		return ctrlGPS;
	}


	public void setCtrlGPS(boolean ctrlGPS) {
		this.ctrlGPS = ctrlGPS;
	}

	public String toString() {
		return super.toString();//"StepMission [Step=" + step + ", time=" + time + "type" + type + "]";
	}

	/**
	 * Raz des param�tres
	 * 
	 */
	public void raz() {
		this.conteneur="";
		
		this.position= "";
		this.positionCible="";
		
		this.gpsPosition = "";
		this.gpsPositionCible="";
		this.displayMessage = "";
		
		this.positionShift ="";
		
		this.ctrlGPS = false;
		
	}

	public String getPositionShift() {
		return positionShift;
	}

	public void setPositionShift(String positionShift) {
		this.positionShift = positionShift;
	}

	public void setJpos(String jpos) {
		this.jpos = jpos;
	}

	public String getJpos() {
		return jpos;
	}

	public void setContainerLength(String lnth) {
		this.containerLength = lnth;
	}
	
	public String getContainerLength(){
		return this.containerLength;
	}
	
	public String getDgx() {
		return dgx;
	}
	
	public void setDgx(String dgx) {
		this.dgx = dgx;
	}
	
	public String getWeight() {
		return weight;
	}
	
	public void setWeight(String weight) {
		this.weight = weight;
	}
	
	public String getOog() {
		return oog;
	}
	
	public void setOog(String oog) {
		this.oog = oog;
	}
	
	public String getSpos(){
		switch(this.containerLength){
		case "20":
			return "2";
		default://40' et 45'
			return "4";		
		}
	}

	public void setForShunt(boolean b) {
		this.toShunt = b;
	}

	public boolean isToShunt() {
		return this.toShunt;
	}

}
