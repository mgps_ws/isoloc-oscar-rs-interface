package info.mgps.isoloc.interfaceGPS.web.Models;
/**
 * class d'erreur
 * occur est egale � Y quand il y a une erreur et N quand il y en a pas : cet attribut est responsable du changement de la couleur en rouge d'une
 * ligne dans le tableau des cavalier dans "Home.jsp"
 * errorMsg contient le message d'erreur
 * @author saad
 *
 */
public class Error {
	private String occur;
	private String errorMsg;
	
	/**
	 * constructeur avec pamaetres
	 * il cree une erreur selon les parametre qu'on lui attribut
	 * @param occur
	 * @param errorMsg
	 */
	public Error(String occur, String errorMsg) {
		this.occur = occur;
		if(!occur.equals("N"))	this.errorMsg = errorMsg;
	}
	
	/**
	 * constructeur sans parametres
	 * cree une erreur vide: avec occur egale � N et pas de errorMsg
	 */
	public Error() {
		raz();
	}

	public String getOccur() {
		return occur;
	}

	public void setOccur(String occur) {
		this.occur = occur;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public String toString() {
		return "Error [occur=" + occur + ", errorMsg=" + errorMsg + "]";
	}
    /**
     * fonction pour creer une erreur vide
     */
	public void raz() {
		this.occur="N";
		this.errorMsg="";
	}
}
