package info.mgps.isoloc.interfaceGPS.web.Models;

import java.util.Date;

import info.mgps.isoloc.interfaceGPS.service.xmlNavis.VesselNames;


/**
 * la classe du cavalier qui contient toutes les informations sur un.
 * @author saad
 *
 */
public class Straddle {
	
	//************************************* D�clarations des variables ******************************************
	private String name;//nom du cavalier pour l'affichage web
	private String chId; // Chid = name du cavalier dans Navis
	private String nameIP="";//Adresse ip connect� pour le cavalier 
	private String identifiant="";//Identifiant du chauffeur de cavalier
	private boolean tmpBool=false;
	private String tempoMission="";//Permet de temporiser la mission lors d'un arret du straddle ou d'un changement de carte
									//je renvoie oui si tempo active
									//Je stopppe la mission 2 cas : 1)je renvoie la mission 2)Navis me reste la mission
	private String memoMsgStraddle="";//M�morisation du message vers le straddle pour pouvoir lui renvoyer
	
	private int cnxServer;
	
	private ParamsMission mission;//la mission en cours
	private int state=0; //0-> disconnected / 1-> connected unavailable / 2-> connected idle / 3-> Mission en cours  
	//responsable du changement de couleur en gris au cas ou le cavalier n'est pas connect� 
	
	private Error erreur;//contient une erreur 
	private boolean autoShunt=false;// true si le cavalier et en mode autoshunt
	private boolean shuntP_Before = false	;//True permet de shunter le cavalier en P avant l'�v�nement de shunt
	private int actualFormId=0;
	//identifiant integer pour les formes
	//1=form_login ; 				2=form_straddle_unavailable
	//3=form_empty_to_origin ; 		4=form_empty_at_origin
	//5=form_laden_to_dest; 		6=form_laden_at_dest
	//7=form_straddle_idle wait ; 	8=form_straddle_auto_rehandle �tape 1
	//9=form_straddle_auto_rehandle �tape 2;			10=form_straddle_manual_rehandle
	private String posSrc; // la position source dans un mouvement complet servira � cr�er VAParc pour l'instant
	private Date datePosSrc; // la date de la pr�sence � la position source pour le straddle
	


	

	private String areaTypePosSrc; // le type de la position source du mouvement

	
	//************************************* Constructeur ******************************************
	/**
	 * constructeur qui donne une state aleatoire pour simuler des cavalier hors ligne.
	 * @param id
	 * @param num
	 * @param mission
	 */
	public Straddle(String NameStraddle) {
		this.chId = NameStraddle;
		this.name=NameStraddle;
		this.mission= new ParamsMission();
		this.erreur=new Error();
		
	}
	
	//************************************* Ascesseurs ******************************************
	public String getNameIP() {
		return nameIP;
	}

	public void setNameIP(String nameIP) {
		this.nameIP = nameIP;
	}
	
	public String getIdentifiant() {
		return identifiant;
	}

	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}

	public int getActualFormId() {
		return actualFormId;
	}

	public int getCnxServer() {
		return cnxServer;
	}

	public void setCnxServer(int cnxServer) {
		this.cnxServer = cnxServer;
	}

	public void setActualFormId(int actualFormId) {
		if(actualFormId != this.actualFormId)
			setGpsPosition("","");
		if(actualFormId == 7 || actualFormId == 2){
			// FORM_STRADDLE_IDLE
			mission.setPosition("");
			mission.setPositionCible("");
		}
		this.actualFormId = actualFormId;
	}

	public String getGpsPosition() {
		return mission.getGpsPosition();
	}

	public String getGpsPositionDisplay(){
		return mission.getGpsPositionCible();
	}
	
	public void setGpsPosition(String gpsPosition,String gpsPositionCibleDisplay) {
		mission.setGpsPosition(gpsPosition);
		mission.setGpsPositionCible(gpsPositionCibleDisplay);
	}

	public void stepCheck(){
		if(this.state == 0){
			this.mission=new ParamsMission();
		}
	}
	
	public String getNum() {
		return name;
	}

	public void setNum(String num) {
		this.name = num;
	}

	public ParamsMission getMission() {
		return mission;
	}

	public void setMission(ParamsMission mission) {
		this.mission = mission;
		stepCheck();
	}
	
	public int getState() {
		return state;
	}

	public void setState(int state) {
		if(state == 0){
			this.mission.raz();
			this.erreur.raz();
			setGpsPosition("","");
		}		
		this.state = state;
	}
	
	/**
	 * Retourner l'etat (available,unavalaible) sous forme de string, afin que l'interface web fasse le lien avec le css
	 * @return
	 */
	public String getStringState(){
		if(state == 0){
			//System.out.println("statut du cavalier "+chId+" unavailable");
			return "unavailable";
		}			
		else{
			//System.out.println("statut du cavalier "+chId+" available");
			return "available";
		}
			
	}
	
	/**
	 * uniquement pour l'interface web pour g�n�rer le css
	 * @return
	 */
	public String getShuntable() {
		
		if(mission.isToShunt() && mission.isCtrlGPS()){
			return "YES";
		}
		else
			return "NO";
	}
	
	/**
	 * retourne si le straddle est � shunter
	 * @return
	 */
	public boolean isShuntable(){
		return mission.isToShunt();
	}

	public Error getErreur() {
		return erreur;
	}

	public void setErreur(Error erreur) {
		this.erreur = erreur;
	}
	
	public boolean isAutoShunt() {
		return autoShunt;
	}

	public void setAutoShunt(boolean autoShunt) {
		this.autoShunt = autoShunt;
	}

	public String getId() {
		return chId;
	}

	public void setId(String id) {
		this.chId = id;
	}
	
	/**
	 * Retourner l'affichage destin� � �crire sur le tableau
	 * 
	 * @return
	 */
	public String getDisplayMessage(){
		return mission.getDisplayMessage();		
	}

	public void setDisplayMessage(String msg){
		 mission.setDisplayMessage(msg);		
	}
	
	public String getDisplayMessageTmp(){
		return mission.getDisplayMessageTmp();		
	}
	
	public void setDisplayMessageTmp(String msg){
		mission.setDisplayMessageTmp(msg);		
	}
	public void setShuntable(boolean b) {
		mission.setForShunt(b);
	}
	
	public void setErreur(String displayMsg) {
		this.erreur.setOccur("Y");
		this.erreur.setErrorMsg(displayMsg);
	}

	/**
	 * reset du message d'erreur + reset de la position GPS
	 * + de la position GPS display
	 */
	public void razErreur() {
		this.erreur.raz();
		mission.setGpsPosition("");
		mission.setGpsPositionCible("");
	}

	public boolean isCtrlGPS() {
		return mission.isCtrlGPS();
	}
	
	public boolean isShuntP_Before() {
		return shuntP_Before;
	}

	public void setShuntP_Before(boolean shuntP_Before) {
		this.shuntP_Before = shuntP_Before;
	}
	
	public String getTempoMission() {
		return tempoMission;
	}

	public boolean isTmpBool() {
		return tmpBool;
	}

	public void setTmpBool(boolean tmpBool) {
		this.tmpBool = tmpBool;
	}

	public void setTempoMissionOk() {
		this.tempoMission = "OUI";
		this.tmpBool=true;	
	}
	
	public void setTempoMissionNok() {
		this.tempoMission = "";
	}
	
	public String getMemoMsgStraddle() {
		return memoMsgStraddle;
	}

	public void setMemoMsgStraddle(String memoMsgStraddle) {
		this.memoMsgStraddle = memoMsgStraddle;
	}
	
	
	//******************************************************************************************
	//************************************* Fonctions ******************************************

	public void connect() {
		setState(1);
		mission.setDisplayMessage("Connect�");
	}

	public void disConnect() {
		setState(0);
	}

	public void setAvailable() {
		setState(2);
		mission.setDisplayMessage("En attente de mission");
	}

	//************************************* D�placements
	public void emptyToOrigin(String area, String area_Type,String bay, String lnth, String ingress, String shipNumber) {
		etapeIntermediare(area, "D:" + area, area_Type,bay,"",lnth,ingress, shipNumber);
	}

	public void setLadenToDest(String area, String area_Type,String bay,String container, String lnth, String ingress, String shipNumber) {
		etapeIntermediare(area, "V:" + area, area_Type,bay,"avec "+displaycontener(container),lnth,ingress, shipNumber);		
	}
	
	private void etapeIntermediare(String area, String cible, String area_Type,String bay,String container, String lnth, String ingress, String shipNumber){
		setState(3);
		mission.setPosition(area);
		mission.setPositionCible(cible);
		mission.setCtrlGPS(false);		
		switch(area_Type.toLowerCase()){
		case "vessel":
			mission.setDisplayMessage("Va au portique " + area+"\nBay "+displayBay(bay)+" " +VesselNames.askVesselName(shipNumber)+  "\n"+lnth+"'  "+container);
			mission.setDisplayPosition(0);
			break;
		case "yardrow":
			mission.setDisplayMessage("Va � la ligne " + area+ " "+displayIngress(ingress)+" \n"+lnth+"'  "+container);
			mission.setDisplayPosition(1);
			break;
		default:
			mission.setDisplayMessage("Va en " + area + "\n"+lnth+"'  "+container);
			mission.setDisplayPosition(1);
			break;
		}
		
	}
	
	//************************************* Prise et pose
	public void setEmptyAtOrigin(String ppos, String area_Type, String eqID, String lnth, String jpos,String twin, String tkps,String dgx, String weight, String bay,
			String oog, String mkvd,String shipNumber) {
		etapeAction(ppos, area_Type, eqID, lnth, jpos, "V:","Prendre",twin,tkps,dgx,weight,bay,true,oog,mkvd, shipNumber);
	}

	public void setLadenAtDest(String ppos, String area_Type,String eqID, String lnth, String jpos,String twin, String tkps,String dgx, String weight, String bay,
			String oog, String mkvd, String shipNumber) {
		etapeAction(ppos, area_Type, eqID, lnth, jpos, "D:", "Poser",twin,tkps,dgx,weight,bay,false,oog,mkvd, shipNumber);
	}
	
	private void etapeAction(String ppos, String area_Type,String eqID, String lnth, String jpos,String lock, String action,
			String twin, String tkps,String dgx, String weight, String bay,Boolean priseBool,String oog,String mkvd, String shipNumber){
		
		setState(3);

		mission.setPosition(ppos);
		mission.setPositionCible(lock + ppos);
		
		mission.setConteneur(eqID);
		mission.setContainerLength(lnth);
		mission.setJpos(jpos);
		
		switch(area_Type.toLowerCase()){
		case "vessel":
			mission.setCtrlGPS(false);
			mission.setDisplayPosition(0);
			if (twin.equals("")) {
				mission.setDisplayMessage( action + " conteneur "+oog +lnth+"'  "+displaycontener(eqID)+"\n" + weight +" T" + dgx  + " au "+displayPortique(ppos,priseBool)+"\nBay "+displayBay(bay)+
						" " +VesselNames.askVesselName(shipNumber));
			}else {
				mission.setDisplayMessage( action + " conteneur "+oog+lnth+"'  "+displaycontener(eqID)+"\n" + weight +" T" + dgx  + " en face du " + displaycontener(twin) +  " au "+displayPortique(ppos,priseBool)+"\nBay "+displayBay(bay)+
						" " +VesselNames.askVesselName(shipNumber));
			}
			
			break;
		case "yardrow":
			mission.setCtrlGPS(true);
			mission.setDisplayPosition(1);
			mission.setDisplayMessage(action + " conteneur "+oog+lnth+"'\n"+displaycontener(eqID)+"\n" + weight +" T" + dgx  + " en "+ppos);	
			break;
		case "grid":
			mission.setCtrlGPS(true);
			mission.setDisplayPosition(1);
			switch (tkps) {
			case "1":
				mission.setDisplayMessage(action + " conteneur "+oog+lnth+"'  "+displaycontener(eqID)+"\n" + weight +" T" + dgx  + " en "+ppos + " avant");
				break;
			case "2":
				mission.setDisplayMessage(action + " conteneur "+oog+lnth+"'  "+displaycontener(eqID)+"\n" + weight +" T" + dgx  + " en "+ppos + " milieu");
				break;
			case "3":
				mission.setDisplayMessage(action + " conteneur "+oog+lnth+"'  "+displaycontener(eqID)+"\n" + weight +" T" + dgx  + " en "+ppos + " arri�re");
				break;
			default:
				mission.setDisplayMessage(action + " conteneur "+oog+lnth+"'  "+displaycontener(eqID)+"\n" + weight +" T" + dgx  + " en "+ppos);
				break;
			}
			break;
		default:
			mission.setCtrlGPS(false);
			mission.setDisplayPosition(0);
			mission.setDisplayMessage(action + " conteneur "+oog+lnth+"'  "+displaycontener(eqID)+"\n" + weight +" T" + dgx  + " en "+ppos);
			break;
		}
		
		//Condition pour le shifting
        if (mkvd.equalsIgnoreCase("SHFT")) {
               mission.setDisplayMessage(mission.getDisplayMessage() + " SHIFTING");
        }
	
	}

	//************************************* Cas sp�ciaux
	/**
	 * Envoyer a la tablette et l'interface web la mission de shifting
	 * Le shiftinf se fait en 2 �tapes prise et pose
	 * @param pposPrise
	 * @param pposPose
	 * @param eqID
	 * @param lnth
	 */
	public void setAutoRehandle(String pposPrise, String pposPose, String eqID, String lnth,String dgx, String weight,String oog) {
		setState(3);
		mission.setPosition(pposPrise);
		mission.setPositionCible("V:" + pposPrise);
		mission.setPositionShift(pposPose);
			
		mission.setConteneur(eqID);
		mission.setContainerLength(lnth);
		mission.setDgx(dgx);
		mission.setWeight(weight);
		mission.setOog(oog);
		
		mission.setCtrlGPS(true);
		mission.setDisplayPosition(1);
		mission.setDisplayMessage("Prendre conteneur "+oog+lnth+"'  "+displaycontener(eqID)+"\n" + weight +" T" + dgx  + " en "+pposPrise);
	}

	/**
	 * Poser conteneur shift
	 */
	public void AutoRehandleEtape2(){
		setState(3);
		mission.setPosition(mission.getPositionShift());
		mission.setPositionCible("D:" + mission.getPositionShift());
			
		mission.setCtrlGPS(true);
		mission.setDisplayPosition(1);
		mission.setDisplayMessage("Poser conteneur "+mission.getOog()+mission.getContainerLength()+"' "+displaycontener(mission.getConteneur())+
				"\n" + mission.getWeight() +" T" + mission.getDgx()  + " en " +mission.getPositionShift());
		
		setShuntP_Before(false);		
		setActualFormId(9);
		
	}
	
	
	public void setManualRehandle(String ppos, String eqID, String lnth,String dgx, String weight,String oog) {
		setState(3);
		mission.setPosition(ppos);
		mission.setPositionCible("D:" + ppos);
		
		
		mission.setConteneur(eqID);
		mission.setContainerLength(lnth);

		mission.setCtrlGPS(true);
		mission.setDisplayPosition(1);
		mission.setDisplayMessage("DEPLACEMENT - \n Poser conteneur "+oog+lnth+"' "+displaycontener(eqID)+"\n" + weight +" T" + dgx  + " en "+ppos);
		
	}

	private String displaycontener(String container){
		try {
			if (container.length() == 11 ){			
				return container.substring(0, 4) +" "+container.substring(4,10)+" "+container.substring(10);	
			}else {
				return  container;
			}
		} catch (Exception e) {
			return  container;
		}
	}
	
	private String displayPortique(String ppos, Boolean priseBool){
		try {
			String tb[]=ppos.split("-");
			if (priseBool) {
				return tb[0]+" ligne "+tb[1] +" d�barquement";
			}else {
				return tb[0] + " embarquement";
			}				
		} catch (Exception e) {
			return ppos;
		}
	}
	
	private String displayBay(String ppos){
		try {
			if (ppos.endsWith("A")) {
				return ppos.substring(0, ppos.length() -1)+ " pont";
			}else if (ppos.endsWith("B")) {
				return ppos.substring(0, ppos.length() -1) + " cale";
			}else {
				return ppos;
			}
		} catch (Exception e) {
			return ppos;
		}
	}
	
	private String displayIngress(String ingress){
		if(ingress.equalsIgnoreCase("LOW"))
			return "NORD";
		else
			if(ingress.equalsIgnoreCase("HIGH"))
				return "SUD";
		return "";
	}

	public void setPosSrc(String ppos, String area_type) {
		this.posSrc = ppos;		
		this.areaTypePosSrc = area_type;
	}
	public String getPosSrc() {
		return posSrc;
	}

	

	public Date getDatePosSrc() {
		return datePosSrc;
	}

	public void setDatePosSrc(Date datePosSrc) {
		this.datePosSrc = datePosSrc;
	}

	public String getAreaTypePosSrc() {
		return areaTypePosSrc;
	}
	
}
