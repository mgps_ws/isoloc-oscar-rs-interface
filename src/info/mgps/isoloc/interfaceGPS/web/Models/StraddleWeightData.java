package info.mgps.isoloc.interfaceGPS.web.Models;

public class StraddleWeightData {

	private String straddleName;
	private String ipAddress;
	
	
	
	
	
	public StraddleWeightData(String straddleName, String ipAddress) {
		super();
		this.straddleName = straddleName;
		this.ipAddress = ipAddress;
	}
	
	
	public String getStraddleName() {
		return straddleName;
	}
	public void setStraddleName(String straddleName) {
		this.straddleName = straddleName;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	
	
}
