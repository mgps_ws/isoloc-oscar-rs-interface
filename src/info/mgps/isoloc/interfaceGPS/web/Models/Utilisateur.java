package info.mgps.isoloc.interfaceGPS.web.Models;

import java.io.Serializable;

public class Utilisateur implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nom;
	private String prenom;
	private String identifiant;
	private int role; // 0-> pas pour le programme c'est le developpeur => tous les acc�s
						// 1-> administrateur
						// 2-> utilisateur priviligi�
						// 3-> utilisateur standard
	
	public Utilisateur(){
		super();
	}
	public Utilisateur(String nom, String prenom, String identifiant, int role) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.identifiant = identifiant;
		this.role = role;
	}
	
	public Utilisateur(String nom, String prenom, String identifiant, String role) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.identifiant = identifiant;
		this.role = toCodeRole(role);
	}
	
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getIdentifiant() {
		return identifiant;
	}
	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}
	public int getRole() {
		return role;
	}
	public String getStringRole(){
		switch(role){
		case 1:
			return "Administrateur";
		case 2:
			return "Priviligie";
		case 3:
			return "Standard";
		case 4:
			return "Securite";
			default: return "indefini";
		}
	}
	
	public static int toCodeRole(String role) {
		switch(role){
		case "administrateur":
			return 1;
		case "priviligie":
			return 2;
		case "standard":
			return 3;
		case "securite":
			return 4;
			default:
				return -1;
		}		
	}
	public void setRole(int role) {
		this.role = role;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
}
