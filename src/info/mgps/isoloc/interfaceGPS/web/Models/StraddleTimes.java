package info.mgps.isoloc.interfaceGPS.web.Models;

import java.util.Date;

import info.mgps.isoloc.interfaceGPS.service.log.AccessDb;

public class StraddleTimes {
	private String straddle;
	private Date connexionDate;
	private int previousEvent;//-1 deconnecte, 0 connecte, 1 idle, 2 emptyToOrigin, 3 emptyAtOrigin, 4 LadenToDest, 5 LadenAtDest,
	// 6 AutoRehandle, 7 ManualRehandle
	private Date idleDate;
	private Date disconnectDate;
	private Date emptyToOriginDate;
	private Date emptyAtOriginDate;
	private Date ladenToDestDate;
	private Date ladenAtDestDate;
	private Date autoRehandleDate;
	private Date manualRehandleDate;

	public StraddleTimes(String straddle) {
		super();
		this.straddle = straddle;
	}

	public void setDateCnx(Date date) {
		// TODO ajouter la date de connexion
		this.connexionDate = date;
	}

	public void setDateIdle(Date date) {
		// TODO la date � laquelle le straddle s'est mis en attente de mission
		this.idleDate = date;
		if(previousEvent == 0){
			// a peine connecte
			AccessDb.addTempsAttenteIdle(straddle, idleDate.getTime()-connexionDate.getTime());
		}else
			if(previousEvent == 5){
				// fini une mission
				AccessDb.addTempsStep4(straddle, idleDate.getTime()-ladenAtDestDate.getTime());
				AccessDb.incrementMissions(straddle);
			}
			else
				if(previousEvent == 7){
					// sort d'un manuelrehandle
					AccessDb.addTempsManuRehandle(straddle, idleDate.getTime()-manualRehandleDate.getTime());
					AccessDb.incrementMissions(straddle);
					AccessDb.incrementManuRehanlde(straddle);
				}
		
		this.previousEvent = 1;
	}

	public void setDateDeconnexion(Date date) {
		// TODO la date de deconnexion du straddle
		this.setDisconnectDate(date);
		
		this.previousEvent = -1;
	}

	public void setDateEmptyToOrigin(Date date) {
		// TODO premiere etape de la mission
		this.emptyToOriginDate = date;
		AccessDb.addTempsAttenteJob(straddle, emptyToOriginDate.getTime()-idleDate.getTime());
		
		this.previousEvent = 2;
	}

	public void setDateEmptyAtOrigin(Date date) {
		// TODO deuxieme etape de la mission
		this.emptyAtOriginDate = date;
		if(this.previousEvent == 2){
			// je suis dans le circuit normal, pas de shifting
			AccessDb.addTempsStep1(straddle, emptyAtOriginDate.getTime()-emptyToOriginDate.getTime());
		}else
			if(this.previousEvent == 6){
				// cas de shifting
				AccessDb.addTempsAutoRehandle(straddle, emptyAtOriginDate.getTime()-autoRehandleDate.getTime());
			}
		
		
		this.previousEvent = 3;
	}

	public void setDateLadenToDest(Date date) {
		// TODO troisieme etape de la mission
		this.ladenToDestDate = date;
		AccessDb.addTempsStep2(straddle, ladenToDestDate.getTime()-emptyAtOriginDate.getTime());
		
		this.previousEvent = 4;
	}

	public void setDateLadenAtDest(Date date) {
		// TODO Quatrieme et derniere etape de la mission
		this.ladenAtDestDate = date;
		AccessDb.addTempsStep3(straddle, ladenAtDestDate.getTime()-ladenToDestDate.getTime());
		
		this.previousEvent = 5;
	}

	public void setDateAutoRehandle(Date date) {
		// TODO reception de shifting calcule
		this.autoRehandleDate = date;		
		AccessDb.addTempsStep1(straddle, autoRehandleDate.getTime()-emptyToOriginDate.getTime());
		AccessDb.incrementAutoRehandle(straddle);
		this.previousEvent = 6;
	}

	public void setDateManualRehandle(Date date) {
		// TODO declenchement d'un job manuel, 
		this.manualRehandleDate = date;
		
		this.previousEvent = 7;
	}

	public Date getDisconnectDate() {
		return disconnectDate;
	}

	public void setDisconnectDate(Date disconnectDate) {
		this.disconnectDate = disconnectDate;
	}

	
}
