package info.mgps.isoloc.interfaceGPS.web.Models;

public class WeightingStraddle {

	
	private String ipAddress;
	private int liftId;
	private float poids1;
	private float poids2;
	private int jobId;
	private String containerID;
	private float jobWeight;
	
	
	public WeightingStraddle(String ipAddress, int liftId, float poids) {
		super();
		this.ipAddress = ipAddress;
		this.liftId = liftId;
		this.poids1 = poids;
		this.jobId = 1;
	}


	public String getIpAddress() {
		return ipAddress;
	}


	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}


	public int getLiftId() {
		return liftId;
	}


	public void setLiftId(int liftId) {
		this.liftId = liftId;
	}


	public float getPoids1() {
		return poids1;
	}


	public void setPoids1(float poids1) {
		this.poids1 = poids1;
	}


	public float getPoids2() {
		return poids2;
	}


	public void setPoids2(float poids2) {
		this.poids2 = poids2;
	}
	
	public void incrementJobId(){
		if(this.jobId < 5000000){
			this.jobId ++;
		}else{
			this.jobId = 1;
		}
		initializeAttributes();
	}
	
	private void initializeAttributes() {
		this.containerID = "";
		this.poids1 = 0;
		this.poids2 = 0;
		this.jobWeight = 0;
	}


	public int getJobId(){
		return this.jobId;
	}


	public String getContainerID() {
		return containerID;
	}


	public void setContainerID(String containerID) {
		this.containerID = containerID;
	}


	public void setJobWeight(float jobWeight) {
		this.jobWeight = jobWeight;
		
	}


	public float getJobWeight() {
		return jobWeight;
	}


	@Override
	public String toString() {
		return "WeightingStraddle [ipAddress=" + ipAddress + ", liftId=" + liftId + ", poids1=" + poids1 + ", poids2="
				+ poids2 + ", jobId=" + jobId + ", containerID=" + containerID + ", jobWeight=" + jobWeight + "]";
	}
	
}
