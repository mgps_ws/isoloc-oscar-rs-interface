package info.mgps.isoloc.interfaceGPS.service.log;


import info.mgps.isoloc.interfaceGPS.utilitaire.LogClass;
import info.mgps.isoloc.interfaceGPS.web.Models.StraddleWeightData;
import info.mgps.isoloc.interfaceGPS.web.Models.Utilisateur;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class AccessDb {

	private static Connection con = null;
	private  static Statement stmt = null;
	//private static String jdbcUrl ="jdbc:sqlserver://127.0.0.1;instanceName=SQLEXPRESS;Database=DbInterfaceLog;user=user;password=user;";
	private  static String jdbcUrl ="jdbc:sqlserver://localhost;instanceName=SQLEXPRESS;Database=DbInterfaceLog;user=user;password=user;";
	private static DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");
	private  static AccessDbThread AccessDbTh;
	private static Thread thUpdateDb;
	private static int nbRow = 1000; 
	
	/**
	 * Permet de rechercher des logs
	 * @param dateStart
	 * @param dateEnd
	 * @param mobile
	 * @param typeMsg
	 * @param emplacement
	 * @param conteneur
	 * @param shunt
	 * @return
	 */
	public static ArrayList<String[]> SelectLogMessage(String dateStart,String dateEnd, String mobile, int typeMsg, String emplacement,
			String conteneur, int shunt){
		String sql ="select TOP "+nbRow+" Date, Mobile, message from logmessage WHERE ";
		//********************** Clause WHERE pour Date
		sql += "(Date >= '" + dateStart + "' AND Date < '" + dateEnd + "') ";	
		//********************** Clause WHERE pour Mobile
		if (!mobile.equals("")){
			sql += "AND Mobile='"+mobile +"' ";
		}
		
		//********************** Clause WHERE pour typeMsg
		//0= tous les messages; 1=messages straddle; 2= messages Navis
		if (typeMsg!=0){
			sql += "AND TypeMsg="+typeMsg +" ";
		}
		
		//********************** Clause WHERE pour emplacement
		if (!emplacement.equals("")){
			sql += "AND emplacement like '%"+emplacement +"%' ";
		}
		
		//********************** Clause WHERE pour conteneur
		if (!conteneur.equals("")){
			sql += "AND conteneur like '%"+conteneur +"%' ";
		}		
		
		//********************** Clause WHERE pour shunt
		//0=pas de shunt; 1= shunt
		if (shunt!=0){
			sql += "AND shunt="+shunt +" ";
		}
		return	SelectLogMessageDb(sql);
	}
	
	/**
	 * Permet de rechercher des logs
	 * @param dateStart
	 * @param dateEnd
	 * @param mobile
	 * @param typeMsg
	 * @param emplacement
	 * @param conteneur
	 * @param shunt
	 * @return
	 */
	public static ArrayList<String[]> SelectPeseeMessage(String dateStart,String dateEnd, String mobile, String conteneur, int peseeDoute, int peseeTOS){
		
		String sql ="select TOP "+nbRow+" datepesee, nomStraddle, containerID, poidsMission, cheWeight, state, writeTOS from LogPeseeStraddle WHERE ";
		//********************** Clause WHERE pour Date
		sql += "(datepesee >= '" + dateStart + "' AND datepesee < '" + dateEnd + "') ";	
		//********************** Clause WHERE pour Mobile
		if (!mobile.equals("")){
			sql += "AND nomStraddle='"+mobile +"' ";
		}
		
		
		
		//********************** Clause WHERE pour conteneur
		if (!conteneur.equals("")){
			sql += "AND containerID like '%"+conteneur +"%' ";
		}	
		if(peseeDoute != 0){
			if(peseeDoute == 1){
				// OUI
				sql += " AND state = 2";
			}else
				if(peseeDoute == 2){
					// NON
					sql += " AND state = 1";
				}
				else
					if(peseeDoute == 3){
						// ECHEC PESEE
						sql += " AND state = 0";
				}
			
		}
		if(peseeTOS != 0){
			if(peseeTOS == 1){
				sql += " AND writeTOS = 1";
			}else
				if(peseeTOS == 2){
					sql += " AND writeTOS = 0";
				}
			
		}
			
		return	SelectPeseeMessageDb(sql);
	}
	
	/**
	 *  Permet de rechercher des logs
	 * @return
	 */
	public static ArrayList<String[]> SelectLogMessage(){
		String sql ="select TOP "+nbRow+" Date, Mobile, message from logmessage order by Date";	
		return	SelectLogMessageDb(sql);
	}
	
	/**
	 * Mise en place de la requ�te pour rechercher les logs
	 * @param sql
	 * @return
	 */
	private static ArrayList<String[]> SelectLogMessageDb(String sql){
		ArrayList<String[]> ar =new ArrayList<String[]>();	
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Connection con1 = DriverManager.getConnection(jdbcUrl);		
			
			Statement stmt1 = con1.createStatement( ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = stmt1.executeQuery(sql);
			while (rs.next()) {
				String[] s = new String[3];
				s[0]=rs.getString(1);
				s[1]=rs.getString(2);
				s[2]=rs.getString(3);
				ar.add(s);
			}
			rs.close();
			stmt1.close();
			con1.close();
		} catch (Exception e) {
			String[] s = {"","",e.getMessage()};
			ar.add(s);	
		}		
		return ar;
	}
	/**
	 * Mise en place de la requ�te pour rechercher les logs
	 * @param sql
	 * @return
	 */
	private static ArrayList<String[]> SelectPeseeMessageDb(String sql){
		ArrayList<String[]> ar =new ArrayList<String[]>();	
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Connection con1 = DriverManager.getConnection(jdbcUrl);		
			
			Statement stmt1 = con1.createStatement( ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = stmt1.executeQuery(sql);
			while (rs.next()) {
				String[] s = new String[7];
				s[0]=rs.getString(1);
				s[1]=rs.getString(2);
				s[2]=rs.getString(3);
				s[3]=rs.getString(4);
				s[4]=rs.getString(5);
				s[5]=rs.getString(6);
				s[6]=rs.getString(7);
				ar.add(s);
			}
			rs.close();
			stmt1.close();
			con1.close();
		} catch (Exception e) {
			String[] s = {"","",e.getMessage()};
			ar.add(s);	
		}		
		return ar;
	}
	public static ArrayList<String[]> selectWeightsToUpdateFred(){
		ArrayList<String[]> ar =new ArrayList<String[]>();	
		String sql = "select id, container, weightche from tbmission where lockweight=1 and lockdb=0";
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String jdbcUrl = "jdbc:sqlserver://10.120.2.16;instanceName=SQLEXPRESS;Database=DbMissionVGM;user=user;password=user;";
			Connection con1 = DriverManager.getConnection(jdbcUrl);		
			
			Statement stmt1 = con1.createStatement( ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = stmt1.executeQuery(sql);
			//System.out.println("size "+rs);
			while (rs.next()) {
				String[] s = new String[3];
				s[0]=String.valueOf(rs.getInt(1));
				s[1]=rs.getString(2);
				s[2]=String.valueOf(rs.getInt(3));
				ar.add(s);
			}
			rs.close();
			stmt1.close();
			con1.close();
		} catch (Exception e) {
			String[] s = {"","",e.getMessage()};
			ar.add(s);	
		}		
		return ar;
	}

	/**
	 * Mise en place de la requ�te d'insertion pour la m�morisation des messages applicatifs
	 * @param level
	 * @param source
	 * @param message
	 */
	public static void MemologApplicatif(int level, String source, String message){
		updateDbThread("INSERT INTO logapplicatif VALUES ('"+myDate()+"',"+level+",'"+source+"','"+message+"')" ); 	
	}
	
	/**
	 * Mise en place de la requ�te d'insertion pour la m�morisation des messages 'sockets'
	 * @param mobile
	 * @param typeMsg
	 * @param emplacement
	 * @param conteneur
	 * @param shunt
	 * @param message
	 */
	public static void MemologMessage( String mobile, int typeMsg, String emplacement,
			String conteneur, int shunt,String message){	
		message= message.replace("'", "");
		updateDbThread("INSERT INTO logmessage VALUES ('"+myDate()+"','"+mobile+"',"+typeMsg+",'"+emplacement+"','"+conteneur+"',"+shunt+
				",'"+message+"')" ); 	
	}
	
//	/**
//	 * Permet de supprimer les logs
//	 * @param sql
//	 */
//	private static void DelLogDb(String sql){
//		updateDb("DELETE FROM ");
//		updateDb("");
//	}
	
	/**
	 * Permet l'insertion des donn�es dans la table locale
	 * dans un autre thread
	 * @param sql
	 * @return 
	 * @return
	 */
	private synchronized static void updateDbThread(String sql){
		AccessDbTh = new AccessDbThread(sql);
		thUpdateDb = new Thread(AccessDbTh);
		thUpdateDb.start();
	}
	
	/**
	 * Permet de faire la mise � jour d'une instruction sql
	 * (insert, update et delete)
	 * @param sql
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public synchronized static int updateDbPublic(String sql) throws ClassNotFoundException, SQLException{
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		con = DriverManager.getConnection(jdbcUrl);			
		stmt = con.createStatement();
		//stmt = con.createStatement( ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
		int ind = stmt.executeUpdate(sql);
		stmt.close();
		con.close();
		return ind;
	}
	
	public synchronized static int updateDbFred(String sql) throws ClassNotFoundException, SQLException{
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		String jdbcUrl = "jdbc:sqlserver://10.120.2.16;instanceName=SQLEXPRESS;Database=DbMissionVGM;user=user;password=user;";
		con = DriverManager.getConnection(jdbcUrl);			
		stmt = con.createStatement();
		//stmt = con.createStatement( ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
		int ind = stmt.executeUpdate(sql);
		stmt.close();
		con.close();
		return ind;
	}
	/**
	 * Permet d'avoir la date du jour
	 * @return
	 */
	private static String myDate(){
		return dateFormat.format(new Date());		
	}
	private static String myDate(Date date){
		return dateFormat.format(date);		
	}
	/**
	 * Permet de retourner un boolean � partir d'une instruction sql qui
	 * demande l'Update d'une seule row
	 * @param sql
	 * @return
	 */
	private static boolean update(String sql){
		try {
			if(updateDbPublic(sql) == 1)
				return true;
			else
				return false;
		} catch (ClassNotFoundException e) {
			LogClass.ecritureError(e.getMessage());
			return false;
		} catch (SQLException e) {
			LogClass.ecritureError(e.getMessage());
			return false;
		}
	}
	
	/**
	 * Ajout d'un utilisateur
	 * @param nom
	 * @param prenom
	 * @param identifiant
	 * @param role
	 * @return
	 */
	public static boolean addUser(String nom, String prenom, String identifiant, String role){
		return update("INSERT INTO Utilisateur (Nom, Prenom, Identifiant, Role) VALUES ('"+nom+"','"+prenom+"','"+identifiant+"',"+Utilisateur.toCodeRole(role)+")");	
	}
	
	/**
	 * Mise � jour d'un utilisateur
	 * @param nom
	 * @param prenom
	 * @param identifiant
	 * @param role
	 * @return
	 */
	public static boolean updateUser(String nom, String prenom, String identifiant, String role) {
			return update("update Utilisateur SET Nom ='"+nom+"', Prenom = '"+prenom+"', role = "+Utilisateur.toCodeRole(role)+" where Identifiant = '"+identifiant+"'");
	}

	/**
	 * Suppression d'un utilisateur
	 * @param identifiant
	 * @return
	 */
	public static boolean deleteUser(String identifiant) {		
			return update("delete from Utilisateur where Identifiant = '"+identifiant+"'"); 

	}
	
	/**
	 * V�rification de l'existence d'un user
	 * @param username
	 * @param password 
	 * @return le role de l'utilisateur
	 */
	public static int isUser(String username, String password){
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Connection con1 = DriverManager.getConnection(jdbcUrl);			
			Statement stmt1 = con1.createStatement( ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
			ResultSet resultat = stmt1.executeQuery("select role from Utilisateur where identifiant='"+username+"' and password = '"+password+"'");
			if(resultat != null){
				resultat.next();
				int exist = resultat.getInt(1);
				resultat.close();
				stmt1.close();
				con1.close();
				//System.out.println("exist = "+exist);
				return exist;
			}
			
			return -1;			
			
		} catch (ClassNotFoundException e) {
			LogClass.ecritureError(e.getMessage());
			return -1;
		} catch (SQLException e) {
			LogClass.ecritureError(e.getMessage());
			return -1;
		}
		
	}
	public static int isUserNoAuth(String user) {
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Connection con1 = DriverManager.getConnection(jdbcUrl);			
			Statement stmt1 = con1.createStatement( ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
			ResultSet resultat = stmt1.executeQuery("select role from Utilisateur where identifiant='"+user+"'");
			if(resultat != null){
				resultat.next();
				int exist = resultat.getInt(1);
				resultat.close();
				stmt1.close();
				con1.close();
				//System.out.println("exist = "+exist);
				return exist;
			}
			
			return -1;			
			
		} catch (ClassNotFoundException e) {
			LogClass.ecritureError(e.getMessage());
			return -1;
		} catch (SQLException e) {
			LogClass.ecritureError(e.getMessage());
			return -1;
		}
	}

	/**
	 * liste des utilisateurs
	 * @param ids
	 * @return
	 */
	public static List<Utilisateur> getUsers(String... ids) {
		try {			
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Connection con1 = DriverManager.getConnection(jdbcUrl);			
			
			Statement stmt1 = con1.createStatement( ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
			
			ResultSet resultat = null;
			if(ids == null || ids.length == 0)
				resultat = stmt1.executeQuery("select * from Utilisateur");
			else{
				String toAdd = "(";
				for(int i=0;i<ids.length;i++){
					toAdd += "'"+ids[i]+"'";
					if(i+1 == ids.length)
						toAdd +=")";
					else
						toAdd += ",";
				}
				resultat = stmt1.executeQuery("select * from Utilisateur where identifiant in "+toAdd);
			}
				
			if(resultat != null){
				/*resultat.last();
				int count = resultat.getRow();
				//System.out.println("Nombre de rows : " + count);
				*/
				
				
				resultat.beforeFirst();
				List<Utilisateur> liste = new ArrayList<Utilisateur>();
				while(resultat.next()){					
					String nom = resultat.getString("Nom");
					String prenom = resultat.getString("Prenom");
					String identifiant = resultat.getString("Identifiant");
					int role = resultat.getInt("Role");
					liste.add(new Utilisateur(nom, prenom, identifiant, role));
				}
				resultat.close();
				stmt1.close();
				con1.close();
				return liste;	
			}
			else
				return null;
		} catch (ClassNotFoundException e) {
			LogClass.ecritureError(e.getMessage());
			return null;
		} catch (SQLException e) {
			LogClass.ecritureError(e.getMessage());
			return null;
		}
	}

	public static void addTempsAttenteIdle(String straddle, long toAdd) {
		// TODO ajouter le long � la date d'attente globale du cavalier
		// convertir le straddle en entier (revient a enlever le C)
		/*if(isNotEmpty() > 0){
			if(isNotEmpty(straddle) > 0){
				updateDbThread("update logstats SET Temps_Attente_Global =Temps_Attente_Global+"+toAdd+" where N_Straddle='"+straddle+"'");
			}			
		}*/		
	}


	public static void addTempsAttenteJob(String straddle, long toAdd) {
		// TODO temps d'attente en �tant idle
		
	}

	public static void addTempsStep1(String straddle, long toAdd) {
		// TODO la duree du step1
		
	}

	public static void addTempsStep2(String straddle, long toAdd) {
		// TODO la duree du step2
		
	}

	public static void addTempsStep3(String straddle, long toAdd) {
		// TODO la duree du step3
		
	}

	public static void addTempsStep4(String straddle, long toAdd) {
		// TODO la duree du step4
		
	}

	public static void addTempsAutoRehandle(String straddle, long toAdd) {
		// TODO duree d'un autoRehandle (Shifting)
		
	}

	public static void addTempsManuRehandle(String straddle, long l) {
		// TODO ajouter au temps total des manual rehandle
		
	}

	public static void incrementAutoRehandle(String straddle) {
		// TODO incrementer le nombre des manual rehandle
		
	}

	public static void incrementMissions(String straddle) {
		// TODO incrementer le nombre des missions total
		
	}

	public static void incrementManuRehanlde(String straddle) {
		// TODO incrementer le nombre des manual rehandles
		
	}

	public static int isNotEmpty(String...straddle){
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Connection con1 = DriverManager.getConnection(jdbcUrl);				
			Statement stmt1 = con1.createStatement( ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
			String toAdd = "";
			if(straddle != null && straddle.length==1){
				toAdd += " where N_Straddle='"+straddle[0]+"'";
			}
			String query = "select count(*) from logstats"+ toAdd;
			ResultSet resultat = stmt1.executeQuery(query);
			if(resultat != null){
				resultat.next();
				int exist = resultat.getInt(1);
				resultat.close();
				stmt1.close();
				con1.close();
				return exist;
			}
			return -1;			
			
		} catch (ClassNotFoundException e) {
			LogClass.ecritureError(e.getMessage());
			return -1;
		} catch (SQLException e) {
			LogClass.ecritureError(e.getMessage());
			return -1;
		}
		
	}

	public static void remplirStraddlesWeighting(Map<String, Boolean> weightBool, Map<String, Boolean> straddlesWeightingName, Map<String, Boolean> straddlesRAZWeightingName,Map<String, StraddleWeightData> mapDataIP, Map<String, StraddleWeightData> mapDataName) {
		try {			
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Connection con1 = DriverManager.getConnection(jdbcUrl);			
			Statement stmt1 = con1.createStatement( ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
			ResultSet resultat = null;
			
			resultat = stmt1.executeQuery("select * from StraddlePesee");
			if(weightBool == null)
				weightBool = new HashMap<String, Boolean>();
			if(mapDataIP == null)
				mapDataIP = new HashMap<String, StraddleWeightData>();
				
			if(resultat != null){
				
				resultat.beforeFirst();
				while(resultat.next()){		
					String numStraddle = resultat.getString("numStraddle");
					String ipAddress = resultat.getString("ipAddress");
					boolean peseeOK = resultat.getBoolean("peseeOK");
					boolean razOK = resultat.getBoolean("razOK");
					@SuppressWarnings("unused")
					String ipAdressPesee = resultat.getString("ipAddressPesee");
					StraddleWeightData st = new StraddleWeightData(numStraddle, ipAddress);
					mapDataIP.put(ipAddress, st);
					mapDataName.put(numStraddle, st);
					weightBool.put(ipAddress, peseeOK);
					straddlesWeightingName.put(numStraddle, peseeOK);
					straddlesRAZWeightingName.put(numStraddle, razOK);
					
					
					
				}
				resultat.close();
				stmt1.close();
				con1.close();
			}
		} catch (ClassNotFoundException e) {
			LogClass.ecritureError(e.getMessage());
			return;
		} catch (SQLException e) {
			LogClass.ecritureError(e.getMessage());
			return;
		}
	}

	public static void writeVGMInfos(Date date, String straddleName, String containerID, float jobWeight, float cheWeight, int etat, boolean ecritureTOS) {
		// TODO Ecriture dans la table logStraddlePesee des infos necessaires	
		// A revoir, erreur = 
		//Il y a plus de colonnes dans l'instruction INSERT que de valeurs sp�cifi�es dans la clause VALUES. Le nombre de valeurs de la clause VALUES doit �tre le m�me que le nombre de colonnes de l'instruction INSERT.
		//.writeVGInfos(new Date(), this.straddleName, this.containerID, this.jobWeight, this.cheWeight,1,false);

		update("INSERT INTO LogPeseeStraddle (datepesee, nomStraddle, containerID, poidsMission, cheWeight, state, writeTOS) VALUES ('"+myDate()+"','"+straddleName+"','"+containerID+"',"+jobWeight+","+cheWeight+","+etat+",'"+ecritureTOS+"')");
	}

	public static void writeVGMFred(int id, String containerID) {
		//update("update Utilisateur SET Nom ='"+nom+"', Prenom = '"+prenom+"', role = "+Utilisateur.toCodeRole(role)+" where Identifiant = '"+identifiant+"'");
		try {
			updateDbFred("update tbmission set lockdb =1 where id="+id);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void setPesee(String chid, boolean active) {
		try {
			updateDbPublic("update StraddlePesee set peseeOK='"+active+"' where numStraddle = '"+chid+"'");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public static void setIPPesee(String chid, String newIP) {
		try {
			updateDbPublic("update StraddlePesee set ipAddress='"+newIP+"' where numStraddle = '"+chid+"'");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void setRAZPesee(String chid, boolean active) {
		try {
			updateDbPublic("update StraddlePesee set razOK='"+active+"' where numStraddle = '"+chid+"'");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void insertRAZRow(String nameStraddle, Date date, boolean execute) {
		
			try {
				updateDbPublic("INSERT INTO LogRAZPeseeStraddle (date, nomStraddle, executed) VALUES ('"+myDate(date)+"','"+nameStraddle+"','"+execute+"')");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		
	}

	public static void updateNbMoves() {
		try {
			updateDbPublic("update NbMoves set moves= moves + 1");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	
}
