package info.mgps.isoloc.interfaceGPS.service.log;

public class AccessDbThread implements Runnable {

//	private   static Connection con = null;
//	private static Statement stmt = null;
//	private static String jdbcUrl ="jdbc:sqlserver://127.0.0.1;instanceName=SQLEXPRESS;Database=DbInterfaceLog;user=user;password=user;";
	private String sql;
	
	public AccessDbThread(String sql){
		this.sql=sql;
	}
	
	@Override
	public void run() {
		try {	
			updateDb(sql);
		} catch (Exception e) {
		
		}	
	}

	
	/**
	 * Permet l'insertion des donn�es dans la table locale
	 * @param sql
	 * @return
	 */
	private synchronized int updateDb(String sql){
		try {
//			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
//			con = DriverManager.getConnection(jdbcUrl);			
//			stmt = con.createStatement();
//			int ind = stmt.executeUpdate(sql);
//			stmt.close();
//			con.close();
			int ind =AccessDb.updateDbPublic(sql);
			//System.out.println(" Run update ok " + ind + "\t"+sql);
			return ind;
		}catch (Exception e) { 
			//Probl�me de la socket close
			//System.out.println(" Run update erreur : " + e.getMessage() + "\t"+sql);
			return 0;	
		}		
	}
}
