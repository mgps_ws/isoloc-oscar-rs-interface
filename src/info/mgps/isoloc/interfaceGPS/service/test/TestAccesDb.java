package info.mgps.isoloc.interfaceGPS.service.test;

import java.util.ArrayList;

import info.mgps.isoloc.interfaceGPS.service.log.AccessDb;
import info.mgps.isoloc.interfaceGPS.utilitaire.LogClass;

public class TestAccesDb {

	public static void main(String[] args) {
		 LogClass.ecriture("testAccesDb", " -------------------- ", 1);
		 //System.out.println(" -------------------- ");
		 testSelect();
		 //testLogApplicatif();
		 //testLogMessage();
		 LogClass.ecriture("testAccesDb", " -------------------- ", 1);
		 //System.out.println(" -------------------- ");		 
	}
	
	private static void testSelect(){
		 //ArrayList<String[]> ar = AccessDb.SelectLogMessage();
		 ArrayList<String[]> ar = AccessDb.SelectLogMessage("10/08/2015","12/08/2015","",0, "","",0 );
		 
		 for (String elem[]: ar) {
			 for (int i = 0; i < elem.length; i++) {
				 System.out.print(elem[i] + "\t");		
			 }
			 System.out.print("\n");	
		 }		
	}
	
//	private static void testLogApplicatif(){
//		AccessDb.MemologApplicatif(1, "TestAccesDb", " Test log applicatif");
//	}
//	
//	private static void testLogMessage(){
//		AccessDb.MemologMessage("C00", 0, "", "", 0, "Test memo message socket");
//	}
	
}
