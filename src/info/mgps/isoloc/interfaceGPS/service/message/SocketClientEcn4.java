package info.mgps.isoloc.interfaceGPS.service.message;

import info.mgps.isoloc.interfaceGPS.service.socketFlash.SocketClient;
import info.mgps.isoloc.interfaceGPS.utilitaire.InOutEcn4;
import info.mgps.isoloc.interfaceGPS.utilitaire.LogClass;

public class SocketClientEcn4 extends SocketClient implements Runnable{

	private int delaiReconnexion=1000;//Permet d'attendre un certain temps en ms avant de tenter une reconnexion
	
	//************** Constructeur **************
	public SocketClientEcn4(String ipAdress,int portCom,String nameThread,boolean sendBytes,int indEnd,int delaiReconnexion) {
		super(ipAdress, portCom, nameThread, sendBytes,indEnd);
		this.delaiReconnexion= delaiReconnexion;
	}
	
	
	//************* fonctions li�es aux �v�nements
	@Override
	public void ecriture(String msg) {
		LogClass.ecriture("SocketClientEcn4", msg, 1);	
	}

	@Override
	public void ecritureError(String msg) {
		LogClass.ecriture("SocketClientEcn4", msg, 3);		
	}

	@Override
	synchronized public void  lecture(String message) {
		LogClass.ecriture("SocketClientEcn4", "Lecture du message re�u depuis ECN4 " + message,1);
		setChanged();
		notifyObservers(message);
				
	}

	@Override
	public void lostConnexion(Exception e) {
		LogClass.ecriture("SocketClientEcn4", "Perte de connexion avec " + this.getIpAdress() + ":" + this.getPortCom(), 1);
		setChanged();
		notifyObservers(0);
		try {
			Thread.currentThread();
			Thread.sleep(delaiReconnexion);
		} catch (InterruptedException e1) {
			LogClass.ecriture("SocketClientEcn4", "Perte de connexion - Probl�me de Thread : " + e1.getMessage() ,  3);
		}
		reconnexion();
	}

	@Override
	public void ConnexionReady(String msg) {
		LogClass.ecriture("SocketClientEcn4", "Connexion avec " + this.getIpAdress() + ":" + this.getPortCom(), 1);	
		setChanged();
		notifyObservers(1);
	}

	public void send(String message) {
		super.send(InOutEcn4.outEcn4(message));
		LogClass.ecriture("SocketClientEcn4\\send" , "Message envoy� : " + message, 1);	
	}
	
	//************ Tentative de reconnexion
	private void reconnexion(){
		this.connect();
	}


	@Override
	public void run() {
		this.connect();
	}
	
}
