package info.mgps.isoloc.interfaceGPS.service.message;

public class ConnectionEcn4Exception extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String message;

	public ConnectionEcn4Exception(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
