package info.mgps.isoloc.interfaceGPS.service.message;

import java.util.Observer;

import info.mgps.isoloc.interfaceGPS.service.socketFlash.SocketServerUDP;
import info.mgps.isoloc.interfaceGPS.service.xmlNavis.MessageXmlWrite;
import info.mgps.isoloc.interfaceGPS.web.web.Controllers.MyController;


/**
 * 
 * @author adm
 *1)dialogue Ecn4/Interface:
 *-g�re l'entr�e de tous les messages Ecn4 et les envoie � Straddle Manager qui g�re la partie m�tier
 *-M�morise un pool des messages transmis � Ecn4
 *
 *2)dialoque Interface/Straddle:
 *-g�re l'envoie des messages vers les straddles
 *-Memorise un pool des messages transmis aux straddles
 *
 *
 */
public class MessageManager{

	private SocketClientEcn4 socketclientEcn4;
	private SocketServerStraddle socketServerStraddle;
	private SocketServerUDP socketServerWeight;
	private Thread thClientEcn4;
	private Thread thSreverStraddle;
	private Thread thServerWeight;
	
	// ************** Constructeur *****************
	public MessageManager(String ipAdressClient,int portComClient,String nameThreadClient,boolean sendBytesClient,int indEndClient, 
			int delaiReconnexionClient, int portServer, int nombreDeConnexionServer, String nameThreadServer, boolean sendByteServer ,
			int indEndServer, int delaiReconnexionServer, int portNumberWeight){
		//1) Lancement du dialogue entre Ecn4 et l'Interface
		socketclientEcn4 = new SocketClientEcn4(ipAdressClient, portComClient, nameThreadClient, sendBytesClient, indEndClient,
				delaiReconnexionClient);
		thClientEcn4 = new Thread(socketclientEcn4);
		thClientEcn4.start();
		
		//2)Lancement du dialogue entre les cavaliers et l'interface
		//je lance le serveur Interface � l'�coute
		socketServerStraddle = new SocketServerStraddle( nameThreadServer,portServer, nombreDeConnexionServer, sendByteServer, indEndServer, delaiReconnexionServer);
		thSreverStraddle = new Thread(socketServerStraddle);
		thSreverStraddle.start();
		
		socketServerWeight = new SocketServerUDP(portNumberWeight, "SocketServerWeight");
		thServerWeight = new Thread(socketServerWeight);
		thServerWeight.start();
		
	}
	
	/**
	 * Permet l'abonnement de l'oberserver au client Ecn4
	 *
	 */
	public void addObserverClientEcn4(Observer o){
		socketclientEcn4.addObserver(o);
	}
	
	/**
	 * Permet l'abonnement de l'oberserver au serveur straddle
	 *
	 */
	public void addObserverServerStraddle(Observer o){
		socketServerStraddle.addObserver(o);
	}
	
	/**
	 * Permet l'abonnement de l'oberserver au serveur straddle
	 *
	 */
	public void addObserverServerWeight(Observer o){
		socketServerWeight.addObserver(o);
	}
	
	public void closeManager(){
		if(socketclientEcn4 != null)
			socketclientEcn4.closeReception();
		if(socketServerStraddle != null)
			socketServerStraddle.stop();
		if(socketServerWeight!= null)
			socketServerWeight.stop();
	}

	
	
	//*****************************************************
	//********** Message vers Ecn4 pour test

	public void ecn4connectStraddle(int incrementMsId, String login,
			String straddleNum) {
		socketclientEcn4.send(MessageXmlWrite.sendLogin(incrementMsId, login, straddleNum));
		
	}
	
	/**
	 *	Envoie du message de deconnexion vers Ecn4 
	 * @param msId
	 * @param straddle
	 * @param state
	 */
	public void ecn4disConnectStraddle(int msId, String straddle, int state) {
		socketclientEcn4.send(MessageXmlWrite.sendDeconnexion(msId, straddle, state));		
	}
	
	/**
	 * Envoie d'un message vers Ecn4
	 * 
	 * @param messageProgressECN4
	 */
	public void ecn4sendMessage(String messageProgressECN4) {
		if(messageProgressECN4 != null && !messageProgressECN4.equals(""))
			socketclientEcn4.send(messageProgressECN4);
	}
	
	//*****************************************************
	//********** Message vers straddle
	/**
	 * 
	 * @param message Straddle
	 */
	public void straddleSendMessage(String NameStraddle, String message) {
		if(message != null && !message.equals("")) {
			if(MyController.isDetectionRowActive()){
				socketServerStraddle.send(message, MyController.getAntiCollisionServerAddress());
			}
			socketServerStraddle.send(message, NameStraddle);
		}	
	}

	public void checkListSendMessage(String message) {
		if(message != null && !message.equals("")) {			
			socketServerStraddle.send(message, MyController.getIpCheckListStraddle());
		}
	}

	
}
