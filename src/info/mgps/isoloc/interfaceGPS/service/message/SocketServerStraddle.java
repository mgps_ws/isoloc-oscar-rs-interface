package info.mgps.isoloc.interfaceGPS.service.message;

import info.mgps.isoloc.interfaceGPS.service.messageStraddle.MessageStraddle;
import info.mgps.isoloc.interfaceGPS.service.socketFlash.SocketServeurManager;
import info.mgps.isoloc.interfaceGPS.utilitaire.InOutStraddle;
import info.mgps.isoloc.interfaceGPS.utilitaire.LogClass;


/**
 * Serveur pour permettre la connexion des cavaliers
 * @author adm
 *
 */
public class SocketServerStraddle extends SocketServeurManager {

	
	private int delaiReconnexion=1000;//Permet d'attendre un certain temps en ms avant de tenter une reconnexion
	
	//******************************  Constructeur
	public SocketServerStraddle(String nameThread, int serverPort, int nombreDeConnexion, boolean sendBytes,int delaiReconnexion) {
		super(nameThread,serverPort, nombreDeConnexion, sendBytes);
		this.delaiReconnexion= delaiReconnexion;
	}

	public SocketServerStraddle(String nameThread, int serverPort,int nombreDeConnexion,boolean sendBytes,int indEnd,int delaiReconnexion) {
		super(nameThread,serverPort, nombreDeConnexion, sendBytes,indEnd);
		this.delaiReconnexion= delaiReconnexion;
	}
	
	
	// *************************** Gestion des erreurs
	@Override
	public void ecriture(String msg) {
		LogClass.ecriture("SocketServerStraddle", msg, 1);	
		
	}

	@Override
	public void ecritureError(String msg) {
		LogClass.ecriture("SocketServerStraddle", msg, 3);	
		
	}

	
	//*************************** Gestion des �v�nements
	@Override
	public void lostConnexionServer(Exception e) {
		LogClass.ecriture("SocketServerStraddle", "Perte de connexion serverStraddle ", 1);
		setChanged();
		notifyObservers(0);
		try {
			Thread.sleep(delaiReconnexion);
		} catch (InterruptedException e1) {
			LogClass.ecriture("SocketServerStraddle", "Perte de connexion - Probl�me de Thread : " + e1.getMessage() ,  3);
		}
		reconnexion();	
	}

	/**
	 * Permet la reconnexion
	 * automatique
	 */
	private void reconnexion() {
		this.connect();
		
	}

	/**
	 * Permet de savoir que le serveur est connect�
	 */
	@Override
	public void connexionReadyServer(String msg) {
		LogClass.ecriture("SocketServerStraddle", msg, 1);	
		setChanged();
		notifyObservers(1);		
	}
	
	/**
	 * Lecture des messages venant des cavaliers
	 */
	@Override
	public void lecture(String message, String namesocketClient) {
		
		LogClass.ecriture("SocketServerStraddle", "Lecture du message re�u depuis " + namesocketClient + " = " + message,1);
		setChanged();
		// TODO gestion du message venant du straddle mais en destination de la checklist
		int type = 2;
		if(message.contains("ccheck") || message.contains("scheck")){
			//msg = new MessageStraddle(2, namesocketClient, InOutStraddle.inStraddle(message));
			type = 3;
			if(message.startsWith("scheck")){
				message = message.replaceAll("scheck", "");
			}
			/*if(message.startsWith("ccheck")){
				// Dans le but de reperage des blocages
				String[] div = message.split(";");
				if(div != null  && div.length == 4 && div[2].equalsIgnoreCase("2")){
					// Dans un message de Straddle bloqu�
					// TODO mettre dans une variable que le straddle div[1] est bloqu�
					// Afficher un bouton sur Home.jsp qui envoie interface;debloc;straddleID;userID
					// straddleID dans le format C10
					// userID est le UserWeb connect�
					
				}
			}*/
			//message = message.replaceAll("check", "");
		}else
			if(message.contains("sweight")){
				type = 4;
			}
		// TODO in straddle prend en charge le caract�re de d�but et la fin <>length
		MessageStraddle msg = new MessageStraddle(type, namesocketClient, InOutStraddle.inStraddle(message));
		notifyObservers(msg);	
	}

	/**
	 * Perte de connexion d'un cavalier
	 */
	@Override
	public void lostConnexionClient(Exception e, String namesocketClient) {
		LogClass.ecriture("SocketServerStraddle", "Perte de connexion avec " + namesocketClient + " Erreur:  " + e.getMessage() , 1);
		setChanged();
		notifyObservers(new MessageStraddle(0, namesocketClient, e.getMessage()));
	}

	/**
	 * Connexion d'un cavalier
	 */
	@Override
	public void connexionReadyClient(String msg, String namesocketClient) {
		LogClass.ecriture("SocketServerStraddle", "Connexion avec " + namesocketClient, 1);	
		setChanged();
		notifyObservers(new MessageStraddle(1, namesocketClient, "connexion ready"));	
	}

	public void send(String message, String namesocketClient) {
		super.send(InOutStraddle.outStraddle(message) + (char) 3 , namesocketClient);
		LogClass.ecriture("SocketServerStraddle\\send" , namesocketClient +  " Message envoy� : " + message, 1);	
	}
	
}
