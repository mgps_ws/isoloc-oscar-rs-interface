package info.mgps.isoloc.interfaceGPS.service.message;

/**
 * Permet de savoir si le message � envoyer est pour Navis ou pour le straddle
 * @author adm
 *
 */
public class MessageFor {

	private int straddle; //1=straddle; 2=Navis; 3-> checkList
	
	private String msg;//Message � envoyer
	private String nameStraddle;
	
	public MessageFor(int straddle, String msg){
		this.straddle = straddle;
		this.msg=msg;
	}

	public MessageFor(int straddle, String nameStraddle, String msg){
		this.straddle = straddle;
		this.nameStraddle=nameStraddle;
		this.msg=msg;
	}
	
	public boolean isStraddle() {
		return (this.straddle==1);
	}
	public boolean isCheckList(){
		return (this.straddle==3);
	}

	public String getMsg() {
		return msg;
	}

	public String getNameStraddle() {
		return nameStraddle;
	}

	public void setNameStraddle(String nameStraddle) {
		this.nameStraddle = nameStraddle;
	}	
	
}