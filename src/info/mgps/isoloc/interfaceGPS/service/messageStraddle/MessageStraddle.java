package info.mgps.isoloc.interfaceGPS.service.messageStraddle;

/**
 * Permet de transferrer les messages et informations des straddles � l'interface
 * @author adm
 *
 */
public class MessageStraddle {

	private int ind;//indice du message si=0 deconnexion; =1 connexion; =2 message; 3->checkList
	
	private String msg;//message cavalier
	private String nameStraddle;//identifiant du straddle = adresse IP
	
	public MessageStraddle(int ind, String nameStraddle, String msg){
		this.ind=ind;
		this.nameStraddle=nameStraddle;
		this.msg=msg;
	}

	public int getInd() {
		return ind;
	}

	public String getMsg() {
		return msg;
	}

	public String getNameStraddle() {
		return nameStraddle;
	}
	
	
}
