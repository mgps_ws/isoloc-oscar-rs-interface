package info.mgps.isoloc.interfaceGPS.service.messageStraddle;

import java.io.IOException;

import info.mgps.isoloc.interfaceGPS.utilitaire.LogClass;
import info.mgps.isoloc.interfaceGPS.web.web.Controllers.MyController;

public class DealMsgStraddle {

	//type login= 		1:cavalier;identifiant;0 ou 1 pour on/off
	//Type available= 	2:cavalier; 0 ou 1 pour on/off
	//type mission= 	3:cavalier;
	//Type particulier manuelRehandle= 4:cavalier;
	
	
	/**
	 * Lecture du message venant du cavalier
	 * @param message
	 * @return
	 */
	public static MessageStraddleDetails ReadMsgStraddle(String message) throws IOException{
		
		MessageStraddleDetails msgStr=new MessageStraddleDetails();
		//Trie par ';'
		String tbStr[] = message.split(";");
		try {
		
			//Memorisation du type de message et du nom du cavalier		
			String tb1[] = tbStr[0].split(":");
			msgStr.setEtat(Integer.parseInt(tb1[0])) ;
			msgStr.setName(tb1[1]) ;
			
			switch (msgStr.getEtat()) {		
			case 1://Login
				msgStr.setIdentifiant(tbStr[1]);
				//msgStr.setIdentifiant("E3B652E7");
				msgStr.setOnOff(Integer.parseInt(tbStr[2]));
				break;

			case 2://Available
				msgStr.setOnOff(Integer.parseInt(tbStr[1]));
				break;
			case 3://Mission standard

				//Etape
				msgStr.setEtape(Integer.parseInt(tbStr[1]));
	
				//Cible ou enchainement des positions
				//Normalement je dois avoir une seule position mais il est possible d'avoir plusieurs positions 
				if (tbStr.length > 2){
					msgStr.setPosition(tbStr[2]);	
				}else {
					//msgStr.setPosition("D:B0000B:123.122:1213.2254:8:2:3");	
					msgStr.setPosition("V:00000B:0:0:0:0:0");	
				}	
				
				break;
			case 4://Manuel Rehandle		
				msgStr.setPosition(tbStr[1]);	
				break;

			}
			
		} catch (Exception e) {
			LogClass.ecriture("DealMsgStraddle/ReadMsgStraddle", e.getMessage(), 3);
		}
		
		return msgStr;
	}
	
	/**
	 * Validation de Login
	 * @param cavalier = nom du cavalier
	 * @param identifiant = identifiant du chauffeur
	 * @return
	 */
	public static String sendLogin(String cavalier,String identifiant, boolean razUP){
		if(razUP){
			return "1:" + cavalier + ";" + identifiant + ";1;1"+MyController.getRAZFrequence();
		}else{
			return "1:" + cavalier + ";" + identifiant + ";1;0";
		}
		
	}
	
	/**
	 * Validation de Logout
	 * @param cavalier = nom du cavlier
	 * @param identifiant = identifiant du chauffeur
	 * @return
	 */
	public static String sendLogout(String cavalier,String identifiant){
		return "1:" + cavalier + ";" + identifiant + ";0";
	}

	/**
	 * Validation de la disponibilit�
	 * @param cavalier
	 * @return
	 */
	public static String sendAvailable(String cavalier){
		return "2:" + cavalier + ";1";
	}
	
	/**
	 * Validation de la indisponibilit�
	 * @param cavalier
	 * @return
	 */
	public static String sendUnavailable(String cavalier){
		return "2:" + cavalier + ";0";
	}
		
	/**
	 * Envoie de la mission
	 * @param cavalier
	 * @param message = message � afficher
	 * @param step	= �tape de la mission
	 * @param cible	= cible 
	 * @param displayCible = afficher cible ou pas
	 * @return
	 */
	public static String sendMission(String cavalier, String message, int step, String cible, int displayCible){
		return "3:" + cavalier + ";" + message + ";" + step + ";" + cible.replace(".", "")  + ";" + displayCible ;
	}
	
	/**
	 * Message � envoyer au cavalier 
	 * ex : message d'erreur ou info
	 * avec l'autorisation de faire du maunel Rehandle
	 * @param cavalier
	 * @param message
	 * @return
	 */
	public static String sendMessageInfo(String cavalier, String message){
		return "4:" + cavalier + ";" + message;
	}
	
	/**
	 * Message � envoyer au cavalier 
	 * ex : message d'erreur ou info
	 * avec l'impossiblilit� de faire du manuelRehandle car je suis dans une mission en cours
	 * @param cavalier
	 * @param message
	 * @return
	 */
	public static String sendMessageInfoWithoutRehandle(String cavalier, String message){
		return "7:" + cavalier + ";" + message;
	}
	
	/**
	 * Message � envoyer au cavalier 
	 * ex : message d'attente de travail
	 * @param cavalier
	 * @param message
	 * @return
	 */
	public static String sendMessageWaitWork(String cavalier, String message){
		return "5:" + cavalier + ";" + message;
	}

	/**
	 * Cas du shunt je reste dans la 'mission'
	 * @param cavalier
	 * @return
	 */
	public static String sendMessageShunt(String DisplayMsg,String cavalier){
		return "6:" + cavalier +";"+ DisplayMsg+" -- Probl�me appeler l'op�rateur";
	}
}
