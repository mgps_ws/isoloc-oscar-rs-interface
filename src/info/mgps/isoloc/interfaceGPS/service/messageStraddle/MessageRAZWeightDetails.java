package info.mgps.isoloc.interfaceGPS.service.messageStraddle;

import java.util.Date;

public class MessageRAZWeightDetails {
	private String message;
	private String realNameStraddle;
	private Date date;
	private boolean execute;
	
	
	
	public MessageRAZWeightDetails(String message){
		this.message = message;
		generateOtherFields();
	}
	
	private void generateOtherFields() {
		String[] tab = this.message.split(";");
		if(tab != null && tab.length == 3){
			this.realNameStraddle = tab [1];
			this.date = new Date();
			if(tab[2].equalsIgnoreCase("1"))
				this.execute = true;
			else
				this.execute = false;
		}
	}

	public String getRealNameStraddle() {
		return realNameStraddle;
	}


	public void setRealNameStraddle(String realNameStraddle) {
		this.realNameStraddle = realNameStraddle;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public MessageRAZWeightDetails() {
		super();
	}


	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public boolean isExecute() {
		return execute;
	}


	public void setExecute(boolean execute) {
		this.execute = execute;
	}
	
}
