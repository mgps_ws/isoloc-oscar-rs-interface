package info.mgps.isoloc.interfaceGPS.service.messageStraddle;

public class Position {

	private String lock;//Verrouillage ou d�verrouillage V ou D
	private String cible;	//ex: B0101B
	private float x;	//position x
	private float y;	//position y
	private int nbSat;	//nombre de satellites
	private int dif;	//dif 
	private int tmpsDif;	//d�lai du diff
	
	
	//*************** Constructeur****************
	public Position(String lock, String cible, float x, float y, int nbSat, int dif, int tmpsDif){
		this.lock=lock;
		this.cible=cible;
		this.x=x;
		this.y=y;
		this.nbSat=nbSat;
		this.dif=dif;
		this.tmpsDif=tmpsDif;
	}

	//*********** Ascesseurs ***************
	public String getLock() {
		return lock;
	}


	public String getCible() {
		return cible;
	}


	public double getX() {
		return x;
	}


	public double getY() {
		return y;
	}


	public int getNbSat() {
		return nbSat;
	}


	public int getDif() {
		return dif;
	}


	public int getTmpsDif() {
		return tmpsDif;
	}
		
	//******************** fonctions de remplissage
	public void setPosition(String lock, String cible, float x, float y, int nbSat, int dif, int tmpsDif){
		this.lock=lock;	
		this.cible=cible;
		this.x=x;
		this.y=y;
		this.nbSat=nbSat;
		this.dif=dif;
		this.tmpsDif=tmpsDif;
	}
	
}
