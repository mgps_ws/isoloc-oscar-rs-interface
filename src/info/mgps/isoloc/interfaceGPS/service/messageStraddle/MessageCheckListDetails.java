package info.mgps.isoloc.interfaceGPS.service.messageStraddle;

public class MessageCheckListDetails {
	private boolean fromCheckList;
	private String message;
	private String realNameStraddle;
	
	
	
	
	
	
	public String getRealNameStraddle() {
		return realNameStraddle;
	}


	public void setRealNameStraddle(String realNameStraddle) {
		this.realNameStraddle = realNameStraddle;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public MessageCheckListDetails() {
		super();
	}


	public boolean isFromCheckList() {
		return fromCheckList;
	}


	public void setFromCheckList(boolean fromCheckList) {
		this.fromCheckList = fromCheckList;
	}


	
	
}
