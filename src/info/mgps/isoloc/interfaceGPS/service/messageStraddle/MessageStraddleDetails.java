package info.mgps.isoloc.interfaceGPS.service.messageStraddle;

import java.util.HashMap;

/**
 * d�finition du message transmit entre le cavalier et l'interface
 * @author adm
 *
 */
/**
 * @author adm
 *
 */
public class MessageStraddleDetails {
	
	//************ D�clarations des variables **************
	private int etat=1;	//1=login, 2=available; 3=mission; 4=manuelRehandle
	private String name="";	//nom du straddle
	private String identifiant="";//Identifiant Ecn4
	private int OnOff=0;//1=on; 0=off
	
	//Dans le cas d'une mission
	private int Etape=1; //de 1 � 4, l'�tape 5 correspond au shifting 
	private HashMap<Integer, Position> positions;
	
	private String displayPosition="";
	
	//*************************constructeur *******************************
	public MessageStraddleDetails() {
		positions = new HashMap<Integer, Position>();
		positions.put(0, new Position("", "", 0,0,0,0,0));
	}
	
	
	//**************** Accesseurs ***********************************
	public int getEtat() {
		return etat;
	}
	
	public void setEtat(int etat) {
		this.etat = etat;
	}
	
	public String getName() {
		return name;
	}
		
	public void setName(String name) {
		this.name = name;
	}
	
	public String getIdentifiant() {
		return identifiant;
	}
	
	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}
	
	public int getOnOff() {
		return OnOff;
	}
	
	public void setOnOff(int onOff) {
		OnOff = onOff;
	}


	public int getEtape() {
		return Etape;
	}


	public void setEtape(int etape) {
		Etape = etape;
	}

	public String getDisplayPosition() {
		return displayPosition;
	}

	public int positionsCount(){
		return this.positions.size();
	}
	
	public Position getPositions(int indice) {
		return this.positions.get(indice);
	}

	public void setPosition(String position) {
		String StrPo[] = position.split("!");
		String strValue[] = StrPo[0].split(":");
		if (strValue.length > 1 ){
			//Enregistrement de la premi�re position 
			this.positions.get(0).setPosition(strValue[0], AddPointCibleAndZC( strValue[1]), Float.parseFloat(strValue[2].replace(",", ".")),Float.parseFloat(strValue[3].replace(",", ".")),
						Integer.parseInt(strValue[4]),Integer.parseInt(strValue[5]),Integer.parseInt(strValue[6]));

			this.displayPosition = strValue[0] + ":" + AddPointCibleAndZC(strValue[1]);

			//Boucle si j'ai enregistrement de position sup�rieure � 1 
			for (int i = 1; i < StrPo.length; i++) {
				strValue = StrPo[i].split(":");
				
				this.positions.put(i, new Position(strValue[0],AddPointCibleAndZC(strValue[1]), Float.parseFloat(strValue[2]),Float.parseFloat(strValue[3]),
						Integer.parseInt(strValue[4]),Integer.parseInt(strValue[5]),Integer.parseInt(strValue[6])));
						
				this.displayPosition += "+" + strValue[0] + ":" + AddPointCibleAndZC(strValue[1]);				
			}
		}else {
			this.displayPosition = position;
		}

	
	}
	// Fonction qui permet d'ajouter le point avec l'info capteur B, M ou H
	// Pour le probl�me de prendre en ZC ne pas ajouter le .B 
	private String AddPointCibleAndZC(String cible){
		try {
			if (cible.startsWith("ZC")){
				/*if (cible.charAt(cible.length()-1)==66 || cible.charAt(cible.length()-1)==77 || cible.charAt(cible.length()-1)==72){
					cible=cible.substring(0, cible.length()-1) + ".B";
				}*/
	//			System.out.println("cible = "+cible);
				if ((cible.charAt(cible.length()-1)==66 || cible.charAt(cible.length()-1)==77 || cible.charAt(cible.length()-1)==72)
				&& (Etape == 2)){
					
					cible=cible.substring(0, cible.length()-1);
					//System.out.println("cible etape 2 = "+cible);
				}else if (cible.charAt(cible.length()-1)==66 || cible.charAt(cible.length()-1)==77 || cible.charAt(cible.length()-1)==72){
					cible=cible.substring(0, cible.length()-1) + ".B";
					//System.out.println("cible etape autre = "+cible);
					
				}
			}else {
				if (cible.length() > 4) {
				//66=B; 77=M; 72=H
					if (cible.charAt(cible.length()-1)==66 || cible.charAt(cible.length()-1)==77 || cible.charAt(cible.length()-1)==72){
						StringBuffer cibleBuffer  = new StringBuffer(cible);
						cibleBuffer = cibleBuffer.insert(cible.length()-1, ".");
						cible = cibleBuffer.toString();
					}
				}
			}
		} catch (Exception e) {
		}
		return cible;
	}
/*	// Fonction qui permet d'ajouter le point avec l'info capteur B, M ou H
	private String AddPointCibleAndZC(String cible){
		try {
			if (cible.startsWith("ZC")){
				if (cible.charAt(cible.length()-1)==66 || cible.charAt(cible.length()-1)==77 || cible.charAt(cible.length()-1)==72){
					cible=cible.substring(0, cible.length()-1) + ".B";
				}
				// A ajouter pour le cas ou on r�cup�re un TC depuis la zone d'�change
				// PB Navis n'envoie pas le .B/.H/.M dans la ppos, � nous d'anticiper le coup sur la position qui vient du GPS
				
				if ((cible.charAt(cible.length()-1)==66 || cible.charAt(cible.length()-1)==77 || cible.charAt(cible.length()-1)==72)&& Etape == 2){
					cible=cible.substring(0, cible.length()-1);
				} else if (cible.charAt(cible.length()-1)==66 || cible.charAt(cible.length()-1)==77 || cible.charAt(cible.length()-1)==72){
					cible=cible.substring(0, cible.length()-1) + ".B";
				}
			}else {
				if (cible.length() > 4) {
				//66=B; 77=M; 72=H
					if (cible.charAt(cible.length()-1)==66 || cible.charAt(cible.length()-1)==77 || cible.charAt(cible.length()-1)==72){
						StringBuffer cibleBuffer  = new StringBuffer(cible);
						cibleBuffer = cibleBuffer.insert(cible.length()-1, ".");
						cible = cibleBuffer.toString();
					}
				}
			}
		} catch (Exception e) {
		}
		return cible;
	}*/
	
}
