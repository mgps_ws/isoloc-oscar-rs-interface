package info.mgps.isoloc.interfaceGPS.service.messageStraddle;

public class DealMessageCheckList {

	public static MessageCheckListDetails readMsgCheckList(String message) {
		// TODO lire le message checkList
		MessageCheckListDetails details = new MessageCheckListDetails();
		if(message.contains("ccheck")){
			// message venant de la checklist
			details.setFromCheckList(true);
		}
		String newMessage = message.replaceAll("ccheck;", "").replaceAll("scheck;", "");
		String nameStraddle = newMessage.split(";")[0];
		details.setRealNameStraddle(nameStraddle);
		if(details.isFromCheckList())
			details.setMessage(newMessage.replace(nameStraddle, "check"));
		else
			details.setMessage(newMessage);
		
		return details;
	}

}
