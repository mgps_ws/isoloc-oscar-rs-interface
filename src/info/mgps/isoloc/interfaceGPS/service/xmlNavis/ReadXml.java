package info.mgps.isoloc.interfaceGPS.service.xmlNavis;

import info.mgps.isoloc.interfaceGPS.utilitaire.LogClass;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class ReadXml {
	
	
	/****
	 * 
	 * 
	 * 
	 * */

	public static MessageXmlRead read(String xmlFile){
		//Initialisation
		MessageXmlRead msgxml = new MessageXmlRead();
		try {

			xmlFile = xmlFile.substring(xmlFile.indexOf("<message"));
				
			final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			
			final DocumentBuilder builder = factory.newDocumentBuilder();
			InputSource in = new InputSource(new StringReader(xmlFile));
			final Document document= builder.parse(in);
			//final Document document= builder.parse( xmlFile);	//Lecture directe d'un fichier xml
			
			final Element racine = document.getDocumentElement();
		
			//Recuperation de la formId
		    msgxml.setFormid(racine.getAttribute("formId"));
			
		    final NodeList racineNoeuds = racine.getChildNodes();
		    final int nbRacineNoeuds = racineNoeuds.getLength();
		
		    // Nombre de noeud
		    if (nbRacineNoeuds > 0) {
		        if(racineNoeuds.item(nbRacineNoeuds-1).getNodeType() == Node.ELEMENT_NODE) {
		        	 //*********** che ************	
		
		        	final Element che = (Element) racineNoeuds.item(nbRacineNoeuds-1);
				    //Recuperation de chid, equipType et status
		        	msgxml.setChid(che.getAttribute("CHID"));
		        	
		        	msgxml.setEquipType(che.getAttribute("equipType"));
		        	msgxml.setStatus(che.getAttribute("status"));
		  		    				    
				    final NodeList works = che.getElementsByTagName("work");		    	
				    if (works.getLength() > 0) {
				    	final Element work = (Element) works.item(0);

				    	//Job
				    	final NodeList jobs = work.getElementsByTagName("job");
				    	if (jobs.getLength() > 0){
				    		final Element job = (Element) jobs.item(0);
				    		msgxml.setTwinPut(job.getAttribute("twinPut")); 	
				    		msgxml.setIngress(job.getAttribute("ingress")); 
				    		msgxml.setMvkd(job.getAttribute("MVKD")); 
				    	}
				    	
					    //Container
				    	final NodeList containers = work.getElementsByTagName("container");
				    	if (containers.getLength() > 0){
				    		final Element container = (Element) containers.item(0);
				    		msgxml.getContainer().addContainer(container.getAttribute("EQID"), container.getAttribute("LNTH") ,
				    				container.getAttribute("JPOS"),container.getAttribute("ISHZ"),container.getAttribute("QWGT"),
				    				container.getAttribute("OD"),container.getAttribute("ODMH"),container.getAttribute("ODMR"),
				    				container.getAttribute("ODML"),container.getAttribute("ODMF"),container.getAttribute("ODMA"),
				    				container.getAttribute("ACRR"),container.getAttribute("DCRR"));				    		
				    	}
				    				    
					    // Positions
					    final NodeList positons = work.getElementsByTagName("position");
					    if (positons.getLength() > 1){
					    	final Element position0 = (Element) positons.item(0);
					    	final Element position1 = (Element) positons.item(1);
					    	msgxml.getPositionPrendre().addPosition(position0.getAttribute("AREA_TYPE"), position0.getAttribute("AREA"), position0.getAttribute("PPOS")
					    			, position0.getAttribute("TKPS"), position0.getAttribute("VBAY"));
					    	msgxml.getPositionPoser().addPosition(position1.getAttribute("AREA_TYPE"), position1.getAttribute("AREA"), position1.getAttribute("PPOS")
					    			, position1.getAttribute("TKPS"), position1.getAttribute("VBAY"));
					    }
				    
				    }
				     //Message d'erreur dans le che
				    final NodeList errmsgs = che.getElementsByTagName("errmsg");
				    if (errmsgs.getLength() > 0) {
				    	//Recuperation des erreurs
				    	final Element errmsg = (Element) errmsgs.item(0);
				    	msgxml.setErrorMsgID(errmsg.getAttribute("msgID"));
				    	msgxml.setDisplayMsg(errmsg.getTextContent());

				    }
				    				    
				    //*********** display Message ************	
				    final NodeList displayMsgs = che.getElementsByTagName("displayMsg");
				    if (displayMsgs.getLength() > 0) {
				    	//Recuperation des erreurs
				    	final Element displayMsg = (Element) displayMsgs.item(0);
				    	msgxml.setErrorMsgID(displayMsg.getAttribute("msgID"));
				    	msgxml.setDisplayMsg(displayMsg.getTextContent());

				    }
		        }
		    }
		 	    
		} catch (ParserConfigurationException e) {
			LogClass.ecriture("ReadXml", e.getMessage(), 3);
			return null;
		} catch (SAXException e) {
			LogClass.ecriture("ReadXml", e.getMessage(), 3);
			return null;
		} catch (IOException e) {
			LogClass.ecriture("ReadXml", e.getMessage(), 3);
			return null;
		} catch(ArrayIndexOutOfBoundsException e){
			LogClass.ecriture("ReadXml", e.getMessage(), 3);
			return null;
		}
			
		return msgxml;
		
	}
	
}
