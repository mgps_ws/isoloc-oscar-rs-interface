package info.mgps.isoloc.interfaceGPS.service.xmlNavis;

import info.mgps.isoloc.interfaceGPS.web.web.Controllers.MyController;

/**
 * 
 * @author adm
 * listes des frames � envoyer � Ecn4
 *
 */
public class MessageXmlWrite {


	/**
	 * Message de connexion
	 * @param login
	 * @param chid
	 * @return message xml login
	 */
	public static String sendLogin( int msid, String login, String chid){
		return "<message type=\"2632\" MSID=\"" +  msid + "\" formId =\"FORM_LOGIN\"><che CHID=\"" + chid + "\"><user AVAL =\"L\" LOGN = \"" + 
		login + "\" PGRM=\"CHE\"/></che></message>";
		
	}
	
	/**
	 * Message de deconnexion
	 * @param chid
	 * @return
	 */
	public static String sendDeconnexion(int msid, String chid, int actualFormId){
		if(actualFormId==2){
			// State idle
			if(MyController.getSdkVersion() == (float)3.7){
				return "<message MSID=\"" + msid + "\" type=\"2632\" formId=\"FORM_IDLE\"><che CHID=\"" + chid + "\"><user AVAL=\"X\"/></che> </message>";
			}
			return "<message MSID=\"" + msid + "\" type=\"2632\" formId=\"FORM_STRADDLE_IDLE\"><che CHID=\"" + chid + "\"><user AVAL=\"X\"/></che> </message>";
		}else
			if(actualFormId == 1){
				// State unavailable
				if(MyController.getSdkVersion() == (float)3.7){
					return "<message MSID=\"" + msid + "\" type=\"2632\" formId=\"FORM_UNAVAILABLE\"><che CHID=\"" + chid + "\"><user AVAL=\"X\"/></che> </message>";
				}
				return "<message MSID=\"" + msid + "\" type=\"2632\" formId=\"FORM_STRADDLE_UNAVAILABLE\"><che CHID=\"" + chid + "\"><user AVAL=\"X\"/></che> </message>";
			}
		
		return "";		
	}
	
	/**
	 * Pour actualFormId == 2
	 * @param msid
	 * @param chid
	 * @return
	 */
	public static String sendDeconnexionIddle(int msid, String chid){
		if(MyController.getSdkVersion() == (float)3.7){
			return "<message MSID=\"" + msid + "\" type=\"2632\" formId=\"FORM_IDLE\"><che CHID=\"" + chid + "\"><user AVAL=\"X\"/></che> </message>";
		}
		return "<message MSID=\"" + msid + "\" type=\"2632\" formId=\"FORM_STRADDLE_IDLE\"><che CHID=\"" + chid + "\"><user AVAL=\"X\"/></che> </message>";
	}
	
	/***
	 * Pour actualFormId == 1
	 * @param msid
	 * @param chid
	 * @param actualFormId
	 * @return
	 */
	public static String sendDeconnexionAvailable(int msid, String chid){
		if(MyController.getSdkVersion() == (float)3.7){
			return "<message MSID=\"" + msid + "\" type=\"2632\" formId=\"FORM_UNAVAILABLE\"><che CHID=\"" + chid + "\"><user AVAL=\"X\"/></che> </message>";
		}
		return "<message MSID=\"" + msid + "\" type=\"2632\" formId=\"FORM_STRADDLE_UNAVAILABLE\"><che CHID=\"" + chid + "\"><user AVAL=\"X\"/></che> </message>";
	}
	
	public static String sendDeconnexionToOrigin(int msid, String chid, String ppos){
		return "<message type=\"2630\" MSID=\"" + msid + "\" formId=\"FORM_EMPTY_TO_ORIGIN\"><che CHID=\"" + chid + "\" action=\"X\"><position PPOS=\"" 
				+ ppos + "\"/></che> </message>";
	}
		
	public static String sendDeconnexionAtOrigin(int msid, String chid, String ppos, String container, String jpos, String spos){
		return "<message type=\"2630\" MSID=\"" + msid + "\" formId=\"FORM_EMPTY_AT_ORIGIN\"><che CHID=\"" + chid + "\" action=\"X\" SPOS=\"" + spos + 
				"\"><container EQID=\"" + container + "\" JPOS=\"" + jpos + "\"/><position PPOS=\"" + ppos + "\" JPOS=\"" + jpos + "\"/></che> </message>";
	}
	
	public static String sendDeconnexionToDest(int msid, String chid, String ppos){
		return "<message type=\"2630\" MSID=\"" + msid + "\" formId=\"FORM_LADEN_TO_DEST\"><che CHID=\"" + chid + "\" action=\"X\"><position PPOS=\"" +
				ppos + "\"/></che> </message>";
	}
	
	public static String sendDeconnexionAtDest(int msid, String chid, String ppos, String jpos){
		return "<message type=\"2630\" MSID=\"" + msid + "\" formId=\"FORM_LADEN_AT_DEST\"><che CHID=\"" + chid + "\" action=\"X\"><position PPOS=\"" + 
				ppos + "\" JPOS=\"" + jpos + "\"/></che> </message>";
	}
	
	public static String sendDeconnexionAutoRehandle(int msid, String chid, String ppos, String container){
		if(MyController.getSdkVersion() == (float)3.7){
			return "<message type=\"2630\" MSID=\"" + msid + "\" formId=\"FORM_AUTO_REHANDLE\"><che CHID=\"" + chid +
					"\" action=\"X\"><container EQID=\"" + container + "\"><position PPOS=\"" + ppos + "\"/></container></che></message>";
		}
		return "<message type=\"2630\" MSID=\"" + msid + "\" formId=\"FORM_STRADDLE_AUTO_REHANDLE\"><che CHID=\"" + chid +
				"\" action=\"X\"><container EQID=\"" + container + "\"><position PPOS=\"" + ppos + "\"/></container></che></message>";
	}
		
	public static String sendDeconnexionManuRehandle(int msid, String chid, String ppos, String container) {
		if(MyController.getSdkVersion() == (float)3.7){
			return "<message type=\"2630\" MSID=\"" + msid + "\" formId=\"FORM_REHANDLE\"><che CHID=\"" + chid + 
					"\" action=\"X\"><container EQID=\"" +  container + "\"><position PPOS=\"" + ppos + "\"/></container></che></message>";
		}
		return "<message type=\"2630\" MSID=\"" + msid + "\" formId=\"FORM_STRADDLE_MANUAL_REHANDLE\"><che CHID=\"" + chid + 
				"\" action=\"X\"><container EQID=\"" +  container + "\"><position PPOS=\"" + ppos + "\"/></container></che></message>";
	}
		
	/**
	 * Message pour rendre disponible
	 * @param chid
	 * @return
	 */
	public static String sendAvailable(int msid, String chid){
		if(MyController.getSdkVersion() == (float)3.7){
			return "<message type=\"2632\" MSID=\"" + msid + "\" formId=\"FORM_UNAVAILABLE\"><che CHID=\"" + chid + "\"><user AVAL=\"A\"/></che> </message>";
		}
		return "<message type=\"2632\" MSID=\"" + msid + "\" formId=\"FORM_STRADDLE_UNAVAILABLE\"><che CHID=\"" + chid + "\"><user AVAL=\"A\"/></che> </message>";
	}
	
	/**
	 * Message pour rendre disponible
	 * @param chid
	 * @return
	 */
	public static String sendUnavailable(int msid, String chid){
		if(MyController.getSdkVersion() == (float)3.7){
			return "<message MSID=\"" + msid + "\" type=\"2632\" formId=\"FORM_IDLE\"><che CHID=\"" + chid + "\"><user AVAL=\"U\" PGRM=\"CHE\"/></che> </message>";
		}
		return "<message MSID=\"" + msid + "\" type=\"2632\" formId=\"FORM_STRADDLE_IDLE\"><che CHID=\"" + chid + "\"><user AVAL=\"U\" PGRM=\"CHE\"/></che> </message>";
	}
	
	/**
	 * Message etape 1 de la mission 'empty to origin'
	 * Direction pour prendre
	 * @param chid
	 * @param ppos
	 * @return
	 */
	public static String sendEmptyToOrigin(int msid, String chid, String ppos){
		return "<message type=\"2630\" MSID=\"" + msid + "\" formId=\"FORM_EMPTY_TO_ORIGIN\"><che CHID=\"" + chid + "\" action=\"B\"><position PPOS=\"" 
				+ ppos + "\"/></che> </message>";
	}
	
	/**
	 * Message etape 2 de la mission 'empty at origin'
	 * prendre container
	 * 
	 * @param msid
	 * @param chid
	 * @param ppos
	 * @param container
	 * @param jpos
	 * @param spos 2-> 20 pieds / 4 -> 40 pieds
	 * @return
	 */
	public static String sendEmptyAtOrigin(int msid, String chid, String ppos, String container, String spos){
		return "<message type=\"2630\" MSID=\"" + msid + "\" formId=\"FORM_EMPTY_AT_ORIGIN\"><che CHID=\"" + chid + "\" action=\"L\" SPOS=\"" + spos + 
				"\"><container EQID=\"" + container + "\" JPOS=\"CTR\"/><position PPOS=\"" + ppos + "\" JPOS=\"CTR\"/></che> </message>";
	}
	
	/**
	 * Message etape 3 de la mission 'laden to dest'
	 * Direction pour poser
	 * @param chid
	 * @param ppos
	 * @return
	 */
	public static String sendLadenToDest(int msid, String chid, String ppos){
		return "<message type=\"2630\" MSID=\"" + msid + "\" formId=\"FORM_LADEN_TO_DEST\"><che CHID=\"" + chid + "\" action=\"B\"><position PPOS=\"" +
				ppos + "\"/></che> </message>";
	}
	
	/**
	 * Message etape 4 de la mission 'laden at dest'
	 * poser container
	 * @param chid
	 * @param ppos
	 * @param jpos
	 * @return
	 */
	public static String sendLadenAtDest(int msid, String chid, String ppos){
		return "<message type=\"2630\" MSID=\"" + msid + "\" formId=\"FORM_LADEN_AT_DEST\"><che CHID=\"" + chid + "\" action=\"D\"><position PPOS=\"" + 
				ppos + "\" JPOS=\"CTR\"/></che> </message>";
	}
	
	/**
	 * Message de auto rehandle
	 * Message de shifting entre l'�tape 'to origin' et 'at origin'
	 * @param msid
	 * @param chid
	 * @param container
	 * @param ppos
	 * @return
	 */
	public static String sendAutoRehandle(int msid, String chid, String ppos, String container){
		if(MyController.getSdkVersion() == (float)3.7){
			return "<message type=\"2630\" MSID=\"" + msid + "\" formId=\"FORM_AUTO_REHANDLE\"><che CHID=\"" + chid +
					"\" action=\"C\"><container EQID=\"" + container + "\"><position PPOS=\"" + ppos + "\"/></container></che></message>";
		}
		return "<message type=\"2630\" MSID=\"" + msid + "\" formId=\"FORM_STRADDLE_AUTO_REHANDLE\"><che CHID=\"" + chid +
				"\" action=\"C\"><container EQID=\"" + container + "\"><position PPOS=\"" + ppos + "\"/></container></che></message>";
	}
	
	/**
	 * Message de manuel rehandle etape 1
	 * envoie seulement de la position GPS de la prise d'un conteneur
	 * @param msid
	 * @param chid
	 * @param ppos
	 * @return
	 */
	public static String sendManuelRehandleEtape1(int msid, String chid, String ppos){
		if(MyController.getSdkVersion() == (float)3.7){
			return "<message MSID=\"" + msid + "\" type=\"2630\" formId=\"FORM_IDLE\"><che CHID=\"" + chid + 
					"\" action=\"M\"><position PPOS=\"" + ppos + "\" JPOS=\"CTR\" /></che></message>";
		}
		return "<message MSID=\"" + msid + "\" type=\"2630\" formId=\"FORM_STRADDLE_IDLE\"><che CHID=\"" + chid + 
				"\" action=\"M\"><position PPOS=\"" + ppos + "\" JPOS=\"CTR\" /></che></message>";
	}
	
	/**
	 * Message de manuel rehandle �tape 2
	 * Transmission de la position de d�verrouillage du conteneur
	 * @param msid
	 * @param chid
	 * @param ppos
	 * @param container
	 * @return
	 */
	public static String sendManuelRehandleEtape2(int msid, String chid, String ppos, String container) {
		if(MyController.getSdkVersion() == (float)3.7){
			return "<message type=\"2630\" MSID=\"" + msid + "\" formId=\"FORM_REHANDLE\"><che CHID=\"" + chid + 
					"\" action=\"C\"><container EQID=\"" +  container + "\"><position PPOS=\"" + ppos + "\"/></container></che></message>";
		}
		return "<message type=\"2630\" MSID=\"" + msid + "\" formId=\"FORM_STRADDLE_MANUAL_REHANDLE\"><che CHID=\"" + chid + 
				"\" action=\"C\"><container EQID=\"" +  container + "\"><position PPOS=\"" + ppos + "\"/></container></che></message>";
	}
	
	
}
