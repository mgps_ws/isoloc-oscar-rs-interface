package info.mgps.isoloc.interfaceGPS.service.xmlNavis;

public class Position {
	
	//******** D�claration des variables
	private String area_Type = "";	//Type de position YardRow, Vessel 
	private String area = ""; //direction
	private String ppos ="";	//Position complete
	private String tkps=""; //Position pour zone d'�change 1=avant; 2=milieu; 3=arri�re
	private String vbay="";//bay 
	//private String shipName="";//Nom du navire
	
	//**************** Assesseurs
	public String getArea_Type() {
		return area_Type;
	}
	public String getArea() {
		return area;
	}
	public String getPpos() {
		return ppos;
	}	
	public String getTkps() {
		return tkps;
	}
	public String getVbay() {
		return vbay;
	}
	//*********** Constructeur
	public Position(){		
	}

	public void addPosition(String area_Typ, String area, String ppos, String tkps,String bay){
		 this.area_Type=area_Typ;
		 this.area=area;
		 this.ppos=ppos;
		 this.tkps = tkps;
		 this.vbay=bay;
	}
}
