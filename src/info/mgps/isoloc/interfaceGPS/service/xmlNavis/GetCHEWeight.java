package info.mgps.isoloc.interfaceGPS.service.xmlNavis;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.catalina.util.Base64;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import info.mgps.isoloc.interfaceGPS.service.log.AccessDb;
import info.mgps.isoloc.interfaceGPS.utilitaire.LogClass;
import info.mgps.isoloc.interfaceGPS.web.web.Controllers.MyController;
import info.mgps.isoloc.interfaceGPS.web.web.Controllers.VGM;

public class GetCHEWeight extends Thread{

		
		private String containerID;
		private float cheWeight;
		private String straddleName;
		private float jobWeight;
		private int fred;
		
		
		
		/**
		 * Constructeur avec le num�ro de visite
		 * @param containerID
		 */
		public GetCHEWeight(String containerID, float cheWeight, String straddleName, float jobWeight){
			
			//System.out.println("mes couilles");
			
			this.containerID = containerID;
			this.cheWeight = cheWeight;
			this.straddleName = straddleName;
			this.jobWeight = jobWeight;
			/**/
			
		}
		
		public GetCHEWeight(String containerID, float cheWeight, String straddleName, float jobWeight, int fred){
			
			
			
			this.containerID = containerID;
			this.cheWeight = cheWeight;
			this.straddleName = straddleName;
			this.jobWeight = jobWeight;
			this.fred = fred;
			/**/
			
		}
		




		/**
		 * Lecture et extraction du vesselname depuis la r�ponse xml
		 * @param message
		 * @return
		 */
		private String read(String message){
			// Lecture du message xml de r�ponse
			String retour= "";
			if(message.contains("<data-table filter=\"CHEWEIGHT\" count=\"0\">")){
				// pas de correspondance
				return "";			
			}else{
				final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				DocumentBuilder builder;
				try {
					builder = factory.newDocumentBuilder();
					InputSource s = new InputSource(new StringReader(message)); 
					final Document document= builder.parse(s);
					final Element racine = document.getDocumentElement();
					NodeList fields = racine.getElementsByTagName("field");
									
					//System.out.println("cntrID "+fields.item(0).getFirstChild().getTextContent());
					
					//System.out.println("length "+fields.item(8).getFirstChild().getTextContent());
					
					String ctnrID = "";
					if(fields.item(0) != null){
						if(fields.item(0).getFirstChild() != null){
							if(fields.item(0).getFirstChild().getTextContent() != null && !fields.item(0).getFirstChild().getTextContent().equals("")){
								ctnrID = fields.item(0).getFirstChild().getTextContent();
							}
						}
							
					}
					String cheWeight = ""; 
					//System.out.println("fields.item(8).getFirstChild().getTextContent() = "+fields.item(8).getFirstChild().getTextContent() );
					if(fields.item(8) != null){
						if(fields.item(8).getFirstChild() != null){
							if(fields.item(8).getFirstChild().getTextContent() != null && !fields.item(8).getFirstChild().getTextContent().equals("")){
								cheWeight = fields.item(8).getFirstChild().getTextContent();
							}
						}
							
					}
					retour = ctnrID+":"+cheWeight;;
			 
				} catch (ParserConfigurationException e) {
					e.printStackTrace();
				} catch (SAXException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				
			}		
			return retour;			
		}

		
		/**
		 * r�cup�ration de la r�ponse de l'url Navis
		 * @param adress l'url de base
		 * @param keys liste des noms des param�tres
		 * @param values liste des valeurs de ces param�tres
		 * @return le nom du Vessel
		 * @throws IOException
		 */
		@SuppressWarnings("static-access")
		public String doGet(String adress, String containerID) throws IOException{
			try{			
			String result = "";
			BufferedReader reader = null;
				LogClass.ecriture("GetCheWeight", this.straddleName+" ddress dans doget "+adress+" containerID "+containerID, 1);
				//System.out.println(this.straddleName+" ddress dans doget "+adress+" containerID "+containerID);
				
				String urlString = adress+"?"+"filtername=CHEWEIGHT&PARM_CTRID="+containerID+"&operatorId=EUROFOS&complexId=FRFOS&facilityId=TDM&yardId=TDM";
				if(MyController.isNavisValidation())
					urlString = adress+"?"+"filtername=CHEWEIGHT&PARM_CTRID="+containerID+"&operatorId=GEN&complexId=STRESS&facilityId=TEST&yardId=GEN";
				LogClass.ecriture("GetCheWeight", "URL: "+urlString, 1);
				//System.out.println("URL: "+urlString);
				URL url = new URL(urlString);
				//filtername=CHEWEIGHT&PARM_CTRID=null&operatorId=EUROFOS&complexId=FRFOS&facilityId=TDM&yardId=TDM
				
				String userPassword = "aptapi:password";
				/*if(MyController.isNavisValidation())
					userPassword = "n4api:lookitup";*/
				String encoding = new String(new Base64().encode(userPassword
						.getBytes()));
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				// add reuqest header
				conn.setRequestMethod("GET");
				conn.setRequestProperty("Content-Type", "text/xml");
				conn.setRequestProperty("charset", "UTF-8");
				conn.setRequestProperty("Authorization", "Basic " + encoding);
				conn.setDoOutput(true);
				// envoi de la requ�te
				
				
				// lecture de la r�ponse
				reader = new BufferedReader(new InputStreamReader(
						(InputStream) conn.getContent()));
				String ligne;
				while ((ligne = reader.readLine()) != null) {
					result += ligne;
				}
				return read(result);
			}catch(MalformedURLException e){
				e.printStackTrace();
				return null;
			}
			
		}


		@Override
		public void start() {
			//super.start();
			try {
				String result = doGet("http://"+MyController.getIpAddressWebServiceCheWeightRequest()+"/apex/api//query",containerID);
				if(result != null){
					LogClass.ecriture("GetCheWeight", this.straddleName+" result du doGet "+result, 1);
					//System.out.println(this.straddleName+" result du doGet "+result);
					String[] res = result.split(":");
					if(res.length <2 || res[1] == null || res[1].equals("")|| Float.valueOf(res[1]) == 0){
						LogClass.ecriture("GetCheWeight", "je suis dans le cas ou j'ecris un poids", 1);
						//System.out.println("je suis dans le cas ou j'ecris un poids");
						// ecriture log dans la base
						//// date, cavalier, conteneur, poids mission, poids constat�, etat (reussi, echec, doute), ecriture TOS ou non
						LogClass.ecriture("GetCheWeight", this.straddleName+" MyController.getVGMUser() "+MyController.getVGMUser()+" MyController.getVGMPassword() "+MyController.getVGMPassword(), 1);
						//System.out.println(this.straddleName+" MyController.getVGMUser() "+MyController.getVGMUser()+" MyController.getVGMPassword() "+MyController.getVGMPassword());
						LogClass.ecriture("GetCheWeight", this.straddleName+" this.containerID "+this.containerID+" this.cheWeight "+this.cheWeight+" MyController.getIpAddressWebServiceCheWeightInsert() "+MyController.getIpAddressWebServiceCheWeightInsert(), 1);
						//System.out.println(this.straddleName+" this.containerID "+this.containerID+" this.cheWeight "+this.cheWeight+" MyController.getIpAddressWebServiceCheWeightInsert() "+MyController.getIpAddressWebServiceCheWeightInsert());
						Date dateInsert = new Date();
						int resultVGM = VGM.insertVGMTOS(MyController.getVGMUser(), MyController.getVGMPassword(), this.straddleName, this.containerID, this.cheWeight, MyController.getIpAddressWebServiceCheWeightInsert(), new SimpleDateFormat("yyyy-MM-dd").format(dateInsert)+"T"+new SimpleDateFormat("HH:mm:ss").format(dateInsert));
						
						if(fred == 0){
							int doute = 1;
							if(Math.abs((this.jobWeight*1000 - this.cheWeight)/100) > 5){
								doute = 2;
							}
							if(resultVGM != -1)
								AccessDb.writeVGMInfos(new Date(), this.straddleName,  this.containerID, this.jobWeight, this.cheWeight,doute,true);
							else
								AccessDb.writeVGMInfos(new Date(), this.straddleName,  this.containerID, this.jobWeight, this.cheWeight,doute,false);
						}							
						else
							AccessDb.writeVGMFred(fred, containerID);
						
					}else{
						LogClass.ecriture("GetCheWeight", this.straddleName+" je suis dans le cas ou je n'ecris pas le poids -- cause poids existant ou autre", 1);
						//System.out.println(this.straddleName+" je suis dans le cas ou je n'ecris pas le poids -- cause poids existant ou autre");
						// ecriture log dans la base
						//// date, cavalier, conteneur, poids mission, poids constat�, etat (reussi, echec, doute), ecriture TOS ou non
						LogClass.ecriture("GetCheWeight", this.straddleName+" MyController.getVGMUser() "+MyController.getVGMUser()+" MyController.getVGMPassword() "+MyController.getVGMPassword(), 1);
						//System.out.println(this.straddleName+" MyController.getVGMUser() "+MyController.getVGMUser()+" MyController.getVGMPassword() "+MyController.getVGMPassword());
						LogClass.ecriture("GetCheWeight", this.straddleName+" this.containerID "+this.containerID+" this.cheWeight "+this.cheWeight+" MyController.getIpAddressWebServiceCheWeightInsert() "+MyController.getIpAddressWebServiceCheWeightInsert(), 1);
						//System.out.println(this.straddleName+" this.containerID "+this.containerID+" this.cheWeight "+this.cheWeight+" MyController.getIpAddressWebServiceCheWeightInsert() "+MyController.getIpAddressWebServiceCheWeightInsert());
						if(fred == 0){
							int doute = 1;
							if(Math.abs((this.jobWeight*1000 - this.cheWeight)/100) > 5){
								doute = 2;
							}
							AccessDb.writeVGMInfos(new Date(), this.straddleName, this.containerID, this.jobWeight, this.cheWeight,doute,false);
						}							
						else
							AccessDb.writeVGMFred(fred, containerID);
						// ecriture succes dans le TOS et la base						
					}
				}else{
					AccessDb.writeVGMInfos(new Date(), this.straddleName, this.containerID, this.jobWeight, this.cheWeight,2,false);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}catch (NumberFormatException e) {
				e.printStackTrace();
			}
			
		}

}
