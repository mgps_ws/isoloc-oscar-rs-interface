package info.mgps.isoloc.interfaceGPS.service.xmlNavis;

import java.text.DecimalFormat;


public class Container {

	//******** D�claration des variables
	private String eqID="";
	private String lnth="";
	private String jpos="";
	private String dgx ="";
	private float weight= 2;
	private DecimalFormat df = new DecimalFormat("0.0");
	
	private String oog ="";// OH OL OW maxi les 3 cas
	private String shipNumberUnload="";//Num�ro de visite pour la d�charge
	private String shipNumberLoad="";//Num�ro de visite pour la charge
	
	//**************** Assesseurs
	public String getEqID() {
		return eqID;
	}
	public String getLnth() {
		return lnth;
	}
	public String getJpos() {
		return jpos;
	}	
	public String getDgx() {
		return dgx;
	}
	public String getWeight() {
		return df.format(weight);
	}
	
	public String getOog(){
		return oog;
	}
	
	public String getShipNumberLoad() {
		return shipNumberLoad;
	}
	
	public String getShipNumberUnload() {
		return shipNumberUnload;
	}
	
	//*********** Constructeur
	public Container() {
	}
	
	//****** Fonctions
	public void addContainer(String eqID, String lnth,String jpos,String dgx,String weight,String od,String odmh,
			String odmr,String odml,String odmf, String odma,String vesselUnload,String vesselLoad){
		this.eqID=eqID;
		this.lnth=lnth;
		this.jpos=jpos;
		if (dgx.equalsIgnoreCase("Y")){
			this.dgx=" dangereux";
		}else {
			this.dgx="";
		}
		if (od.equalsIgnoreCase("Y")){
			if (!odmh.equals("0")){
				oog ="OH ";
			}
			if (!odmr.equals("0") || !odml.equals("0")){
				oog = oog + "OW ";
			}
			if (!odmf.equals("0") || !odma.equals("0")){
				oog =oog +"OL ";
			}
		}
		
		this.shipNumberLoad=vesselLoad;
		this.shipNumberUnload=vesselUnload;
		
		//le poids est en kg il faut que je le transforme en tonne:
		try {
			this.weight= Float.parseFloat(weight) / 1000;		
		} catch (Exception e) {
			this.weight=2;
		}
	}
	
}
