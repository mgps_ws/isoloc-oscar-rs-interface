package info.mgps.isoloc.interfaceGPS.service.xmlNavis;

public class MessageXmlRead {

	//******** D�claration des variables
	private String formid="" ;//nom de la forme
	private String chid="" ;//Nom du cavalier
	private String equipType="" ;//Type d"equipement
	private String status="";//Statut du straddle
	
	private String errorMsgID="" ;//Code du message d'erreur
	private String displayMsg="" ;//Message d'erreur
	
	private Container container = null;
	
	private Position positionPrendre = null;
	private Position positionPoser = null;
	
	private String twinPut="";//Num�ro de conteneur twin lors d'une d�pose au portique
	private String ingress = ""; //LOW=Nord, High = Sud
	
	//identifiant integer pour les formes
	//1=form_login ; 				2=form_straddle_unavailable
	//3=form_empty_to_origin ; 		4=form_empty_at_origin
	//5=form_laden_to_dest; 		6=form_laden_at_dest
	//7=form_straddle_idle wait ; 	8=form_straddle_auto_rehandle �tape 1
	//9=form_straddle_auto_rehandle �tape 2;			10=form_straddle_manual_rehandle
	private int formidInt = 0;
	private String Mvkd="";  //Permet de connaitre le type de mouvement RECV, DLVR, Yard, DSCH, SHFT'
	//**************** Assesseurs
	public String getFormid() {
		return formid;
	}

	public void setFormid(String formid) {
		this.formid = formid;
		
		switch (formid) {
		
		case "FORM_LOGIN":
			this.formidInt = 1;
			break;
		case "FORM_STRADDLE_UNAVAILABLE":
			this.formidInt = 2;
			break;
		case "FORM_UNAVAILABLE":
			this.formidInt = 2;
			break;
		case "FORM_EMPTY_TO_ORIGIN":
			this.formidInt = 3;
			break;
		case "FORM_EMPTY_AT_ORIGIN":
			this.formidInt = 4;
			break;
		case "FORM_LADEN_TO_DEST":
			this.formidInt = 5;
			break;
		case "FORM_LADEN_AT_DEST":
			this.formidInt = 6;
			break;
		case "FORM_IDLE":
			this.formidInt = 7;
			break;
		case "FORM_STRADDLE_IDLE":
			this.formidInt = 7;
			break;		
		case "FORM_STRADDLE_AUTO_REHANDLE":
			this.formidInt = 8;
			break;	
		case "FORM_AUTO_REHANDLE":
			this.formidInt = 8;
			break;	
		case "FORM_STRADDLE_MANUAL_REHANDLE":
			this.formidInt = 10;
			break;	
		case "FORM_REHANDLE":
			this.formidInt = 10;
			break;	
		default:
			this.formidInt = 0;
			break;
		}
	}

	public String getChid() {
		return chid;
	}

	public void setChid(String chid) {
		this.chid = chid;
	}

	public String getEquipType() {
		return equipType;
	}

	public void setEquipType(String equipType) {
		this.equipType = equipType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrorMsgID() {
		return errorMsgID;
	}

	public void setErrorMsgID(String errorMsgID) {
		this.errorMsgID = errorMsgID;
	}

	public String getDisplayMsg() {
		return displayMsg;
	}

	public void setDisplayMsg(String displayMsg) {
		this.displayMsg = displayMsg;
	}

	public Container getContainer() {
		return container;
	}

	public void setContainer(Container container) {
		this.container = container;
	}

	public Position getPositionPrendre() {
		return positionPrendre;
	}

	public void setPositionPrendre(Position positionPrendre) {
		this.positionPrendre = positionPrendre;
	}

	public Position getPositionPoser() {
		return positionPoser;
	}

	public void setPositionPoser(Position positionPoser) {
		this.positionPoser = positionPoser;
	}

	public int getFormidInt() {
		return formidInt;
	}
	
	public String getTwinPut() {
		return twinPut;
	}

	public void setTwinPut(String twinPut) {
		this.twinPut = twinPut;
	}
	
	public void setIngress(String attribute) {
		this.ingress = attribute;
	}
	public String getIngress(){
		return this.ingress;
	}

	public String getMvkd() {
		return Mvkd;
	}

	public void setMvkd(String mvkd) {
		Mvkd = mvkd;
	}

	//*********** Constructeur		
	public MessageXmlRead(){
		container = new Container();
		positionPoser = new Position();
		positionPrendre = new Position();
	}

	//******** fonctions

	public String print(){
		String msg ="";
		msg += "\nfomrid= " + this.formid +"\n" ;
		msg += "formidInt= " + this.formidInt +"\n" ;
		msg += "chid= " + this.chid +"\n" ;
		msg += "equipType= " + this.equipType +"\n" ;
		msg += "status= " + this.status +"\n" ;
		msg += "twinPut= " + this.twinPut +"\n\n" ;
		
		msg += "msgID= " + this.errorMsgID +"\n" ;
		msg += "displayMsg= " + this.displayMsg +"\n\n" ;
		
		msg += "container id= " + this.container.getEqID() +"\n" ;
		msg += "container lg= " + this.container.getLnth() +"\n" ;
		msg += "container jpos= " + this.container.getJpos() +"\n\n" ;
		
		msg += "Prendre areatype= " + this.positionPrendre.getArea_Type() +"\n" ;
		msg += "Prendre area= " + this.positionPrendre.getArea() +"\n" ;
		msg += "Prendre ppos= " + this.positionPrendre.getPpos() +"\n" ;
		msg += "Prendre tkps= " + this.positionPrendre.getTkps() +"\n\n" ;
		
		msg += "Poser areatype= " + this.positionPoser.getArea_Type() +"\n" ;
		msg += "Poser area= " + this.positionPoser.getArea() +"\n" ;
		msg += "Poser ppos= " + this.positionPoser.getPpos() +"\n" ;
		msg += "Poser tkps= " + this.positionPoser.getTkps() +"\n\n" ;
		
		return msg;
		
	}

	
	
}
