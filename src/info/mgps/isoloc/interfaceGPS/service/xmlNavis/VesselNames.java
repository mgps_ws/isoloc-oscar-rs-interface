package info.mgps.isoloc.interfaceGPS.service.xmlNavis;

import info.mgps.isoloc.interfaceGPS.web.web.Controllers.MyController;

import java.util.HashMap;
import java.util.Map;

public class VesselNames {
	private static Map<String, String> associationsNumberVessel;
	// TODO a remettre tout ce qui est comment�
	
	public static String askVesselName(String visitNumber){
		if(associationsNumberVessel!= null && associationsNumberVessel.get(visitNumber) != null){
			return associationsNumberVessel.get(visitNumber);
		}
		else{
			if(MyController.isVesselNameOK()){
				GetVesselNameThread getName = new GetVesselNameThread(visitNumber);
				getName.start();
			}
			
		}
		return "";
	}


	public static void setVesselName(String vesselVisit, String result){
		if(associationsNumberVessel == null){
				associationsNumberVessel = new HashMap<String, String>();
			}
		synchronized (associationsNumberVessel) {
			associationsNumberVessel.put(vesselVisit, result);
		}	
		
	}
	
}
