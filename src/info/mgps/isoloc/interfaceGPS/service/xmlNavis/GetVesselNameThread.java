package info.mgps.isoloc.interfaceGPS.service.xmlNavis;

import info.mgps.isoloc.interfaceGPS.web.web.Controllers.MyController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.tomcat.util.codec.binary.Base64;

import sun.net.www.protocol.http.HttpURLConnection;


@SuppressWarnings("restriction")
public class GetVesselNameThread extends Thread{
	private List<String> keys;
	private List<String> values;
	private String vesselVisit;
	
	
	/**
	 * Constructeur avec le num�ro de visite
	 * @param vesselVisit
	 */
	public GetVesselNameThread(String vesselVisit){
		this.vesselVisit = vesselVisit;
		this.keys = new ArrayList<String>();
		this.keys.add("filtername");
		this.keys.add("CTRID");
		this.keys.add("operatorId");
		this.keys.add("complexId");
		this.keys.add("facilityId");
		this.keys.add("yardId");
		this.values = new ArrayList<String>();
		/*this.values.add("CHEWEIGHT");
		this.values.add(vesselVisit);*/
		this.values.add("VSLNAME");
		this.values.add(vesselVisit);
		this.values.add("EUROFOS");
		this.values.add("FRFOS");
		this.values.add("TDM");
		this.values.add("TDM");
		
	}
	

	/**
	 * Red�finition du start du thread pour aller chercher le nom du navire et le stocker dans VesseNames
	 */
	@Override
	public synchronized void start() {
		super.start();
		try {
			String result = doGet("http://"+MyController.getIpAddressWebServiceShip()+"/apex/api//query",keys, values);
			VesselNames.setVesselName(this.vesselVisit, result);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	/**
	 * Lecture et extraction du vesselname depuis la r�ponse xml
	 * @param message
	 * @return
	 */
	private static String read(String message){
		// Lecture du message xml de r�ponse
		if(message.contains("<data-table filter=\"VSLNAME\" count=\"0\">")){
			// pas de correspondance
			return "";			
		}else{
			String[] toto = message.split("<field>");
			String[] toto1 = toto[1].split("</field>");
			return toto1[0];
			
		}
		
	}

	
	/**
	 * r�cup�ration de la r�ponse de l'url Navis
	 * @param adress l'url de base
	 * @param keys liste des noms des param�tres
	 * @param values liste des valeurs de ces param�tres
	 * @return le nom du Vessel
	 * @throws IOException
	 */
	public static String doGet(String adress,List<String> keys,List<String> values) throws IOException{
		String result = "";
		BufferedReader reader = null;
		
			// encodage des param�tres de la requ�te
			String data = "";
			for (int i = 0; i < keys.size(); i++) {
				if (i != 0)
					data += "&";
				data += URLEncoder.encode(keys.get(i), "UTF-8") + "="
						+ URLEncoder.encode(values.get(i), "UTF-8");
			}
			// cr�ation de la connection
			URL url = new URL(adress+"?"+data);
			String userPassword = "aptapi:password";
			String encoding = new String(new Base64().encode(userPassword
					.getBytes()));
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			// add reuqest header
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Content-Type", "text/xml");
			conn.setRequestProperty("charset", "UTF-8");
			conn.setRequestProperty("Authorization", "Basic " + encoding);
			conn.setDoOutput(true);
			// envoi de la requ�te
			
			
			// lecture de la r�ponse
			reader = new BufferedReader(new InputStreamReader(
					(InputStream) conn.getContent()));
			String ligne;
			while ((ligne = reader.readLine()) != null) {
				result += ligne;
			}
		 
		
		return read(result);
	}


}
