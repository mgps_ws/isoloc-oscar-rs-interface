package info.mgps.isoloc.interfaceGPS.service.socketFlash;


import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Arrays;
import java.util.Observable;

import info.mgps.isoloc.interfaceGPS.utilitaire.LogClass;

/** 
 * @param
 * Permet la reception des messages TCP/IP. 	
 */
public class ReceptionUDP extends Observable implements Runnable {
	 
    private byte[] receivedData;
    private volatile boolean read = true;
    DatagramSocket serverSocket;
    private String message;

    
    /**
     * 
     * @param in
     * lecture par readLine
     */
    public ReceptionUDP(DatagramSocket serverSocket, byte[] in){
    	this.serverSocket = serverSocket;
        this.receivedData = in;      
    }
    
   
     
	public void run() {
		read = true;
		while (read) {
			
			try {
				DatagramPacket receivePacket = new DatagramPacket(receivedData, receivedData.length);
				
				serverSocket.receive(receivePacket);
				// lecture
				LogClass.ecriture("ReceptionUDP", "message bbytes "+Arrays.toString(receivePacket.getData())+" depuis "+receivePacket.getAddress().getHostAddress(), 1);
				//System.out.println("message bbytes "+Arrays.toString(receivePacket.getData())+" depuis "+receivePacket.getAddress().getHostAddress());
				LogClass.ecriture("ReceptionUDP", "longueur de la data "+receivePacket.getLength()+" depuis "+receivePacket.getAddress().getHostAddress(), 1);
				//System.out.println("longueur de la data "+receivePacket.getLength()+" depuis "+receivePacket.getAddress().getHostAddress());
				
				message = new String(receivePacket.getData());
				LogClass.ecriture("ReceptionUDP", "message brut = "+message, 1);
				//System.out.println("message brut = "+message);
				if(message.length() > receivePacket.getLength())
					message = message.substring(0, receivePacket.getLength());
				LogClass.ecriture("ReceptionUDP", "message apres traitement longueur  = "+message+" depuis "+receivePacket.getAddress().getHostAddress(), 1);
				//System.out.println("message apres traitement longueur  = "+message+" depuis "+receivePacket.getAddress().getHostAddress());
				InetAddress IPAddress = receivePacket.getAddress();
				int port = receivePacket.getPort();
				PaquetWeight paquet = new PaquetWeight(IPAddress.getHostAddress(), port, message);
				LogClass.ecriture("ReceptionUDP", "paquet re�u (message transforme) = "+paquet, 1);
				//System.out.println("paquet re�u (message transforme) = "+paquet);
				// Declenchement de l'evenement
				if(paquet.isValid()){
					setChanged();
					notifyObservers(paquet);
					message = "";
				}else{
					LogClass.ecriture("ReceptionUDP", "poids non pris en compte, message = "+message, 3);
					//System.err.println("poids non pris en compte, message = "+message);
				}
				

				// Reinitilisation de la variable si elle devient trop grande
				if (message.length() > 1500)
					message = "";
				

			} catch (InterruptedIOException e) {
				read = false;
				setChanged();
				notifyObservers(e);
			} catch (IOException e) {
				read = false;
				setChanged();
				notifyObservers(e);
			}
		}
	}

}
