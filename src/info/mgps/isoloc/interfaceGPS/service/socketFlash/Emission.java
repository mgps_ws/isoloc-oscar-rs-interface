package info.mgps.isoloc.interfaceGPS.service.socketFlash;

import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;

import javax.xml.bind.DatatypeConverter;

/** 
 * @param
 * Permet d'envoyer un message String par TCP/IP style Ecn4
 * dans un aute Thread. 	
 */
public class Emission  implements Runnable {
	private String message = null;
	private PrintWriter out = null; 	
	private PrintStream outBytes = null; 
	private boolean sendBytes = false;
	
	public Emission(String message,PrintWriter out) {
		this.message = message;
		this.out = out;
	}
	
	public Emission(String message,PrintStream out,boolean sendBytes) {
		this.message = message;
		this.outBytes = out;
		this.sendBytes = sendBytes;		
	}
	
	public void run() {	
			
		if (sendBytes){
			try {
				outBytes.write(toByteArray(message));
			} catch (IOException e) {
				e.printStackTrace();
			}
			outBytes.flush();			
		}else {
			out.print(message);			
			out.flush();
		}			 
	}
	
	
	public static byte[] toByteArray(String s) {
        return DatatypeConverter.parseHexBinary(s);
}

	
}
