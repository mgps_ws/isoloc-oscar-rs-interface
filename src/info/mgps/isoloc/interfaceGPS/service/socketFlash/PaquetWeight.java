package info.mgps.isoloc.interfaceGPS.service.socketFlash;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PaquetWeight {

	private String IPAddress;
	private int portNumber;
	private String message;
	private Date date;
	private int liftId;
	private float cheWeight;
	private boolean valid;
	
	/**
	 * @param iPAddress adresse IP de l'emetteur
	 * @param portNumber numero de port de l'emetteur 
	 * @param message le message recu depuis l'emetteur (Strainstall)
	 */
	public PaquetWeight(String iPAddress, int portNumber, String message) {
		super();
		IPAddress = iPAddress;
		this.portNumber = portNumber;
		this.message = message;
		String[] decomposition = message.split(",");
		if(decomposition.length == 3){
			// on est bon
			valid = true;
			//TODO date depuis string a revoir
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			try {
				this.date = dateFormat.parse(decomposition[0]);
				this.liftId = Integer.valueOf(decomposition[1].trim());
				this.cheWeight = Float.valueOf(decomposition[2].trim().replaceAll(",", "."));
			} catch (ParseException e) {
				e.printStackTrace();
				valid = false;
			}//new Date(decomposition[0]);
			
			
		}else{
			valid = false;
		}
	}

	public String getIPAddress() {
		return IPAddress;
	}

	public void setIPAddress(String iPAddress) {
		IPAddress = iPAddress;
	}

	public int getPortNumber() {
		return portNumber;
	}

	public void setPortNumber(int portNumber) {
		this.portNumber = portNumber;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getLiftId() {
		return liftId;
	}

	public void setLiftId(int liftId) {
		this.liftId = liftId;
	}

	public float getCheWeight() {
		return cheWeight;
	}

	public void setCheWeight(float cheWeight) {
		this.cheWeight = cheWeight;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	@Override
	public String toString() {
		return "PaquetWeight [IPAddress=" + IPAddress + ", portNumber=" + portNumber + ", message=" + message + "]";
	}
	
	
	
	
}
