package info.mgps.isoloc.interfaceGPS.service.socketFlash;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.Observable;

/** 
 * @param
 * Permet la reception des messages TCP/IP. 	
 */
public class Reception extends Observable implements Runnable {
	 
    private BufferedReader in;
    private Boolean readLine = false;
    private volatile boolean read = true;
    private int ind;
    private StringBuffer message = new StringBuffer();
    private String msg="";
    private int indEnd = 3;
    
    /**
     * 
     * @param in
     * lecture par readLine
     */
    public Reception(BufferedReader in){
        this.in = in;      
        this.readLine = true;
    }
    
    /**
     * 
     * @param in
     * @param indEnd par d�faut =3
     * lecture par read donc j'ai besoin d'un fin de lecture en entier
     */
    public Reception(BufferedReader in, int indEnd ){
        this.in = in;
        this.indEnd=indEnd;
        this.readLine = false;
    }
     
	public void run() {
		read = true;
		while (read) {
			
			try {
				if (readLine) {
					msg = in.readLine();
					//System.out.println(msg);
					if (msg != null) {
						setChanged();
						notifyObservers(msg);
					} else {
						/*setChanged();
						notifyObservers("lost_connection");*/
						throw new PerteConnexionException(" Perte de connexion ");
					}

				} else {

					 // Lecture
					 ind = in.read();
					 //System.out.println(Character.toString((char) ind) + " "+ ind);
					
					 if (ind<0)	throw new PerteConnexionException(" Perte de connexion ");
					 
					 if (ind == indEnd) {					
					 // Declenchement de l'evenement
					 setChanged();
					 notifyObservers(message.toString());
					 message.delete(0, message.length());
					
					 } else {
					 message.append(Character.toString((char) ind));
					 }
					
					 // Reinitilisation de la variable si elle devient trop grande
					 if (message.length() > 1500) message.delete(0, message.length());
				}

			} catch (InterruptedIOException e) {
				read = false;
				setChanged();
				notifyObservers(e);
			} catch (IOException e) {
				read = false;
				setChanged();
				notifyObservers(e);
			} catch (PerteConnexionException e) {
				e.printStackTrace();
				read = false;
				setChanged();
				notifyObservers(e);
			}
		}
	}

}
