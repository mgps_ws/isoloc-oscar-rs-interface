package info.mgps.isoloc.interfaceGPS.service.socketFlash;

public class PerteConnexionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PerteConnexionException(String s) {
		super(s);
	}
	
}
