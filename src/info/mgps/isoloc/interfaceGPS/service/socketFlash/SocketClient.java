package info.mgps.isoloc.interfaceGPS.service.socketFlash;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Observable;
import java.util.Observer;

public abstract class SocketClient extends Observable implements  Observer{

	//Declaration des variables
	public Socket socket = null;	
	private String name="";
	
	private Reception rec;
	private BufferedReader in = null;
	private Thread threadReception;
	private Boolean readLine = false;
	private int indEnd=3;
		
	private boolean sendBytes = false;
	
	//
	private String ipAdress;	//Adresse IP de reception
	private int portCom;		//Port de reception
	
	
	//********************************************
	//Assesseurs
    
	public String getIpAdress() {
		return ipAdress;
	}

	public void setIpAdress(String ipAdress) {
		this.ipAdress = ipAdress;
	}
	
	public Integer getPortCom() {
		return portCom;
	}

	public void setPortCom(Integer portCom) {
		this.portCom = portCom;
	}
	
	public String getName() {
		return name;
	}
	
	//*******************************************************
    //*************** Constructeur Et Gestion d'erreur ******
	/** 
	 * 	Constructeur
	 * @param ipAdress : adresse du serveur
	 * @param portCom : port du serveur
	 * @param sendBytes : sendBytes si true envoie des messages par bytes[]
	 * par d�faut le lie par readLine
	 * 
	 */
	public SocketClient( String ipAdress,int portCom,String nameThread,boolean sendBytes) {
		this.setIpAdress(ipAdress);
		this.portCom = portCom;		
		this.name = nameThread;
		this.sendBytes=sendBytes;
		this.readLine = true;
	}
	
	/**
	 * Constructeur
	 * @param ipAdress : adresse du serveur
	 * @param portCom : port du serveur
	 * @param sendBytes : sendBytes si true envoie des messages par bytes[]
	 * @param indEnd : indice de fin de lecture(lecture par read)
	 * 
	 */
	public SocketClient( String ipAdress,int portCom,String nameThread,boolean sendBytes,int indEnd) {
		this.setIpAdress(ipAdress);
		this.portCom = portCom;
		this.readLine = false;
		this.name = nameThread;
		this.indEnd=indEnd;
		this.sendBytes=sendBytes;
	}
	
	/** 
	 * @param
	 * Ecriture des messages d'erreur
	 * 
	 */
    public abstract void ecriture(String msg);
    
    /** 
	 * @param
	 * 	msg: message d'erreur � traiter(logger ou afficher)		
	 */
    public abstract void ecritureError(String msg);
    
    /** 
	 * @param
	 * 	infosource : info � afficher avant le message
	 * msg: message
	 * level: niveau d'alerte
	 * 0=message de debug
	 * 1=message d'information
	 * 2=message d'avertissement
	 * 3=message d'erreur
	 * 
	 */
    public void ecriture(String infoSource,String msg,Integer level){	    	
    	switch(level){
    	case 0 : 
    		ecritureError("Level:" + level + "    "+ infoSource  + "   " + msg);
    		break;
    	case 1:
    		ecritureError("Level:" + level + "    "+ infoSource  + "   " + msg);
    		break;
    	case 2:
    		ecritureError("Level:" + level + "    "+ infoSource  + "   " + msg);
    		break;
    	case 3 :
    		ecritureError("Level:" + level + "    "+ infoSource  + "   " + msg);
    		break;
    	}      	
    }
    
	//*******************************************************
    //*************** Fonctions *****************************
    
    
    /**
     * @param
     * Permet d'acceder a l'arret du thread de connection
     */
    public void arretReception(String msg){
    	
    	try {
    		if ( socket != null) {
    			socket.close();
    			lostConnexion(new IOException("closed connexion : " + msg));
    		}else{
    			lostConnexion(new IOException("socket=null"));
    		}
    			
    	} catch (IOException e) {
    		lostConnexion(e);
    		ecriture("socketClient\\arretReception", e.getMessage(), 3);
    	}
    }
    
    public void closeReception(){
    	if ( socket != null) {
    		try {
				socket.close();
			} catch (IOException e) {
				ecriture("socketClient\\closeReception ", e.getMessage(), 3);
			}
    	}
    }
    
	/** 
	 * Permet la connection des clients en TCP/IP.
	 * Il est en attente de connection.
	 * Une fois connecte, je gere l'ecoute.	
	 * @throws IOException 
	 * @throws UnknownHostException 
	 */
	public void connect() {

		try {
		
			if (this.isConnected()) {
				//ecriture("socketClient\\connect\\isConnected", "connection ip= " + ipAdress + " port= " + portCom, 1);
				ConnexionReady(this.getName() + " Ready : connection ip= " + ipAdress + " port= " + portCom);
				
			}else{
				this.socket = new Socket(ipAdress, portCom);
				name = name + this.socket.getInetAddress().getHostAddress();

				// Lancement de l'ecoute
				startListen();
				
				//Message de connexion ready
				//ecriture("socketClient\\connect", "connection ip= " + ipAdress + " port= " + portCom, 1);
				ConnexionReady(this.getName() + " Ready : connection ip= " + ipAdress + " port= " + portCom);
			}
		
		} catch (IOException e) {
			lostConnexion(e);
			//ecriture("socketClient\\connect", e.getMessage(), 3 );
		}
	
	
	}
	
	/** 
	 * @param
	 * Permet de connaitre l'etat de la connection. 	
	 * 
	 */
	public boolean isConnected(){
		if (socket == null){
			return false;			
		}else if (socket.isClosed())  {
			return false;
		}else {
			return socket.isConnected();
		}			
	}
	
	/** 
	 * @param
	 * Permet d'envoyer un message String par TCP/IP.
	 * 
	 */
	public void send(String message) {
		//ecriture("socketClient\\send", "Message dans le send "+message, 1 );
		
		try{
			if (socket== null){
				lostConnexion(new IOException("socket=null"));
			}else {
				if (!socket.isClosed()){
					if (sendBytes){
			
						Thread thTx = new Thread(new Emission(message,new PrintStream(socket.getOutputStream()), true));
						thTx.start();
						
					}else {
						
						Thread thTx = new Thread(new Emission(message,new PrintWriter(new BufferedWriter(new OutputStreamWriter(this.socket.getOutputStream())),true)));
						thTx.start();
						
					}
				}
			}
		} catch (IOException e) {
			lostConnexion(e);
			ecriture("socketClient\\send", "Erreur Send SocketClient I/O: "+e.getMessage(), 3 );
		}	
	}
	
	/**
	 * @param
	 * Permet de lancer l'ecoute du port selectionne.
	 */
	public void startListen() {
		try{
			//Initialisation de bufferIn
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			
			if (readLine) {
				rec = new Reception(in);
			} else{
				rec = new Reception(in, indEnd);
			}
							
			//Abonnement de reception aux evenements
			rec.addObserver(this);
			//Lancement du thread de reception
			threadReception = new Thread(rec);
			threadReception.setName(name);
			threadReception.start();
			
		} catch (IOException e) {
			lostConnexion(e);
			ecriture("socketClient\\startListen", "Erreur I/O dans startListen: "+e.getMessage(), 3 );
		}	
	}

	/**
	 * @param
	 * Traitement de l'evenement de lecture du port selectionne.
	 * Attention : gestion de l'arr�t du thread de reception et de la perte de connexion
	 */
	@Override
	public synchronized void update(Observable o, Object arg) {
		if( o instanceof Reception){
			if (arg!=null) {
				if (arg instanceof Exception){
					arretReception(((Exception) arg).getMessage());			
				}else{
					lecture((String)arg);
				}
			}
		}	
	}

	/**
	 * @param
	 * Traitement de l'evenement de lecture du port selectionne.
	 */
	public abstract void lecture(String arg);
	
	/**
	 * @param
	 * Traitement de l'evenement de perte de connexion.
	 */
	public abstract void lostConnexion(Exception e);
	
	/**
	 * @param
	 * Traitement de l'evenement de connexion.
	 */
	public abstract void ConnexionReady(String msg);
	
}
