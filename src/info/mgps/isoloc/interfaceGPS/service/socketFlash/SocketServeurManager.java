package info.mgps.isoloc.interfaceGPS.service.socketFlash;


import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import info.mgps.isoloc.interfaceGPS.web.web.Controllers.MyController;

public abstract class SocketServeurManager  extends Observable  implements Observer, Runnable{

	//Declaration des variables
	private int serverPort;		//Port de reception
	private ServerSocket serverSocket = null;
	private boolean v_isStopped = false;
    private ExecutorService threadPool;
    
    private boolean readLine = false;
	private int indEnd=3;
	private boolean sendBytes = false;
	
    protected HashMap<String, SocketServer>  socketsClient;
	private String name; // nom du Thread en cours
	
	//********************************************
	//Assesseurs
	public Integer getPortCom() {
		return serverPort;
	}

	public void setPortCom(Integer serverPort) {
		this.serverPort = serverPort;
	}
	
    private synchronized boolean isStopped() {
        return this.v_isStopped;
    }
    
	//*******************************************************
    //*************** Constructeur et Gestion d'erreur ******
	/** 
	 * Constructeur
	 * @param serverPort : Port � l'�coute
	 * @param nombreDeConnexion : nombre de connexion maximum autoris�e
	 * @param sendBytes : sendBytes si true envoie des messages par bytes[]
	 * par d�faut le lie par readLine
	 * 
	 */
	public SocketServeurManager(String nameThread, int serverPort,int nombreDeConnexion,boolean sendBytes) {
		this.name=nameThread;
		this.serverPort = serverPort;
		this.socketsClient = new  HashMap<String, SocketServer>() ;
		this.threadPool = Executors.newFixedThreadPool(nombreDeConnexion);
		this.sendBytes=sendBytes;
		this.readLine=true;
	}
	
	/**
	 * Constructeur
	 * @param serverPort : Port � l'�coute
	 * @param nombreDeConnexion : nombre de connexion maximum autoris�e
	 * @param sendBytes : sendBytes si true envoie des messages par bytes[]
	 * @param indEnd : indice de fin de lecture(lecture par read)
	 * 
	 */
	public SocketServeurManager(String nameThread, int serverPort,int nombreDeConnexion,boolean sendBytes,int indEnd) {
		this.name=nameThread;
		this.serverPort = serverPort;		
		this.socketsClient = new  HashMap<String, SocketServer>() ;
		this.threadPool = Executors.newFixedThreadPool(nombreDeConnexion);
		this.sendBytes = sendBytes;
		this.readLine = false;
		this.indEnd=indEnd;		
	}
	
	/** 
	 * @param
	 * Ecriture des messages d'erreur
	 * 
	 */
    public abstract void ecriture(String msg);
    
    /** 
	 * @param
	 * 	msg: message d'erreur � traiter(logger ou afficher)		
	 */
    public abstract void ecritureError(String msg);
    
    /** 
	 * @param
	 * 	infosource : info � afficher avant le message
	 * msg: message
	 * level: niveau d'alerte
	 * 0=message de debug
	 * 1=message d'information
	 * 2=message d'avertissement
	 * 3=message d'erreur
	 * 
	 */
    public void ecriture(String infoSource,String msg,Integer level){	    	
    	switch(level){
    	case 0 : 
    		ecritureError("Level:" + level + "    "+ infoSource  + "   " + msg);
    		break;
    	case 1:
    		ecritureError("Level:" + level + "    "+ infoSource  + "   " + msg);
    		break;
    	case 2:
    		ecritureError("Level:" + level + "    "+ infoSource  + "   " + msg);
    		break;
    	case 3 :
    		ecritureError("Level:" + level + "    "+ infoSource  + "   " + msg);
    		break;
    	}      	
    }
	
    
	//*******************************************************
    //*************** Fonctions sur la socket server *****************************
        
    /**
     * Arr�t du thread de reception
     * et fermeture de la socket
     * Il faut faire un run pour le d�clencher
     * 
     */
    public synchronized void stop(){
    	//Permet d'arr�ter le thread de reception
        this.v_isStopped = true;
        try {//Permet de fermer la socket server
            this.serverSocket.close();
            lostConnexionServer( new IOException("Server closed"));
        } catch (IOException e) {
            throw new RuntimeException("Error closing server", e);
        }
    }
    
    /**
     *Initialisation de serversocket
     * 
     */
    private void openServerSocket() {
        try {
        
            this.serverSocket = new ServerSocket(this.serverPort);
 
			if (!this.isClosed()) {
				//ecriture("SocketServeurManager\\openServerSocket\\isClosed", " port= " + this.serverPort, 1);
				connexionReadyServer(" Server already :  port= " + this.serverPort);
			
			}else{
				this.serverSocket = new ServerSocket( this.serverPort);
			
				//Message de connexion ready
				//ecriture("SocketServeurManager\\openServerSocket", " port= " + this.serverPort, 1);
				connexionReadyServer(" Server ready :  port= " + this.serverPort);
			}

        } catch (IOException e) {
        	lostConnexionServer(e);
            throw new RuntimeException("Cannot open port " + this.serverPort, e);
        }
    }
    
	/** 
	 * @param
	 * Permet de connaitre l'etat de la connection. 	
	 * 
	 */
	public boolean isClosed(){
		if (serverSocket == null){
			return false;			
		}else {
			return serverSocket.isClosed();
		}			
	}
	
	/** 
	 * @param
	 * Permet la connection des clients en TCP/IP.
	 * Il est en attente de connection.
	 * Une fois connecte, je gere l'ecoute.	
	 */
    public void run(){ 
    	connect();
    }
	
	protected void connect(){
	   	synchronized(this){
			Thread.currentThread().setName(name);
            Thread.currentThread();
        }

        openServerSocket();
        String ipName = "";
   
        while(! isStopped()){
        
            Socket clientSocket = null;
            
            try {
                clientSocket = this.serverSocket.accept();      
                ipName = clientSocket.getInetAddress().getHostAddress();
                //si ma socket existe je la supprime pour la recr�er
                if (socketsClient.containsKey(ipName)){socketsClient.remove(ipName);}
               
                //Ajout de socket server dans ma HashMap
                if (readLine){
                	socketsClient.put(ipName, new SocketServer(clientSocket,ipName,this.sendBytes));   
                }else {
                	socketsClient.put(ipName, new SocketServer(clientSocket, ipName, this.sendBytes,this.indEnd));   
                }
                                       
                //Abonnement � mon observable
                socketsClient.get(ipName).addObserver(this);
                
                connexionReadyClient("New client: " + ipName, ipName);
                //ecriture("accept connexion","New client:" + ipName  +  " Nbr de client : " + socketsClient.size(), 1);
                
            } catch (IOException e) {
                if(isStopped()) {
                	lostConnexionServer(e);
                	ecriture("Server Stopped: isStopped=true.") ;
                    break;
                }
                lostConnexionServer(e);
            	ecriture("Server Stopped") ;
            }
			this.threadPool.execute( socketsClient.get(ipName));
        }
        
        this.threadPool.shutdown();
        ecriture("Thread Server Stopped.") ;
		
	}
	//*******************************************************
    //*************** Fonctions des sockets clients *****************************
	/** 
	 * @param
	 * Permet de connaitre l'etat d'une connection. 	
	 */
	public boolean isConnected(String namesocketClient){
		if (socketsClient.containsKey(namesocketClient)){
			return socketsClient.get(namesocketClient).isConnected();
		}
		else{
			return false;
		}			
	}
	
	/** 
	 * @param
	 * Permet d'envoyer un message String par TCP/IP.
	 * message= message
	 * namesocketClient= identifiant de la socket client
	 */
	public void send(String message,String namesocketClient) {
		
		
		if (socketsClient.containsKey(namesocketClient)){
			socketsClient.get(namesocketClient).send(message);
		}else{
			
			if(MyController.isDetectionRowActive() && socketsClient.containsKey(MyController.getAntiCollisionServerAddress())){
				socketsClient.get(MyController.getAntiCollisionServerAddress()).send(message);
			}
		}
			
	}
	
	/**
	 * @param
	 * Traitement de l'evenement de lecture du port selectionne.
	 */
	@Override
	public synchronized void update(Observable o, Object arg) {
		if( o instanceof SocketServer){	
			if (arg!=null) {
				if (socketsClient.containsKey(((SocketServer) o).getName())){
					if (arg instanceof Exception){
						lostConnexionClient((Exception)arg,((SocketServer) o).getName());
						
					}else{
						lecture((String)arg,((SocketServer) o).getName());
					}
				}					
			}
		}	
	}

	/**
	 * @param
	 * Traitement de l'evenement de lecture d'un client.
	 */
	public abstract void lecture(String message, String namesocketClient);
	   
	/**
	 * @param
	 * Traitement de l'evenement de perte de connexion du seveur.
	 */
	public abstract void lostConnexionServer(Exception e);
	
	/**
	 * @param
	 * Traitement de l'evenement de connexion du serveur.
	 */
	public abstract void connexionReadyServer(String msg);
	
	/**
	 * @param
	 * Traitement de l'evenement de perte de connexion d'un client.
	 */
	public abstract void lostConnexionClient(Exception e, String namesocketClient);
	
	/**
	 * @param
	 * Traitement de l'evenement de connexion d'un client.
	 */
	public abstract void connexionReadyClient(String msg, String namesocketClient);
	
}
