package info.mgps.isoloc.interfaceGPS.service.socketFlash;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Observable;
import java.util.Observer;

public class SocketServer extends Observable implements Observer, Runnable {

	//Declaration des variables
	private Socket socket = null;	
	private String name="";
	
	private Reception rec;
	private BufferedReader in = null;	
	private Thread threadReception;
	private Boolean readLine = true;
	private int indEnd=3;
	
	private boolean sendBytes = false;
		
	//********************************************
	//Assesseurs
		public String getName() {
		return name;
	}
	
	//*******************************************************
    //*************** Constructeur et Gestion d'erreur ******
	/** 
	 * @param
	 * Constructeur.
	 * @param sendBytes : sendBytes si true envoie des messages par bytes[]
	 * par d�faut le lie par readLine
	 * 
	 */
	public SocketServer(Socket socket, String name,boolean sendBytes) {
		this.socket = socket;
		this.name = name;
		this.sendBytes=sendBytes;	
		this.readLine=true;	
	}
	
	/** 
	 * Constructeur.
	 * @param sendBytes : sendBytes si true envoie des messages par bytes[]
	 * @param indEnd : indice de fin de lecture(lecture par read)
	 * 
	 */
	public SocketServer(Socket socket, String name,boolean sendBytes,int indEnd) {
		this.socket = socket;
		this.name = name;
		this.sendBytes=sendBytes;
		this.readLine=false;
		this.indEnd=indEnd;
	}


	//*******************************************************
    //*************** Fonctions *****************************
    
    /**
     * @param
     * Permet d'acceder a l'arret du thread de connection
     */
    public void arretReception(String msg){
    	
    	try {
    		if ( socket != null) {
    			socket.close();
    			lostConnexion(new IOException("closed connexion : " + msg));
    		}else{
    			lostConnexion(new IOException("socket=null"));
    		}
    			
    	} catch (IOException e) {
    		lostConnexion(e);
    	}
    }
    
	/** 
	 * @param
	 * Permet de connaitre l'etat de la connection. 	
	 */
	public boolean isConnected(){
		if (socket == null){
			return false;			
		}else if (socket.isClosed())  {
			return false;
		}else {
			return socket.isConnected();
		}		
	}
	
	
	/**
	 * Permet d'envoyer un message String par TCP/IP.
	 * @param message le message a envoyer
	 */
	public void send(String message) {
		try{
			
			if (socket == null) {
				lostConnexion(new IOException("socket=null"));
			} else {	
				
				if (!socket.isClosed()){
					if (sendBytes){
						
						Thread thTx = new Thread(new Emission(message,new PrintStream(socket.getOutputStream()), true));
						thTx.start();
					
					}else {
					
						Thread thTx = new Thread(new Emission(message,new PrintWriter(new BufferedWriter(new OutputStreamWriter(this.socket.getOutputStream())),true)));
						thTx.start();
					
					}
				}
			}
		} catch (IOException e) {
			lostConnexion(e);	
		}
	}
	
	/**
	 * @param
	 * Permet de lancer l'ecoute du port selectionne
	 */
	public void StartListen() {
		try{
			//Initialisation de bufferIn
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			
			if (readLine) {
				rec = new Reception(in);
			} else{
				rec = new Reception(in, indEnd);
			}
			
			//Abonnement de reception aux evenement
			rec.addObserver(this);
			//Lancement du thread de reception
			threadReception = new Thread(rec);
			threadReception.setName("Isotoc.Interface.ecn4.ReceptionSocket");
			threadReception.start();
			
		} catch (IOException e) {
			lostConnexion(e);	
		}	
	}

	/**
	 * @param
	 * Traitement de l'evenement de lecture du port selectionne.
	 */
	@Override
	public synchronized void update(Observable o, Object arg) {
		
		if( o instanceof Reception){
			if (arg!=null) {
				if (arg instanceof Exception){	
					arretReception(((Exception) arg).getMessage());			
				}else{
					lecture((String)arg);
				}
			}			
		}	
	}

	/**
	 * @param
	 * Traitement de l'evenement de lecture du port selectionne.
	 */
	public void lecture(String arg) {
		this.setChanged();
		this.notifyObservers( arg);
	}
	
	/**
	 * @param
	 * Traitement de l'evenement de perte de connexion.
	 */
	public void lostConnexion(Exception e){
		this.setChanged();
		this.notifyObservers( e);
	}
	
	/**
	 * @param
	 * Permet de se connecter sur un thread different.
	 */
	@Override
	public void run() {
		StartListen();
		
	}
	
	
}
