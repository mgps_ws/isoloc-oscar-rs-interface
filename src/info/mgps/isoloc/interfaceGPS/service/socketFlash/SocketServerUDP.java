package info.mgps.isoloc.interfaceGPS.service.socketFlash;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.Observable;
import java.util.Observer;

public class SocketServerUDP extends Observable implements Observer, Runnable {

	//Declaration des variables
	private DatagramSocket serverSocket = null;	
	private String name="";
	private Thread threadReception;	
	//********************************************
	//Assesseurs
		public String getName() {
		return name;
	}
	
	//*******************************************************
    //*************** Constructeur et Gestion d'erreur ******
	/** 
	 * @param
	 * Constructeur.
	 * @param sendBytes : sendBytes si true envoie des messages par bytes[]
	 * par d�faut le lie par readLine
	 * 
	 */
	public SocketServerUDP(int portNumberWeight, String name) {
		try {
			this.serverSocket = new DatagramSocket(portNumberWeight);
		} catch (SocketException e) {
			e.printStackTrace();
		}
		this.name = name;
	}
	
	/** 
	 * Constructeur.
	 * @param sendBytes : sendBytes si true envoie des messages par bytes[]
	 * @param indEnd : indice de fin de lecture(lecture par read)
	 * 
	 */
	


	//*******************************************************
    //*************** Fonctions *****************************
    
    /**
     * @param
     * Permet d'acceder a l'arret du thread de connection
     */
    public void arretReception(String msg){
    	
    	if ( serverSocket != null) {
			serverSocket.close();
			lostConnexion(new IOException("closed connexion : " + msg));
		}else{
			lostConnexion(new IOException("socket=null"));
		}
    }
    
	/** 
	 * @param
	 * Permet de connaitre l'etat de la connection. 	
	 */
	public boolean isConnected(){
		if (serverSocket == null){
			return false;			
		}else if (serverSocket.isClosed())  {
			return false;
		}else {
			return serverSocket.isConnected();
		}		
	}
	
	

	
	/**
	 * @param
	 * Permet de lancer l'ecoute du port selectionne
	 */
	public void StartListen() {
		
			byte[] receiveData = new byte[1024];
            
            ReceptionUDP rec = new ReceptionUDP(serverSocket, receiveData);			
			//Abonnement de reception aux evenement
			rec.addObserver(this);
			//Lancement du thread de reception
			threadReception = new Thread(rec);
			threadReception.setName("Isotoc.Interface.ecn4.ReceptionSocket");
			threadReception.start();
			
			
	}

	/**
	 * @param
	 * Traitement de l'evenement de lecture du port selectionne.
	 */
	@Override
	public synchronized void update(Observable o, Object arg) {
		
		if( o instanceof ReceptionUDP){
			if (arg!=null) {
				if (arg instanceof Exception){	
					arretReception(((Exception) arg).getMessage());			
				}else{
					lecture((PaquetWeight)arg);
				}
			}			
		}	
	}

	/**
	 * @param
	 * Traitement de l'evenement de lecture du port selectionne.
	 */
	public void lecture(PaquetWeight arg) {
		this.setChanged();
		this.notifyObservers(arg);
	}
	
	/**
	 * @param
	 * Traitement de l'evenement de perte de connexion.
	 */
	public void lostConnexion(Exception e){
		this.setChanged();
		this.notifyObservers( e);
	}
	
	/**
	 * @param
	 * Permet de se connecter sur un thread different.
	 */
	@Override
	public void run() {
		StartListen();
		
	}

	public void stop() {
		if(serverSocket != null && serverSocket.isConnected()){
			serverSocket.close();
		}
	}
	
	
}
