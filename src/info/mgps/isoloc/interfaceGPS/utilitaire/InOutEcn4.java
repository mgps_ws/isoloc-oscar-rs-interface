package info.mgps.isoloc.interfaceGPS.utilitaire;

public class InOutEcn4 {

	private static String startMSg = "1f41";
	private static String endMsg = "ff";
	
	/**
	 * Lecture d'un message Ecn4
	 * @param message
	 * @return String message 
	 * 
	 */
	public static String inEcn4(String message){
		return null;
		
	}
	
	/**
	 * Transformation d'un message pour l"envoyer vers Ecn4
	 * @param message
	 * @return String message transformer pour Ecn4
	 * 
	 */
	public static String outEcn4(String message){
		//System.out.println("InOutEcn4 message to send = "+message);
		return IntTo2Bytes(message.length() + 2) + startMSg + asciiToHex(message) +  endMsg ;
	}
	
	private static String asciiToHex(String asciiValue)
	{
	    char[] chars = asciiValue.toCharArray();
	    StringBuffer hex = new StringBuffer();
	    for (int i = 0; i < chars.length; i++)
	    {
	        hex.append(Integer.toHexString((int) chars[i]));
	    }
	    return hex.toString();
	}
	
	private static String IntTo2Bytes(int lg){
		String str = Integer.toHexString(lg);
		switch (str.length()) {
		case 1:
			return "000" + str;
		case 2:
			return "00" + str;
		case 3:
			return "0" + str;
		default:
			return str;
		}
	}
	
}
