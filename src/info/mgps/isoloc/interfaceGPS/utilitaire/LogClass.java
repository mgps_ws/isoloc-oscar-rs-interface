package info.mgps.isoloc.interfaceGPS.utilitaire;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import info.mgps.isoloc.interfaceGPS.service.log.AccessDb;
import info.mgps.isoloc.interfaceGPS.web.web.Controllers.MyController;

public class LogClass {

	
	
	
	/** 
	 * @param
	 * msg: message standard � traiter(logger ou afficher)	
	 * 
	 */
    public static void ecriture(String msg){	    	
    	if (MyController.isDebug())	System.out.println(msg);      	
    }
    
    /** 
	 * @param
	 * 	msg: message d'erreur � traiter(logger ou afficher)		
	 */
    public static void ecritureError(String msg){	    	
    //if (MyController.isDebug())	System.err.println(msg);  
    	//LoggerFactory.getLogger(LogClass.class).info(new Date()+"\t"+msg);
    	System.out.println(new Date()+"\t"+msg);
    }
    
    /** 
	 * @param
	 * 	infosource : info � afficher avant le message
	 * msg: message
	 * level: niveau d'alerte
	 * 0=message de debug
	 * 1=message d'information
	 * 2=message d'avertissement
	 * 3=message d'erreur
	 * 
	 */
    public static void ecriture(String infoSource,String msg,Integer level){
    	
    	switch(level){
	    	case 0 : 
	    		ecritureError("Level:" + level + "    "+ infoSource  + "   " + msg);
	    		break;
	    	case 1:
	    		ecritureError("Level:" + level + "    "+ infoSource  + "   " + msg);
	    		break;
	    	case 2:
	    		ecritureError("Level:" + level + "    "+ infoSource  + "   " + msg);
	    		break;
	    	case 3 :
	    		ecritureError("Level:" + level + "    "+ infoSource  + "   " + msg);
	    		break;
    	}
    	//M�morisation des log applicatifs sur une base locale
    	try {
    		//Message d'erreur pour la requete sql �viter '
    		msg= msg.replace("'", "");
    		AccessDb.MemologApplicatif(level, infoSource, msg);
		} catch (Exception e) {
			ecritureError("Level:" + level + "    "+ infoSource  + "   " + msg + " " + e.getMessage());
		}
   	
    }
	
}
