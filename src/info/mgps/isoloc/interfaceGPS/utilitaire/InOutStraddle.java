package info.mgps.isoloc.interfaceGPS.utilitaire;

public class InOutStraddle {

	static String[] tb;
	
	/**
	 * R�cup�re le message socket
	 * @param message = message � traiter
	 * @return
	 */
	public static String inStraddle(String message){
		try {
			// TODO respecter le format initial pour la checklist
			if (message.length() > 0 && (message.charAt(0) == 2) ) {
				message = message.substring(1);
				
				tb = message.split("<>");
				if (tb.length> 0 && (tb[0].length()== Integer.parseInt(tb[1]))){
					return tb[0];
				}
			}
		} catch (Exception e) {
			
			LogClass.ecriture("InOutStraddle/inStraddle", e.getMessage(), 3);
		}
		return "";
	}
	
	/**
	 * Envoie du message au straddle
	 * @param message = message � envoyer
	 * @return
	 */
	public static String outStraddle(String message){
		return (char)2 + message + "<>" + message.length() + (char)3;
	}
	
}
