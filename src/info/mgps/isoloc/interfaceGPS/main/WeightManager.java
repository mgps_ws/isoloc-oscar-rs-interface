package info.mgps.isoloc.interfaceGPS.main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import info.mgps.isoloc.interfaceGPS.service.log.AccessDb;
import info.mgps.isoloc.interfaceGPS.service.socketFlash.PaquetWeight;
import info.mgps.isoloc.interfaceGPS.utilitaire.LogClass;
import info.mgps.isoloc.interfaceGPS.web.Models.StraddleWeightData;
import info.mgps.isoloc.interfaceGPS.web.Models.WeightingStraddle;
import info.mgps.isoloc.interfaceGPS.web.web.Controllers.ExecutionVGM;
import info.mgps.isoloc.interfaceGPS.web.web.Controllers.MyController;

public class WeightManager {

	// La classe qui gere la pesee venant des Straddles (materiel Strainstall)
	// Le format des messages re�us
	// 2016-06-22 15:18:13, 5, 47050
	private boolean isUp = true;
	private Map<String, WeightingStraddle> weightingMap;// la structure qui stocke les infos de pes�e par cavalier
	private Map<String, Boolean> straddlesWeightingIP;
	private Map<String, Boolean> straddlesWeightingName;
	private Map<String, Boolean> straddlesRAZWeightingName;
	private Map<String, StraddleWeightData> straddlesDataIP;
	private Map<String, StraddleWeightData> straddlesDataName;
	private List<String> straddlesList;
	
	
	public WeightManager(StraddleManager straddleManager){
		
		this.weightingMap = new HashMap<String, WeightingStraddle>();		
		this.straddlesWeightingIP = new HashMap<String, Boolean>(); // active ou desactive, indexe par adresseIP
		this.straddlesWeightingName = new HashMap<String, Boolean>(); // active ou desactive, indexe par nom straddle
		this.straddlesRAZWeightingName = new HashMap<String, Boolean>(); // RAZ active ou desactive, indexe par nom straddle
		this.straddlesDataIP = new HashMap<String, StraddleWeightData>(); // donnees du straddle, indexe par adresse IP	
		this.straddlesDataName = new HashMap<String, StraddleWeightData>(); // Donnees du straddle indexe par nom du straddle
		
		// insertion de tous les straddles depuis la base de donnees
		AccessDb.remplirStraddlesWeighting(this.straddlesWeightingIP, this.straddlesWeightingName, this.straddlesRAZWeightingName, this.straddlesDataIP, this.straddlesDataName);
		this.straddlesList = getStraddles();
		this.straddlesList.sort(new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {				
				return o1.compareTo(o2);
			}
		});
		straddleManager.setWeightingManager(this);
	}


	


	public void newWeight(PaquetWeight newWeight) {
		// TODO La gestion d'un nouveau poids donn�
		if(isUp){
			// La pesee est activee
			if(isWeightingUp(newWeight)){
				if(weightingMap.containsKey(getStraddleName(newWeight))){
					WeightingStraddle ancienPoids = weightingMap.get(getStraddleName(newWeight));
					if(weightingMap.get(getStraddleName(newWeight)).getIpAddress() == null || weightingMap.get(getStraddleName(newWeight)).getIpAddress().equals("")){
						
						ancienPoids.setIpAddress(newWeight.getIPAddress());
						ancienPoids.setPoids1(newWeight.getCheWeight());
						ancienPoids.setLiftId(newWeight.getLiftId());
						//weightingMap.put(getStraddleName(newWeight), new WeightingStraddle(newWeight.getIPAddress(), newWeight.getLiftId(), newWeight.getCheWeight()));
					}else
					
					
					// si le contenu existe on l'exploite
					
					if(newWeight.getLiftId() == ancienPoids.getLiftId()){
						// si la mission n'a pas chang�, tout va bien sinon, on ignore
						// A noter que la mission dans le sens poids ne change que quand on passe a l'�tape prendre TC ou manual rehandle
						
							ancienPoids.setPoids2(newWeight.getCheWeight());
							String straddleName = getStraddleName(newWeight);
							ExecutionVGM.majWeightTOS(weightingMap.get(straddleName).getContainerID(), ancienPoids.getPoids2(), straddleName,weightingMap.get(straddleName).getJobWeight());
							//ExecutionVGM.majWeightTOS("CMAU1234567", 0, "C10", 0);
							
						
							
					}
					else
					if (newWeight.getLiftId() == 1 && ancienPoids.getLiftId() != 1) {
						// redemarrage, donc nouveau poids.. r�initialiser
						// l'ancien poids..le mettre a 1
						ancienPoids.setLiftId(1);
						ancienPoids.setPoids1(newWeight.getCheWeight());
					} else {
						// pas le m�me liftID....a priori mettre dans le poids1
						// r�initialiser le contenu
						// TODO je viens d'ajouter l'affectation du nouveau liftId qui ne se faisait pas avant (19/08/2016)
						ancienPoids.setLiftId(newWeight.getLiftId());
						ancienPoids.setPoids1(newWeight.getCheWeight());
						
					}
						
				}else{
					// inserer le contenu
					weightingMap.put(getStraddleName(newWeight), new WeightingStraddle(newWeight.getIPAddress(), newWeight.getLiftId(), newWeight.getCheWeight()));					
				}
			}
			
		}
	}
	






	private boolean isWeightingUp(PaquetWeight newWeight) {
		// si le straddle est activ� en pes�e (depend de la base de donnees)		
		return this.straddlesWeightingIP.get(newWeight.getIPAddress());
	}

	public boolean isRAZUp(String straddleName){
		return this.straddlesRAZWeightingName.get(straddleName);
	}




	public Map<String, StraddleWeightData> getStraddlesDataName() {
		return straddlesDataName;
	}





	private String getStraddleName(PaquetWeight newWeight) {
		// retourner le nom du straddle en donnant l'objet paquetWeight		
		return this.straddlesDataIP.get(newWeight.getIPAddress()).getStraddleName();
	}

	private String getStraddleName(String ipAddress) {
		// retourner le nom du straddle en donnant l'objet paquetWeight		
		return this.straddlesDataIP.get(ipAddress).getStraddleName();
	}



	public void setIsUp(boolean up){
		this.isUp = up;
	}
	
	public boolean isUp(){
		return isUp;		
	}





	public void incrementJobId(String chid) {
		if(isUp(chid)){
			LogClass.ecriture("StraddleManager", chid+" weightingMap.containsKey(chid) "+weightingMap.containsKey(chid), 1);
			//System.out.println(chid+" weightingMap.containsKey(chid) "+weightingMap.containsKey(chid));
			if(weightingMap.containsKey(chid)){
				LogClass.ecriture("StraddleManager", chid+" les poids 1="+weightingMap.get(chid).getPoids1()+" 2="+weightingMap.get(chid).getPoids2(), 1);
				//System.out.println(chid+" les poids 1="+weightingMap.get(chid).getPoids1()+" 2="+weightingMap.get(chid).getPoids2());
				if(weightingMap.get(chid).getPoids1() == 0 && weightingMap.get(chid).getPoids2() == 0){
					LogClass.ecriture("StraddleManager", chid+" incremet Job ID avec poids1 et poids2 =0 -- implique echec weighting", 1);
					//System.out.println(chid+" incremet Job ID avec poids1 et poids2 =0 -- implique echec weighting");
					echecWeighting(chid, weightingMap.get(chid).getContainerID(),weightingMap.get(chid).getJobWeight());
				}else
					if(weightingMap.get(chid).getPoids2() != 0){
						LogClass.ecriture("StraddleManager", chid+" dans le weightingMap.get(chid).getPoids2() != 0", 1);
						//System.out.print(chid+" dans le weightingMap.get(chid).getPoids2() != 0");;
						ExecutionVGM.majWeightTOS(weightingMap.get(chid).getContainerID(), weightingMap.get(chid).getPoids2(), chid, weightingMap.get(chid).getJobWeight());
					}else
						if(weightingMap.get(chid).getPoids1() != 0){
							LogClass.ecriture(chid+" dans le weightingMap.get(chid).getPoids1() != 0");
							//System.out.print(chid+" dans le weightingMap.get(chid).getPoids1() != 0");;
							ExecutionVGM.majWeightTOS(weightingMap.get(chid).getContainerID(), weightingMap.get(chid).getPoids1(), chid, weightingMap.get(chid).getJobWeight());
						}
				weightingMap.get(chid).incrementJobId();
			}
		}
		
	}





	private void echecWeighting(String straddleName, String containerID, float jobWeight) {
		// Enregistrer dans la base de donn�es l'�chec de la pes�e
		AccessDb.writeVGMInfos(new Date(), straddleName, containerID, jobWeight, 0, 2, false);
	}





	private boolean isUp(String chid) {	
		//System.out.println("straddlesWeightingName.get(chid) "+chid + "   " +straddlesWeightingName.get(chid));
		return straddlesWeightingName.get(chid);
	}





	public void setContainerID(String chid, String eqID, String weight) {
		//System.out.println("setting container ID "+chid + " "+eqID+" weight "+weight+" weightingMap.containsKey(chid) "+weightingMap.containsKey(chid));
		if(isUp(chid))
		if(weightingMap.containsKey(chid)){
			weightingMap.get(chid).setContainerID(eqID);
			weightingMap.get(chid).setJobWeight(Float.valueOf(weight.replaceAll(",", ".")));
		}else{
			weightingMap.put(chid, new WeightingStraddle("", 1, 0));
			weightingMap.get(chid).setContainerID(eqID);
			weightingMap.get(chid).setJobWeight(Float.valueOf(weight.replaceAll(",", ".")));
		}
	}





	public void setPesee(String chid, boolean active) {
		if(getStraddleIP(chid) != null){
			this.straddlesWeightingName.put(chid, active);			
			this.straddlesWeightingIP.put(getStraddleIP(chid), active);
			AccessDb.setPesee(chid, active);
		}else{
			LogClass.ecritureError("erreur ecriture dans la base pour pesee straddlename = "+chid);
		}
		
	}
	
	public void setRAZPesee(String chid, boolean active) {
		if(getStraddleIP(chid) != null){
			this.straddlesRAZWeightingName.put(chid, active);			
			//this.straddlesWeightingIP.put(getStraddleIP(chid), active);
			AccessDb.setRAZPesee(chid, active);
		}else{
			LogClass.ecritureError("erreur ecriture dans la base pour pesee straddlename = "+chid);
		}
		
	}

	private void setNewIP(String chid, String newIP) {
		// mettre a jour l'information IP dans la base de donnees
		AccessDb.setIPPesee(chid, newIP);
	}



	private String getStraddleIP(String chid) {
		Iterator it = straddlesDataIP.keySet().iterator();
		while(it.hasNext()){
			String ip = (String) it.next();
			if(straddlesDataIP.get(ip).getStraddleName().equalsIgnoreCase(chid)){
				return ip;
			}
		}
		return null;
	}





	public void majPesee(String[] checkbox, String[] checkboxRAZ, String[] ipPesee) {
		if(checkbox == null || checkbox.length==0){
			setAllPeseeDisabled();
			return;
		}else{
			List<String> liste =  Arrays.asList(checkbox);
			List<String> listeRAZ = null;
			boolean cond = false;
			if(checkboxRAZ != null && checkboxRAZ.length > 0){
				listeRAZ = Arrays.asList(checkboxRAZ);
				cond = true;
			}
			
			for(int i=0; i<straddlesList.size();i++){
				if(!ipPesee[i].equalsIgnoreCase(straddlesDataName.get(straddlesList.get(i)).getIpAddress())){
					String ancienIP = straddlesDataName.get(straddlesList.get(i)).getIpAddress();
					straddlesDataIP.remove(ancienIP);
					straddlesDataName.get(straddlesList.get(i)).setIpAddress(ipPesee[i]);
					straddlesDataIP.put(ipPesee[i], straddlesDataName.get(straddlesList.get(i)));
					setNewIP(straddlesList.get(i), ipPesee[i]);
				}// sinon Pas de changement, on ne fait rien
				if(cond){
					if(listeRAZ.contains(straddlesList.get(i))){
						setRAZPesee(straddlesList.get(i), true);
						straddlesRAZWeightingName.put(straddlesList.get(i), true);
						//straddlesWeightingIP.put(getStraddleIP(straddlesList.get(i)), true);
					}else{
						setRAZPesee(straddlesList.get(i), false);
						straddlesRAZWeightingName.put(straddlesList.get(i), false);
						//straddlesWeightingIP.put(getStraddleIP(straddlesList.get(i)), false);
					}
				}
				
				if(liste.contains(straddlesList.get(i))){
					setPesee(straddlesList.get(i), true);
					straddlesWeightingName.put(straddlesList.get(i), true);
					straddlesWeightingIP.put(getStraddleIP(straddlesList.get(i)), true);
				}else{
					setPesee(straddlesList.get(i), false);
					straddlesWeightingName.put(straddlesList.get(i), false);
					straddlesWeightingIP.put(getStraddleIP(straddlesList.get(i)), false);
					setRAZPesee(straddlesList.get(i), false);
					straddlesRAZWeightingName.put(straddlesList.get(i), false);
				}
				
				
			}			
		}
	}
	
	
	






	private void setAllPeseeDisabled() {
		for(int i=0; i<straddlesList.size();i++){
				setPesee(straddlesList.get(i), false);
				straddlesWeightingName.put(straddlesList.get(i), false);
				straddlesWeightingIP.put(getStraddleIP(straddlesList.get(i)), false);
		}
		//setIsUp(false);		
	}
	
	public Map<String, Boolean> getWeightingList(){
		return this.straddlesWeightingName;
	}
	public Map<String, Boolean> getRAZWeightingList(){
		return this.straddlesRAZWeightingName;
	}
	private List<String> getStraddles(){
		List<String> liste = new ArrayList<String>();
		Iterator<String> it = straddlesWeightingName.keySet().iterator();
		while(it.hasNext()){
			liste.add(it.next());
		}
		return liste;
	}





	public List<String> getStraddlesList() {
		return straddlesList;
	}





	public void setStraddlesList(List<String> straddlesList) {
		this.straddlesList = straddlesList;
	}
	
	
}
