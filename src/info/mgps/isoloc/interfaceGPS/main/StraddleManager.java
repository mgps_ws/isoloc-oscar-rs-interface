package info.mgps.isoloc.interfaceGPS.main;

import info.mgps.isoloc.interfaceGPS.service.log.AccessDb;
import info.mgps.isoloc.interfaceGPS.service.message.MessageFor;
import info.mgps.isoloc.interfaceGPS.service.message.MessageManager;
import info.mgps.isoloc.interfaceGPS.service.messageStraddle.DealMsgStraddle;
import info.mgps.isoloc.interfaceGPS.service.messageStraddle.MessageCheckListDetails;
import info.mgps.isoloc.interfaceGPS.service.messageStraddle.MessageRAZWeightDetails;
import info.mgps.isoloc.interfaceGPS.service.messageStraddle.MessageStraddleDetails;
import info.mgps.isoloc.interfaceGPS.utilitaire.LogClass;
import info.mgps.isoloc.interfaceGPS.service.xmlNavis.MessageXmlRead;
import info.mgps.isoloc.interfaceGPS.service.xmlNavis.MessageXmlWrite;
import info.mgps.isoloc.interfaceGPS.web.Models.Error;
import info.mgps.isoloc.interfaceGPS.web.Models.ParamsMission;
import info.mgps.isoloc.interfaceGPS.web.Models.Straddle;
import info.mgps.isoloc.interfaceGPS.web.Models.StraddleTimes;
import info.mgps.isoloc.interfaceGPS.web.web.Controllers.MyController;
import info.ttc.egee.xmltos.VAPXml;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;

/**
 * la classe qui g�re les cavaliers
 * @author saad
 */
public class StraddleManager {
	///////////////////////////////////////////////////////////variables
	private Map<String,Straddle> straddles;
	private Map<String, String> filtre = new HashMap<String, String>();
	private int cnxECN4;//pour s'assurer de la connexion � Navis
	private int cnxServer;//pour voir si le serveur et en ecoute ou pas 
	private String msgNotStraddle="";//pour les message qui provienne de navis avec un identifiant straddle erron�
	//private CheckListWSClient webServiceCheckList;
	private int refresh = 6000;
	private Map<String, StraddleTimes> straddleTimes;
	
	private MessageManager messageManager;
	private Map<String, Boolean> checkListUp;
	private WeightManager weightingManager;
	
	
	public int getRefresh(){
		return this.refresh;
	}
	///////////////////////////////////////////////////////////constructeurs
	public StraddleManager(Map<String,Straddle> straddles) {
		this.straddles=new TreeMap<String,Straddle>();
		this.straddles = straddles;
		this.checkListUp = new HashMap<String, Boolean>();
	}
	
	public StraddleManager(int refresh/*CheckListWSClient webServiceCheckList*/) {
		this.straddles=new TreeMap<String,Straddle>();
		this.refresh = refresh * 1000;
		this.straddleTimes = new HashMap<String, StraddleTimes>();
		this.checkListUp = new HashMap<String, Boolean>();
		

		//this.webServiceCheckList = webServiceCheckList;
		/*for(int i=1; i<=35;i++){
			if(i<10)
				this.straddles.put("C0"+i, new Straddle("C0"+i));
			else
				this.straddles.put("C"+i, new Straddle("C"+i));
		}*/
	}

	///////////////////////////////////////////////////////////accesseurs
	public Map<String,Straddle> getStraddles() {
		return straddles;
	}

	public void setStraddles(Map<String,Straddle> straddles) {
		this.straddles = straddles;
	}

	public String toString() {
		return "StraddleManager [straddles=" + straddles + "]";
	}
	
	

	/**
	 * retrourne un cavalier selon l'indice i
	 * @param i
	 * @return Straddle
	 */
	public Straddle getByIndex(int i){
		return this.straddles.get(i);
	}
	
	/**
	 * retourne un cavalier selon le numero donn� en param�tre
	 * @param num
	 * @return
	 */
	public Straddle getByNum(String num){
		return straddles.get(num);
	}
	
	public void setCnxECN4(int cnxECN4) {
		this.cnxECN4 = cnxECN4;
	}

	public void setCnxServer(int cnxServer) {
		this.cnxServer = cnxServer;
	}

	public int getCnxECN4() {
		return cnxECN4;
	}

	public int getCnxServer() {
		return cnxServer;
	}
	
	public String getMsgNotStraddle() {
		return msgNotStraddle;
	}

	public void setMsgNotStraddle(String msgNotStraddle) {
		this.msgNotStraddle = msgNotStraddle;
	}

	/////////////////////////////////////////////////////////////////methodes
	/**
	 * ajoute un cavalier au tableau des cavaliers
	 * @param straddle
	 */
	private void add(Straddle straddle){
		this.straddles.put(straddle.getNum(), straddle);
		this.checkListUp.put(straddle.getNum(), false);// TODO a mettre � true quand c'est en production
	}
	
	private boolean isStraddle(String NameStraddle){
		return straddles.containsKey(NameStraddle);
	}
	/**
	 * Pour l'affichage des filtres
	 * retourne les evenements (erreurs,shunts ...)
	 * @return
	 */
	public StraddleManager getEvents(){
		//TODO a v�rifier webservice
		StraddleManager sm = new StraddleManager(this.refresh/*this.webServiceCheckList*/);
        sm.setCnxECN4(this.getCnxECN4());
        sm.setCnxServer(this.getCnxServer());
        sm.setMsgNotStraddle(this.getMsgNotStraddle());
		Iterator<String> it = straddles.keySet().iterator();
		sm.checkListUp = this.checkListUp;
		while(it.hasNext()){
			String numStrad = it.next();
			if(straddles.get(numStrad).isShuntable() || straddles.get(numStrad).getErreur().getOccur().equals("Y")){
				sm.add(straddles.get(numStrad));
			}
		}		
		return sm;
	}
	
	/**
	 * just pour les testes: genere des cavaliers static
	 */
	public void staticStraddles(){
		ParamsMission sm;
		for(int i=0;i<25;i++){
			Straddle str;
			if(i<10){
				sm = new ParamsMission("CMAU654987"+i,"A0101.B");
				sm.setDisplayMessage("Prendre conteneur "+sm.getConteneur()+" en "+sm.getPosition());
				sm.setContainerLength("40");
			}
				
			else
				if(i<20){
					sm= new ParamsMission(""+i,"A" + Integer.toString(i+1));
					sm.setDisplayMessage("Aller en "+sm.getConteneur()+" en "+sm.getPosition());
					
				}
					
				else{
					sm = new ParamsMission("CMAU654987"+i,"A0302.M");
					sm.setDisplayMessage("Deposer conteneur "+sm.getConteneur()+" "+sm.getContainerLength()+"\" en "+sm.getPosition());
					sm.setContainerLength("20");
				}
					
			
			if(i+1<10){
				str=new Straddle("C0" + (i+1));
				str.setActualFormId(4);
			}				
			else{
				if(i<20){
					str=new Straddle("C" + (i+1));
					str.setActualFormId(3);
				}else{
					str=new Straddle("C" + (i+1));
					str.setActualFormId(6);
				}
				
			}
			
			str.stepCheck();
			str.setState(1);
			add(str);
		}
	}
	
	/**
	 * retourne la longueur du tableau qui contient les cavaliers
	 * @return
	 */
	public int size(){
		return this.straddles.size();
	}

	public String getFiltre(String user) {
		if(filtre.get(user) == null || filtre.get(user).equalsIgnoreCase("tout"))
		return "tout";
		else
			return filtre.get(user);
	}
	
	public void setFiltre(String user,String filtre) {
		this.filtre.put(user, filtre);
		//this.filtre = filtre;
	}
	
	public void setStradState(String straddleNum, int state){
		this.straddles.get(straddleNum).setState(state);
	}
	
	public List<Straddle> getOrderedList(){
		List<Straddle> liste = new ArrayList<Straddle>();
		for(Map.Entry<String, Straddle> entry : straddles.entrySet()){
			liste.add(entry.getValue());
		}
		return liste;
	}
	public List<String> getOrderedListNames(){
		List<String> liste = new ArrayList<String>();
		for(Map.Entry<String, Straddle> entry : straddles.entrySet()){
			liste.add(entry.getValue().getNum());
		}
		return liste;
	}


	/**
	 * affecte un cavalier(selon son numero) par le mode autoshunt
	 * @param straddleNum
	 */
	public void setAutoShunt(String straddleNum) {
		if(this.getByNum(straddleNum).isAutoShunt())
			this.getByNum(straddleNum).setAutoShunt(false);
		else
			this.getByNum(straddleNum).setAutoShunt(true);
	}

	public boolean isCheckListEnabled(String name){
		if(this.checkListUp.get(name) != null){
			return this.checkListUp.get(name);
		}else
			return false;
	}
	/**
	 * pour s'assurer si on devrait shunter ou pas
	 * cette fonction affecte la gPSPositiondu cavalier
	 * @param straddleNum le numer du cavalier
	 * @param cible = ex:B0101B
	 * @param gPSPosition la position que le gps envoie : V:B0101B
	 * @return boolean
	 */
	public boolean checkForShunt(String straddleNum, String cibleDisplay, String gPSPosition) {
		
		if(gPSPosition!=null && !gPSPosition.equals("") && this.getByNum(straddleNum).getState() != 0){
			//M�morisation de la position GPS venant du cavalier
			this.getByNum(straddleNum).setGpsPosition(gPSPosition , cibleDisplay);
			
//			System.out.println("cibleDisplay:" + cibleDisplay + " gPSPosition:" + gPSPosition + " CtrlGPS:" +
//			this.getByNum(straddleNum).isCtrlGPS() + " AutoShunt: " + this.getByNum(straddleNum).isAutoShunt());
			
			//V�rification de la condition de Shunt cible<>cibleGPS
			// modifier la comparaison avec une fonction locale qui prend en charge le d�but de la position (si commence par ZC, on ne compare que
			// les ZCXXX au lieu de comparer ZCXXX.X, � voir si on compare le startswith ou bien r�duire la taille du plus long et comparer les deux
			//!this.getByNum(straddleNum).getMission().getPositionCible().equals(cibleDisplay)
			if(!equalsPositions(this.getByNum(straddleNum).getMission().getPositionCible(),cibleDisplay)){
				//Condition de non shunt: autoshunt=true ou pas de controle GPS isCtrlGPS=false
				if(this.getByNum(straddleNum).isAutoShunt() || !this.getByNum(straddleNum).isCtrlGPS()){
					//Pas de shunt
					MemoShunt(straddleNum, cibleDisplay, "Position Navis: " +  cibleDisplay + " Position GPS: " + gPSPosition);
					return false;
					//Condition du shunt P Before
				}else if (this.getByNum(straddleNum).isShuntP_Before()){
					//1)V�rification du shunt P
					if (verificationShuntP(gPSPosition)){
						//Shunt P before ok					
						AccessDb.MemologMessage(straddleNum, 1, "", "", 1, "SHUNT P avant -- Position Navis: " +getByNum(straddleNum).getMission().getPosition()+
								" Position GPS: "+cibleDisplay+" Position Transmise: "+gPSPosition);
						//Changement de la position cible par celle donn�e par le GPS car le shunt ex�cut� donne celle de la mission
						getByNum(straddleNum).getMission().setPosition(gPSPosition);
						//Raz de la variable Shunt P Before
						getByNum(straddleNum).setShuntP_Before(false);
						return false;
					}else{
						//Shunt
						AccessDb.MemologMessage(straddleNum, 1, cibleDisplay,"",  1, "Position Navis: " +  cibleDisplay + " Position GPS: " + gPSPosition);
						//Je mets ma variable de shunt � true qui me permet d'afficher l'�v�nement de shunt
						this.getByNum(straddleNum).setShuntable(true);
						return true;
					}
				}
				else{
					//Je mets ma variable de shunt � true qui me permet d'afficher l'�v�nement de shunt
					this.getByNum(straddleNum).setShuntable(true);
				}
				//Shunt
				AccessDb.MemologMessage(straddleNum, 1, cibleDisplay,"",  1, "Position Navis: " +  cibleDisplay + " Position GPS: " + gPSPosition);
				return true;
			}else{
				//Pas de shunt
				MemoShunt(straddleNum, cibleDisplay, "Position Navis: " +  cibleDisplay + " Position GPS: " + gPSPosition);
				return false;
			}
		}
		//Pas de shunt
		MemoShunt(straddleNum, cibleDisplay, "Position Navis: " +  cibleDisplay + " Position GPS: " + gPSPosition);
		return false;
	}

	private boolean equalsPositions(String positionCible, String cibleDisplay) {
		if(positionCible.startsWith("ZC")){
			// En zone d'�change, v�rifier que les 5 premiers caracteres
			return positionCible.subSequence(0, 5).equals(cibleDisplay.subSequence(0, 5));
		}else
			return positionCible.equals(cibleDisplay);
	}

	/**
	 * Permet de m�moriser le shunt ou non  de la fonction CheckForShunt
	 * en incluant le shunt si le GPSDisplay  est incoorect dans le format
	 * @param mobile
	 * @param typeMsg
	 * @param emplacement
	 * @param conteneur
	 * @param shunt
	 * @param message
	 */
	private void MemoShunt(String mobile, String emplacement, String message){
		if (emplacement.length()>10){
			AccessDb.MemologMessage(mobile, 1, emplacement,"",  1, message);			
		}else{
			AccessDb.MemologMessage(mobile, 1, emplacement,"",  0, message);
		}
	}
	public void setMission(String straddleNum, String position, String mission,String missionType,String conteneur) {
		if(position!=null && !position.equals("") && this.getByNum(straddleNum).getState() != 0){
			ParamsMission m;
			m=new ParamsMission();
			if(mission.equals("Vas en")){
				m=new ParamsMission("", position);
			}
			else if(mission.equals("Prendre le conteneur")){
				m=new ParamsMission(conteneur, position);
			}
			else if(mission.equals("Deposer le conteneur")){
				m=new ParamsMission(conteneur, position);
			}
			this.getByNum(straddleNum).setMission(m);
		}
	}
	/**
	 * affecte l'attribut error d'un cavalier selon son numero
	 * @param straddleCheck numero du cavalier
	 * @param error
	 */
	public void setError(String straddleCheck, Error error) {
		getByNum(straddleCheck).setErreur(error);
	}

	
	public MessageFor dealMessageCheckList(String nameStraddle, MessageCheckListDetails readMsgCheckList) {
		// traitement des messages venant de la checkList
		int type = 1; // initialisation de la part de la checklist (envoi au straddle type 1)
		if(!readMsgCheckList.isFromCheckList()){
			// venant de la part du straddle
			type = 3;			
		}	
		
		
		
		return new MessageFor(type, readMsgCheckList.getMessage());
	}


	/**
	 * Traitement du message venant de Ecn4
	 * @param msg
	 * @return string = message � envoyer aux cavaliers 
	 */
	public MessageFor dealMessageECN4(int msId, MessageXmlRead msg) {	
		//Message d'ereur qui ne correspond � aucun cavalier
		if (!isStraddle(msg.getChid())){
			if (msg.getErrorMsgID() != null && !msg.getErrorMsgID().equals("0") ) {
			this.setMsgNotStraddle(msg.getDisplayMsg());
			}
			LogClass.ecriture("StraddleManager/dealMessageECN4", "L'identifiant : "  + msg.getChid() + " n'existe pas.", 1);
			return new MessageFor(1, "","");	
		}
				
		//Reset de la variable de shunt P Before
		getByNum(msg.getChid()).setShuntP_Before(false);
		
		//Reset de la variable tempoMission
		if (getByNum(msg.getChid()).isTmpBool()){
			getByNum(msg.getChid()).setTempoMissionNok();
			getByNum(msg.getChid()).setTmpBool(false);
		}
		
		//Condition d'affichage de l'erreur :
		//Si j'ai la m�me actualForm j'affiche l'erreur sur l'interface
		//getByNum(msg.getChid()).getActualFormId()!=0  : Permet de ne pas avoir d'erreur au d�marrage de l'Interface
		if((msg.getErrorMsgID() != null) && (!msg.getErrorMsgID().equals("0"))){
		
			if (msg.getErrorMsgID().equals("200")){
	
				try {
					String tb[] = msg.getDisplayMsg().split("'");
					if (tb.length > 1) {
						msg.setDisplayMsg("Carte inconnue : " + tb[1] + "\nAppeler le bureau");
					}	
				} catch (Exception e) {

				}
				
				//J'affiche l'erreur et je l'envoie au straddle
				getByNum(msg.getChid()).setErreur(msg.getDisplayMsg());
				AccessDb.MemologMessage(msg.getChid(),2, "", "", 0, msg.getDisplayMsg());
				//Traitement du message actualisation de la form
				getByNum(msg.getChid()).setActualFormId(msg.getFormidInt());
				getByNum(msg.getChid()).setMemoMsgStraddle(DealMsgStraddle.sendMessageInfoWithoutRehandle(msg.getChid(), msg.getDisplayMsg()));
				return new MessageFor(1, getByNum(msg.getChid()).getNameIP(), getByNum(msg.getChid()).getMemoMsgStraddle());
				
			}else if (msg.getErrorMsgID().equals("7")) { 
                //Pour le traitement de ce message d'erreur je ne fais rien
                //C'est un message d'erreur qui viens lorsque le chauffeur appuie 2 fois sur entr�e
                //La premi�re est ok et la deuxi�me il ya le message d'erreur
               
               
          }else if (msg.getErrorMsgID().equals("555")) { 
              //Pour le traitement de ce message d'erreur je ne fais rien
              //C'est un message d'erreur qui viens lorsque le chauffeur appuie sur entr�e et que Navis n'a pas encore calcul� la mission
              //Container XXXXXXXXXXX planned to FLD Contact Operator
             
             
        }else if (msg.getFormidInt() == 10) {	
				getByNum(msg.getChid()).setErreur(msg.getDisplayMsg());
				AccessDb.MemologMessage(msg.getChid(),2, "", "", 0, msg.getDisplayMsg());
				//Traitement du message actualisation de la form
				getByNum(msg.getChid()).setActualFormId(msg.getFormidInt());
				getByNum(msg.getChid()).setMemoMsgStraddle(DealMsgStraddle.sendMessageInfoWithoutRehandle(msg.getChid(), msg.getDisplayMsg()));
				return new MessageFor(1, getByNum(msg.getChid()).getNameIP(),getByNum(msg.getChid()).getMemoMsgStraddle());	
				
			// si m�me form je n'affiche pas l'erreur dans le cas d'une reconnection
				//Vu l'actuel fonctionnement �a ne sert plus
			//}else if (getByNum(msg.getChid()).getActualFormId()!=msg.getFormidInt() && ((getByNum(msg.getChid()).getActualFormId()!=0))) {
			}else if ((getByNum(msg.getChid()).getActualFormId()!=0)) {
				//J'affiche l'erreur et je l'envoie au straddle
				getByNum(msg.getChid()).setErreur(msg.getDisplayMsg());
				AccessDb.MemologMessage(msg.getChid(),2, "", "", 0, msg.getDisplayMsg());
				//Traitement du message actualisation de la form
				getByNum(msg.getChid()).setActualFormId(msg.getFormidInt());
				//Selon la forme actuelle je permet ou non le manuelRehandle
				//Je permets le manuelRehandle
				if((msg.getFormidInt() == 1) || (msg.getFormidInt() == 2)  || (msg.getFormidInt() == 7))  {
					getByNum(msg.getChid()).setMemoMsgStraddle(DealMsgStraddle.sendMessageInfo(msg.getChid(), msg.getDisplayMsg()));
					return new MessageFor(1, getByNum(msg.getChid()).getNameIP(),getByNum(msg.getChid()).getMemoMsgStraddle());	
				}else {//je ne permets pas le manuelRehandle
					getByNum(msg.getChid()).setMemoMsgStraddle(DealMsgStraddle.sendMessageInfoWithoutRehandle(msg.getChid(), msg.getDisplayMsg()));
					return new MessageFor(1, getByNum(msg.getChid()).getNameIP(),getByNum(msg.getChid()).getMemoMsgStraddle());	
				}
			}
				
		}else{		
			//Je fait un reset de l'erreur + raz position GPS et display GPS position
			getByNum(msg.getChid()).razErreur();
			// Suppression du warning si le shunt se passe bien avec ECN4 (pas d'erreur en retour)
			getByNum(msg.getChid()).setShuntable(false);
			// Raz du display Position GPS
		}
	
	
		//Traitement du message actualisation de la form
		getByNum(msg.getChid()).setActualFormId(msg.getFormidInt());
		
//		//traitement du second message de d�connexion
//		if (getByNum(msg.getChid()).isTopDeconnexion()){
//			AccessDb.MemologMessage(msg.getChid(), 1, "", "", 0, getByNum(msg.getChid()).getDisplayMessage());		
//			//Je mets ma variable � true pour me permettre d'envoyer le second message de d�connexion
//			getByNum(msg.getChid()).setTopDeconnexion(false);
//			return new MessageFor(false, MsgDisconnect(msId , msg.getChid(), getByNum(msg.getChid()).getActualFormId()));
//		}
		int nbMoves = 0;
		switch(msg.getFormidInt()){
		
		case 1:
			// FORM_LOGIN
			straddleTimes.get(msg.getChid()).setDateDeconnexion(new Date());
			getByNum(msg.getChid()).disConnect();
			AccessDb.MemologMessage(msg.getChid(),2, "", "", 0, getByNum(msg.getChid()).getDisplayMessage());
			getByNum(msg.getChid()).setMemoMsgStraddle(DealMsgStraddle.sendLogout(msg.getChid(),  getByNum(msg.getChid()).getIdentifiant()));
			return new MessageFor(1, getByNum(msg.getChid()).getNameIP(),getByNum(msg.getChid()).getMemoMsgStraddle());		
		case 2:
			// FORM_STRADDLE_UNAVAILABLE
			// avant la connexion, v�rifier si checklist est activ�e, pour faire le travail demand�.
			// TODO connexion du straddle, enregistrer la date et faire le necessaire juste quand on re�oi le idle
			// pour enregistrer le temps ecoule entre les deux
			// faire le lien avec la checkList cavaliers
			if(MyController.isCheckListUp()){
				LogClass.ecriture("StraddleManager", "CheckList UP", 1);
				//System.out.println("checklist up");
				// Si la checkList est activ�e
				if(checkListUp.get(msg.getChid())!=null && checkListUp.get(msg.getChid())){
					LogClass.ecriture("StraddleManager", "checklist straddle up "+msg.getChid(), 1);
					//System.out.println("checklist straddle up "+msg.getChid());
					messageManager.checkListSendMessage(DealMsgStraddle.sendLogin(msg.getChid(), getByNum(msg.getChid()).getIdentifiant(), this.weightingManager.isRAZUp(msg.getChid())));
				}
				
			}
			//straddleTimes.get(msg.getChid()).setDateCnx(new Date());
			// Si pas activ�e on continue sans probl�me, le soucis est que la checklist doit faire � la fin le travail fait ici en dessous
			getByNum(msg.getChid()).connect();
			AccessDb.MemologMessage(msg.getChid(),2, "", "", 0, getByNum(msg.getChid()).getDisplayMessage());
			getByNum(msg.getChid()).setMemoMsgStraddle(DealMsgStraddle.sendLogin(msg.getChid(),  getByNum(msg.getChid()).getIdentifiant(), this.weightingManager.isRAZUp(msg.getChid())));
			return new MessageFor(1, getByNum(msg.getChid()).getNameIP(),getByNum(msg.getChid()).getMemoMsgStraddle());	
		case 3:
			// FORM_STRADDLE_EMPTY_TO_ORIGIN
			//straddleTimes.get(msg.getChid()).setDateEmptyToOrigin(new Date());
			getByNum(msg.getChid()).emptyToOrigin(msg.getPositionPrendre().getArea(), msg.getPositionPrendre().getArea_Type(),msg.getPositionPrendre().getVbay(),
					msg.getContainer().getLnth(), msg.getIngress(), msg.getContainer().getShipNumberUnload());
			AccessDb.MemologMessage(msg.getChid(),2,  "",  "", 0, getByNum(msg.getChid()).getDisplayMessage());
			getByNum(msg.getChid()).setMemoMsgStraddle(DealMsgStraddle.sendMission(msg.getChid(), getByNum(msg.getChid()).getDisplayMessage(), 1, 
					 getByNum(msg.getChid()).getMission().getPosition(),getByNum(msg.getChid()).getMission().getDisplayPosition()));
			return new MessageFor(1, getByNum(msg.getChid()).getNameIP(),getByNum(msg.getChid()).getMemoMsgStraddle());	
		case 4:
			// FORM_STRADDLE_EMPTY_AT_ORIGIN
			//straddleTimes.get(msg.getChid()).setDateEmptyAtOrigin(new Date());
			if(weightingManager.isUp()){
				weightingManager.incrementJobId(msg.getChid());
				weightingManager.setContainerID(msg.getChid(), msg.getContainer().getEqID(), msg.getContainer().getWeight());
			}
			getByNum(msg.getChid()).setPosSrc(msg.getPositionPrendre().getPpos(), msg.getPositionPrendre().getArea_Type());
			getByNum(msg.getChid()).setEmptyAtOrigin( msg.getPositionPrendre().getPpos(), msg.getPositionPrendre().getArea_Type(),
					msg.getContainer().getEqID(), msg.getContainer().getLnth(), msg.getContainer().getJpos(),
					msg.getTwinPut(),msg.getPositionPrendre().getTkps(),msg.getContainer().getDgx(),msg.getContainer().getWeight(),msg.getPositionPrendre().getVbay(),
					msg.getContainer().getOog(),msg.getMvkd(), msg.getContainer().getShipNumberUnload());
			AccessDb.MemologMessage(msg.getChid(),2, msg.getPositionPrendre().getPpos(), msg.getContainer().getEqID(), 0, getByNum(msg.getChid()).getDisplayMessage());
			getByNum(msg.getChid()).setMemoMsgStraddle(DealMsgStraddle.sendMission(msg.getChid(), getByNum(msg.getChid()).getDisplayMessage(), 2, 
					 getByNum(msg.getChid()).getMission().getPosition(),getByNum(msg.getChid()).getMission().getDisplayPosition()));
			return new MessageFor(1, getByNum(msg.getChid()).getNameIP(),getByNum(msg.getChid()).getMemoMsgStraddle());		
		case 5:
			// FORM_STRADDLE_LADEN_TO_DEST
			
			//straddleTimes.get(msg.getChid()).setDateLadenToDest(new Date());
			getByNum(msg.getChid()).setLadenToDest(msg.getPositionPoser().getArea(), msg.getPositionPoser().getArea_Type(),msg.getPositionPoser().getVbay(),
					msg.getContainer().getEqID(),msg.getContainer().getLnth(), msg.getIngress(),msg.getContainer().getShipNumberLoad());
			AccessDb.MemologMessage(msg.getChid(),2, "", "", 0, getByNum(msg.getChid()).getDisplayMessage());
			getByNum(msg.getChid()).setMemoMsgStraddle(DealMsgStraddle.sendMission(msg.getChid(), getByNum(msg.getChid()).getDisplayMessage(), 3, 
					 getByNum(msg.getChid()).getMission().getPosition(), getByNum(msg.getChid()).getMission().getDisplayPosition()));
//			EGEEVAP.createVAP(chid,date1, date1,conteneur, posPrise, posPos);
			
			updateNbMoves();
			nbMoves ++;
			if(nbMoves == 3){
				updateNbMoves();
				nbMoves = 0;
			}
			return new MessageFor(1, getByNum(msg.getChid()).getNameIP(),getByNum(msg.getChid()).getMemoMsgStraddle());	
		case 6:
			// FORM_STRADDLE_LADEN_AT_DEST
			//straddleTimes.get(msg.getChid()).setDateLadenAtDest(new Date());
			getByNum(msg.getChid()).setLadenAtDest(msg.getPositionPoser().getPpos(), msg.getPositionPoser().getArea_Type(),
					msg.getContainer().getEqID(), msg.getContainer().getLnth(),msg.getContainer().getJpos(),
					msg.getTwinPut(),msg.getPositionPoser().getTkps(),msg.getContainer().getDgx(),msg.getContainer().getWeight(),msg.getPositionPoser().getVbay(),
					msg.getContainer().getOog() ,msg.getMvkd(),msg.getContainer().getShipNumberLoad());
			AccessDb.MemologMessage(msg.getChid(),2, msg.getPositionPoser().getPpos(), msg.getContainer().getEqID(), 0, getByNum(msg.getChid()).getDisplayMessage());
			getByNum(msg.getChid()).setMemoMsgStraddle(DealMsgStraddle.sendMission(msg.getChid(), getByNum(msg.getChid()).getDisplayMessage(), 4, 
					 getByNum(msg.getChid()).getMission().getPosition(),getByNum(msg.getChid()).getMission().getDisplayPosition()));
			return new MessageFor(1, getByNum(msg.getChid()).getNameIP(),getByNum(msg.getChid()).getMemoMsgStraddle());	
		case 7:
			// FORM_STRADDLE_IDLE
			//straddleTimes.get(msg.getChid()).setDateIdle(new Date());
			getByNum(msg.getChid()).setAvailable();
			AccessDb.MemologMessage(msg.getChid(),2, "", "", 0, getByNum(msg.getChid()).getDisplayMessage());
			getByNum(msg.getChid()).setMemoMsgStraddle(DealMsgStraddle.sendMessageWaitWork(msg.getChid(), "Rien � faire Patientez "));
			return new MessageFor(1, getByNum(msg.getChid()).getNameIP(),getByNum(msg.getChid()).getMemoMsgStraddle());	
		case 8:
			// FORM_STRADDLE_AUTO_REHANDLE Etape1
			if(weightingManager.isUp()){
				weightingManager.incrementJobId(msg.getChid());
				weightingManager.setContainerID(msg.getChid(), msg.getContainer().getEqID(), msg.getContainer().getWeight());
			}
			
			//straddleTimes.get(msg.getChid()).setDateAutoRehandle(new Date());
			getByNum(msg.getChid()).setAutoRehandle(msg.getPositionPrendre().getPpos(),msg.getPositionPoser().getPpos(),msg.getContainer().getEqID(), msg.getContainer().getLnth(),
					msg.getContainer().getDgx(),msg.getContainer().getWeight(),msg.getContainer().getOog());
			AccessDb.MemologMessage(msg.getChid(),2, msg.getPositionPrendre().getPpos() +  ";" + 
					msg.getPositionPoser().getPpos(), msg.getContainer().getEqID(), 0, getByNum(msg.getChid()).getDisplayMessage());
			getByNum(msg.getChid()).setMemoMsgStraddle(DealMsgStraddle.sendMission(msg.getChid(), getByNum(msg.getChid()).getDisplayMessage(),5,
					 getByNum(msg.getChid()).getMission().getPosition(),getByNum(msg.getChid()).getMission().getDisplayPosition()));
			return new MessageFor(1, getByNum(msg.getChid()).getNameIP(), getByNum(msg.getChid()).getMemoMsgStraddle());	
		case 10:
			// FORM_STRADDLE_MANUAL_REHANDLE
			//straddleTimes.get(msg.getChid()).setDateManualRehandle(new Date());
			getByNum(msg.getChid()).setManualRehandle(msg.getPositionPoser().getPpos(), msg.getContainer().getEqID(), msg.getContainer().getLnth(),
					msg.getContainer().getDgx(),msg.getContainer().getWeight(),msg.getContainer().getOog());
			AccessDb.MemologMessage(msg.getChid(),2, msg.getPositionPoser().getPpos(), msg.getContainer().getEqID(), 0, getByNum(msg.getChid()).getDisplayMessage());
			getByNum(msg.getChid()).setMemoMsgStraddle(DealMsgStraddle.sendMission(msg.getChid(), getByNum(msg.getChid()).getDisplayMessage(),  7, 
					 getByNum(msg.getChid()).getMission().getPosition(),getByNum(msg.getChid()).getMission().getDisplayPosition()));
			return new MessageFor(1, getByNum(msg.getChid()).getNameIP(), getByNum(msg.getChid()).getMemoMsgStraddle());	
		default :
			AccessDb.MemologMessage(msg.getChid(),2, "", "", 0, getByNum(msg.getChid()).getDisplayMessage());
			return new MessageFor(1, getByNum(msg.getChid()).getNameIP(), "");					
		}	
	}

	private void updateNbMoves() {
		AccessDb.updateNbMoves();
		
	}
	/**
	 * Retourner le message XML � envoyer vers ECN4
	 * 
	 * @param msId
	 * @param straddleNum
	 * @param position
	 * @return
	 */
	public MessageFor getMessageProgressECN4(int msId, String straddleNum, String position) {		
		// Traitement des �tapes des missions
		return dealEtapeMission(msId, straddleNum, position);
	}

	/**
	 * Traitement des �tapes des missions
	 * @param msId
	 * @param straddleNum
	 * @param position
	 * @return
	 */
	private MessageFor dealEtapeMission(int msId, String straddleNum, String position){
		switch (getByNum(straddleNum).getActualFormId()) {
		case 3:
			// FORM_STRADDLE_EMPTY_TO_ORIGIN			
			return new MessageFor(2, MessageXmlWrite.sendEmptyToOrigin(msId, straddleNum, position));
		case 4:
			// FORM_STRADDLE_EMPTY_AT_ORIGIN
			getByNum(straddleNum).setDatePosSrc(new Date());
			return new MessageFor(2, MessageXmlWrite.sendEmptyAtOrigin(msId, straddleNum, position, getByNum(straddleNum).getMission().getConteneur(),getByNum(straddleNum).getMission().getSpos()));
		case 5:
			// FORM_STRADDLE_LADEN_TO_DEST
			return new MessageFor(2, MessageXmlWrite.sendLadenToDest(msId, straddleNum, position));
		case 6:
			// FORM_STRADDLE_LADEN_AT_DEST		
			// TODO ici mettre l'appel vers la fonction qui cr�e le VAParc pour EGEE
			String areaTypeSrc = getByNum(straddleNum).getAreaTypePosSrc();
			/*if(areaTypeSrc.equalsIgnoreCase("grid")){
				// A priori la zone d'�change et la gare doivent �tre grid � confirmer
				// Dans le xsd o distingue Grid et Rail, mais pour la p�niche, il faut un nouveau ou d�duire dans EGEE
				
			}	*/		
			VAPXml vapxml = new VAPXml();
			vapxml.createVAP(straddleNum, getByNum(straddleNum).getDatePosSrc(), new Date(), getByNum(straddleNum).getMission().getConteneur(), getByNum(straddleNum).getPosSrc(), getByNum(straddleNum).getMission().getPosition(), getByNum(straddleNum).getAreaTypePosSrc(), "EUROFOS");
			//vapxml.createVAP(straddleNum, , datePoseTcParc, numConteneur, posPriseTcZE, posPoseTcParc, typeMvt, manut);
			getByNum(straddleNum).setPosSrc("", "");			
			return new MessageFor(2, MessageXmlWrite.sendLadenAtDest(msId, straddleNum, position));
		case 8:
			// FORM_STRADDLE_AUTO_REHANDLE Etape1	
			getByNum(straddleNum).AutoRehandleEtape2();//Je mets � jour pour l'�tape suivante
			getByNum(straddleNum).setMemoMsgStraddle(DealMsgStraddle.sendMission(straddleNum, getByNum(straddleNum).getDisplayMessage(),6,
					 getByNum(straddleNum).getMission().getPosition(),getByNum(straddleNum).getMission().getDisplayPosition()));
			return new MessageFor(1,getByNum(straddleNum).getMemoMsgStraddle());	
		case 9:
			// FORM_STRADDLE_AUTO_REHANDLE Etape2
			return new MessageFor(2, MessageXmlWrite.sendAutoRehandle(msId, straddleNum, position, getByNum(straddleNum).getMission().getConteneur()));
		case 10:
			// FORM_STRADDLE_MANUAL_REHANDLE
			return new MessageFor(2, MessageXmlWrite.sendManuelRehandleEtape2(msId, straddleNum, position, getByNum(straddleNum).getMission().getConteneur()));
		default:
			return new MessageFor(2, "");
		}
	}
	
	/**
	 * Traitement du messsage venant du straddle
	 * @param nameStraddle = adresse IP
	 * @param MessageStraddleDetails = Message venant du straddle
	 * @return
	 */
	public MessageFor dealMessageStraddle(int msId, String nameIP, MessageStraddleDetails msg) {
		
		//M�morisation de l'adresse IP du cavalier pour la communication;
		getByNum(msg.getName()).setNameIP(nameIP);
		
		switch (msg.getEtat()) {
		
		case 1:	//Type connexion
			if (msg.getOnOff() == 1 ){
				//M�morisation de l'identifiant lors de la connexion
				AccessDb.MemologMessage(msg.getName(), 1, "", "", 0,  "---- Connexion de : " + msg.getIdentifiant());
				if (getByNum(msg.getName()).isTmpBool()){
					getByNum(msg.getName()).setTempoMissionOk();
					msgTemporisation(msg.getName(), msg.getIdentifiant());
					return new MessageFor(1, DealMsgStraddle.sendMessageInfo(msg.getName(), " Probl�me de connexion \n Appeler l'op�rateur"));
				}
				getByNum(msg.getName()).setIdentifiant(msg.getIdentifiant());	//"E3B652E7"); //
				
				return new MessageFor(2, MessageXmlWrite.sendLogin(msId, msg.getIdentifiant(), msg.getName()));
				
			}else{
				AccessDb.MemologMessage(msg.getName(), 1, "", "", 0, "----Deconnexion de : "+msg.getIdentifiant() );
				//Je peux me d�connecter
				if (getByNum(msg.getName()).getActualFormId() == 0 || getByNum(msg.getName()).getActualFormId() == 1 || 
						getByNum(msg.getName()).getActualFormId() == 2 || getByNum(msg.getName()).getActualFormId() == 7){
					return new MessageFor(2, MsgDisconnect(msId, msg.getName(), getByNum(msg.getName()).getActualFormId()));
				}else {
					msgEnleveCarte(msg.getName(), msg.getIdentifiant());
					return new MessageFor(2, "");
				}
			}
 
		case 2://Type available
			if (msg.getOnOff() == 1 ){
				AccessDb.MemologMessage(msg.getName(), 1, "", "", 0, "----Disponible " + msg.getName());
				return new MessageFor(2, MessageXmlWrite.sendAvailable(msId, msg.getName()));
			}else{
				AccessDb.MemologMessage(msg.getName(), 1, "", "", 0,"----Indisponible "  + msg.getName());
				return new MessageFor(2, MessageXmlWrite.sendUnavailable(msId, msg.getName()));
			}
			
		case 3:	//Mission
			//Le log applicatif se fait dans la fonction checkForShunt
			if (!checkForShunt(msg.getName(),  msg.getDisplayPosition(), msg.getPositions(msg.positionsCount()-1).getCible())){				
				return dealEtapeMission(msId,  msg.getName(), getByNum(msg.getName()).getMission().getPosition());			
			}else {				
				return new MessageFor(1, DealMsgStraddle.sendMessageShunt(getByNum(msg.getName()).getDisplayMessage(),msg.getName()));
			}
			
		case 4:	//Manuel Rehandle
			AccessDb.MemologMessage(msg.getName(), 1, "",  msg.getPositions(0).getCible(), 0," Manuel Rehandle ");
			return new MessageFor(2, MessageXmlWrite.sendManuelRehandleEtape1(msId, msg.getName(), msg.getPositions(msg.positionsCount()-1).getCible()));
		
			
		default :
			return new MessageFor(2, "");
		}
		
	}

	public String extractStraddleName(String nameStraddle){
		if(nameStraddle.equalsIgnoreCase(MyController.getAntiCollisionServerAddress())){
			return "anticollision";
		}else
			if(nameStraddle.equalsIgnoreCase(MyController.getIpCheckListStraddle())){
				LogClass.ecriture("StraddleManager", "connexion de la checkList", 1);
				//System.out.println("connexion de la checkList");
				return "checkList";
			}
		StringTokenizer s = new StringTokenizer(nameStraddle,".");
		s.nextToken();
		s.nextToken();
		String name;
		String id=s.nextToken();
		if(Integer.parseInt(id)<10){
			name = "C0" +  id;
		}
		else{
			name = "C" +  id;
		}
		return name;
	}
	/**
	 * Connection d'un cavalier 
	 * et cr�ation du cavalier s'il n'existe pas.
	 * @param nameStraddle = ip du straddle sachant que le num�ro de straddle = 3i�me integer
	 */
	public void connect(String nameStraddle) {
		try {
			String name=this.extractStraddleName(nameStraddle);
			LogClass.ecriture("StraddleManager", "extract "+name, 1);
			//System.out.println("extract "+name);
			if(!name.equalsIgnoreCase("anticollision") && !name.equalsIgnoreCase("checkList")){
				if (!straddles.containsKey(name)){
					Straddle str = new Straddle(name);
					str.setNameIP(nameStraddle);
					add(str);
					//this.straddles.put(name, str);
					this.straddleTimes.put(name, new StraddleTimes(name));
				}
				this.getByNum(name).setCnxServer(1);
			}
			
		} catch (Exception e) {
			LogClass.ecriture("StraddleManager/connect", e.getMessage(), 3);
		}	
	}

	/**
	 * D�connection du cavalier
	 * @param nameStraddle
	 */
	public MessageFor disconnect(int msId, String nameStraddle) {
		try {			
			String name = this.extractStraddleName(nameStraddle);
			if(!name.equalsIgnoreCase("anticollision") && !name.equalsIgnoreCase("checkList")){
				if (straddles.containsKey(name)){
					this.getByNum(name).setCnxServer(0);
				}
				//Je peux me d�connecter
				if (getByNum(name).getActualFormId() == 0 || getByNum(name).getActualFormId() == 1 || 
						getByNum(name).getActualFormId() == 2 || getByNum(name).getActualFormId() == 7){
					return new MessageFor(2, MsgDisconnect(msId, name, getByNum(name).getActualFormId()));
				}else {
					msgDeconnexion(name, getByNum(name).getIdentifiant());
					return new MessageFor(2, "");
				}
			}
			return null;
			
		} catch (Exception e) {
			LogClass.ecriture("StraddleManager/disconnect", e.getMessage(), 3);
			return new MessageFor(2, "");
		}		
	}
	
	/***
	 * Retour de la d�connexion en fonction de l'actualFormid
	 * @param msid
	 * @param chid
	 * @param actualFormId
	 * @return
	 */
	private String MsgDisconnect(int msid, String chid, int actualFormId){
		return MessageXmlWrite.sendDeconnexionIddle(msid, chid);
//		Traitement d'une d�connexion avec job d'abord puis d�connexion ensuite
//		switch (actualFormId) {
//		case 1:// State idle
//			return MessageXmlWrite.sendDeconnexionIddle(msid, chid);
//		case 2:// State unavailable
//			return MessageXmlWrite.sendDeconnexionAvailable(msid, chid);
//		case 3:// state ToOrigin
//			return MessageXmlWrite.sendDeconnexionToOrigin(msid, chid, getByNum(chid).getMission().getPosition());
//		case 4://State AtOrigin
//			return MessageXmlWrite.sendDeconnexionAtOrigin(msid, chid,  getByNum(chid).getMission().getPosition(), getByNum(chid).getMission().getConteneur(),
//					getByNum(chid).getMission().getJpos(), getByNum(chid).getMission().getSpos());
//		case 5://State ToDest
//			return MessageXmlWrite.sendDeconnexionToDest(msid, chid, getByNum(chid).getMission().getPosition());
//		case 6://State AtDest
//			return MessageXmlWrite.sendDeconnexionAtDest(msid, chid, getByNum(chid).getMission().getPosition(), getByNum(chid).getMission().getJpos());
//		case 7:// State idle
//			return MessageXmlWrite.sendDeconnexionIddle(msid, chid);
//		case 8://State AutoRehandle
//			return MessageXmlWrite.sendDeconnexionAutoRehandle(msid, chid, getByNum(chid).getMission().getPosition(), getByNum(chid).getMission().getConteneur());
//		case 9://State AutoRehandle
//			return MessageXmlWrite.sendDeconnexionAutoRehandle(msid, chid, getByNum(chid).getMission().getPosition(), getByNum(chid).getMission().getConteneur());
//		case 10://State ManuelRehandle
//			return MessageXmlWrite.sendDeconnexionManuRehandle(msid, chid, getByNum(chid).getMission().getPosition(), getByNum(chid).getMission().getConteneur());
//		default:
//			return "";
//		}
	}

	/**
	 * V�rification de la position GPS et de la structure du message
	 * @param GpsPosition
	 * @return true si position ok
	 */
	public boolean verificationShuntP(String GpsPosition){
		if (GpsPosition.startsWith("00000")){
			return false;
		}else{
			return true;
		}	
	}
	
	/**
	 * V�rification de la position GPS et de la structure du message
	 * avec le nom du straddle
	 * @param GpsPosition
	 * @return true si position ok
	 */
	public boolean verificationShuntPStraddle(String StraddleName){
		return verificationShuntP(getByNum(StraddleName).getGpsPosition());
	}
	
	/**
	 * La mise � jour des straddles quand on re�oit le r�sultat de l'autoshunt
	 * 
	 * @param checkbox
	 */
	public void majAutoShunt(String[] checkbox) {
		if(checkbox == null || checkbox.length==0){
			setAllNotShuntAuto();
			return;
		}			
		List<String> liste =  Arrays.asList(checkbox);
		for(Map.Entry<String, Straddle> entry : straddles.entrySet()){
			if(liste.contains(entry.getKey())){
				entry.getValue().setAutoShunt(true);
			}else{
				entry.getValue().setAutoShunt(false);
			}
		}
		
	}
	/**
	 * La mise � jour des straddles quand on re�oit le r�sultat de checklist
	 * 
	 * @param checkbox
	 */
	//TODO a finaliser ne pas agir
	public void majCheckList(String[] checkbox) {
		if(checkbox == null || checkbox.length==0){
			setAllCheckListDisabled();
			return;
		}
		setAllCheckListDisabled();
		List<String> liste =  Arrays.asList(checkbox);
		for(int i=0; i<liste.size();i++){
			this.checkListUp.put(liste.get(i), true);
		}
		MyController.setCheckListUp(true);
	}
	

	private void setAllCheckListDisabled() {
		for(Map.Entry<String, Boolean> entry : checkListUp.entrySet()){
			entry.setValue(false);
		}
		MyController.setCheckListUp(false);
	}
	/**
	 * Tout mettre en autoshunt false
	 */
	private void setAllNotShuntAuto() {
		for(Map.Entry<String, Straddle> entry : straddles.entrySet()){			
			entry.getValue().setAutoShunt(false);			
		}
	}
	
	//Param�trage de l'identifiant et du message de temporisation
	private void  msgTemporisation(String Numstraddle, String identifiant){	
		if (getByNum(Numstraddle).getIdentifiant().equals(identifiant)){
			getByNum(Numstraddle).setDisplayMessage(" Le chauffeur du " + Numstraddle + " cherche � se reconnecter en cours de mission.");			
		}else{
			getByNum(Numstraddle).setDisplayMessage(" Changement de carte : " + getByNum(Numstraddle).getIdentifiant() +" par la "+ identifiant);			
		}		
	}
	
	//Message j'enl�ve la carte avec m�morisation du message
	private void  msgEnleveCarte(String Numstraddle, String identifiant){	
		//Je mets ma variable de temporisation � true
		if (!getByNum(Numstraddle).isTmpBool()){
				getByNum(Numstraddle).setTempoMissionOk();
		getByNum(Numstraddle).setDisplayMessageTmp(getByNum(Numstraddle).getDisplayMessage());
		}
		getByNum(Numstraddle).setDisplayMessage(" Le chauffeur du " + Numstraddle + " a enlev� sa carte en cours de mission.");				
	}
	
	//Message de d�connexion non autoris�e avec m�morisation du message
	private void  msgDeconnexion(String Numstraddle, String identifiant){	
		//Je mets ma variable de temporisation � true
		if (!getByNum(Numstraddle).isTmpBool()){
			getByNum(Numstraddle).setTempoMissionOk();
			getByNum(Numstraddle).setDisplayMessageTmp(getByNum(Numstraddle).getDisplayMessage());
		}
		getByNum(Numstraddle).setDisplayMessage(" Le chauffeur du " + Numstraddle + " s'est d�connect� en cours de mission.");				
	}
	//Permet de reseter les variables apr�s un renvoi
	public void resetMissionAfterRenvoi(String Numstraddle){
		getByNum(Numstraddle).setTempoMissionNok();	
		getByNum(Numstraddle).setDisplayMessage(getByNum(Numstraddle).getDisplayMessageTmp());
	}
	
	public void setRefresh(int refresh) {
		this.refresh = refresh * 1000;		
	}
	
	public void setMessageManager(MessageManager messageManager) {
		this.messageManager = messageManager;
	}
	
	public void setWeightingManager(WeightManager weightManager) {
		this.weightingManager = weightManager; 
	}
	public void dealMessageRAZWeight(String nameStraddle, MessageRAZWeightDetails details) {
		// Traitement du message venant du straddle pour l'accus� de la RAZ r�ussie ou pas
		AccessDb.insertRAZRow(nameStraddle, details.getDate(), details.isExecute());
	}
	public boolean razPeson(String nameStraddle) {
		if(straddles.get(nameStraddle) != null && straddles.get(nameStraddle).getState() != 0){
			// Straddle connect�
			messageManager.straddleSendMessage(getByNum(nameStraddle).getNameIP(), "cweight;1");
			return true;
		}
		return false;
	}

	




}
