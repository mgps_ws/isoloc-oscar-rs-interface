package info.mgps.isoloc.interfaceGPS.main;


import java.util.Observable;
import java.util.Observer;


import info.mgps.isoloc.interfaceGPS.service.log.AccessDb;
import info.mgps.isoloc.interfaceGPS.service.message.ConnectionEcn4Exception;
import info.mgps.isoloc.interfaceGPS.service.message.MessageFor;
import info.mgps.isoloc.interfaceGPS.service.message.MessageManager;
import info.mgps.isoloc.interfaceGPS.service.messageStraddle.DealMessageCheckList;
import info.mgps.isoloc.interfaceGPS.service.messageStraddle.DealMessageRAZWeight;
import info.mgps.isoloc.interfaceGPS.service.messageStraddle.DealMsgStraddle;
import info.mgps.isoloc.interfaceGPS.service.messageStraddle.MessageCheckListDetails;
import info.mgps.isoloc.interfaceGPS.service.messageStraddle.MessageRAZWeightDetails;
import info.mgps.isoloc.interfaceGPS.service.messageStraddle.MessageStraddle;
import info.mgps.isoloc.interfaceGPS.service.socketFlash.PaquetWeight;
import info.mgps.isoloc.interfaceGPS.service.socketFlash.SocketServerUDP;
import info.mgps.isoloc.interfaceGPS.service.message.SocketClientEcn4;
import info.mgps.isoloc.interfaceGPS.service.message.SocketServerStraddle;
import info.mgps.isoloc.interfaceGPS.utilitaire.LogClass;
import info.mgps.isoloc.interfaceGPS.service.xmlNavis.MessageXmlRead;
import info.mgps.isoloc.interfaceGPS.service.xmlNavis.ReadXml;
import info.mgps.isoloc.interfaceGPS.web.Models.Error;
import info.mgps.isoloc.interfaceGPS.web.web.Controllers.MyController;


public class WorkManagerStart implements Observer{

	private StraddleManager straddleManager;
	private MessageManager messageManager; 
	private WeightManager weightingManager;
	
	
	//private CheckListWSClient webServiceCheckList;
	private int msId;
	/**
	 * incr�menttation du msId pour les messages envoy�s dans ECN4
	 * on remets � 1 tous les 5 millions messages (environ 3 mois)
	 * 
	 * @return l'entier
	 */
	private int incrementMsId(){
		synchronized (this) {
			if(msId < 5000000){
				msId ++;
				return msId;
			}else
				msId = 1;
			return msId;
		}
		
	}
	
	/**
	 * Lancement de l'application
	 * 
	 * 
	 * **/
	public WorkManagerStart(int refresh, String ipAdressClient,int portComClient,String nameThreadClient,boolean sendBytesClient,int indEndClient, 
			int delaiReconnexionClient, int portServer, int nombreDeConnexionServer, String nameThreadServer, boolean sendByteServer ,
			int indEndServer, int delaiReconnexionServer, int portNumberWeight){
		//webServiceCheckList = new CheckListWSClient();
		
		straddleManager = new StraddleManager(refresh/*webServiceCheckList*/);	
		weightingManager = new WeightManager(straddleManager);
		//straddleManager.staticStraddles(); // Popur test
		messageManager = new MessageManager(ipAdressClient, portComClient, nameThreadClient, sendBytesClient, indEndClient,
				delaiReconnexionClient, portServer, nombreDeConnexionServer, nameThreadServer, sendByteServer, indEndServer,delaiReconnexionServer, portNumberWeight);
		straddleManager.setMessageManager(messageManager);
//		messageManager = new MessageManager("10.120.1.50", 13101, "ClientECN4", true, 255,
//				1000, 13230, 60, "ServeurStraddle", false,3, 1000 ); // Navis
//		messageManager = new MessageManager("192.168.10.162", 13101, "ClientECN4", true, 255,
//				1000, 13230, 60, "ServeurStraddle", false,3, 1000 );//Test Pc Fred
		
		//Je m'abonne au socket client et serveur pour recup�rer les messages des sockets
		messageManager.addObserverClientEcn4(this);
		messageManager.addObserverServerStraddle(this);
		messageManager.addObserverServerWeight(this);
		
		
		
	}
	
	

	public WeightManager getWeightingManager() {
		return weightingManager;
	}

	public void setWeightingManager(WeightManager weightingManager) {
		this.weightingManager = weightingManager;
	}

	public StraddleManager getStraddleManager() {
		return straddleManager;
	}

	public void setStraddleManager(StraddleManager straddleManager) {
		this.straddleManager = straddleManager;
	}

	public MessageManager getMessageManager() {
		return messageManager;
	}

	public void setMessageManager(MessageManager messageManager) {
		this.messageManager = messageManager;
	}

	public void connectStraddle(String login, String straddleNum) throws ConnectionEcn4Exception{		
		messageManager.ecn4connectStraddle(incrementMsId(), login, straddleNum);		
	}

	public void disConnectStraddle(String straddleNum) throws ConnectionEcn4Exception {
		messageManager.ecn4disConnectStraddle(incrementMsId(), straddleNum, straddleManager.getByNum(straddleNum).getState());
	}

	/**
	 * just pour l'interface graphique, gere les filtres
	 * @return
	 */
	public boolean isAllFilter(String user) {		
		return straddleManager.getFiltre(user).equals("tout");
	}
	
	/**
	 * retourne les evenements (erreurs,shunts ...)
	 * @return
	 */
	public Object getActiveEvents() {
		return straddleManager.getEvents();
	}

	/**
	 * la fonction qui fait le shunt
	 * elle envoie � navis la position qu'on a pris apr�s le shunt 
	 * @param button
	 * @param straddleNum
	 * @param newPosition
	 */
	public void shunt(String button, String straddleNum,String newPosition,String GPSPos) {
		//String msg = straddleManager.shunt(button, straddleNum,newPosition);
		//Envoyer
		if(button.equals("S")){
			newPosition = straddleManager.getByNum(straddleNum).getMission().getPosition();
		}else if (button.equals("P")) {
			newPosition = straddleManager.getByNum(straddleNum).getGpsPosition();
		}
		
		AccessDb.MemologMessage(straddleNum, 1, "", "", 1, "SHUNT "+button+" -- Position Navis: " +straddleManager.getByNum(straddleNum).getMission().getPosition()+
				" Position GPS: "+GPSPos+" Position Transmise: "+newPosition);
		MessageFor msgFor = straddleManager.getMessageProgressECN4(incrementMsId(), straddleNum, newPosition);
		
		//Si le message est pour le straddle
		if (msgFor.isStraddle()){
			messageManager.straddleSendMessage(straddleManager.getByNum(straddleNum).getNameIP(), msgFor.getMsg());		
		}else {//Sinon je l'envoie � Navis
			messageManager.ecn4sendMessage(msgFor.getMsg());
		}

	}
	/**
	 * affecte un cavalier avec le mode autoshunt
	 * @param straddleNum
	 */
	public void setAutoShunt(String straddleNum) {
		straddleManager.setAutoShunt(straddleNum);
	}

	public void setStradError(String straddleCheck, Error error) {
		straddleManager.setError(straddleCheck, error);
	}
	
	/**
	 ************ Test ***********************
	 * pour s'assurer si on devrait shunter ou pas
	 * cette fonction affecte la gPSPosition du cavalier
	 * @param straddleNum le numer du cavalier
	 * @param gPSPosition la position que le gps envoie cible ex: V:B0101B
	 */
	public void checkForShunt(String straddleNum, String gPSPosition) {
		if (!straddleManager.checkForShunt(straddleNum, gPSPosition, gPSPosition)){
			//Message d'envoie recup�ration cible mission navis
			MessageFor msgFor = straddleManager.getMessageProgressECN4(incrementMsId(), straddleNum, straddleManager.getByNum(straddleNum).getMission().getPosition());
			
			//Si le message est pour le straddle
			if (msgFor.isStraddle()){
				messageManager.straddleSendMessage(straddleManager.getByNum(straddleNum).getNameIP(), msgFor.getMsg());		
			}else {//Sinon je l'envoie � Navis
				messageManager.ecn4sendMessage(msgFor.getMsg());
			}
			
		}else
			straddleManager.getByNum(straddleNum).setShuntable(true);
	}

	//************* Test
	public void setStradMission(String straddleNum, String position,
			String mission, String missionType, String conteneur) {
		straddleManager.setMission(straddleNum,position,mission,missionType,conteneur);
	}

	public void setStradFilter(String user, String filtre) {
		straddleManager.setFiltre(user, filtre);
	}

	/**
	 * Traitement des �v�nements venant de Ecn4 (Navis)
	 * Mise � jour des informations straddles 
	 * et envoie de la mission aux cavaliers
	 * 
	 */
	@Override
	public void update(Observable o, Object arg) {
		LogClass.ecriture("WorkManager", "o = "+o+" arg = "+arg, 1);
		//System.out.println("o = "+o+" arg = "+arg);
		//if(arg instanceof MessageStraddle)
		try {
			// ****************** Traitement des messages venant de Ecn4 *************
			if( o instanceof SocketClientEcn4){
				if (arg!=null) {
					//Traitement des informations de connexions
					if (arg instanceof Integer){
						//si int=1 connexion
						//si int=0 deconnexion
						this.straddleManager.setCnxECN4(((Integer) arg).intValue());
					} else if (arg instanceof String) {
						
						MessageXmlRead msg = ReadXml.read((String)arg);
						
						if(msg != null){			
							//V�rification que le message est bien pour le straddle
							if (msg.getEquipType().equalsIgnoreCase("STRAD")) {
								//Mise � jour des param�tres Stradlles
								//Et envoie du message vers les cavaliers	
								MessageFor msgFor = straddleManager.dealMessageECN4(incrementMsId(), msg);
								//Si le message est pour le straddle.
								if (msgFor.isStraddle()){
									messageManager.straddleSendMessage(msgFor.getNameStraddle(), msgFor.getMsg());		
								}else {//Sinon je l'envoie � Navis
									
									messageManager.ecn4sendMessage(msgFor.getMsg());
								}
							}
			
						}
						
					}			
				}	
				//********************** Traitement des messages venant des cavaliers ***********
			}else if (o instanceof SocketServerStraddle) {
				if (arg!=null) {
					//Traitement des informations du serveur Straddle
					if (arg instanceof Integer){
						//si int=1 serveur � l'�coute (ready)
						//si int=0 serveur pas � l'�coute (closed)
						this.straddleManager.setCnxServer(((Integer) arg).intValue());
					} else if (arg instanceof MessageStraddle) {
					
						//Gestion des connexions et d�connexions
						MessageStraddle msgStraddle = (MessageStraddle) arg;
						LogClass.ecriture("WorkManager", "msgstraddle "+msgStraddle.getMsg()+" "+msgStraddle.getNameStraddle()+" "+msgStraddle.getInd(), 1);
						//System.out.println("msgstraddle "+msgStraddle.getMsg()+" "+msgStraddle.getNameStraddle()+" "+msgStraddle.getInd());
						switch (msgStraddle.getInd()) {
						case 0:		//Deconnexion
							MessageFor msgForDis = this.straddleManager.disconnect(incrementMsId(),msgStraddle.getNameStraddle());
							if (!msgForDis.isStraddle()){
								messageManager.ecn4sendMessage(msgForDis.getMsg());
							}
							break;
						case 1:		//Connexion
							
							this.straddleManager.connect(msgStraddle.getNameStraddle());
							break;
						case 2:		//Traitement des messages venant du cavalier
							MessageFor msgFor = straddleManager.dealMessageStraddle(incrementMsId(), msgStraddle.getNameStraddle(),  
									DealMsgStraddle.ReadMsgStraddle(msgStraddle.getMsg()));
							//Si le message est pour le straddle
							if (msgFor.isStraddle()){
								messageManager.straddleSendMessage(msgStraddle.getNameStraddle(), msgFor.getMsg());		
							}else {//Sinon je l'envoie � Navis
								messageManager.ecn4sendMessage(msgFor.getMsg());
							}
							break;
						case 3: //checkList Cavalier
							MessageCheckListDetails details = DealMessageCheckList.readMsgCheckList(msgStraddle.getMsg());
							String nameStraddle = details.getRealNameStraddle();
							MessageFor msgFor1 = straddleManager.dealMessageCheckList(msgStraddle.getNameStraddle(), details);
							
							if (msgFor1.isStraddle()){
								// il faut trouver l'ip du straddle
								messageManager.straddleSendMessage(straddleManager.getByNum(nameStraddle).getNameIP(), msgFor1.getMsg());		
							}else
								if(msgFor1.isCheckList()){
									messageManager.checkListSendMessage(msgFor1.getMsg());
								}
							break;
						case 4:
							// Message d'acquittement de pes�e
							MessageRAZWeightDetails detailsRAZ = DealMessageRAZWeight.readMsgRAZWeight(msgStraddle.getMsg());
							String straddleName = detailsRAZ.getRealNameStraddle();
							straddleManager.dealMessageRAZWeight(straddleName, detailsRAZ);
						}	
					}	
				}
			}else
				if(o instanceof SocketServerUDP){
					// TODO la gestion des pes�es ICI
					weightingManager.newWeight((PaquetWeight)arg);
				}
		} catch (Exception e) {
			LogClass.ecriture("WorkManager/update",e.getMessage()+ " " + e.toString(),3);
		}
	}

	/**
	 * V�rification de la position GPS
	 * @param straddleCheck
	 * @return true si position ok
	 */
	public boolean verificationShuntP(String straddleCheck){
		return this.straddleManager.verificationShuntPStraddle(straddleCheck);
	}
	
	/**
	 * Renvoie de la mission en cours sur le straddle concern�
	 * @param straddleCheck
	 */
	public void renvoieMission(String straddleCheck ){
		//messageManager.straddleSendMessage(straddleManager.getByNum(straddleCheck).getNameIP(),"");
		messageManager.straddleSendMessage(straddleManager.getByNum( straddleCheck).getNameIP(),straddleManager.getByNum( straddleCheck).getMemoMsgStraddle());	
		this.straddleManager.resetMissionAfterRenvoi(straddleCheck);
	}

	public boolean razPeson(String nameStraddle) {
		// raz du peson avec le nom donne en parametre
		if(straddleManager.razPeson(nameStraddle)){
			return true;
		}else{
			return false;
		}
	}
	
}
