package info.ttc.egee.architecture.activity.egee.notifications;

import info.mgps.isotop.task.applus.body.imp.Equipement;
import info.mgps.isotop.task.applus.body.imp.Reference;
import info.mgps.isotop.task.applus.common.Actions;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.List;
import java.util.Vector;

public class Rdv extends Actions implements Serializable { 

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Reference reference;
	private List<Equipement> equipements = new Vector<Equipement>();
	public Rdv() {
		super();
	}

	public Rdv(Reference reference) {
		super();
		this.reference = reference;
	}
	@XmlElement(name = "references-rdv")
	public Reference getReference() { 
		return reference; 
	} 

	public void setReference(Reference reference) { 
		this.reference = reference; 
	} 

	@XmlElement(name = "equipement-rdv")
	public List<Equipement> getEquipements() { 
		return equipements; 
	} 

	public void setEquipements(List<Equipement> equipements) { 
		this.equipements = equipements; 
	} 
	@Override
	public String getType() {
		return "rdv";
	}
	public void addEquipementToRDV(Equipement child) {
        child.setRdv(this);
        this.equipements.add(child);
    }
	@Override
	public String toString() {
		return "Rdv [reference=" + reference + "]";
	}

	

} 
