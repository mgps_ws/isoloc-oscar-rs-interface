package info.ttc.egee.architecture.activity.egee.notifications;

import java.util.List;
import java.util.Vector;
import info.mgps.isotop.task.applus.common.Actions;
import info.mgps.isotop.task.applus.body.imp.Equipement;
import info.mgps.isotop.task.applus.body.imp.Reference;
import javax.xml.bind.annotation.XmlElement;

public final class Vag extends Actions {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Reference reference;
	private List<Equipement> listEquipement= new Vector<Equipement>();


	public Vag() {
		super();
	}

	public Vag(Reference reference) {
		super();
		this.reference = reference;
	}
	

	// Reference-Rdv utilise classe Reference
	@XmlElement(name = "references-vag")
	public Reference getReference() { 
		return reference; 
	} 

	public void setReference(Reference reference) { 
		this.reference = reference; 
	} 
	// Equipement-Rdv utilise classe Equipement
	@XmlElement(name = "equipement-vag")
	public List<Equipement> getListEquipement() { 
		return listEquipement; 
	} 

	public void setEquipement(List<Equipement> listEquipement) { 
		this.listEquipement = listEquipement; 
	} 
	public void addEquipementToVAG(Equipement child) {
        child.setVag(this);
        this.listEquipement.add(child);
    }
	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return "vag";
	}


}
