package info.ttc.egee.architecture.activity.egee.notifications;

import java.util.List;
import java.util.Vector;

import info.mgps.isotop.task.applus.body.imp.Equipement;
import info.mgps.isotop.task.applus.body.imp.Reference;
import info.mgps.isotop.task.applus.common.Actions;
import javax.xml.bind.annotation.XmlElement;

public class Vze extends Actions {
	/**
	 * vu zone d'�change
	 */
	private static final long serialVersionUID = 1L;
	private Reference reference;
	private List<Equipement> listEquipement= new Vector<Equipement>();


	public Vze() {
		super();
	}

	public Vze(Reference reference) {
		super();
		this.reference = reference;
	}

	// Reference-Rdv utilise classe Reference
	@XmlElement(name = "references-vze")
	public Reference getReference() { 
		return reference; 
	} 

	public void setReference(Reference reference) { 
		this.reference = reference; 
	} 

	@XmlElement(name = "equipement-vze")
	public List<Equipement> getListEquipement() { 
		return listEquipement; 
	} 

	public void setListEquipement(List<Equipement> listEquipement) { 
		this.listEquipement = listEquipement; 
	} 
	
	public void addEquipementToVZE(Equipement child) {
        child.setVze(this);
        this.listEquipement.add(child);
    }
	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return "vze";
	}

}