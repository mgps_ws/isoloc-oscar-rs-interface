package info.ttc.egee.architecture.activity.egee.notifications;

import info.mgps.isotop.task.applus.body.imp.Equipement;
import info.mgps.isotop.task.applus.body.imp.Reference;
import info.mgps.isotop.task.applus.common.Actions;
import javax.xml.bind.annotation.XmlElement;

public class Vap extends Actions {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Reference reference;
	private Equipement equipement;


	public Vap() {
		super();
	}

	public Vap(Reference reference,Equipement equipement) {
		super();
		this.reference = reference;
		this.equipement = equipement;
	}
	

	// Reference-Rdv utilise classe Reference
	@XmlElement(name = "references-vap")
	public Reference getReference() { 
		return reference; 
	} 

	public void setReference(Reference reference) { 
		this.reference = reference; 
	} 
	
	// Equipement-Rdv utilise classe Equipement
	@XmlElement(name = "equipement-vap")
	public Equipement getEquipement() { 
		return equipement; 
	} 

	public void setEquipement(Equipement equipement) { 
		this.equipement = equipement; 
	} 
	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return "vap";
	}

}
