package info.ttc.egee.xmltos;

import info.mgps.isotop.task.applus.body.imp.Equipement;
import info.mgps.isotop.task.applus.body.imp.Reference;
import info.mgps.isotop.task.applus.body.imp.Tiers;
import info.mgps.isotop.task.applus.common.ApplusMessType;
import info.mgps.isotop.task.applus.header.imp.Destinataire;
import info.mgps.isotop.task.applus.header.imp.Emetteur;
import info.mgps.isotop.task.applus.header.imp.Interchange;
import info.mgps.isotop.task.applus.header.imp.Message;
import info.mgps.isotop.task.applus.header.imp.MessageSet;
import info.mgps.isotop.task.applus.header.imp.Notification;
import info.ttc.egee.architecture.activity.egee.notifications.Vap;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class VAPXml {
	
	private static String 		emetteur = "ISOLOC";
  	private static String 		destinataire = "EGEE";
  	private static String		directoryVap =  "C:\\Vaps\\";
  	
  	SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyHHmmss");
	SimpleDateFormat formatterTos = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat formatterEgee = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	
	public void createVAP(String chid, Date datePriseTcZE, Date datePoseTcParc, String numConteneur, String posPriseTcZE, String posPoseTcParc,String typeMvt, String manut) {
		
        Date now = new Date();
        String hdcreatefile = formatter.format(now);
        String endOfnumConteneur = numConteneur.substring(numConteneur.length() - 3);
		Vap vap = new Vap();
        
        Reference ref = new Reference();
        ref.setSic("VAP"+hdcreatefile+endOfnumConteneur);
        if(!chid.equals("")){
        	ref.setChid(chid);
        }
        
        if(!datePriseTcZE.equals(null)){
        	ref.setDatePriseTcZEFromXml(formatterEgee.format(datePriseTcZE));
        }
        if(!datePoseTcParc.equals(null)){
        	ref.setDatePoseTcParcFromXml(formatterEgee.format(datePoseTcParc));
        }
        if(!posPriseTcZE.equals("")){
        	ref.setPosPriseTcZE(posPriseTcZE);
        }
        if(!posPoseTcParc.equals("")){
        	ref.setPosPoseTcParc(posPoseTcParc);
        }
        
        if(!typeMvt.equals("")){
        	ref.setTypeMvtVap(typeMvt);
        }
        
        Tiers tiers = new Tiers();
        tiers.setManut(manut);
        
    	Equipement eq = new Equipement();
        eq.setId(numConteneur);

        vap.setReference(ref);
        vap.setTiers(tiers);
        vap.setEquipement(eq);  
        
        
        Interchange interchange = new Interchange();
        interchange.setId("VAP"+hdcreatefile+numConteneur);
        interchange.setInter_from(emetteur);
        interchange.setInter_to(destinataire);
        
        MessageSet msgSet = new MessageSet();
        msgSet.setId("VAP"+hdcreatefile+numConteneur);
        
        Destinataire dest = new Destinataire();
        dest.setUser(destinataire);
        dest.setTiersprof(destinataire);
        
        Emetteur emett = new Emetteur();
        emett.setUser(emetteur);
        emett.setTiersprof(emetteur);
        
        Notification notif = new Notification();
        notif.setId("VAP"+hdcreatefile+endOfnumConteneur);
        notif.setType("VAP");
        notif.setAction("CREATE");
        notif.setApplusmess(vap);
        
        
        Message message = new Message();
        List<ApplusMessType> listAppMsg = new Vector<ApplusMessType>();
        listAppMsg.add(notif);
        message.setApplusMessTypes(listAppMsg);
        
        msgSet.setDestinataire(dest);
        msgSet.setEmetteur(emett);
        msgSet.setMessages(message);
        
        interchange.setMessageSet(msgSet);
        ///////////////Ecriture dans XML///////////////
        String filename = directoryVap +"VAP"+"_"+ hdcreatefile +"_"+emetteur + "_"+numConteneur+ ".xml";
		File file = new File(filename);
	    if (!file.exists()){
	    	file.getParentFile().mkdir();
	    	try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	    
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(Interchange.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
    		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

    		jaxbMarshaller.marshal(interchange, file);
    		//jaxbMarshaller.marshal(interchange, System.out);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		
	  }
  
		
	}

