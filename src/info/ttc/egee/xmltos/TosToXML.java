package info.ttc.egee.xmltos;


import info.mgps.isoloc.interfaceGPS.utilitaire.LogClass;
import info.mgps.isotop.task.applus.body.imp.Equipement;
import info.mgps.isotop.task.applus.body.imp.Reference;
import info.mgps.isotop.task.applus.body.imp.Tiers;
import info.mgps.isotop.task.applus.common.ApplusMessType;
import info.mgps.isotop.task.applus.header.imp.Destinataire;
import info.mgps.isotop.task.applus.header.imp.Emetteur;
import info.mgps.isotop.task.applus.header.imp.Interchange;
import info.mgps.isotop.task.applus.header.imp.Message;
import info.mgps.isotop.task.applus.header.imp.MessageSet;
import info.mgps.isotop.task.applus.header.imp.Notification;
import info.ttc.egee.architecture.activity.egee.notifications.Rdv;
import info.ttc.egee.architecture.activity.egee.notifications.Vag;
import info.ttc.egee.architecture.activity.egee.notifications.Vze;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class TosToXML {

	private static String     demoHost = "localhost";// M5 192.9.100.5
  	private static String     demoPort = "II7";//21071
  	private static String       demoDb = "M5::gatefos";
  	private static String     demoUser = "user=camionxx";
  	private static String demoPassword = "password=camionxx";
 
  	private static String    demoDbUrl = "jdbc:ingres://" + 
  	                                     demoHost + 
  	                                     ":" + demoPort +
  	                                     "/" + demoDb + 
  	                                     ";" + demoUser + 
  	                                     ";" + demoPassword;
  	private static Connection 	conn = null;
  	private static Statement 	stmt = null;
  
  	private static String 		tableNameRdv = "rdvrdv";
  	private static String 		tableNameTc = "rdvtc";
  
  	private static String 		emetteur = "EUROFMA";
  	private static String 		destinataire = "EGEE";
     
	SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyHHmmss");
	SimpleDateFormat formatterTos = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat formatterEgee = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    //Map<String,Map<String,InfoTcFromTos>> rdvMap;
	Map<String,List<InfoTcFromTos> >rdvMap;
	
    public TosToXML() {
		super();
		rdvMap = new HashMap<String,List<InfoTcFromTos>>();
	}
    /////////////////////////Connect/Close DB /////////////////////////////
	public  void createConnection()
      {
          try
          {	
              Class.forName("com.ingres.jdbc.IngresDriver").newInstance();
              //Get a connection
              conn = DriverManager.getConnection(demoDbUrl);
              LogClass.ecriture("TosToXML","successfully connected to " + demoDb,1);
              //System.out.println("successfully connected to " + demoDb);
          }
          catch (Exception except)
          {
              except.printStackTrace();
          }
      }
	
	public  void closeConnection()
    {
		 try
	         {	if(conn != null){
	        	//Get a connection
	             conn.close();
	             LogClass.ecriture("TosToXML","successfully closed connection to " + demoDb,1);
	             //System.out.println("successfully closed connection to " + demoDb);
	         }      
        }
        catch (Exception except)
        {
            except.printStackTrace();
        }
    }
	//////////////////////////Rendez Vous//////////////////////////////////
    public void getRendezVousByTc(String numConteneur){
    	 
    	  //List<String> listRdvNumbers = new Vector<String>();
    	  try
          {
              stmt = conn.createStatement();
              ResultSet results = stmt.executeQuery("SELECT numrdv, amqrdv, quoi, reference, typmvrdv, book " +
              		"FROM " + tableNameTc +" WHERE conteneur='"+numConteneur+"'");//+"AND typmvrdv LIKE 'E%'" // AND amqrdv <>''
             /* ResultSetMetaData rsmd = results.getMetaData();
              int numberCols = rsmd.getColumnCount();
              for (int i=1; i<=numberCols; i++)
              {
                  //print Column Names
                  System.out.print(rsmd.getColumnLabel(i)+"\t\t");  
              }

              System.out.println("\n====");*/
              while(results.next())
              {	  
                  String rdvNumber = results.getString(1).trim();
                  String amqrdv = results.getString(2).trim();
                  String quoi = results.getString(3).trim();
                  String reference = results.getString(4).trim();
                  String typmvr = results.getString(5).substring(0, 1);
                  String book = results.getString(6).trim();
                  InfoTcFromTos info = new InfoTcFromTos(numConteneur,amqrdv, quoi, reference, typmvr, book);
                  
                  if(rdvMap.containsKey(rdvNumber)){
                	  rdvMap.get(rdvNumber).add(info);
                  }
                  else
                  {
                	  List<InfoTcFromTos> tcInfoByRdvMap = new Vector<InfoTcFromTos>();
                	  tcInfoByRdvMap.add(info);
                	  rdvMap.put(rdvNumber, tcInfoByRdvMap);
                  }
                 // listRdvNumbers.add(rdvNumber);
                  //System.out.println("numero de Rdv  : "+ rdvNumber);
              }
              results.close();
              stmt.close();
          }
          catch (SQLException sqlExcept)
          {
              sqlExcept.printStackTrace();
          }

      }
    
    public void getRendezVousByTcAndAmq(String numConteneur,String numAMQ){
   	 
  	  //List<String> listRdvNumbers = new Vector<String>();
  	  try
        {
            stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery("SELECT numrdv, amqrdv, quoi, reference, typmvrdv, book " +
            		"FROM " + tableNameTc +" WHERE conteneur='"+numConteneur+"' AND amqrdv='"+numAMQ+"'");//+"AND typmvrdv LIKE 'E%'" // AND amqrdv <>''
           /* ResultSetMetaData rsmd = results.getMetaData();
            int numberCols = rsmd.getColumnCount();
            for (int i=1; i<=numberCols; i++)
            {
                //print Column Names
                System.out.print(rsmd.getColumnLabel(i)+"\t\t");  
            }

            System.out.println("\n====");*/
            while(results.next())
            {	  
                String rdvNumber = results.getString(1).trim();
                String amqrdv = results.getString(2).trim();
                String quoi = results.getString(3).trim();
                String reference = results.getString(4).trim();
                String typmvr = results.getString(5).substring(0, 1);
                String book = results.getString(6).trim();
                InfoTcFromTos info = new InfoTcFromTos(numConteneur,amqrdv, quoi, reference, typmvr, book);
                
                if(rdvMap.containsKey(rdvNumber)){
              	  rdvMap.get(rdvNumber).add(info);
                }
                else
                {
              	  List<InfoTcFromTos> tcInfoByRdvMap = new Vector<InfoTcFromTos>();
              	  tcInfoByRdvMap.add(info);
              	  rdvMap.put(rdvNumber, tcInfoByRdvMap);
                }
               // listRdvNumbers.add(rdvNumber);
                //System.out.println("numero de Rdv  : "+ rdvNumber);
            }
            results.close();
            stmt.close();
        }
        catch (SQLException sqlExcept)
        {
            sqlExcept.printStackTrace();
        }

    }
         
	public void convertRdvToXML(String numRendezVous,List<InfoTcFromTos> tcAndAmqMap) {

        Date now = new Date();
        String hdcreatefile = formatter.format(now);
		try
        {
            stmt = conn.createStatement();
            stmt.setMaxRows(1);
            ResultSet results = stmt.executeQuery("SELECT statrdv, hdval, hdprevue, hdt5, codesoc, codesocex " +
            		"FROM " + tableNameRdv +" WHERE numrdv='"+numRendezVous+"'");
            while(results.next())
            {
                //String numrdv = results.getString(1);
                String statrdv = results.getString(1).trim();
                String hdval = results.getString(2).trim();
                String hdprevue = results.getString(3).trim();
                String hdt5 = results.getString(4).trim();//heure fin RDV
                String codesoc = results.getString(5).trim();
                String codesocex = results.getString(6).trim();
                
                Rdv rdv = new Rdv();
                
                Reference ref = new Reference();
                ref.setSic(numRendezVous);
                if (!statrdv.equals("")) ref.setStatut(statrdv);
                if (!hdval.equals("")) {
                	Date date = formatterTos.parse(hdval);
                	String hdvalEgee = formatterEgee.format(date);
                	ref.setDateFromXml(hdvalEgee);
                }
                	
                if (!hdprevue.equals("")){
                	Date date = formatterTos.parse(hdprevue);
                	String hdprevueEgee = formatterEgee.format(date);
                	ref.setDateProgrameFromXml(hdprevueEgee);
                }
                if (!hdt5.equals("")){
                	Date date = formatterTos.parse(hdt5);
                	String hdt5Egee = formatterEgee.format(date);
                	ref.setDateFinFromXml(hdt5Egee);
                }
                String bookOfTc = rdvMap.get(numRendezVous).get(0).getBook();
                //System.out.println(" bookOfTc : " + bookOfTc);
                if (!bookOfTc.equals("")) ref.setCbk(bookOfTc);
                
                
                Tiers tiers = new Tiers();
                tiers.setManut(emetteur);
                if(!codesoc.equals("")) tiers.setSteCreateRdv(codesoc);
                if(!codesocex.equals("")) tiers.setSteFinRdv(codesocex);
                
                List<Equipement> listEquipementRdv = new Vector<Equipement>();
                for (InfoTcFromTos entry : tcAndAmqMap) {
                	Equipement eq = new Equipement();
                    eq.setId(entry.getNumTc());
                    if(!entry.getAmqrdv().equals("")) eq.setAmq(entry.getAmqrdv());
                    if(!entry.getQuoi().equals("")) eq.setTypeInRDV(entry.getQuoi());
                    if(!entry.getReference().equals("")) eq.setReferenceInRDV(entry.getReference());
                    if(!entry.getTypmvr().equals("")) eq.setSens(entry.getTypmvr());
                    listEquipementRdv.add(eq);
                }
                rdv.setReference(ref);
                rdv.setTiers(tiers);
                rdv.setEquipements(listEquipementRdv);  
                
                
                Interchange interchange = new Interchange();
                interchange.setId(numRendezVous+hdcreatefile);
                interchange.setInter_from(emetteur);
                interchange.setInter_to(destinataire);
                
                MessageSet msgSet = new MessageSet();
                msgSet.setId(numRendezVous+hdcreatefile);
                
                Destinataire dest = new Destinataire();
                dest.setUser(destinataire);
                dest.setTiersprof(destinataire);
                
                Emetteur emett = new Emetteur();
                emett.setUser(emetteur);
                emett.setTiersprof(emetteur);
                
                Notification notif = new Notification();
                notif.setId(numRendezVous);
                notif.setType("RDV");
                notif.setAction("CREATE");
                notif.setApplusmess(rdv);
                
                
                Message message = new Message();
                List<ApplusMessType> listAppMsg = new Vector<ApplusMessType>();
                listAppMsg.add(notif);
                message.setApplusMessTypes(listAppMsg);
                
                msgSet.setDestinataire(dest);
                msgSet.setEmetteur(emett);
                msgSet.setMessages(message);
                
                interchange.setMessageSet(msgSet);
                
                results.close();
                stmt.close();
                ///////////////Ecriture dans XML///////////////
                String filename = "F:\\RdvsAmq\\RDV"+"_"+ hdcreatefile +"_"+emetteur + "_"+numRendezVous+ ".xml";
        		File file = new File(filename);
        		JAXBContext jaxbContext;
				try {
					jaxbContext = JAXBContext.newInstance(Interchange.class);
					Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	        		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

	        		jaxbMarshaller.marshal(interchange, file);
	        		//jaxbMarshaller.marshal(interchange, System.out);
				} catch (JAXBException e) {
					e.printStackTrace();
				}
        		
        	  }
            }catch (SQLException sqlExcept){
	            sqlExcept.printStackTrace();
	        } catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	
	public void convertALLRdvsToXML(String fileName){
		
		rdvMap.clear();
	 	BufferedReader in;
		try {
			in = new BufferedReader(new FileReader(fileName));
			String line;
			while ((line = in.readLine()) != null)
			{ 
				
				StringTokenizer stringTokenizer = new StringTokenizer(line);

			    while (stringTokenizer.hasMoreElements()) {
				    String numConteneur = stringTokenizer.nextElement().toString();
				    String numAMQ = stringTokenizer.nextElement().toString();
				    getRendezVousByTcAndAmq(numConteneur, numAMQ);
				    break;
			   }	
			  //getRendezVousByTc(line);
			  //System.out.println ("Numero de conteneur extrait :  " +line);
			}
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		 for (Map.Entry<String, List<InfoTcFromTos>> entry : rdvMap.entrySet()) {
			
			 convertRdvToXML(entry.getKey(),entry.getValue());
		}	
		 
	 }
	//////////////////////////Vue � gate/////////////////////////////////////
	public void convertVagToXML(String numRendezVous,List<InfoTcFromTos> tcAndAmqMap) {
		
        Date now = new Date();
        String hdcreatefile = formatter.format(now);
		try
        {
            stmt = conn.createStatement();
            stmt.setMaxRows(1);
            ResultSet results = stmt.executeQuery("SELECT hdt1, voieex " +
            		"FROM " + tableNameRdv +" WHERE numrdv='"+numRendezVous+"'");
            while(results.next())
            {
                //String numrdv = results.getString(1);
                String hdt1 = results.getString(1).trim();
                String voieex = results.getString(2).trim();
                Vag vag = new Vag();
                
                Reference ref = new Reference();
                ref.setSic("VAG"+numRendezVous);
                if (!hdt1.equals("")){
                	Date date = formatterTos.parse(hdt1);
                	String hdt1Egee = formatterEgee.format(date);
                	ref.setDateFromXml(hdt1Egee);
                }
                if (!voieex.equals("")) ref.setVoieGate(voieex);
              
                Tiers tiers = new Tiers();
                tiers.setManut(emetteur);
                
                List<Equipement> listEquipementVag = new Vector<Equipement>();
                for (InfoTcFromTos entry : tcAndAmqMap) {
                	Equipement eq = new Equipement();
                    eq.setId(entry.getNumTc());
                    listEquipementVag.add(eq);
                }
                vag.setReference(ref);
                vag.setTiers(tiers);
                vag.setEquipement(listEquipementVag);  
                
                
                Interchange interchange = new Interchange();
                interchange.setId("VAG"+numRendezVous+hdcreatefile);
                interchange.setInter_from(emetteur);
                interchange.setInter_to(destinataire);
                
                MessageSet msgSet = new MessageSet();
                msgSet.setId("VAG"+numRendezVous+hdcreatefile);
                
                Destinataire dest = new Destinataire();
                dest.setUser(destinataire);
                dest.setTiersprof(destinataire);
                
                Emetteur emett = new Emetteur();
                emett.setUser(emetteur);
                emett.setTiersprof(emetteur);
                
                Notification notif = new Notification();
                notif.setId("VAG"+numRendezVous);
                notif.setType("VAG");
                notif.setAction("CREATE");
                notif.setApplusmess(vag);
                
                
                Message message = new Message();
                List<ApplusMessType> listAppMsg = new Vector<ApplusMessType>();
                listAppMsg.add(notif);
                message.setApplusMessTypes(listAppMsg);
                
                msgSet.setDestinataire(dest);
                msgSet.setEmetteur(emett);
                msgSet.setMessages(message);
                
                interchange.setMessageSet(msgSet);
                
                results.close();
                stmt.close();
                ///////////////Ecriture dans XML///////////////
                String filename = "F:\\VagsAmq\\VAG"+"_"+ hdcreatefile +"_"+emetteur + "_"+numRendezVous+ ".xml";
        		File file = new File(filename);
        		JAXBContext jaxbContext;
				try {
					jaxbContext = JAXBContext.newInstance(Interchange.class);
					Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	        		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

	        		jaxbMarshaller.marshal(interchange, file);
	        		//jaxbMarshaller.marshal(interchange, System.out);
				} catch (JAXBException e) {
					e.printStackTrace();
				}
        		
        	  }
            }catch (SQLException sqlExcept){
	            sqlExcept.printStackTrace();
	        } catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	}
	
	public void convertALLVagsToXML(String fileName){
		
		rdvMap.clear();
		BufferedReader in;
		try {
			in = new BufferedReader(new FileReader(fileName));
			String line;
			while ((line = in.readLine()) != null)
			{
				
				StringTokenizer stringTokenizer = new StringTokenizer(line);

			    while (stringTokenizer.hasMoreElements()) {
				    String numConteneur = stringTokenizer.nextElement().toString();
				    String numAMQ = stringTokenizer.nextElement().toString();
				    getRendezVousByTcAndAmq(numConteneur, numAMQ);
				    break;
			    }  
			  //getRendezVousByTc(line);
			  //System.out.println ("Numero de conteneur extrait :  " +line);
			}
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		 for (Map.Entry<String, List<InfoTcFromTos>> entry : rdvMap.entrySet()) {
			
			 convertVagToXML(entry.getKey(),entry.getValue());
		}	
	}
	//////////////////////////Vue zone d'�change/////////////////////////////
	public void convertVzeToXML(String numRendezVous,List<InfoTcFromTos> tcAndAmqMap) {
		
		Date now = new Date();
        String hdcreatefile = formatter.format(now);
		try
        {
            stmt = conn.createStatement();
            stmt.setMaxRows(1);
            ResultSet results = stmt.executeQuery("SELECT hdt2 " +
            		"FROM " + tableNameRdv +" WHERE numrdv='"+numRendezVous+"'");
            while(results.next())
            {
                //String numrdv = results.getString(1);
                String hdt2 = results.getString(1).trim();
                
                Vze vze = new Vze();
                
                Reference ref = new Reference();
                ref.setSic("VZE"+numRendezVous);
                if (!hdt2.equals("")){ 	
                	Date date = formatterTos.parse(hdt2);
                	String hdt2Egee = formatterEgee.format(date);
                	ref.setDateFromXml(hdt2Egee);
                }
              
                Tiers tiers = new Tiers();
                tiers.setManut(emetteur);
                
                List<Equipement> listEquipementVag = new Vector<Equipement>();
                for (InfoTcFromTos entry : tcAndAmqMap) {
                	Equipement eq = new Equipement();
                    eq.setId(entry.getNumTc());
                    listEquipementVag.add(eq);
                }
                vze.setReference(ref);
                vze.setTiers(tiers);
                vze.setListEquipement(listEquipementVag);  
                
                
                Interchange interchange = new Interchange();
                interchange.setId("VZE"+numRendezVous+hdcreatefile);
                interchange.setInter_from(emetteur);
                interchange.setInter_to(destinataire);
                
                MessageSet msgSet = new MessageSet();
                msgSet.setId("VZE"+numRendezVous+hdcreatefile);
                
                Destinataire dest = new Destinataire();
                dest.setUser(destinataire);
                dest.setTiersprof(destinataire);
                
                Emetteur emett = new Emetteur();
                emett.setUser(emetteur);
                emett.setTiersprof(emetteur);
                
                Notification notif = new Notification();
                notif.setId("VZE"+numRendezVous);
                notif.setType("VZE");
                notif.setAction("CREATE");
                notif.setApplusmess(vze);
                
                
                Message message = new Message();
                List<ApplusMessType> listAppMsg = new Vector<ApplusMessType>();
                listAppMsg.add(notif);
                message.setApplusMessTypes(listAppMsg);
                
                msgSet.setDestinataire(dest);
                msgSet.setEmetteur(emett);
                msgSet.setMessages(message);
                
                interchange.setMessageSet(msgSet);
                
                results.close();
                stmt.close();
                ///////////////Ecriture dans XML///////////////
                String filename = "F:\\VzesAmq\\VZE"+"_"+ hdcreatefile +"_"+emetteur + "_"+numRendezVous+ ".xml";
        		File file = new File(filename);
        		JAXBContext jaxbContext;
				try {
					jaxbContext = JAXBContext.newInstance(Interchange.class);
					Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	        		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

	        		jaxbMarshaller.marshal(interchange, file);
	        		//jaxbMarshaller.marshal(interchange, System.out);
				} catch (JAXBException e) {
					e.printStackTrace();
				}
        		
        	  }
            }catch (SQLException sqlExcept){
	            sqlExcept.printStackTrace();
	        } catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	public void convertALLVzesToXML(String fileName){
		
		rdvMap.clear();
		BufferedReader in;
		try {
			in = new BufferedReader(new FileReader(fileName));
			String line;
			while ((line = in.readLine()) != null)
			{
				StringTokenizer stringTokenizer = new StringTokenizer(line);

			    while (stringTokenizer.hasMoreElements()) {
				    String numConteneur = stringTokenizer.nextElement().toString();
				    String numAMQ = stringTokenizer.nextElement().toString();
				    getRendezVousByTcAndAmq(numConteneur, numAMQ);
				    break;
			    }
			  //getRendezVousByTc(line);
			  //System.out.println ("Numero de conteneur extrait :  " +line);
			}
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		 for (Map.Entry<String, List<InfoTcFromTos>> entry : rdvMap.entrySet()) {
			
			 convertVzeToXML(entry.getKey(),entry.getValue());
		}	
	}
}
