package info.ttc.egee.xmltos;



public class InfoTcFromTos {

	private String numTc;
	private String amqrdv;
	private String quoi;
	private String reference;
	private String typmvr;
	private String book;
	
	public InfoTcFromTos() {
		super();
	}

	public InfoTcFromTos(String numTc, String amqrdv, String quoi,
			String reference, String typmvr, String book) {
		super();
		this.numTc = numTc;
		this.amqrdv = amqrdv;
		this.quoi = quoi;
		this.reference = reference;
		this.typmvr = typmvr;
		this.book = book;
	}

	public String getNumTc() {
		return numTc;
	}

	public void setNumTc(String numTc) {
		this.numTc = numTc;
	}

	public String getAmqrdv() {
		return amqrdv;
	}

	public void setAmqrdv(String amqrdv) {
		this.amqrdv = amqrdv;
	}

	public String getQuoi() {
		return quoi;
	}

	public void setQuoi(String quoi) {
		this.quoi = quoi;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getTypmvr() {
		return typmvr;
	}

	public void setTypmvr(String typmvr) {
		this.typmvr = typmvr;
	}

	public String getBook() {
		return book;
	}

	public void setBook(String book) {
		this.book = book;
	}

	@Override
	public String toString() {
		return "InfoTcFromTos [numTc=" + numTc + ", amqrdv=" + amqrdv
				+ ", quoi=" + quoi + ", reference=" + reference + ", typmvr="
				+ typmvr + ", book=" + book + "]";
	}
	
	
}