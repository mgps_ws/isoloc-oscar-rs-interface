/**
 * 
 * Argo Services 
 * 
 */
@javax.xml.bind.annotation.XmlSchema(namespace = "http://www.navis.com/services/argobasicservice", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package com.navis.services.argobasicservice;
