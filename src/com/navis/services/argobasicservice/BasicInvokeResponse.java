
package com.navis.services.argobasicservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="basicInvokeResponse" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "basicInvokeResponse"
})
@XmlRootElement(name = "basicInvokeResponse")
public class BasicInvokeResponse {

    @XmlElement(required = true)
    protected String basicInvokeResponse;

    /**
     * Gets the value of the basicInvokeResponse property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBasicInvokeResponse() {
        return basicInvokeResponse;
    }

    /**
     * Sets the value of the basicInvokeResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBasicInvokeResponse(String value) {
        this.basicInvokeResponse = value;
    }

}
