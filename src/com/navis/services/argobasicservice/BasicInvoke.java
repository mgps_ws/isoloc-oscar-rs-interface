
package com.navis.services.argobasicservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="scopeCoordinateIds" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="xmlDoc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "scopeCoordinateIds",
    "xmlDoc"
})
@XmlRootElement(name = "basicInvoke")
public class BasicInvoke {

    @XmlElement(required = true)
    protected String scopeCoordinateIds;
    @XmlElement(required = true)
    protected String xmlDoc;

    /**
     * Gets the value of the scopeCoordinateIds property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScopeCoordinateIds() {
        return scopeCoordinateIds;
    }

    /**
     * Sets the value of the scopeCoordinateIds property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScopeCoordinateIds(String value) {
        this.scopeCoordinateIds = value;
    }

    /**
     * Gets the value of the xmlDoc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXmlDoc() {
        return xmlDoc;
    }

    /**
     * Sets the value of the xmlDoc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXmlDoc(String value) {
        this.xmlDoc = value;
    }

}
