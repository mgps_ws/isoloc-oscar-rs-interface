package com.testClient;

import info.mgps.isoloc.interfaceGPS.service.log.AccessDb;
import info.mgps.isoloc.interfaceGPS.service.xmlNavis.GetCHEWeight;
import info.mgps.isoloc.interfaceGPS.web.web.Controllers.MyController;
import info.mgps.isoloc.interfaceGPS.web.web.Controllers.VGM;
import sun.net.www.protocol.http.HttpURLConnection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.navis.services.argobasicservice.ArgobasicService;
import com.navis.services.argobasicservice.ArgobasicServicePort;
import com.sun.xml.internal.messaging.saaj.util.Base64;



@SuppressWarnings("restriction")
public class ArgoBasicServiceTest {

	/**
	 * @param args
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws InterruptedException, IOException {
		/*//authentication("testmgps","passwor");
		//System.out.println(equalsPositions("ZCB18", "ZCB18.B"));
		Date date1 =  new Date();
		//Thread.sleep(15000);
		Calendar cal = GregorianCalendar.getInstance();
		cal.setTime(date1);
		cal.add(GregorianCalendar.HOUR, 1);
		Date date2 = cal.getTime();
		long machin = date2.getTime() - date1.getTime();
		Date date = new Date(machin);
		DateFormat formatter = new SimpleDateFormat("HH:mm:ss:SSS");
		String dateFormatted = formatter.format(date);
		System.out.println("machin "+machin + " date= "+date+" dateFormatted = "+dateFormatted);
		
		//System.out.println(AccessDb.getUsers("testmgps"));
		//System.out.println("ping message is? "+(authentication("navis", "please")==1));
		System.out.println("je mets a jour le poids "+VGM.insertVGM("navis", "please", "TCNU8255678", 24000, "10.120.2.75:9080"));*/
		
		/*List<String> keys = new ArrayList<String>();
		keys.add("filtername");
		keys.add("PARM_CTRID");
		keys.add("operatorId");
		keys.add("complexId");
		keys.add("facilityId");
		keys.add("yardId");
		List<String> values = new ArrayList<String>();
		values.add("CHEWEIGHT");
		values.add(vesselVisit);
		values.add("CHEWEIGHT");
		values.add("TCNU8255678");
		values.add("EUROFOS");
		values.add("FRFOS");
		values.add("TDM");
		values.add("TDM");
		System.out.println(doGet("http://"+"10.120.2.75:10080/"+"/apex/api//query",keys, values));*/
		//AccessDb.writeVGMInfos(new Date(), "C10", "CMAU1234567", 15000, 24000, 1, true);
		//AccessDb.addTempsAttenteIdle("C01", 123456);
		//System.out.println("authenticiation "+authentication("testmgps", "password"));
		/*GetCheWeightThread thrd = new GetCheWeightThread("CMAU1234567", 0, "C10", 0);
		thrd.start();*/
		GetCHEWeight che = new GetCHEWeight("XPPU100014", 2700, "C10", 44500);
		che.start();
		//VGM.insertVGM("navis", "please", "CMAU4045435", 0, "10.120.2.89:9080");
		//System.out.println(che.doGet("http://10.120.2.89:10080/apex/api//query", "HJCU8468834"));
				//http://10.120.2.75:10080/apex/api//query
	}
	private static String read(String message){
		// Lecture du message xml de r�ponse
		if(message.contains("<data-table filter=\"CHEWEIGHT\" count=\"0\">")){
			// pas de correspondance
			return "";			
		}else{
			final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder;
			try {
				builder = factory.newDocumentBuilder();
				InputSource s = new InputSource(new StringReader(message)); 
				final Document document= builder.parse(s);
				final Element racine = document.getDocumentElement();
				NodeList perso = racine.getElementsByTagName("field");
		 
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			} catch (SAXException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			
			
			String[] toto = message.split("<field>");
			String[] toto1 = toto[1].split("</field>");
			return toto1[0];
			
		}
		
	}

	
	/**
	 * r�cup�ration de la r�ponse de l'url Navis
	 * @param adress l'url de base
	 * @param keys liste des noms des param�tres
	 * @param values liste des valeurs de ces param�tres
	 * @return le nom du Vessel
	 * @throws IOException
	 */
	public static String doGet(String adress,List<String> keys,List<String> values) throws IOException{
		String result = "";
		BufferedReader reader = null;
		
			// encodage des param�tres de la requ�te
			String data = "";
			for (int i = 0; i < keys.size(); i++) {
				if (i != 0)
					data += "&";
				data += URLEncoder.encode(keys.get(i), "UTF-8") + "="
						+ URLEncoder.encode(values.get(i), "UTF-8");
			}
			// cr�ation de la connection
			URL url = new URL(adress+"?"+data);
			String userPassword = "aptapi:password";
			String encoding = new String(new Base64().encode(userPassword
					.getBytes()));
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			// add reuqest header
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Content-Type", "text/xml");
			conn.setRequestProperty("charset", "UTF-8");
			conn.setRequestProperty("Authorization", "Basic " + encoding);
			conn.setDoOutput(true);
			// envoi de la requ�te
			
			
			// lecture de la r�ponse
			reader = new BufferedReader(new InputStreamReader(
					(InputStream) conn.getContent()));
			String ligne;
			while ((ligne = reader.readLine()) != null) {
				result += ligne;
			}
		 
		
		return read(result);
	}


	public static int authentication(String user, String password){
		try{
			ArgobasicService argoService = new ArgobasicService();
			ArgobasicServicePort argoServicePort = argoService.getArgobasicServicePort();
			Map<String,List<String>>headers = new HashMap<String,List<String>>(); 
			
			byte[]   bytesEncoded = Base64.encode((user+":"+password).getBytes());
			//System.out.println("ecncoded value is " + "Basic "+new String(bytesEncoded));
			headers.put("Authorization",Collections.singletonList("Basic "+new String(bytesEncoded ))); 
			BindingProvider bp = ((BindingProvider) argoServicePort); 
			bp.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers); 
			String response = argoServicePort.basicInvoke("EUROFOS/FRFOS/TDM/TDM", "<?xml version=\"1.0\" encoding=\"UTF-8\"?><ping/>");
			//String response = argoServicePort.basicInvoke("EUROFOS/FRFOS/TDM/TDM", "<icu><units><unit-identity id=\"TCNU8255678\" type=\"CONTAINERIZED\"/></units><properties><property tag=\"GrossWeightKgYardMeasured\" value=\"24000\"/></properties></icu>");
			
			//
			if(response.contains("<status>OK</status>")){
				return 1;
			}else
				return 0;
		}catch(javax.xml.ws.soap.SOAPFaultException e){
			return 0;// probl�me d'authentification
		}
		
		//<status>OK</status>
		//javax.xml.ws.soap.SOAPFaultException
		
	}


}
